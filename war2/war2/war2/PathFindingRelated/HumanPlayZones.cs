﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.PathFindingRelated
{
    /**
     * Returns the list of positions on the map that are occupied by a hulan player
     * So the AIA know it can go there for its pathfinding (because it can attack)
     * */
    public class HumanPlayZones
    {
        private List<Vector2> playerPositions_;

        public HumanPlayZones()
        {
            playerPositions_ = new List<Vector2>();
        }

        internal List<Microsoft.Xna.Framework.Vector2> getPositions()
        {
            return playerPositions_;
        }

        internal void Update()
        {
            lock (playerPositions_)
            {
                playerPositions_ = new List<Vector2>();

                // If unit is human controlled adds it and the number of space it takes to the list
                foreach (Unit u in Game1.map.units)
                {
                    if (Game1.map.players[u.owner].isHumanPlayer)
                    {
                        for (int i = 0; i < (int)(u.spriteSize.X / 32); i++)
                            for (int j = 0; j < (int)(u.spriteSize.Y / 32); j++)
                                playerPositions_.Add(new Vector2(u.position.X + i, u.position.Y + j));
                    }
                }
            }
            return;
        }
    }
}
