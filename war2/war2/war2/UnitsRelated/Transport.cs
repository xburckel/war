﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.UnitsRelated;

namespace war2
{
        [Serializable]
    class Transport : Unit
    {
        public List<Unit> transporting;
        public HashSet<Unit> carryRequest;
        public bool unloadRequest; // for network unload order to avoid modifying collection error
        bool previousAshore;

        public Transport(int hp, int mana, float speed,
         int damage,
         bool flying,
         bool sailing,
         int goldCost,
         int woodCost,
         int oilCost,
         int range,
         int sight,
         Vector2 position,
        int buildtime,
            UnitType type,
        int armor, int owner)
            : base(hp, mana, speed,
                damage,
                flying,
                sailing,
                goldCost,
                woodCost,
                oilCost,
                range,
                sight,
                position,
                buildtime,
                type,
                armor, owner)
        {
            transporting = new List<Unit>();
            carryRequest = new HashSet<Unit>();
            unloadRequest = false;
        }

        public bool carry(Unit u)
        {
            // network
           /* if (u.owner == Game1.currentPLayer) // TODO : here and on unload, only one request pro transport
            {
                TransportLoadRequest tlr = new TransportLoadRequest();
                tlr.transportindex = index;
                tlr.unitIndex = u.index;
                tlr.trpos = position;
                tlr.unitpos = u.position;
                Game1.map.players[Game1.currentPLayer].playerMoves.trloads.Add(tlr);
            }*/
            if (!u.sailing && !u.flying && transporting.Count < 6)
            {
                transporting.Add(u);
                Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.DOCK], position);
                Game1.map.selected.Clear();
                return true;
            }

            return false;
        }

        private bool Ashore()
        {
            int posx = (int)Math.Round(this.position.X + this.spriteSize.X / 2 / 32);
            int posy = (int)Math.Round(this.position.Y + this.spriteSize.Y / 2 / 32);

            for (int i = -3; i < 3; i++)
                for (int j = -3; j < 3; j++)
                {
                    if (Map.Inbounds(posx + i, posy + j))
                    {
                        if (Game1.map.array[posx + i, posy + j] == MapElement.Grass || Game1.map.array[posx + i, posy + j] == MapElement.Mud)
                            return true;
                    }
                }


            return false;

        }


        public void unload()
        {
            /*
            if (owner == Game1.currentPLayer) // network
            {
                TransportUnloadRequest tur = new TransportUnloadRequest();
                tur.transportPosition = position;
                tur.index = index;
                Game1.map.players[Game1.currentPLayer].playerMoves.trunload.Add(tur);
            }*/

            if (Ashore())
            {
                if (!previousAshore)
                {
                    previousAshore = true;
                    Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.DOCK], position);
                }
                foreach (Unit u in transporting)
                {
                    u.path.Clear();
                    u.position = u.findClearSortieSpot(this.position);
                    Game1.map.groundTraversableMapTiles[(int)u.position.X, (int)u.position.Y] = false;
                    u.destination = u.position;
                    u.currentAnimation.getCurrentAnime().changeAction(DisplayAction.Stand);
                    Game1.map.groundTraversableMapTiles[(int)u.position.X, (int)u.position.Y] = false;
                    Game1.map.newUnits.Add(u); ;
                }
                Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.DOCK], position);

                transporting.Clear();
            }
            else if (previousAshore)
            {
                previousAshore = false;
                Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.DOCK], position);
            }
        }

        public override void Update(Map map, GameTime gameTime)
        {
            base.Update(map, gameTime);

            if (unloadRequest)
            {
                unload();
                unloadRequest = false;
            }

            foreach (Unit u in carryRequest)
            {
                if (Ashore() && Vector2.Distance(this.position + spriteSize / 2 / 32, u.position + u.spriteSize / 2 / 32) <= 3)
                {
                    if (carry(u))
                        map.unitsToRemove.Add(u);
                }
            }
            foreach (Unit u in transporting)
                carryRequest.Remove(u);
        }

        public override void DrawLeftPanelIcons(SpriteBatch spriteBatch)
        {
            Game1.leftPanel.DrawUnloadButton(Game1.map.players[this.owner].isOrc);

        }


    }
}
