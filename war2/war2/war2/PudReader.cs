﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PudTranslater
{
    public enum ResourceType { None = -1, Gold, Oil, Wood };

    public enum MapElement
    {

        Forest = 0, Water, Mud, Grass, Rock,
        FullForest1, FullForest2, FullForest3, FullForest4, FullForest5, FullForest6, FullForest7, FullForest8, FullForest9, FullForest10, FullForest11,
        Grass1, Grass2, Grass3, Grass4, Grass5, Grass6, Grass7, Grass8, Grass9, Grass10, Grass11, Grass12, Grass13, Grass14, Grass15,
        Mud1, Mud2, Mud3, Mud4, Mud5, Mud6, Mud7, Mud8, Mud9, Mud10, Mud11, Mud12, Mud13, Mud14, Mud15,
        Water1, Water2, Water3, Water4, Water5, Water6, Water7, Water8, Water9, Water10, Water11, Water12, Water13, Water14, Water15,
        Water16, Water17, Water18, Water19, Water20, Water21, Water22, Water23, Water24, Water25, Water26, Water27, Water28, Water29, Water30,
        Rock1, Rock2, Rock3, Rock4, Rock5, Rock6, Rock7, Rock8, Rock9, Rock10, Rock11, Rock12, Rock13, Rock14, Rock15,
        Rock16, Rock17, Rock18, Rock19, Rock20,


        Gold, Oil,


        GrassTopLeft, GrassTopRight, GrassTopHalf, GrassBottomLeft, GrassLeftHalf, GrassTopRightBottomLeft, GrassTopHalfBottomLeft, GrassBottomRight, GrassTopLeftBottomRight, GrassRightHalf, GrassTopHalfBottomRight,
        GrassBottomHalf, GrassTopLeftBottomHalf, GrassTopRightBottomHalf,
        CoastTopLeft, CoastTopRight, CoastTopHalf, CoastBottomLeft, CoastLeftHalf, CoastTopRightBottomLeft, CoastTopHalfBottomLeft, CoastBottomRight, CoastTopLeftBottomRight, CoastRightHalf, CoastTopHalfBottomRight,
        CoastBottomHalf, CoastTopLeftBottomHalf, CoastTopRightBottomHalf,
        RockTopLeft, RockTopRight, RockTopHalf, RockBottomLeft, RockLeftHalf, RockTopRightBottomLeft, RockTopHalfBottomLeft, RockBottomRight, RockTopLeftBottomRight, RockRightHalf, RockTopHalfBottomRight,
        RockBottomHalf, RockTopLeftBottomHalf, RockTopRightBottomHalf, // rock & coast
        WaterTopLeft, WaterTopRight, WaterTopHalf, WaterBottomLeft, WaterLeftHalf, WaterTopRightBottomLeft, WaterTopHalfBottomLeft, WaterBottomRight, WaterTopLeftBottomRight, WaterRightHalf, WaterTopHalfBottomRight,
        WaterBottomHalf, WaterTopLeftBottomHalf, WaterTopRightBottomHalf, // water & coast





        /*
         * 09** orc wall 
            08** human wall 
            07** forest and grass 
            06** dark grass and grass 
            05** coast and grass 
            04** mount and coast 
            03** dark coast and coast 
            02** water and coast 
            01** dark water and water 
            Filling: 
            **0* top-left 
            **1* top-right 
            **2* top-half 
            **3* bottom-left 
            **4* left-half 
            **5* top-right & bottom-left 
            **6* top-half & bottom-left 
            **7* bottom-right 
            **8* top-left & bottom-right 
            **9* right-half 
            **A* top-half & bottom-right 
            **B* bottom-half 
            **C* top-left & bottom-half 
            **D* top-right & bottom-half */


    };


    public enum UnitType
    {
        None = -1, Paysan = 0, Fantassin = 1, Archer = 2, Catapulte = 3, Chevalier = 4, Paladin = 5, Nains = 6,
        Mage = 7, Petrolier = 8, Destroyer = 9, Battleship = 10, Griffon = 11, Sousmarin = 12, Transport = 13, MachineVolante = 14,
        Farm = 15, Tower, BallistaTower, CannonTower, MilitaryBase, LumberMill, Stable, Forge, NavalChantier, NavalPlatform, Foundry, Raffinery, Church, MageTower, GryffinTower, GoblinWorkBench, TownHall, StrongHold, Fortress
        // unittype >= 15 = building

    };

    public struct MapResources
    {
        public ResourceType type;
        public int amount;
        public int PositionX;
        public int PositionY;

        public MapResources(ResourceType type, int amount, int posx, int posy)
        {
            this.type = type;
            this.amount = amount;
            this.PositionX = posx;
            this.PositionY = posy;
        }

    }

    public struct Unit
    {
        public Unit(UnitType type, int posx, int posy, int owner)
        {
            this.owner = owner;
            this.type = type;
            this.PositionX = posx;
            this.PositionY = posy;
        }

        public int owner;
        public UnitType type;
        public int PositionX;
        public int PositionY;

    }



    class Program
    {



        public void Read()
        {


            string dirname = Directory.GetCurrentDirectory();
            var files = from file in Directory.EnumerateFiles(dirname, "*.pud", SearchOption.AllDirectories)
                        select new
                        {
                            File = file,
                        };

            foreach (var f in files)
            {
                // if (Path.GetFileName(f.File).Contains("my")) // we did them all
                //     return;

                int mapsize = 128;
                string mapName = Path.GetFileName(f.File);


                List<List<MapElement>> melements = new List<List<MapElement>>();
                List<Unit> units = new List<Unit>();
                List<MapResources> resources = new List<MapResources>();

                string sectionName = "";
                using (FileStream stream = new FileStream(mapName, FileMode.Open))
                {
                    // Read bytes from stream and interpret them as ints
                    int value = 0;
                    while ((value = stream.ReadByte()) != -1)
                    {
                        char c = (char)value;

                        sectionName += c;

                        if (sectionName.Contains("DIM "))
                            break;
                    }
                    for (int i = 0; i < 4; i++)
                        stream.ReadByte();// ignore size section

                    int mapsizex = stream.ReadByte(); // read map size
                    mapsizex += stream.ReadByte();

                    int mapsizey = stream.ReadByte();
                    mapsizey += stream.ReadByte();

                    mapsize = mapsizex;



                    for (int i = 0; i < mapsize; i++)
                    {
                        melements.Add(new List<MapElement>());
                    }
                    for (int i = 0; i < mapsize; i++)
                    {
                        melements[i] = new List<MapElement>();
                    }

                    sectionName = "";


                    while ((value = stream.ReadByte()) != -1)
                    {
                        char c = (char)value;

                        sectionName += c;

                        if (sectionName.Contains("MTXM"))
                            break;
                    }
                    if ((sectionName.Contains("MTXM")))
                    {
                        sectionName = "";
                        int tmp;
                        for (int i = 0; i < 4; i++)
                            stream.ReadByte();// ignore size section

                        for (int i = 0; i < mapsize; i++)
                            for (int j = 0; j < mapsize; j++)
                            {
                                tmp = stream.ReadByte();
                                sectionName += tmp;
                                int[] Item = new int[2];
                                Item[1] = tmp;

                                tmp = stream.ReadByte();
                                Item[0] = tmp;
                                sectionName += (char)tmp;

                                if ((Item[0] == 0)) // not border
                                {
                                    /*            

                                        001* light water 
                                        002* dark water 
                                        003* light coast 
                                        004* dark coast 
                                        005* light ground 
                                        006* dark ground 
                                        007* forest 
                                        008* mountains 
                                        009* human wall 
                                        00A* orc walls 
                                        00B* human walls 
                                        00C* orc walls 
                                    */

                                    if (Item[1] > 47 + 96) // walls
                                        melements[i].Add(MapElement.Rock);
                                    else if (Item[1] > 47 + 80) // mountain
                                        melements[i].Add(MapElement.Rock);
                                    else if (Item[1] > 47 + 64) // forest
                                        melements[i].Add(MapElement.Forest);

                                    else if (Item[1] > 47 + 48) // dark grass
                                        melements[i].Add(MapElement.Grass);
                                    else if (Item[1] > 47 + 32) // grass
                                        melements[i].Add(MapElement.Grass);
                                    else if (Item[1] > 47 + 16) // dark coast
                                        melements[i].Add(MapElement.Mud);
                                    else if (Item[1] > 47) // light coast
                                        melements[i].Add(MapElement.Mud);

                                    else // coast or water
                                        melements[i].Add(MapElement.Water);
                                }
                                else // borders
                                {

                                    if (Item[0] > 6) // forest and grass 
                                    {
                                        int index = Item[1] / 16;
                                        melements[i].Add((MapElement)((int)MapElement.GrassTopLeft + index));
                                    }
                                    else if (Item[0] > 5)
                                        melements[i].Add(MapElement.Grass); // dark grass and grass

                                    else if (Item[0] > 4) // coast and grass
                                    {
                                        int index = Item[1] / 16;
                                        melements[i].Add((MapElement)((int)MapElement.GrassTopLeft + 14 + index));
                                    }
                                    else if (Item[0] > 3) // mount and coast
                                    {
                                        int index = Item[1] / 16;
                                        melements[i].Add((MapElement)((int)MapElement.GrassTopLeft + 28 + index));
                                    }
                                    else if (Item[0] > 2)
                                        melements[i].Add(MapElement.Mud); // dark coast and coast
                                    else if (Item[0] > 1) // coast and water
                                    {
                                        int index = Item[1] / 16;
                                        melements[i].Add((MapElement)((int)MapElement.GrassTopLeft + 42 + index));



                                        /*
                                        **0* top-left 
                                        **1* top-right 
                                        **2* top-half 
                                        **3* bottom-left 
                                        **4* left-half 
                                        **5* top-right & bottom-left 
                                        **6* top-half & bottom-left 
                                        **7* bottom-right 
                                        **8* top-left & bottom-right 
                                        **9* right-half 
                                        **A* top-half & bottom-right 
                                        **B* bottom-half 
                                        **C* top-left & bottom-half 
                                        **D* top-right & bottom-half 
                                        */

                                    }




                                    /*
                                    09** orc wall 
                                    08** human wall 
                                    07** forest and grass 
                                    06** dark grass and grass 
                                    05** coast and grass 
                                    04** mount and coast 
                                    03** dark coast and coast 
                                    02** water and coast 
                                    01** dark water and water */

                                    else
                                    {
                                        int index = Item[1] / 16;
                                        melements[i].Add((MapElement)((int)MapElement.Water));
                                    }
                                }
                            }

                    }



                    sectionName = "";


                    while ((value = stream.ReadByte()) != -1)
                    {
                        char c = (char)value;

                        sectionName += c;

                        if (sectionName.Contains("UNIT"))
                            break;
                    }
                    if (sectionName.Contains("UNIT"))
                    {
                        for (int i = 0; i < 4; i++)
                            stream.ReadByte();

                        while ((value = stream.ReadByte()) != -1)
                        {
                            int posx = value;
                            posx += stream.ReadByte();
                            int posy = stream.ReadByte();
                            posy += stream.ReadByte();

                            int type = stream.ReadByte();
                            int owner = stream.ReadByte();
                            int wtf = stream.ReadByte() * 256;
                            wtf += stream.ReadByte();
                            Unit u = new Unit(UnitType.None, posx, posy, owner);

                            if (type == 92) // gold mine
                            {
                                MapResources r = new MapResources(ResourceType.Gold, 50000, posx, posy);
                                resources.Add(r);
                            }
                            else if (type == 93)
                            {
                                MapResources r = new MapResources(ResourceType.Oil, 50000, posx, posy);
                                resources.Add(r);

                            }


                            else if (type == 0 || type == 1) // fantassin
                            {
                                u.type = UnitType.Fantassin;
                            }
                            else if (type == 2 || type == 3)
                            {
                                u.type = UnitType.Paysan;

                            }
                            else if (type == 4 || type == 5)
                            {
                                u.type = UnitType.Catapulte;

                            }
                            else if (type == 6 || type == 7)
                            {
                                u.type = UnitType.Chevalier;

                            }
                            else if (type == 8 || type == 9)
                            {
                                u.type = UnitType.Archer;

                            }
                            else if (type == 10 || type == 11)
                            {
                                u.type = UnitType.Mage;

                            }
                            else if (type == 12 || type == 13)
                            {
                                u.type = UnitType.Paladin;

                            }
                            else if (type == 14 || type == 15) // dwarves
                            {
                                u.type = UnitType.Nains;

                            }

                            else if (type == 26 || type == 27) // petrolier
                            {
                                u.type = UnitType.Petrolier;

                            }
                            else if (type == 28 || type == 29) // transport
                            {
                                u.type = UnitType.Transport;

                            }
                            else if (type == 30 || type == 31) // destroyer
                            {
                                u.type = UnitType.Destroyer;

                            }
                            else if (type == 32 || type == 33) // battleship
                            {
                                u.type = UnitType.Battleship;

                            }
                            else if (type == 38 || type == 39) // submarine (yellow)
                            {
                                u.type = UnitType.Sousmarin;

                            }
                            else if (type == 40 || type == 41) // flymachine
                            {
                                u.type = UnitType.MachineVolante;

                            }
                            else if (type == 42 || type == 43) // dragooon
                            {
                                u.type = UnitType.Griffon;

                            }
                            else if (type == 58 || type == 59) //farm
                            {
                                u.type = UnitType.Farm;

                            }
                            else if (type == 60 || type == 61) // base militaire
                            {
                                u.type = UnitType.MilitaryBase;

                            }
                            else if (type == 62 || type == 63) //eglise
                            {
                                u.type = UnitType.Church;

                            }
                            else if (type == 64 || type == 65) // scout tower
                            {
                                u.type = UnitType.Tower;

                            }
                            else if (type == 66 || type == 67) // stables
                            {
                                u.type = UnitType.Stable;

                            }
                            else if (type == 68 || type == 69) // gnomeinventor
                            {
                                u.type = UnitType.GoblinWorkBench;

                            }
                            else if (type == 70 || type == 71) // gryphontower
                            {
                                u.type = UnitType.GryffinTower;
                            }
                            else if (type == 72 || type == 73) // chantier naval
                            {
                                u.type = UnitType.NavalChantier;
                            }
                            else if (type == 74 || type == 75) // town hall
                            {
                                u.type = UnitType.TownHall;

                            }
                            else if (type == 76 || type == 77) // lumber mill
                            {
                                u.type = UnitType.LumberMill;

                            }
                            else if (type == 78 || type == 79) // foundry
                            {
                                u.type = UnitType.Foundry;
                            }
                            else if (type == 80 || type == 81) // mage tower
                            {
                                u.type = UnitType.MageTower;
                            }
                            else if (type == 82 || type == 83) //forge
                            {
                                u.type = UnitType.Forge;
                            }
                            else if (type == 84 || type == 85) // raffinery
                            {
                                u.type = UnitType.Raffinery;
                            }
                            else if (type == 86 || type == 87) // puit de pétrole
                            {
                                u.type = UnitType.NavalPlatform;
                            }
                            else if (type == 88 || type == 89) // stronghold
                            {
                                u.type = UnitType.StrongHold;
                            }
                            else if (type == 90 || type == 91) // fortress
                            {
                                u.type = UnitType.Fortress;

                            }
                            else if (type == 96 || type == 97) // tower ballista
                            {
                                u.type = UnitType.BallistaTower;
                            }
                            else if (type == 98 || type == 99) //  tower cannon
                            {
                                u.type = UnitType.CannonTower;
                            }
                            if (u.type != UnitType.None)
                                units.Add(u);

                        }

                    }
                }




                StreamWriter w = new StreamWriter("my" + mapName);

                w.WriteLine("Size:" + mapsize); for (int i = 0; i < mapsize; i++)
                {
                    for (int j = 0; j < mapsize; j++)
                    {
                        MapElement tmp = melements[i][j];
                        w.Write((int)tmp);
                        w.Write(",");
                    }
                    w.WriteLine();
                }

                w.Write("Units");
                w.WriteLine();

                for (int i = 0; i < mapsize; i++)
                {
                    for (int j = 0; j < mapsize; j++)
                    {
                        bool somethinghere = false;

                        foreach (Unit u in units)
                        {
                            if (u.PositionX == i && u.PositionY == j)
                            {
                                w.Write(u.owner);
                                w.Write(";");
                                w.Write((int)u.type);
                                somethinghere = true;
                                break;
                            }


                        }
                        if (!somethinghere)
                            w.Write("-");
                        w.Write(",");

                    }
                    w.WriteLine();

                }

                w.WriteLine();

                w.Write("Resources");
                w.WriteLine();

                for (int i = 0; i < mapsize; i++)
                {
                    for (int j = 0; j < mapsize; j++)
                    {
                        bool somethinghere = false;

                        foreach (MapResources r in resources)
                        {
                            if (r.PositionX == i && r.PositionY == j)
                            {
                                if (r.type == ResourceType.Gold)
                                    w.Write("G");
                                else
                                    w.Write("O");
                                w.Write(r.amount);
                                somethinghere = true;
                                break;
                            }


                        }
                        if (!somethinghere)
                            w.Write("-");
                        w.Write(",");

                    }
                    w.WriteLine();

                }
                w.WriteLine();
                w.WriteLine();
                w.WriteLine();
                w.Close();
            }
        }
    }
}
