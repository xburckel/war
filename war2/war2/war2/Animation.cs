﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2
{
    public enum AnimationType { None, littleFire, bigFire, blizzard, decay, SpellFire, ArcaneBarrage, CatapultHit, CannonHit, FireBallHit, Explosion};

    [Serializable]
    public class Animation
    {
        public List<Vector2> positionsInSprite;
        public int intervalBetween;
        private Vector2[] animationSpriteSizes;
        public Vector2 mapPos;
        private int timer;
        public int animationIndex;
        public AnimationType ant;
        private bool expired;
        private bool canExpire;

        public bool animationEnded()
        {
            return canExpire && expired;
        }

        public Animation(AnimationType anT, Vector2 mappos, bool canExpire = false)
        {
            this.canExpire = canExpire;
            expired = false;
            this.ant = anT;
            this.mapPos = mappos;
            positionsInSprite = new List<Vector2>();
            animationSpriteSizes = new Vector2[20];
            intervalBetween = 100;
            switch (anT)
            {
                case AnimationType.Explosion:
                    positionsInSprite.Add(new Vector2(9, 184));
                    animationSpriteSizes[0] = (new Vector2(50, 31));
                    positionsInSprite.Add(new Vector2(73, 185));
                    animationSpriteSizes[1] = (new Vector2(49, 30));
                    positionsInSprite.Add(new Vector2(138, 184));
                    animationSpriteSizes[2] = (new Vector2(44, 38));
                    positionsInSprite.Add(new Vector2(202, 183));
                    animationSpriteSizes[3] = (new Vector2(48, 39));
                    positionsInSprite.Add(new Vector2(265, 180));
                    animationSpriteSizes[4] = (new Vector2(50, 42));
                    positionsInSprite.Add(new Vector2(329, 178));
                    animationSpriteSizes[5] = (new Vector2(57, 49));
                    positionsInSprite.Add(new Vector2(394, 175));
                    animationSpriteSizes[6] = (new Vector2(56, 52));
                    positionsInSprite.Add(new Vector2(458, 170));
                    animationSpriteSizes[7] = (new Vector2(58, 58));
                    positionsInSprite.Add(new Vector2(588, 168));
                    animationSpriteSizes[8] = (new Vector2(61, 59));
                    positionsInSprite.Add(new Vector2(660, 170));
                    animationSpriteSizes[9] = (new Vector2(60, 57));
                    positionsInSprite.Add(new Vector2(724, 178));
                    animationSpriteSizes[10] = (new Vector2(61, 49));
                    positionsInSprite.Add(new Vector2(788, 182));
                    animationSpriteSizes[11] = (new Vector2(56, 43));
                    positionsInSprite.Add(new Vector2(854, 180));
                    animationSpriteSizes[12] = (new Vector2(54, 43));
                    positionsInSprite.Add(new Vector2(918, 175));
                    animationSpriteSizes[13] = (new Vector2(52, 47));
                    positionsInSprite.Add(new Vector2(980, 174));
                    animationSpriteSizes[14] = (new Vector2(57, 44));
                    intervalBetween = 50;
                    break;

                case AnimationType.CannonHit:
                    positionsInSprite.Add(new Vector2(11, 86));
                    animationSpriteSizes[0] = (new Vector2(17, 17));
                    positionsInSprite.Add(new Vector2(42, 85));
                    animationSpriteSizes[1] = (new Vector2(28, 26));
                    positionsInSprite.Add(new Vector2(82, 82));
                    animationSpriteSizes[2] = (new Vector2(30, 30));
                    positionsInSprite.Add(new Vector2(119, 81));
                    animationSpriteSizes[3] = (new Vector2(32, 32));
                    intervalBetween = 100;
                    break;

                case AnimationType.FireBallHit:
                    positionsInSprite.Add(new Vector2(819, 59));
                    animationSpriteSizes[0] = (new Vector2(18, 32));
                    positionsInSprite.Add(new Vector2(819, 59));
                    animationSpriteSizes[1] = (new Vector2(18, 32));
                    positionsInSprite.Add(new Vector2(819, 59));
                    animationSpriteSizes[2] = (new Vector2(18, 32));
                    positionsInSprite.Add(new Vector2(819, 59));
                    animationSpriteSizes[3] = (new Vector2(18, 32));
                    intervalBetween = 200;
                    break;

                case AnimationType.CatapultHit:
                    positionsInSprite.Add(new Vector2(163, 122));
                    animationSpriteSizes[0] = (new Vector2(30, 18));
                    positionsInSprite.Add(new Vector2(206, 116));
                    animationSpriteSizes[1] = (new Vector2(40, 29));
                    positionsInSprite.Add(new Vector2(257, 112));
                    animationSpriteSizes[2] = (new Vector2(48, 42));
                    positionsInSprite.Add(new Vector2(315, 109));
                    animationSpriteSizes[3] = (new Vector2(48, 48));
                    positionsInSprite.Add(new Vector2(372, 111));
                    animationSpriteSizes[4] = (new Vector2(48, 46));
                    positionsInSprite.Add(new Vector2(434, 112));
                    animationSpriteSizes[5] = (new Vector2(48, 48));
                    intervalBetween = 100;
                    break;

                case AnimationType.littleFire:
                    positionsInSprite.Add(new Vector2(605, 124));
                    animationSpriteSizes[0] = (new Vector2(30, 22));
                    positionsInSprite.Add(new Vector2(637, 124));
                    animationSpriteSizes[1] = (new Vector2(30, 23));
                    positionsInSprite.Add(new Vector2(669, 123));            
                    animationSpriteSizes[2] = (new Vector2(30, 24));
                    positionsInSprite.Add(new Vector2(702, 123));       
                    animationSpriteSizes[3] = (new Vector2(29, 25));
                    positionsInSprite.Add(new Vector2(734, 121));
                    animationSpriteSizes[4] = (new Vector2(29, 26));
                    positionsInSprite.Add(new Vector2(768, 122));
                    animationSpriteSizes[5] = (new Vector2(28, 25));
                    intervalBetween = 100;

                    break;
                case AnimationType.bigFire:
                    positionsInSprite.Add(new Vector2(532, 4));
                    animationSpriteSizes[0] = (new Vector2(47, 46));
                    positionsInSprite.Add(new Vector2(587, 4));
                    animationSpriteSizes[1] = (new Vector2(47, 46));
                    positionsInSprite.Add(new Vector2(643, 10));
                    animationSpriteSizes[2] = (new Vector2(47, 48));
                    positionsInSprite.Add(new Vector2(698, 8));
                    animationSpriteSizes[3] = (new Vector2(47, 42));
                    positionsInSprite.Add(new Vector2(751, 5));
                    animationSpriteSizes[4] = (new Vector2(47, 45));
                    positionsInSprite.Add(new Vector2(533, 59));
                    animationSpriteSizes[5] = (new Vector2(46, 45));
                    positionsInSprite.Add(new Vector2(587, 58));
                    animationSpriteSizes[6] = (new Vector2(47, 46));
                    positionsInSprite.Add(new Vector2(645, 60));
                    animationSpriteSizes[7] = (new Vector2(45, 44));
                    positionsInSprite.Add(new Vector2(698, 57));
                    animationSpriteSizes[8] = (new Vector2(47, 47));
                    positionsInSprite.Add(new Vector2(751, 56));
                    animationSpriteSizes[9] = (new Vector2(47, 47));
                    break;
                case AnimationType.blizzard:
                    positionsInSprite.Add(new Vector2(9, 260));
                    animationSpriteSizes[0] = (new Vector2(13, 16));
                    positionsInSprite.Add(new Vector2(37, 259));
                    animationSpriteSizes[1] = (new Vector2(14, 16));
                    positionsInSprite.Add(new Vector2(66, 258));
                    animationSpriteSizes[2] = (new Vector2(15, 16));
                    positionsInSprite.Add(new Vector2(100, 251));
                    animationSpriteSizes[3] = (new Vector2(29, 32));
                    break;
                case AnimationType.decay:
                    positionsInSprite.Add(new Vector2(1008, 59));
                    animationSpriteSizes[0] = (new Vector2(40, 25));
                    positionsInSprite.Add(new Vector2(1063, 56));
                    animationSpriteSizes[1] = (new Vector2(27, 28));
                    positionsInSprite.Add(new Vector2(1008, 118));
                    animationSpriteSizes[2] = (new Vector2(26, 30));
                    positionsInSprite.Add(new Vector2(1048, 98));
                    animationSpriteSizes[3] = (new Vector2(46, 48));
                    break;
                case AnimationType.ArcaneBarrage:
                    positionsInSprite.Add(new Vector2(18, 390));
                    animationSpriteSizes[0] = (new Vector2(103, 77));
                    positionsInSprite.Add(new Vector2(186, 394));
                    animationSpriteSizes[1] = (new Vector2(133, 129));
                    positionsInSprite.Add(new Vector2(385, 392));
                    animationSpriteSizes[2] = (new Vector2(153, 139));
                    positionsInSprite.Add(new Vector2(597, 421));
                    animationSpriteSizes[3] = (new Vector2(153, 133));
                    break;
                case AnimationType.SpellFire:
                    // TODO : this will planter

                    for (int j = 0; j < 4; j++)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            animationSpriteSizes[i + j] = (new Vector2(180, 180));
                            positionsInSprite.Add(new Vector2((960 / 5) * i, (768 / 4) * j));
                        }
                    }


                    break;

            }


          //  this.texture = texture;
         //   this.intervalBetween = intervalBetween;
         //   this.positionsInSprite = positions;
            timer = 0;
            animationIndex = 0;
        }

        public void Update(GameTime gametime)
        {
            if (ant != AnimationType.None)
            {

                timer += gametime.ElapsedGameTime.Milliseconds;
                if (ant == AnimationType.FireBallHit)
                {
                    if (timer > intervalBetween)
                    {
                        Animation.AddAnimation(AnimationType.Explosion, this.mapPos, true);
                        Random r = new Random();

                        Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.EXPLODE], mapPos / 32);

                        mapPos += new Vector2(0, -32);
                        if (!Map.Inbounds((int)mapPos.X / 32, (int)mapPos.Y / 32))
                            mapPos -= new Vector2(0, -32);
                        Utils.Utils.dealSplashDamage(mapPos, 16, 20 / (animationIndex + 1));

                    }
                }
                if (ant == AnimationType.ArcaneBarrage)
                {
                    if (animationIndex == 0)
                        intervalBetween = 2000;
                    if (timer > intervalBetween && animationIndex == 0) // dont change sprite for this one
                    {
                        intervalBetween = 100;
                        animationIndex++;
                    }
                    if (timer > intervalBetween && animationIndex >= 1) // dont change sprite for this one
                    {
                        Random r = new Random();
                        int rand = r.Next(1, 3);
                        animationIndex = rand;
                        timer = 0;
                    }
                }
                else if (timer > intervalBetween)
                {
                    timer = 0;
                    if (animationIndex + 1 == positionsInSprite.Count)
                        if (canExpire)
                            expired = true;
                    animationIndex = (animationIndex + 1) % positionsInSprite.Count;

                }
            }
        }
        public void Draw(SpriteBatch spbatch, float rotation = 0)
        {
            if (ant != AnimationType.None && ant != AnimationType.SpellFire)
                spbatch.Draw(Game1.projectileTexture, mapPos, new Rectangle((int)positionsInSprite[animationIndex].X, (int)positionsInSprite[animationIndex].Y, (int) animationSpriteSizes[animationIndex].X, (int) animationSpriteSizes[animationIndex].Y),
Color.Wheat, rotation, Game1.statics.ImpossiblePos, 1, SpriteEffects.None, 0);
            else if (ant == AnimationType.SpellFire)
                spbatch.Draw(Game1.statics.spellFireTexture, mapPos, new Rectangle((int)positionsInSprite[animationIndex].X, (int)positionsInSprite[animationIndex].Y, (int) animationSpriteSizes[animationIndex].X, (int) animationSpriteSizes[animationIndex].Y), Color.Wheat);


        }

        public static void AddAnimation(AnimationType anType, Vector2 mappos, bool canExpire)
        {
            Animation np = new Animation(anType, mappos, canExpire);
            Game1.animationstoAdd.Add(np);
        }

    }
}
