﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using war2.Menus;
using war2.PathFindingRelated;
using war2.Tooltips;
using war2.Utils;

namespace war2
{
    public class Statics
    {
        public bool debug = false;
        public Vector2 ImpossiblePos = new Vector2(-1, -1);
        public Random ra;
        public Texture2D spellFireTexture;
        public List<Texture2D> playerColorRectangle;
        public List<UnitsRelated.SingleAmelioration> ameliorations = new List<UnitsRelated.SingleAmelioration>();
        public GameOverMenu govermenu;
        public bool fogOfWarEnabled = true;
        public bool istryingtobuild; // to draw building when selected by current player
        public int cancelBuild_value;
        public int tile_size_counting_zoom;
        public List<UnitType> basicBehaviourAttackIngUnits;
        public List<UnitType> basicSailingBuildings;
        public List<UnitType> canSeeSubmarines;
        public Message printedMessage;
        public InformationMessage infoMessage;
        public AnnoucementMessage annouceMessage;
        public Vector2 CreateUnitIconCoordinate;
        public CheatCode allCheatCodesList;
        public int harvestTimeGold;
        public int harvestTimeOil;
        public int harvestTimeWood;
        public Stopwatch debugWatch;
        public SpriteFont font; // font for everything
        public CustomMenuItem menuItem; // to generate menu items
        public war2.PathFindingRelated.Threading threading;
        public bool invulnerableCheatcodeOn;
        public MinimapIcon miniMapIcon; // bject that represents minimap display for preview
        public HandleCollisions collisionUpdates;
        public ConstructionToolTip constructionToolTips;
        public CacheComputedPaths pathFindingResults;

        public Statics()
        {
            invulnerableCheatcodeOn = false;
            debugWatch = new Stopwatch();
            harvestTimeGold = 5;
            harvestTimeWood = 10;
            harvestTimeOil = 8;
            basicBehaviourAttackIngUnits = new List<UnitType>(new UnitType[]{UnitType.Fantassin, UnitType.Archer, UnitType.Catapulte, UnitType.Destroyer, UnitType.Paladin, UnitType.Chevalier, UnitType.Battleship, UnitType.Griffon, UnitType.Sousmarin});
            tile_size_counting_zoom = 32;
            ra = new Random();
            govermenu = null;
            spellFireTexture = Game1.content.Load<Texture2D>("Misc/fire_001");
            playerColorRectangle = new List<Texture2D>();
            Texture2D rectText = new Texture2D(Game1.graphics.GraphicsDevice, 32, 32);
            // War2Color { Red = 0, Blue, Green, Violet, Orange, Yellow, White, Black };
            Color[] data = new Color[32 * 32];
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Red;
            rectText = new Texture2D(Game1.graphics.GraphicsDevice, 32, 32);
            rectText.SetData(data);

            playerColorRectangle.Add(rectText);
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Blue;
            rectText = new Texture2D(Game1.graphics.GraphicsDevice, 32, 32);
            rectText.SetData(data);
            playerColorRectangle.Add(rectText);
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Green;
            rectText = new Texture2D(Game1.graphics.GraphicsDevice, 32, 32);
            rectText.SetData(data);
            playerColorRectangle.Add(rectText);
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Violet;
            rectText = new Texture2D(Game1.graphics.GraphicsDevice, 32, 32);
            rectText.SetData(data);
            playerColorRectangle.Add(rectText);
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Orange;
            rectText = new Texture2D(Game1.graphics.GraphicsDevice, 32, 32);
            rectText.SetData(data);
            playerColorRectangle.Add(rectText);
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Yellow;
            rectText = new Texture2D(Game1.graphics.GraphicsDevice, 32, 32);
            rectText.SetData(data);
            playerColorRectangle.Add(rectText);
            for (int i = 0; i < data.Length; ++i) data[i] = Color.White;
            rectText = new Texture2D(Game1.graphics.GraphicsDevice, 32, 32);
            rectText.SetData(data);
            playerColorRectangle.Add(rectText);
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Black;
            rectText = new Texture2D(Game1.graphics.GraphicsDevice, 32, 32);
            rectText.SetData(data);
            playerColorRectangle.Add(rectText);

            istryingtobuild = false;

            basicSailingBuildings = new List<UnitType>(new UnitType[]{UnitType.Foundry, UnitType.Raffinery, UnitType.NavalChantier});
            canSeeSubmarines = new List<UnitType>(new UnitType[] { UnitType.Tower, UnitType.BallistaTower, UnitType.CannonTower, UnitType.Sousmarin, UnitType.Griffon, UnitType.MachineVolante});
            infoMessage = new InformationMessage("");
            annouceMessage = new AnnoucementMessage("");
            printedMessage = new Message("");
            allCheatCodesList = new CheatCode();
            CreateUnitIconCoordinate = new Vector2(LeftPanel.LEFTPANEL_TOPLEFT_BUILD_ICON_POSITION_X, LeftPanel.LEFTPANEL_TOPLEFT_BUILD_ICON_POSITION_Y);
            font = Game1.content.Load<SpriteFont>("war2font");
            menuItem = new CustomMenuItem();
            threading = new PathFindingRelated.Threading();
            miniMapIcon = new MinimapIcon("map.pud");
            collisionUpdates = new HandleCollisions();
            constructionToolTips = new ConstructionToolTip();
            pathFindingResults = new CacheComputedPaths();
        }

    }
}
