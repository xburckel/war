﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Net;
using war2.UnitsRelated;
using Microsoft.Xna.Framework.Input;
using war2.AI;
namespace war2
{
    public enum War2Color { Red = 0, Blue, Green, Violet, Orange, Yellow, White, Black }; // dont change the order


    [Serializable]
    public class Player
    {
        public bool isHumanPlayer;
        public int number;
        public Boolean isOrc;
        public War2Color playerColor;
        public int gold;
        public int oil;
        public int wood;
        public int food;
        public int usedFood;
        public bool[,] explored;
        public bool[,] hasVision;
        //public FogVision[,] discovered;


        public TechTree playerTechs;
        private Dictionary<UnitType, int> has;
        public bool buildPanelToDraw;
        public bool advancedbuildPanelToDraw;
        public bool network;
        public bool isHost;
        public Color playerColorRectangle;
        public PlayerMoves playerMoves;// for multiplayer
        public IA playerAI;
        private int timerBetweenAttackAnnoucements = 0;
        private int timerBetweenTownAttackAnnoucements = 0;
        public Ameliorations playerAmeliorations;

        public bool TownAttackedAnnoucement = false;
        public bool attackedAnnoucement = false;
      //  private Texture2D circletexture;

        public Player()
        {

        }

        public bool checkForGameOver()
        {
            foreach (Unit u in Game1.map.units)
            {
                if (u.owner == number)
                    return false;
            }

            return true;
        }

        public Player(int number, Boolean isOrc, bool network = false, bool isHost = false)
        {
            playerAmeliorations = new Ameliorations();
            playerAI = new IA(number);
            has = new Dictionary<UnitType, int>();
            //circletexture =  Utils.Utils.CreateCircle(32);


            if (number % 2 == 0)
                isOrc = false;

            if (number == (int)War2Color.Red)
                playerColorRectangle = Color.Red;
            else if (number == (int)War2Color.Blue)
                playerColorRectangle = Color.Blue;

            else if (number == (int)War2Color.Green)
                playerColorRectangle = Color.Green;
            else if (number == (int)War2Color.Black)
                playerColorRectangle = Color.Black;
            else if (number == (int)War2Color.Orange)
                playerColorRectangle = Color.Orange;

            else if (number == (int)War2Color.Violet)
                playerColorRectangle = Color.Violet;

            else if (number == (int)War2Color.White)
                playerColorRectangle = Color.White;

            else
                playerColorRectangle = Color.Yellow;

                playerMoves = new PlayerMoves(number);
            this.network = network;
            this.isHost = isHost;

            if (number != Game1.currentPLayer)
            {
                isHost = false;
                isHumanPlayer = false;
            }
            else
            {
                isHumanPlayer = true;
                isHost = true;
            }
            playerTechs = new TechTree();
            this.number = number;
            this.isOrc = isOrc;
            playerColor = (War2Color)number;
            this.gold = 2000;
            this.oil = 2000;
            this.wood = 1000;
            food = 0;
            usedFood = 0;
            explored = new bool[Map.mapsize, Map.mapsize];
            hasVision = new bool[Map.mapsize, Map.mapsize];

            advancedbuildPanelToDraw = false;
            buildPanelToDraw = false;
            for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                {
                    explored[i, j] = false;
                    hasVision[i, j] = false;
                }

        }


        // returns null on nothing found
        public Building GetNearestStorePosition(ResourceType rt, Vector2 position, bool sailing)
        {
            position.X = (float) Math.Round(position.X);
            position.Y = (float) Math.Round(position.Y);
            Building b = null;
            float distance = 10000;
            Unit ut = new Unit();
            ut.sailing = sailing;
            ut.flying = false;

            if (rt == ResourceType.Gold)
                foreach (Unit u in Game1.map.units)
                {
                    if (u.owner == this.number && (u.type == UnitType.TownHall || u.type == UnitType.Fortress || u.type == UnitType.StrongHold) && ((Building)u).isBuilt())
                    {
                        if (Vector2.Distance(position, u.position) < distance) // its better
                        {
                            List<Vector2> tiles = Utils.Utils.getAllPositions((int) u.spriteSize.X / 32, (int) u.spriteSize.Y / 32, u.position);


                            if (ut.CalculatePath(position, u.position, Game1.map, tiles).Count > 0) // there is a path
                            {
                                distance = Vector2.Distance(position, u.position);
                                b = (Building)u;
                            }
                        }

                    }

                }

            else if (rt == ResourceType.Wood)
            {
                foreach (Unit u in Game1.map.units)
                {
                    if (u.owner == this.number && (u.type == UnitType.LumberMill || u.type == UnitType.TownHall || u.type == UnitType.Fortress || u.type == UnitType.StrongHold) && ((Building)u).isBuilt())
                    {
                        List<Vector2> tiles = Utils.Utils.getAllPositions((int)u.spriteSize.X / 32, (int)u.spriteSize.Y / 32, u.position);

                        if (Vector2.Distance(position, u.position) < distance) // its better
                        {
                            if (ut.CalculatePath(position, u.position, Game1.map, tiles).Count > 0) // there is a path
                            {
                                distance = Vector2.Distance(position, u.position);
                                b = (Building)u;
                            }
                        }

                    }

                }

            }
            else if (rt == ResourceType.Oil)
            {
                foreach (Unit u in Game1.map.units)
                {
                    if (u.owner == this.number && (u.type == UnitType.Raffinery || u.type == UnitType.NavalChantier) && ((Building)u).isBuilt())
                    {
                        if (Vector2.Distance(position, u.position) < distance) // its better
                        {
                            List<Vector2> tiles = Utils.Utils.getAllPositions((int)u.spriteSize.X / 32, (int)u.spriteSize.Y / 32, u.position);

                            if (ut.CalculatePath(position, u.position, Game1.map, tiles).Count > 0) // there is a path
                            {
                                distance = Vector2.Distance(position, u.position);
                                b = (Building)u;
                            }
                        }

                    }

                }
            }
            return b;
        }


        private void calculateFromPos(Vector2 pos, int sight)
        {

            for (int i = 0; i <= sight; i++)
                for (int j = 0; j <= sight; j++)
                {
                    // +i +j; + i - j; - i + j; -i -j
                    if (pos.X - i >= 0 && pos.Y - j > 0)
                    {
                        explored[(int)pos.X - i, (int)pos.Y - j] = true;
                        hasVision[(int)pos.X - i, (int)pos.Y - j] = true;
                    }
                    if (pos.X - i >= 0 && pos.Y + j < Map.mapsize)
                    {
                        explored[(int)pos.X - i, (int)pos.Y + j] = true;
                        hasVision[(int)pos.X - i, (int)pos.Y + j] = true;
                    }
                    if (pos.X + i < Map.mapsize && pos.Y + j < Map.mapsize)
                    {
                        explored[(int)pos.X + i, (int)pos.Y + j] = true;
                        hasVision[(int)pos.X + i, (int)pos.Y + j] = true;
                    }
                    if (pos.X + i < Map.mapsize && pos.Y - j > 0)
                    {
                        explored[(int)pos.X + i, (int)pos.Y - j] = true;
                        hasVision[(int)pos.X + i, (int)pos.Y - j] = true;
                    }
                }
        }

        public void calculateVision()
        {
            for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                {
                    hasVision[i, j] = false;
                }


            foreach (Unit u in Game1.map.units)
            {
                if (u.owner == number)
                {
                    if ((int)u.type < Unit.littlebuildingindex)
                    {
                        calculateFromPos(u.position, u.sight);
                    }
                    else if (u.isABuilding && ((Building)u).isBuilt())
                    {
                        calculateFromPos(u.position, u.sight);
                        calculateFromPos(new Vector2(u.position.X + (u.spriteSize.X / 32) - 1, u.position.Y), u.sight);
                        calculateFromPos(new Vector2(u.position.X + (u.spriteSize.X / 32) - 1, u.position.Y + (u.spriteSize.Y / 32) - 1), u.sight);
                        calculateFromPos(new Vector2(u.position.X, u.position.Y + (u.spriteSize.Y / 32) - 1), u.sight);                    
                    }


                }
            }

        }

        public void DrawFogOfWar(GraphicsDeviceManager graphics, SpriteBatch spriteBatch)
        {
           
            Vector2 coor = new Vector2(0, 0);

            for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                {
                    if (!hasVision[i, j] && explored[i, j])
                    {
                        int[] positions = Game1.map.GetMaptilesPosition(i, j);
                        Vector2 pos = new Vector2(i * 32, j * 32);
                        spriteBatch.Draw(Game1.currentTile, pos, new Rectangle(positions[0], positions[1], 32, 32), Color.Gray);
                    }
                    if (hasVision[i, j] && explored[i, j] && Game1.map.resources[i, j] != null && (Game1.map.resources[i, j].type == ResourceType.Gold || Game1.map.resources[i, j].type == ResourceType.Oil))
                    {
                        for (int v = 0; v < 3; v++)
                        {
                            for (int l = 0; l < 3; l++)
                            {
                                hasVision[i + v, j + l] = true;
                            }
                        }
                    }
                }
            for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                {
                    if (this.explored[i, j] != true)
                    {
                        coor.X = i * 32;
                        coor.Y = j * 32;

                        //spriteBatch.Draw(circletexture, coor, Color.Black);
                    }
                }

            foreach (Resource r in Game1.map.resources)
            {
                if (r != null)
                {
                    int i = (int)r.Position.X;
                    int j = (int)r.Position.Y;
                    if (!hasVision[i, j] && explored[i, j])
                    {
                        if (r.type == ResourceType.Gold)
                        {

                           spriteBatch.Draw(Game1.ResourceTexture, new Vector2(r.Position.X * 32, r.Position.Y * 32), new Rectangle(12, 105, 96, 96), Color.Gray);

                        }
                        else if (r.type == ResourceType.Oil)
                        {

                            spriteBatch.Draw(Game1.ResourceTexture, new Vector2(r.Position.X * 32, r.Position.Y * 32), new Rectangle(12, 200, 96, 96), Color.Gray);
                        }
                    }

                }
            }
            foreach (Unit u in Game1.map.units)
            {
                if (u.position != Game1.statics.ImpossiblePos)
                {
                    if (hasVision[(int)(Math.Round(u.position.X)), (int)Math.Round(u.position.Y)])
                        u.discovered = true;

                    if ((int)u.type >= Unit.littlebuildingindex)
                    {
                        int i = (int)u.position.X;
                        int j = (int)u.position.Y;
                        if (!hasVision[i, j] && explored[i, j] && u.discovered)
                        {
                            Building b = (Building)u;
                            spriteBatch.Draw(Game1.unitTextureAssociated[(int)b.type, b.owner], new Vector2(b.position.X * 32, b.position.Y * 32), new Rectangle((int)Math.Round(b.spriteSheetPosition.X), (int)Math.Round(b.spriteSheetPosition.Y), (int)Math.Round(b.spriteSize.X), (int)Math.Round(b.spriteSize.Y)), Color.Gray);
                        }
                    }
                    if (u.type == UnitType.Paysan || u.type == UnitType.Petrolier)
                    {
                        if (((Harvester)u).selectedBuilding != null && u.owner == Game1.currentPLayer)
                        {

                            ((Harvester)u).drawSelectedBuilding(spriteBatch);
                        }
                    }
                }

            }


        }


        public void Update(GameTime gameTime)
        {
            this.CalculateFood();
            if (number == Game1.currentPLayer)
            {
                timerBetweenAttackAnnoucements += gameTime.ElapsedGameTime.Milliseconds;
                timerBetweenTownAttackAnnoucements += gameTime.ElapsedGameTime.Milliseconds;

                if (timerBetweenAttackAnnoucements > 30000 && attackedAnnoucement)
                {
                    timerBetweenAttackAnnoucements = 0;
                    attackedAnnoucement = false;
                    Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.KnightUnitAttack1]);
                }
                if (timerBetweenTownAttackAnnoucements > 30000 && TownAttackedAnnoucement)
                {
                    timerBetweenTownAttackAnnoucements = 0;
                    TownAttackedAnnoucement = false;
                    Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.KnightTownAttack1]);
                }
                if (attackedAnnoucement)
                    attackedAnnoucement = false;
                if (TownAttackedAnnoucement)
                    TownAttackedAnnoucement = false;

            }

            if (number != Game1.currentPLayer) // its an AI
            {
                playerAI.Update(gameTime);
            }

        }

        /**
         * Returns tthe list of all units the player can currently build
         */
        public List<UnitType> PlayerCanBuild()
        {
            List<UnitType> playerCanBuildList = new List<UnitType>();

            foreach (UnitType ut in Enum.GetValues(typeof(UnitType)))
            {
                if (this.playerTechs.hasTech(ut))
                    playerCanBuildList.Add(ut);
            }
            return playerCanBuildList;
        }

        public int PlayerHas(UnitType t, UnitType equivalentUnitType = UnitType.None, UnitType equivalentUnitType2 = UnitType.None)
        {
            if (!has.ContainsKey(t))
                has[t] = 0;
            if (!has.ContainsKey(equivalentUnitType))
                has[equivalentUnitType] = 0;
            if (!has.ContainsKey(equivalentUnitType2))
                has[equivalentUnitType2] = 0;

            return has[t] + has[equivalentUnitType] + has[equivalentUnitType2];
        }
        // also checks for stronghold, lumber mills && fortress
        internal void CalculateFood()
        {
            food = 0;
            usedFood = 0;

            foreach (var key in has.Keys.ToList()) // resets the list of things the play has
                has[key] = 0;

            foreach (Unit u in Game1.map.units)
            {
                if (u.owner == number)
                {
                    if (has.ContainsKey(u.type))
                        has[u.type] += 1;
                    else
                        has[u.type] = 1;

                    if ((int)u.type >= Unit.littlebuildingindex)
                    {
                        Building b = (Building)u;


                        if (b.type == UnitType.Farm && b.isBuilt())
                            food += 4;
                        else if (u.type == UnitType.Fortress || u.type == UnitType.StrongHold)
                            food += 1;
                        else if (b.type == UnitType.TownHall && b.isBuilt())
                            food += 1;
                    }
                    else if (u.owner == number)
                    {
                        this.usedFood += 1;
                    }
                }
            }



        }




    }

}
