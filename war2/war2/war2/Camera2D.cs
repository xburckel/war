﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2
{
        [Serializable]

 public class Camera2D

    {

        #region Fields

 
        protected float _zoom;
        protected Matrix _transform;
        protected Matrix _inverseTransform;
        public Vector2 _pos;
        protected float _rotation;
        protected Viewport _viewport;
        protected MouseState _mState;
        protected KeyboardState _keyState;
        protected Int32 _scroll;
        public float INIT_ZOOM_VALUE = 2.5f;
 

        #endregion

        #region Properties

 

        public float Zoom

        {

            get { return _zoom; }

            set { _zoom = value; }

        }

        /// <summary>

        /// Camera View Matrix Property

        /// </summary>
        /// 
            public MouseState getTrueMousePos(MouseState msTate)
        {
           // return Mouse.GetState();
            Vector2 newPos = Vector2.Transform(new Vector2(msTate.X, msTate.Y), Matrix.Invert(_transform));

            MouseState mstate = new MouseState((int) newPos.X, (int) newPos.Y, msTate.ScrollWheelValue, msTate.LeftButton, msTate.MiddleButton, msTate.RightButton, msTate.XButton1, msTate.XButton2);
            return mstate;

        }



        public Matrix Transform

        {

            get { return _transform; }

            set { _transform = value; }

        }

        /// <summary>

        /// Inverse of the view matrix, can be used to get objects screen coordinates

        /// from its object coordinates

        /// </summary>

        public Matrix InverseTransform

        {

            get { return _inverseTransform; }

        }

        public Vector2 Pos

        {

            get { return _pos; }

            set { _pos = value; }

        }

        public float Rotation

        {

            get { return _rotation; }

            set { _rotation = value; }

        }

 

        #endregion

        #region Constructor


        public Camera2D(Viewport viewport)

        {

            _zoom = INIT_ZOOM_VALUE;

            _scroll = 1;

            _rotation = 0.0f;

            _pos = Game1.statics.ImpossiblePos;

            _viewport = viewport;

        }

        #endregion

        #region Methods

            /**
             * Translates a game position to its mouse coordinates 
             * */
        public Point ToMousePosition(Vector2 position)
        {
            return new Point((int) (position.X * Zoom * 32 + Pos.X), (int) (position.Y * Zoom * 32 + Pos.Y));
        }

        public void setInitialPosition(List<Unit> mapUnits)
        {
            foreach (Unit u in mapUnits)
            {
                if (u.owner == Game1.currentPLayer)
                {
                    Game1.cam.Pos = -32 * u.position * Game1.cam.Zoom + new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width, Game1.graphics.GraphicsDevice.Viewport.Height) / 2;
                    break;
                }
            }
        }

        /// <summary>

        /// Update the camera view

        /// </summary>

        public void Update(GraphicsDeviceManager graphics, Map map)

        {

            //Call Camera Input

            Input();

            //Clamp zoom value

            _zoom = MathHelper.Clamp(_zoom, 0.0f, 10.0f);

            //Clamp rotation value

            _rotation = ClampAngle(_rotation);



            if (_pos.X < (-(Map.mapsize * 32 * Zoom - graphics.GraphicsDevice.Viewport.Width)))
                _pos.X = (-(Map.mapsize * 32 * Zoom - graphics.GraphicsDevice.Viewport.Width));
            if (_pos.Y < (-(Map.mapsize * 32 * Zoom - graphics.GraphicsDevice.Viewport.Height)))
                _pos.Y = (-(Map.mapsize * 32 * Zoom - graphics.GraphicsDevice.Viewport.Height));

            if (_pos.X > 175) // leftpanel width
                _pos.X = 175;

            if (_pos.Y > 14) // toppanel height
                _pos.Y = 14;


            _transform =    Matrix.CreateRotationZ(_rotation) *

                            Matrix.CreateScale(new Vector3(_zoom, _zoom, 1)) *

                            Matrix.CreateTranslation((int) Math.Round( _pos.X), (int) Math.Round( _pos.Y), 0);

            //Update inverse matrix

            _inverseTransform = Matrix.Invert(_transform);

        }

 

        /// <summary>

        /// Example Input Method, rotates using cursor keys and zooms using mouse wheel

        /// </summary>

        protected virtual void Input()

        {

            _mState = Mouse.GetState();

            _keyState = Keyboard.GetState();



            //Check rotation
/*
            if (_keyState.IsKeyDown(Keys.Left))
121
            {
122
                _rotation -= 0.1f;
123
            }
124
            if (_keyState.IsKeyDown(Keys.Right))
125
            {

                _rotation += 0.1f;

            }*/

            //Check Move

            if (_keyState.IsKeyDown(Keys.Left))

            {

                _pos.X += 10.0f;

            }

            if (_keyState.IsKeyDown(Keys.Right))

            {

                _pos.X -= 10.0f;

            }

            if (_keyState.IsKeyDown(Keys.Up))

            {

                _pos.Y += 10.0f;

            }

            if (_keyState.IsKeyDown(Keys.Down))

            {

                _pos.Y -= 10.0f;

            }

        }

 
        public void setPosition(Vector2 position)
        {
            this.Pos = position;
        }
        /// <summary>

        /// Clamps a radian value between -pi and pi

        /// </summary>

        /// <param name="radians">angle to be clamped</param>

        /// <returns>clamped angle</returns>

        protected float ClampAngle(float radians)

        {

            while (radians < -MathHelper.Pi)

            {

                radians += MathHelper.TwoPi;

            }

            while (radians > MathHelper.Pi)

            {

                radians -= MathHelper.TwoPi;

            }

            return radians;

        }


    }

}

#endregion
