﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.SpriteSheetCode
{
    public class SingleSpritePosition
    {
        public int posX;
        public int posY;
        public int sizeX;
        public int sizeY;

        public SingleSpritePosition(int posX, int posY, int sizeX, int sizeY)
        {
            this.posX = posX;
            this.posY = posY;
            this.sizeX = sizeX;
            this.sizeY = sizeY;
        }
    }
}
