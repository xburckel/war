﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2
{
    public class BoundingBox
    {
        // unused atm
        Vector2 position;
        int width;
        int height;

        public BoundingBox(Unit u)
        {
            this.position = new Vector2(u.position.X, u.position.Y);
            if (u.sailing || u.flying)
            {
                width = 2;
                height = 1;
            }
            else
            {
                width = 1;
                height = 1;
            }
        }

        public void Update(Vector2 position)
        {
            this.position = new Vector2(position.X, position.Y);
        }

        public bool Collides(BoundingBox collidesWith)
        {
            if (position.X + width >= collidesWith.position.X &&
                position.X <= collidesWith.position.X + collidesWith.width &&
                position.Y + height >= collidesWith.position.Y &&
                position.Y <= collidesWith.position.Y + collidesWith.height)
            {
                return true;
            }
            return false;
        }


    }
}
