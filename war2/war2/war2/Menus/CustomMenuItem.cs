﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.Menus
{
    /**
     * Class used to handle custom menu items and their related actions
     */
    public class CustomMenuItem
    {
       Texture2D customMenu;
       const int TEXTURE_WIDTH = 178;
       const int TEXTURE_HEIGHT = 23;
       Dictionary<string, Tuple<Tuple<Action, Vector2>, Vector2>> methods;
       MouseState prevMs;
        
        public CustomMenuItem()
        {
            this.customMenu = Game1.content.Load<Texture2D>("custom_menu_select");
            methods = new Dictionary<string, Tuple<Tuple<Action, Vector2>, Vector2>>();
            prevMs = Mouse.GetState();
        }
        /**
         * Draws a text menu at posx, posy
         * Spritebatch is optionnal, if not supplied, will create new one
         * Action is the method that will be invoked upon click
         */
        public void DrawMenu(int posX, int posY, 
            string text, 
            Action onclick = null, 
            SpriteBatch spbatch = null, 
            bool spbatchBeginEnd = true,
            int maxXSize = 0, int maxYSize = 0)
        {
            if (spbatch == null)
                spbatch = new SpriteBatch(Game1.graphics.GraphicsDevice);
            if (spbatchBeginEnd)
                spbatch.Begin();

            if (maxXSize == 0 || maxXSize > TEXTURE_WIDTH)
                maxXSize = TEXTURE_WIDTH;
            if (maxYSize == 0 || maxYSize > TEXTURE_HEIGHT)
                maxYSize = TEXTURE_HEIGHT;

            if (onclick != null && !methods.ContainsKey(text))
                methods.Add(text, new Tuple<Tuple<Action,Vector2>,Vector2>(new Tuple<Action, Vector2>(onclick, new Vector2(posX, posY)), new Vector2(maxXSize, maxYSize)));

            spbatch.Draw(this.customMenu, new Rectangle(posX, posY, maxXSize, maxYSize), Color.White);
            // + x / 20 = to have some margin
            spbatch.DrawString(Game1.statics.font, text, new Vector2(posX + (maxXSize / 20), posY + (maxYSize / 20)), Color.Gold);

            if (spbatchBeginEnd)
                spbatch.End();
        }

        public void Update()
        {
            MouseState ms = Mouse.GetState();

            if (ms.LeftButton == ButtonState.Pressed && (prevMs.LeftButton.Equals(ButtonState.Released)))
            {
                foreach (KeyValuePair<string, Tuple<Tuple<Action, Vector2>, Vector2>> pair in methods)
                {
                    // Note: Item1.Item2 = button position, Item2 = button width/hight
                    if (pair.Value.Item1.Item2.X < ms.X
                        && pair.Value.Item1.Item2.X + pair.Value.Item2.X > ms.X
                        && pair.Value.Item1.Item2.Y < ms.Y
                        && pair.Value.Item1.Item2.Y + pair.Value.Item2.Y  > ms.Y) // if menu clicked, launch its related action
                    {
                        pair.Value.Item1.Item1.Invoke();
                        Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.BigButtonClick]);
                    }
                }
            }
            // assuming DrawMenu is called every turn
            methods.Clear();
            prevMs = ms;
        }

    }
}
