﻿using EpPathFinding;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.Utils;

namespace war2.UnitsRelated
{
    [Serializable]
    public class IA
    {
        private struct Point
        {
            public int x;
            public int y;

            public Point(int x, int y)
            {
                // TODO: Complete member initialization
                this.x = x;
                this.y = y;
            }
        }
        int townHallToMineAcceptableDistance = 12;
        public enum AIType { Air, Ground1, Water, ToDetermine }
        public static int AggroRange = 10;
        //  int type;
        int owner;
        int updateTimer = 0;
        public Vector2 TownHallPosition = Game1.statics.ImpossiblePos;
        public Vector2 goldminepos = Game1.statics.ImpossiblePos;
        public Vector2 oilPatchPos = Game1.statics.ImpossiblePos;

        Vector2 secondaryTownHallPos;


        public List<long> attackingUnitsIndex = new List<long>();
        public int targetCheckTimer = 0;
        bool myturnplayed = false;
        private AIType type;
        private int availableGold;
        private int availableWood;
        private int availableOil;
        int temp_rand;
        private bool lacksSpace;
        private int potentialFood; // includes pending construction orders
        public bool isUnderAttack;
        public List<Vector2> attackPositions;
        public bool constantAttackBehaviour;
        int[] numberOfUnits;
        int currentDistanceTownHallToGMine;
        public int numberOfLostUnits; // used to avoid getting stuck to tier 1 for AI's with attacking units behaviour
        bool stillSpaceForNewBases;
        List<Building> pendingConstructionOrders;
        List<Unit> myunits;
        List<Building> mybuildings;
        List<Harvester> myAvailablePeasants;
        List<Harvester> mytankers;


        public IA(int owner)
        {
            stillSpaceForNewBases = true;
            TownHallPosition = Game1.statics.ImpossiblePos;
            goldminepos = Game1.statics.ImpossiblePos;
            oilPatchPos = Game1.statics.ImpossiblePos;
            secondaryTownHallPos = Game1.statics.ImpossiblePos;
            currentDistanceTownHallToGMine = 999999;
            this.owner = owner;
            type = AIType.ToDetermine;
            lacksSpace = false;
            temp_rand = 3;
            isUnderAttack = false;
            constantAttackBehaviour = false;
            attackPositions = new List<Vector2>();
            int tmp = (Game1.statics.ra.Next(2));
            if (tmp == 1)
                constantAttackBehaviour = true; // all units will go into attack behaviour as long as there is an attack

            numberOfLostUnits = 0;
            pendingConstructionOrders = new List<Building>();
        }

        // checks if the unit can possibily attack from its position
        private bool checkCanAttack(Unit u, List<Vector2> cantAttackFromThesePos)
        {
            foreach (Vector2 cantAttack in cantAttackFromThesePos)
            {
                if (u.sailing && Game1.map.mapAreas.stGridsWater[(int) Math.Round(cantAttack.X), (int) Math.Round(cantAttack.Y)] 
                    == Game1.map.mapAreas.stGridsWater[(int) Math.Round(u.position.X), (int) Math.Round(u.position.Y)])
                    return false;
                if (!u.sailing && !u.flying && Game1.map.mapAreas.stGridsGround[(int)Math.Round(cantAttack.X), (int)Math.Round(cantAttack.Y)]
                    == Game1.map.mapAreas.stGridsGround[(int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)])
                    return false;
                }
            return true;
        }

        private void InitIA()
        {
            // check if there is a path between one of the ai's unit and your units
            type = AIType.Air;
            foreach (Unit u in Game1.map.units)
            {
                if (u.owner == Game1.currentPLayer)
                {
                    foreach (Unit u2 in Game1.map.units)
                    {
                        if (u2.owner == owner)
                        {
                            if (!u2.sailing && !u2.flying && u2.CalculatePath(u2.position, u.position, Game1.map).Count > 0)
                            {
                                // if (Game1.statics.ra.Next(2) == 1)
                                type = AIType.Ground1;
                                // else
                                //     type = AIType.Ground2;
                                break;
                            }
                        }
                    }
                }
            }


            if (type == AIType.Air && Game1.map.OilResources.Count > 4)
            {
                float numberOfWater = 0f;
                foreach (bool b in Game1.map.waterTraversableMapTiles)
                {
                    if (b)
                        numberOfWater++;
                }
                if ((numberOfWater / (float)(Map.mapsize * Map.mapsize)) > 0.15f) // more than 15% of map is water
                {
                    type = AIType.Water;
                }
            }
        }


        public void Update(GameTime gameTime)
        {
            potentialFood = 0;
            // At the beginning of the game, choose the AI type depending on the map
            if (type == AIType.ToDetermine) 
            {
                InitIA();
            }


            updateTimer += gameTime.ElapsedGameTime.Milliseconds;
            targetCheckTimer += gameTime.ElapsedGameTime.Milliseconds;
            if (updateTimer > 800)
            {
                updateTimer = 0;
                myturnplayed = false;
            }
            // take new decisions :
            /*
             * Check harvesters inactive
             * Assign them task
             * Evaluate army value
             * Assign army task if necessary :
             * Check under attack
             * Check enough troops for attack
             * Take building decision :
             * - Build buildings ?
             * - Build harvesters ?
             * - Build army ?
             */
            if (updateTimer > owner * 100 && myturnplayed == false)
            {

                Game1.statics.debugWatch.Restart();
                pendingConstructionOrders.Clear();

                myturnplayed = true;

                Game1.map.players[owner].CalculateFood();
                availableGold = Game1.map.players[owner].gold;
                availableWood = Game1.map.players[owner].wood;
                availableOil = Game1.map.players[owner].oil;

                myunits = new List<Unit>();
                mybuildings = new List<Building>();
                myAvailablePeasants = new List<Harvester>(); // only peasants
                mytankers = new List<Harvester>(); // only tnkers

                List<Unit> attackers = new List<Unit>();
                List<long> notIn = new List<long>();

                #region Attack behaviour
                foreach (long l in attackingUnitsIndex)
                    notIn.Add(l);
                numberOfUnits = new int[(int) UnitType.Fortress + 1]; // this will need to increase in case new units
                for (int i = 0; i < numberOfUnits.Length; i++)
                    numberOfUnits[i] = 0;

                    foreach (Unit u in Game1.map.units)
                    {
                        if (u.owner == owner)
                        {
                            numberOfUnits[(int)u.type]++;

                            foreach (long i in attackingUnitsIndex)
                            {
                                if (u.index == i)
                                {
                                    notIn.Remove(i);
                                    attackers.Add(u);
                                    break;
                                }
                            }
                        }
                    }
                foreach (long l in notIn)
                    attackingUnitsIndex.Remove(l);

                
                   Game1.statics.debugWatch.Restart();

                List<Vector2> cantAttackFromThisPos = new List<Vector2>();
                foreach (Unit u in attackers)
                {
                    if (checkCanAttack(u, cantAttackFromThisPos))
                    {
                        bool attackable = attackMove(u, gameTime);
                        if (!attackable && u.path.Count == 0)
                            cantAttackFromThisPos.Add(u.position);
                    }

                }
                Game1.statics.debugWatch.Stop();
                #endregion

                if (Game1.statics.debugWatch.ElapsedMilliseconds > 100)
                    Debug.debugString = "player : " + ((War2Color)owner).ToString() + " moved-attack units in " + Game1.statics.debugWatch.ElapsedMilliseconds + "miliseconds";
                Game1.statics.debugWatch.Restart();
                



                bool foundTH = false;

                foreach (Unit u in Game1.map.units)
                {
                    if (u.owner == this.owner)
                    {
                        if (u.type == UnitType.Farm)
                            potentialFood += 4;
                        if ((int)u.type >= Unit.largebuildingindex)
                            potentialFood += 1;
                        if (u.type == UnitType.Petrolier)
                            mytankers.Add((Harvester)u);
                        if (u.type == UnitType.Paysan && ((Harvester) u).constructingBuilding == null)
                            myAvailablePeasants.Add((Harvester)u);
                        else if ((int)u.type >= Unit.littlebuildingindex)
                        {
                            if ((int)u.type >= Unit.largebuildingindex && foundTH == false)
                            {
                                TownHallPosition = u.position;
                                foundTH = true;
                            }
                            mybuildings.Add((Building)u);
                        }
                        else
                            myunits.Add(u);
                    }
                }


                if (goldminepos.Equals(Game1.statics.ImpossiblePos) && myAvailablePeasants.Count > 0)
                {
                    handleNoGoldMineYet(myAvailablePeasants, mybuildings);
                }
                updateDistanceTownHallToMine(mybuildings);

                if (!(numberOfUnits[(int)UnitType.TownHall] > 0) && !(numberOfUnits[(int)UnitType.Fortress] > 0) && !(numberOfUnits[(int)UnitType.StrongHold] > 0))
                {
                    if (myAvailablePeasants.Count > 0 && goldminepos != Game1.statics.ImpossiblePos)
                        TownHallPosition = findGoodBuildingSpot(UnitType.TownHall, goldminepos, myAvailablePeasants, false).Item2;
                }

                HandleNewUnitsCreation();
                HandleNewBuildingsCreation(myAvailablePeasants);


                foreach (Building b in pendingConstructionOrders)
                {
                    availableGold -= b.goldCost;
                    availableWood -= b.woodCost;
                    availableOil -= b.oilCost;
                }

                #region AIA attack behaviour
                if (type == AIType.Air)
                {
                    foreach (Unit u in myunits)
                        if (u.type == UnitType.Griffon)
                            attackingUnitsIndex.Add(u.index);
                }

                if (type == AIType.Water)
                {
                    foreach (Unit u in myunits)
                        if (u.sailing && Game1.statics.basicBehaviourAttackIngUnits.Contains(u.type))
                            attackingUnitsIndex.Add(u.index);
                }

                if (isUnderAttack) //
                {
                    isUnderAttack = false;
                    foreach (Unit u in myunits)
                    {
                        if (Game1.statics.basicBehaviourAttackIngUnits.Contains(u.type) && constantAttackBehaviour) // if we are an AI solidaire from units all over the map
                        {
                            attackingUnitsIndex.Add(u.index);
                        }
                        else if (Game1.statics.basicBehaviourAttackIngUnits.Contains(u.type))
                        {
                            foreach (Vector2 attackPos in this.attackPositions)
                            {
                                if (Vector2.Distance(attackPos, u.position) < Map.mapsize / 4) // if the attack was not so far away
                                {
                                    attackingUnitsIndex.Add(u.index);
                                    break;
                                }
                            }
                        }
                    }
                }

                else if ((type == AIType.Ground1) && myunits.Count > 25)
                    foreach (Unit u in myunits)
                        if (Game1.statics.basicBehaviourAttackIngUnits.Contains(u.type))
                            attackingUnitsIndex.Add(u.index);
                #endregion

                determineUpgrades(myunits, mybuildings);



 
                
                if ((numberOfUnits[(int) UnitType.NavalPlatform] > 0) && ((numberOfUnits[(int) UnitType.NavalChantier] > 0) || (numberOfUnits[(int) UnitType.Raffinery] > 0)))
                    setTankersToCollect(mytankers);

                DispatchInactiveHarvesters();


                if (Game1.statics.debug && Game1.statics.debugWatch.ElapsedMilliseconds > 100)
                    Game1.statics.annouceMessage.UpdateMessage("player : " + ((War2Color) owner).ToString() + " built in " + Game1.statics.debugWatch.ElapsedMilliseconds + "miliseconds");
            }

            attackPositions.Clear();
        }

        /** 
         * Tries to give each inactive harvester a task
         * Inactive = not harvesting, not moving, not building, not repairing and no stuck
         */
        private void DispatchInactiveHarvesters()
        {
            foreach (Harvester h in myAvailablePeasants)
            {
                if (h.IsDoingNothing())
                {
                    if (h.carrying == ResourceType.None)
                    {
                        if (availableGold > 4000) // lots of gold, go for wood
                            h.currentResource = Resource.findAnotherResource(ResourceType.Wood, h);
                        else if (availableGold < 2500 && availableWood > 1300) // lots of wood, go for gold
                            h.currentResource = Resource.findAnotherResource(ResourceType.Gold, h);
                        else if (availableGold < 800) // really not much gold
                            h.currentResource = Resource.findAnotherResource(ResourceType.Gold, h);
                        else if (availableWood < 500) // really not much wood
                            h.currentResource = Resource.findAnotherResource(ResourceType.Wood, h);
                        else // go for gold as a default
                            h.currentResource = Resource.findAnotherResource(ResourceType.Gold, h);

                        if (h.currentResource != null)
                            h.Move(h.currentResource.Position, Game1.map, true);
                    }
                    else
                    {
                        h.Move(TownHallPosition);
                    }

                }
            }
        }

        /**
         * While AI has resources and free harvesters
         * 1) Looks for the next Building to be built
         * 2) Checks if it is not currently under construction
         * 3) Sends building orders
         */
        private void HandleNewBuildingsCreation(List<Harvester> myAvailablePeasants)
        {
            List<UnitType> banned = new List<UnitType>();
            UnitType t = NextToBeBuilt(false);
            // Iterate through build order until we are producing everything we need
            while (t != UnitType.None)
            {
                if (Game1.map.players[owner].playerTechs.hasTech(t))
                {
                    Harvester used = BuildBuilding(t);
                    if (used != null)
                    {
                        pendingConstructionOrders.Add(used.constructingBuilding);
                        // This peasant is not available anymore
                        myAvailablePeasants.Remove(used);
                    }
                    // so we don't get multiple time the same unit
                    banned.Add(t);
                    t = NextToBeBuilt(true, banned);
                }
                else
                {
                    t = UnitType.None;
                }
            }
        }
        /**
         * While we have resources and buildings, tries to build according to build path
         */
        private void HandleNewUnitsCreation()
        {
            List<UnitType> banned = new List<UnitType>();
            UnitType t = NextToBeBuilt(true);
            // Iterate through build order until we are producing everything we need
            while (t != UnitType.None)
            {
                
                if (Game1.map.players[owner].playerTechs.hasTech(t))
                {
                    BuildUnit(t);
                    // so we don't get multiple time the same unit
                    banned.Add(t);
                    t = NextToBeBuilt(true, banned);
                }
                else
                {
                    t = UnitType.None;
                }
            }
        }

        /** Checks if the AIA's player has enough resources to build a specific unit, by taking into account current construction orders costs
         * Set isBuilding to false if it is a simple unit
         */
        private bool HasResourcesToBuild(UnitType u, bool isBuilding = false)
        {
            int goldCost = 0;
            int woodCost = 0;
            int oilCost = 0;

            if (isBuilding)
            {
                goldCost = UnitProperties.getBuildingProperties(u)[(int)BuildingProperties.GOLDCOST];
                woodCost = UnitProperties.getBuildingProperties(u)[(int)BuildingProperties.WOODCOST];
                oilCost = UnitProperties.getBuildingProperties(u)[(int)BuildingProperties.OILCOST];
            }
            else
            {
                goldCost = UnitProperties.getBuildingProperties(u)[(int)BuildingProperties.GOLDCOST];
                woodCost = UnitProperties.getBuildingProperties(u)[(int)BuildingProperties.WOODCOST];
                oilCost = UnitProperties.getBuildingProperties(u)[(int)BuildingProperties.OILCOST];
            }

            return (availableGold > goldCost && availableWood > woodCost && availableOil > oilCost);
        }

        // Checks for an available mine and sets it as new mine
        // sets townhall position as impossiblepos if a mine is found so that we build a new one at the mine position
        // if no mine found sets distance to mine as 0 to act as if all is ok and no need to build a new townhall
        private void handleNoGoldMineYet(List<Harvester> mypeasants, List<Building> mybuildings)
        {
            Dictionary<float, Vector2> mnes = new Dictionary<float, Vector2>();
            currentDistanceTownHallToGMine = 999999;
            Vector2 positionFromWhichToTest = mypeasants[0].position;
            secondaryTownHallPos = Game1.statics.ImpossiblePos;
            if ((numberOfUnits[(int) UnitType.TownHall] > 0))
            {
                positionFromWhichToTest = TownHallPosition;
            }
            List<Vector2> forcewalkableStartPos = Utils.Utils.getAllPositions(4, 4, positionFromWhichToTest);

            foreach (Resource r in Game1.map.GoldResources) // find nearest gold mine not by distance but by path
            {
                List<Vector2> forcewalkable = forcewalkableStartPos;
                forcewalkable.AddRange(Utils.Utils.getAllPositions(3, 3, r.Position));

                float distance = Utils.Utils.getTrueWalkingDistance(positionFromWhichToTest, r.Position, forcewalkable);
                if (distance != Int32.MaxValue)
                {
                    while (mnes.ContainsKey(distance))
                        distance += 0.001f;
                    mnes.Add(distance, r.Position);
                }
            }

            var items = from pair in mnes
                        orderby pair.Key ascending
                        select pair;

            foreach (KeyValuePair<float, Vector2> candidatePos in items) // take nearest gold mine not taken by someone
            {
                bool taken = false;
                foreach (Unit u in Game1.map.units)
                {
                    List<Vector2> forcewalkable = forcewalkableStartPos;
                    forcewalkable.AddRange(Utils.Utils.getAllPositions(3, 3, candidatePos.Value));
                    float distance = Utils.Utils.getTrueWalkingDistance(u.position, candidatePos.Value, forcewalkable);
                    if (distance != Int32.MaxValue)
                    {
                        if ((u.owner != owner && (u.type == UnitType.TownHall || u.type == UnitType.StrongHold || u.type == UnitType.Fortress) && distance <= townHallToMineAcceptableDistance)
                            || (u.owner == Game1.currentPLayer && u.isABuilding && distance <= 10))
                        {
                            taken = true;
                            break;
                        }
                    }
                }
                if (!taken)
                {
                    goldminepos = candidatePos.Value;
                    secondaryTownHallPos = goldminepos;

                    break;
                }

            }

            if (goldminepos != Game1.statics.ImpossiblePos && ((numberOfUnits[(int) UnitType.TownHall] > 0) || (numberOfUnits[(int) UnitType.Fortress] > 0) || (numberOfUnits[(int) UnitType.StrongHold] > 0)))
                foreach (Unit u in mybuildings) // check if too far away
                {
                    if (u.type == UnitType.TownHall || u.type == UnitType.StrongHold || u.type == UnitType.Fortress)
                    {
                        if (Vector2.Distance(goldminepos, u.position) <= currentDistanceTownHallToGMine)
                        {
                            currentDistanceTownHallToGMine = (int)Vector2.Distance(goldminepos, u.position);
                        }
                    }
                }
            else
                currentDistanceTownHallToGMine = 0;
        }

        private void setTankersToCollect(List<Harvester> myTankers)
        {
            foreach (Harvester h in myTankers)
            {
                if (h.currentResource == null)
                        h.currentResource = Resource.findAnotherResource(ResourceType.Oil, h);


                if (h.carrying == ResourceType.None && h.currentResource != null && h.disabled <= 0)
                {
                    h.Move(oilPatchPos, Game1.map);
                }
            }
        }

        /**
         * Builds a unit (not a building)
         * */
        public void BuildUnit(UnitType t)
        {
            #region building unit logic
            switch (t)
            {
                case UnitType.Paysan:
                    foreach (Building b in mybuildings)
                    {
                        if (b.unitbuilding == UnitType.None)
                        {
                            if ((int)b.type >= Unit.largebuildingindex)
                            {
                                b.BuildUnit(t);
                                break;
                            }

                        }

                    }
                    break;
                case UnitType.Archer:
                case UnitType.Catapulte:
                case UnitType.Fantassin:
                case UnitType.Chevalier:
                    foreach (Building b in mybuildings)
                    {
                        if (b.unitbuilding == UnitType.None)
                        {
                            if (b.type == UnitType.MilitaryBase)
                            {
                                b.BuildUnit(t);
                            }

                        }

                    }
                    break;
                case UnitType.Battleship:
                case UnitType.Transport:
                case UnitType.Destroyer:
                case UnitType.Petrolier:
                case UnitType.Sousmarin:

                    foreach (Building b in mybuildings)
                    {
                        if (b.unitbuilding == UnitType.None)
                        {
                            if (b.type == UnitType.NavalChantier)
                            {
                                b.BuildUnit(t);
                            }

                        }

                    }
                    break;
                case UnitType.Griffon:
                    foreach (Building b in mybuildings)
                    {
                        if (b.unitbuilding == UnitType.None)
                        {
                            if (b.type == UnitType.GryffinTower)
                            {
                                b.BuildUnit(t);
                            }

                        }

                    }
                    break;
            }
            #endregion
        }


        /**
         * Gives the order to build a unit or building and handles the associated logic of finding a harvester, etc...
         * posx, posy to force look from a specific position, elsel ooks from townhall position
         * returns the selected harvester for the task
         * */
        public Harvester BuildBuilding(UnitType t)
        {
            #region Tankers building logic
            // special case if naval platform
            if (t == UnitType.NavalPlatform)
            {
                if (this.type == AIType.Water && oilPatchPos == Game1.statics.ImpossiblePos)
                {
                    if ((numberOfUnits[(int)UnitType.NavalChantier] > 0) || (numberOfUnits[(int)UnitType.Raffinery] > 0))
                    {
                        foreach (Unit u in Game1.map.units)
                        {
                            if (u.owner == this.owner && (u.type == UnitType.NavalChantier || u.type == UnitType.Raffinery))
                            {
                                Vector2 nearest = new Vector2(9999, 9999);
                                Resource found = null;
                                foreach (Resource r in Game1.map.OilResources)
                                {
                                    if (Vector2.Distance(r.Position, u.position) < Vector2.Distance(u.position, nearest))
                                    {
                                        nearest = r.Position;
                                        found = r;
                                    }
                                }
                                oilPatchPos = nearest;
                                break;
                            }
                        }
                    }

                }
            }
            #endregion
            else if (this.myAvailablePeasants.Count == 0) // if all harvesters are taken, do nothing
                return null;

            Vector2 buildPos;
            buildPos = TownHallPosition;

            if (t != UnitType.None)
            {
                Unit selectedUnit = Game1.map.CreateUnit(t, buildPos, owner);
                // if we have enough money available
                if (availableGold >= selectedUnit.goldCost && availableOil >= selectedUnit.oilCost && availableWood >= selectedUnit.woodCost)
                {
                    Unit tmp = new Unit();
                    #region IA building built atatck behaviour
                    // When these types of building are built, launch an attack
                    if (t == UnitType.LumberMill || t == UnitType.Forge || t == UnitType.Stable)
                    {
                        if (type == AIType.Ground1)
                            foreach (Unit u in myunits)
                                if (Game1.statics.basicBehaviourAttackIngUnits.Contains(u.type))
                                    attackingUnitsIndex.Add(u.index);
                    }

                    // also launch attack
                    if (t == UnitType.StrongHold)
                    {
                        foreach (Building b in mybuildings)
                        {
                            if (b.type == UnitType.TownHall)
                            {
                                b.Upgrade(UnitType.StrongHold);
                                numberOfLostUnits = 0;
                                // launch attack
                                if (type == AIType.Ground1)
                                    foreach (Unit u in myunits)
                                        if (Game1.statics.basicBehaviourAttackIngUnits.Contains(u.type))
                                            attackingUnitsIndex.Add(u.index);
                                break;
                            }

                        }
                        return null; // no harvester was required
                    }
                    // also launch attack
                    else if (t == UnitType.Fortress)
                    {
                        foreach (Building b in mybuildings)
                        {
                            if (b.type == UnitType.StrongHold)
                            {
                                numberOfLostUnits = 0;
                                b.Upgrade(UnitType.Fortress);
                                //launch attack
                                if (type == AIType.Ground1)
                                    foreach (Unit u in myunits)
                                        if (Game1.statics.basicBehaviourAttackIngUnits.Contains(u.type))
                                            attackingUnitsIndex.Add(u.index);
                            }

                        }
                        return null; // no harvester was required
                    }
                    #endregion

                    List<Harvester> tempArrayHarvesters = new List<Harvester>();
                    tempArrayHarvesters.AddRange(myAvailablePeasants);
                    tempArrayHarvesters.AddRange(mytankers);

                    if (!buildPos.Equals(Game1.statics.ImpossiblePos)) // build the building if found position
                    {
                        Tuple<Harvester, Vector2> builderAndBuildPos = new Tuple<Harvester, Vector2>(tempArrayHarvesters.First(), buildPos);


                        if (t == UnitType.NavalPlatform && !(mytankers.Count() == 0))
                        {
                            float distance = 99999999;
                            buildPos = Game1.statics.ImpossiblePos;
                            foreach (Resource r in Game1.map.OilResources)
                            {
                                if (r.platForm == null && Vector2.Distance(r.Position, TownHallPosition) < distance)
                                {
                                    distance = Vector2.Distance(r.Position, TownHallPosition);
                                    builderAndBuildPos = new Tuple<Harvester, Vector2>(mytankers.First(), r.Position);
                                }
                            }

                        }
                        else if (t != UnitType.TownHall) // try not to build right at town hall
                            builderAndBuildPos = findGoodBuildingSpot(t, new Vector2(TownHallPosition.X, TownHallPosition.Y), tempArrayHarvesters, true);
                        else
                            builderAndBuildPos = findGoodBuildingSpot(t, buildPos, tempArrayHarvesters);
                        buildPos = builderAndBuildPos.Item2;

                        if (!buildPos.Equals(Game1.statics.ImpossiblePos)) // build the building if found position
                        {
                            lacksSpace = false;
                            builderAndBuildPos.Item1.selectedBuilding = (Building)selectedUnit;
                            builderAndBuildPos.Item1.GiveBuildingConstructOrder(buildPos);
                            return builderAndBuildPos.Item1; // returns the found harvester
                        }
                        else
                        {
                            if (t == UnitType.TownHall)
                                stillSpaceForNewBases = false; // do not try and rebuild a townhall
                            lacksSpace = true;
                        }

                    }
                    else // the harvester is stuck and creates a lot of lag, go clear space ; temporary solution
                        lacksSpace = true;
                }
            }
            return null;
        }

        // Little helper to make code less lengthy
        private bool ConditionsHelper(UnitType ut, int number, List<UnitType> banned)
        {
            return (Game1.map.players[owner].PlayerHas(ut) < number) && !banned.Contains(ut);
        }

        /** 
         * Returns the next unit to be built by following a build order, taking into account the pending construction orders
         * We can force a certain type of unit not to be returned by passing a list of unitype to be banned as arguement
         * 
         */
        private UnitType NextToBeBuilt(bool weWantUnit, List<UnitType> banned = null) // build order depending on if we need a unit or not
        {
            UnitType b = UnitType.None;
            if (banned == null)
                banned = new List<UnitType>();
            if (!weWantUnit) // if it is a building we need, take into account pending construction orders
                foreach (Unit u in pendingConstructionOrders)
                    banned.Add(u.type);

            if (weWantUnit) // returns the next unit to be built
            {
                #region units
                if (type == AIType.Ground1)
                {
                    #region ground attack units build cycle
                    #region Tier 1
                    if (!(Game1.map.players[owner].PlayerHas(UnitType.StrongHold, UnitType.Fortress) > 0))
                    {
                        if (ConditionsHelper(UnitType.Paysan, 10, banned))
                            b = UnitType.Paysan;
                        else if (ConditionsHelper(UnitType.Fantassin, 3, banned))  // build little force
                            b = UnitType.Fantassin;
                        else if (ConditionsHelper(UnitType.Archer, 2, banned))  // build little force
                            b = UnitType.Archer;
                        else if (ConditionsHelper(UnitType.Fantassin, 5, banned))  // build little force
                            b = UnitType.Fantassin;
                        else if (ConditionsHelper(UnitType.Catapulte, 1, banned))  // build little force
                            b = UnitType.Catapulte;
                    }
                    #endregion
                    #region Tier 2
                    else if ((Game1.map.players[owner].PlayerHas(UnitType.StrongHold) > 0))
                    {
                        if (ConditionsHelper(UnitType.Paysan, 12, banned))
                            b = UnitType.Paysan;
                        else if (ConditionsHelper(UnitType.Archer, 4, banned))
                            b = UnitType.Archer;
                        else if (ConditionsHelper(UnitType.Chevalier, 8, banned))
                            b = UnitType.Chevalier;
                        else if (ConditionsHelper(UnitType.Catapulte, 2, banned))
                            b = UnitType.Catapulte;
                    }
                    #endregion
                    #region Tier 3
                    else if ((Game1.map.players[owner].PlayerHas(UnitType.Fortress) > 0))
                    {
                        if (ConditionsHelper(UnitType.Paysan, 15, banned))
                            b = UnitType.Paysan;
                        else if (ConditionsHelper(UnitType.Archer, 6, banned))
                            b = UnitType.Archer;
                        else if (ConditionsHelper(UnitType.Chevalier, 15, banned))
                            b = UnitType.Chevalier;
                        else if ((ConditionsHelper(UnitType.Griffon, 5, banned)))
                            b = UnitType.Griffon;
                    }
                    #endregion
                    #endregion
                }
                else if (type == AIType.Air)
                {
                    #region air attack units
                    // respect a build order
                    if (!(numberOfUnits[(int)UnitType.StrongHold] > 0) && !(numberOfUnits[(int)UnitType.Fortress] > 0))
                    {
                        if (ConditionsHelper(UnitType.Paysan, 10, banned))
                            b = UnitType.Paysan;
                        else if (ConditionsHelper(UnitType.Fantassin, 3, banned))  // build little force
                            b = UnitType.Fantassin;

                        else if (ConditionsHelper(UnitType.Archer, 3, banned))  // build little force
                            b = UnitType.Archer;
                    }
                    else if ((numberOfUnits[(int)UnitType.Fortress] > 0))
                    {
                        if (ConditionsHelper(UnitType.Paysan, 15, banned))
                            b = UnitType.Paysan;
                        else if (ConditionsHelper(UnitType.Archer, 4, banned))  // build little force
                            b = UnitType.Archer;
                        else
                            b = UnitType.Griffon;
                    }
                    #endregion
                }
                else
                {
                    #region Water AIA
                    if (ConditionsHelper(UnitType.Paysan, 10, banned))
                        b = UnitType.Paysan;
                    else if (ConditionsHelper(UnitType.Archer, 5, banned))  // build little force against air assaults
                        b = UnitType.Archer;

                    else if (ConditionsHelper(UnitType.Petrolier, 5, banned))  // build little force
                        b =  UnitType.Petrolier;

                    int randoum = Game1.statics.ra.Next(3);

                    if (ConditionsHelper(UnitType.Sousmarin, 3, banned))
                    {
                        if (randoum == 1)
                            b = UnitType.Destroyer;
                        else
                            b = UnitType.Sousmarin;
                    }
                    if (ConditionsHelper(UnitType.Battleship, 3, banned))
                    {
                        b = UnitType.Battleship;
                    }
                    #endregion
                }
                #endregion
            }
            else // Return the next BULDING to be built
            {
                #region Buildings
                // check for basic buildings first (farms...)
                if (getBasicBuildings() != UnitType.None)
                    return getBasicBuildings();

                if (type == AIType.Ground1)
                {
                    #region ground attack units build cycle
                    #region Tier 1
                    if (!(Game1.map.players[owner].PlayerHas(UnitType.StrongHold, UnitType.Fortress) > 0))
                    {
                        // if too many resources, build another military base
                        if (availableGold > 4000 && availableWood > 4000 && ConditionsHelper(UnitType.MilitaryBase, 2, banned))
                            b = UnitType.MilitaryBase;
                        else if (ConditionsHelper(UnitType.LumberMill, 1, banned))
                            b = UnitType.LumberMill;

                        else if (ConditionsHelper(UnitType.Forge, 1, banned))
                            b = UnitType.Forge;
                        else if (ConditionsHelper(UnitType.StrongHold, 1, banned))
                            b = UnitType.StrongHold; // up tier
                    }
                    #endregion

                    #region Tier 2
                    else if ((Game1.map.players[owner].PlayerHas(UnitType.StrongHold) > 0))
                    {
                        if (ConditionsHelper(UnitType.Forge, 1, banned))
                            b = UnitType.Forge;
                        else if (ConditionsHelper(UnitType.Stable, 1, banned))
                            b = UnitType.Stable;
                        else if (ConditionsHelper(UnitType.MilitaryBase, 2, banned))
                            b = UnitType.MilitaryBase;
                        else if (ConditionsHelper(UnitType.Fortress, 1, banned))
                            b = UnitType.Fortress; // up tier
                    }
                    #region Tier 3
                    else if ((Game1.map.players[owner].PlayerHas(UnitType.Fortress) > 0))
                    {
                        if (ConditionsHelper(UnitType.Forge, 1, banned))
                            b = UnitType.Forge;
                        else if (ConditionsHelper(UnitType.Stable, 1, banned))
                            b = UnitType.Stable;
                        else if (ConditionsHelper(UnitType.Church, 1, banned))
                            b = UnitType.Church;
                        else if (ConditionsHelper(UnitType.MilitaryBase, 3, banned))
                            b = UnitType.MilitaryBase;
                        else if (ConditionsHelper(UnitType.MageTower, 1, banned))
                            b = UnitType.MageTower;
                        else if (ConditionsHelper(UnitType.GryffinTower, 1, banned))
                            b = UnitType.GryffinTower;
                        else if (ConditionsHelper(UnitType.MilitaryBase, 4, banned))
                            b = UnitType.MilitaryBase;
                    }
                    #endregion
                    #endregion


                    return b;
                    #endregion
                }
                else if (type == AIType.Air)
                {
                    #region air attack AIA


                    #region Tier 1
                    if (!(Game1.map.players[owner].PlayerHas(UnitType.StrongHold, UnitType.Fortress) > 0))
                    {
                        // if too many resources, build another military base
                        if (availableGold > 4000 && availableWood > 4000 && ConditionsHelper(UnitType.MilitaryBase, 2, banned))
                            b = UnitType.MilitaryBase;
                        else if (ConditionsHelper(UnitType.LumberMill, 1, banned))
                            b = UnitType.LumberMill;

                        else if (ConditionsHelper(UnitType.Forge, 1, banned))
                            b = UnitType.Forge;
                        else if (ConditionsHelper(UnitType.StrongHold, 2, banned))
                            b = UnitType.StrongHold; // up tier
                    }
                    #endregion

                    #region Tier 2
                    else if ((Game1.map.players[owner].PlayerHas(UnitType.StrongHold) > 0))
                    {
                        if (ConditionsHelper(UnitType.Forge, 1, banned))
                            b = UnitType.Forge;
                        else if (ConditionsHelper(UnitType.Stable, 1, banned))
                            b = UnitType.Stable;

                        else if (ConditionsHelper(UnitType.Fortress, 1, banned))
                            b = UnitType.Fortress; // up tier
                    }
                    #endregion
                    #region Tier 3
                    else if ((Game1.map.players[owner].PlayerHas(UnitType.Fortress) > 0))
                    {
                        if (ConditionsHelper(UnitType.Forge, 1, banned))
                            b = UnitType.Forge;
                        else if (ConditionsHelper(UnitType.Stable, 1, banned))
                            b = UnitType.Stable;
                        else if (ConditionsHelper(UnitType.GryffinTower, 3, banned))
                            b = UnitType.GryffinTower;
                    }
                    #endregion
                    #endregion
                }
                else // water
                {
                    #region water IA
                    if (ConditionsHelper(UnitType.NavalChantier, 1, banned))
                        b = UnitType.NavalChantier;
                    else if (ConditionsHelper(UnitType.NavalPlatform, 1, banned))
                        b = UnitType.NavalPlatform;
                    else if (ConditionsHelper(UnitType.Raffinery, 1, banned))
                        b = UnitType.Raffinery;
                    else if (ConditionsHelper(UnitType.StrongHold, 1, banned))
                        b = UnitType.StrongHold;
                    else if (ConditionsHelper(UnitType.GoblinWorkBench, 1, banned))
                        b = UnitType.GoblinWorkBench;
                    else if (ConditionsHelper(UnitType.Foundry, 1, banned))
                        b = UnitType.Foundry;
                    else if (ConditionsHelper(UnitType.NavalChantier, 2, banned))
                        b = UnitType.NavalChantier;
                    #endregion
                }


                #endregion
            }
            return b;
        }

        // basic means town hall, farms, lumbermill, military base
        private UnitType getBasicBuildings()
        {

            if (((numberOfUnits[(int) UnitType.StrongHold] == 0) 
                && (numberOfUnits[(int) UnitType.Fortress] == 0) 
                && (numberOfUnits[(int) UnitType.TownHall] == 0))
                /*|| (currentDistanceTownHallToGMine > (townHallToMineAcceptableDistance) && goldminepos != Game1.statics.ImpossiblePos && stillSpaceForNewBases)*/) // too far away from mine or no townhall
                return UnitType.TownHall;

            if (Game1.map.players[owner].usedFood >= potentialFood)
                return UnitType.Farm;

            if ((numberOfUnits[(int) UnitType.MilitaryBase] ) == 0)
                return UnitType.MilitaryBase;

            if (!(numberOfUnits[(int) UnitType.LumberMill] > 0))
                return UnitType.LumberMill;

            return UnitType.None;
        }

        // compute number of buildings from each type
        private void determineUpgrades(List<Unit> playerOtherUnits, List<Building> playerBuildings)
        {


                foreach (Building b2 in playerBuildings)
                {
                    if (b2.type == UnitType.NavalPlatform)
                    {
                        oilPatchPos = b2.position;
                    }

                    if (b2.type == UnitType.LumberMill)
                    {
                        if (b2.isBuilt() && b2.sa == null && Game1.map.players[owner].playerAmeliorations.getDamageAmelioration(UnitType.Archer) < UnitProperties.ARCHER_MAX_AMELIORATION_DAMAGE)
                            b2.Ameliorate(0);
                    }
                    if (b2.type == UnitType.Forge)
                    {
                        if (b2.isBuilt() && b2.sa == null && Game1.map.players[owner].playerAmeliorations.getDamageAmelioration(UnitType.Fantassin) < UnitProperties.FANTASSIN_MAX_AMELIORATION_DAMAGE)
                            b2.Ameliorate(0);
                        if (b2.isBuilt() && b2.sa == null && Game1.map.players[owner].playerAmeliorations.getArmorAmelioration(UnitType.Fantassin) < UnitProperties.FANTASSIN_MAX_AMELIORATION_ARMOR)
                            b2.Ameliorate(1);
                    }
                    if (b2.type == UnitType.Church)
                    {
                        if (b2.isBuilt() && b2.sa == null && !Game1.map.players[owner].playerAmeliorations.hasPaladin)
                            b2.Ameliorate(0);
                    }
                }
        }

        /** 
         * For AIA to find a suitable building place
         * Starts from position position and explores all nearby tiles until then side of the map
         * */
        public Tuple<Harvester, Vector2> findGoodBuildingSpot(UnitType t, Vector2 position, List<Harvester> harvesters, bool hasTownHall = false) // we should center around the main gold mine / the townhall
        {
            HashSet<int> walkableregions = new HashSet<int>();

            Vector2 shittyPosition = Game1.statics.ImpossiblePos;
            Vector2 goodPosition = Game1.statics.ImpossiblePos;

            harvesters.RemoveAll(x => !isAvailableHarvester(x, t != UnitType.NavalPlatform));
            harvesters.RemoveAll(x => (Game1.statics.threading.IsLookingForPath(x)));
            List<Harvester> onGoingPathFindingRequest = new List<Harvester>();

            if (harvesters.Count == 0)
                return new Tuple<Harvester, Vector2>(null, Game1.statics.ImpossiblePos);
            foreach (Harvester v in harvesters)
            {
                    walkableregions.Add(Game1.map.mapAreas.stGridsGround[(int)Math.Round(v.position.X), (int)Math.Round(v.position.Y)]);
            }
            walkableregions.Remove(0);

            Tuple<Harvester, Vector2> goodPos = new Tuple<Harvester, Vector2>(null, Game1.statics.ImpossiblePos);
            Unit selectedBuilding = Game1.map.CreateUnit(t, position, owner);
            bool[,] visited = new bool[Map.mapsize, Map.mapsize];
            for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                    if (!Game1.statics.basicSailingBuildings.Contains(t) &&  // if there is a path to tile / tile is same region
                        t != UnitType.NavalPlatform && // if we are building 
                        !walkableregions.Contains(Game1.map.mapAreas.stGridsGround[i, j]))
                    {
                        visited[i, j] = true;
                    }
                    else
                        visited[i, j] = false;


            if ((int)t >= Unit.littlebuildingindex)
            {
                /**  .............
                 *  .. 2 2 2 2 2..
                 *  .. 2 1 1 1 2 ..
                 *  .. 2 1 0 1 2 ..
                 *  .. 2 1 1 1 2 ..
                 *  .. 2 2 2 2 2 ..
                 *   .............
                 *   increase by one tile as long as not found and marks as visited so we only test the new "frame" and not retest
                */

                for (int i = 0; i < Map.mapsize; i++)
                {
                    for (int j = -i + (int)position.X; j < i + (int) position.X; j++)
                    {
                        for (int k = -i + (int) position.Y; k < i + (int) position.Y; k++)
                        {
                            if (harvesters.Count == 0)
                                return goodPos;
                            if (Map.Inbounds(j, k) && !visited[j, k])
                            {
                                visited[j, k] = true;
                                if (Harvester.IsCorrectBuildPosition(new Vector2(j, k), selectedBuilding, harvesters.First()))
                                {
                                    bool notGoodPlace = false; // ias tend to build at stupid places, so they get stuck.. avoid this
                                    bool notIdealPlace = false;
                                    if (t != UnitType.NavalChantier && t != UnitType.NavalPlatform && t != UnitType.Raffinery && t != UnitType.Foundry)
                                    {
                                        if (Vector2.Distance(new Vector2(goldminepos.X + 1, goldminepos.Y + 1), new Vector2(j, k)) <= 7)
                                        {
                                            notGoodPlace = true;
                                        }
                                        if (owner != Game1.currentPLayer && selectedBuilding.type == UnitType.TownHall) // if it is an AI trying to build secondary townhall far away from the gold mine, prevent it
                                            if (Vector2.Distance(new Vector2(goldminepos.X, goldminepos.Y), new Vector2(j, k)) > this.townHallToMineAcceptableDistance)
                                            {
                                                if ((numberOfUnits[(int)UnitType.TownHall] > 0) || (numberOfUnits[(int)UnitType.Fortress] > 0) || (numberOfUnits[(int)UnitType.StrongHold] > 0))
                                                    notGoodPlace = true;
                                                else
                                                    notIdealPlace = true;
                                            }
                                        if ((numberOfUnits[(int)UnitType.TownHall] > 0)) // check not too near town hall
                                            if (Vector2.Distance(new Vector2(TownHallPosition.X + 1, TownHallPosition.Y + 1), new Vector2(j, k)) <= 5)
                                            {
                                                notIdealPlace = true;
                                            }
                                        if ((int)t >= Unit.mediumbuildingindex && (int)t < Unit.largebuildingindex)
                                        {
                                            // let a bit of space for medium buildings
                                            if (Map.Inbounds(j - 1, k))
                                                if (Game1.map.groundTraversableMapTiles[j - 1, k] == false)
                                                    notIdealPlace = true;
                                            if (Map.Inbounds(j - 1, k - 2))
                                                if (Game1.map.groundTraversableMapTiles[j - 1, k - 2] == false)
                                                    notIdealPlace = true;
                                            if (Map.Inbounds(j + 3, k))
                                                if (Game1.map.groundTraversableMapTiles[j + 3, k] == false)
                                                    notIdealPlace = true;
                                            if (Map.Inbounds(j + 3, k + 2))
                                                if (Game1.map.groundTraversableMapTiles[j + 3, k + 2] == false)
                                                    notIdealPlace = true;
                                            if (Map.Inbounds(j, k + 3))
                                                if (Game1.map.groundTraversableMapTiles[j, k + 3] == false)
                                                    notIdealPlace = true;
                                            if (Map.Inbounds(j + 2, k + 3))
                                                if (Game1.map.groundTraversableMapTiles[j + 2, k + 3] == false)
                                                    notIdealPlace = true;
                                            if (Map.Inbounds(j, k - 1))
                                                if (Game1.map.groundTraversableMapTiles[j, k - 1] == false)
                                                    notIdealPlace = true;
                                            if (Map.Inbounds(j + 2, k - 1))
                                                if (Game1.map.groundTraversableMapTiles[j + 2, k - 1] == false)
                                                    notIdealPlace = true;
                                        }
                                    }
                                    else
                                    {
                                        if (Vector2.Distance(oilPatchPos, new Vector2(j, k)) <= 7)
                                        {
                                            notGoodPlace = true;
                                        }
                                    }

                                    if (!notGoodPlace)
                                    {
                                        if (!notIdealPlace) // it is an ideal / good position, just go with it and try to return it
                                        {
                                            List<Vector2> tempPosWalkable = new List<Vector2>();
                                            if (selectedBuilding.type == UnitType.NavalChantier || selectedBuilding.type == UnitType.Raffinery || selectedBuilding.type == UnitType.Foundry)
                                                for (int v = -2; v < 2; v++) // this I'll have to debug someday
                                                    for (int w = -2; w < 2; w++)
                                                        if (Map.Inbounds(j + 1 + v, k + 1 + w))
                                                            tempPosWalkable.Add(new Vector2(j + 1 + v, k + 1 + w));

                                            List<Harvester> onGoingPathFindingHarvesters = new List<Harvester>();
                                            foreach (Harvester tmph in harvesters)
                                            {
                                                if (Game1.map.mapAreas.checkPosReachable(tmph.position, new Vector2(j, k), (!tmph.sailing && !tmph.flying), tmph.flying, tmph.sailing))
                                                {
                                                    if (tmph.disabled <= 0 && tmph.IsDoingNothing())
                                                    {
                                                        List<GridPos> path = Game1.statics.threading.pathFindingRequest(tmph, new Vector2(j, k), Game1.map, tempPosWalkable);
                                                        if (path != null &&
                                                            path.Count > 0) // Means the request is finished
                                                        {
                                                            return new Tuple<Harvester, Vector2>(tmph, new Vector2(j, k));
                                                        }
                                                        else if (path == null) // This harvester already has a pathfinding request, remove it from the list
                                                            onGoingPathFindingHarvesters.Add(tmph);
                                                        // If the request gave an empty gridpos, continue the big loop to test another position
                                                    }
                                                }
                                            }
                                            harvesters.RemoveAll(h => onGoingPathFindingHarvesters.Contains(h)); // Don't use harvesters with pathfinding requests ongoing anymore
                                        }
                                        else // register the not ideal place if it is closer to town hall than the previous one
                                        {
                                            if (Vector2.Distance(TownHallPosition, new Vector2(j, k)) < Vector2.Distance(goldminepos, shittyPosition) || shittyPosition == Game1.statics.ImpossiblePos)
                                            {
                                                foreach (Harvester tmph in harvesters)
                                                {
                                                    if (Game1.map.mapAreas.checkPosReachable(tmph.position, new Vector2(j, k), (!tmph.sailing && !tmph.flying), tmph.flying, tmph.sailing))
                                                    {
                                                        shittyPosition = new Vector2(j, k);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                List<Vector2> tempPosWalkabl = new List<Vector2>();
                    if (selectedBuilding.type == UnitType.NavalChantier || selectedBuilding.type == UnitType.Raffinery || selectedBuilding.type == UnitType.Foundry)
                        for (int v = -2; v < 2; v++) // this I'll have to debug someday
                            for (int w = -2; w < 2; w++)
                                if (Map.Inbounds((int) shittyPosition.X + 1 + v, (int) shittyPosition.Y + 1 + w))
                                    tempPosWalkabl.Add(new Vector2((int) shittyPosition.X + 1 + v, (int) shittyPosition.Y + 1 + w));

                    foreach (Harvester tmph in harvesters)
                    {
                        if (Game1.map.mapAreas.checkPosReachable(tmph.position, shittyPosition, (!tmph.sailing && !tmph.flying), tmph.flying, tmph.sailing))
                        {
                            if (tmph.disabled <= 0 && tmph.IsDoingNothing())
                            {
                                List<GridPos> path = Game1.statics.threading.pathFindingRequest(tmph, shittyPosition, Game1.map, tempPosWalkabl);
                                if (path != null && path.Count > 0)
                                {
                                    return new Tuple<Harvester, Vector2>(tmph, shittyPosition);
                                }
                            }
                        }
                    }
                }
            
            return goodPos;

        }


        // used for AIA only
        public static bool attackMove(Unit u, GameTime gameTime)
        {
            bool reachable = false;

            if (u.target == null && u.path.Count == 0)
            {

                foreach (Unit u2 in Game1.map.units)
                {
                    if (u2.owner == Game1.currentPLayer)
                    {
                        List<Vector2> buildpositions = new List<Vector2>();
                        if ((int)u2.type >= Unit.littlebuildingindex) // so we dont get stuck because the position is on topleft
                        {
                            buildpositions = Utils.Utils.getAllPositions((int)u2.spriteSize.X / 32, (int)u2.spriteSize.Y / 32, u2.position); // let us walk on the building itself
                        }
                        if (Game1.map.mapAreas.checkPosReachable(u.position, u2.position, (!u.sailing && !u.flying), u.flying, u.sailing))
                            reachable = true;
                        List<GridPos> temp = Game1.statics.threading.SafePathFindingRequest(u, u2.position, Game1.map, buildpositions);
                        if (temp.Count > 0) // there is a way
                        {
                            u.target = u2;
                            u.path = temp;
                            u.destination = u2.position;
                            reachable = true;
                            break;
                        }

                    }
                }
            }
            else if (u.onRangeForAttack())
            {
                u.Attack(gameTime);
                reachable = true;
            }
            else if (u.path.Count == 0)
            {
                u.Move(u.target.position, Game1.map, true);
                reachable = true;
            }

            return reachable;
        }

        /* 1) Calculate every distance
         * 2) Make two lists : one of units, the other of buildings
         * 3) Order by distance these lists
         * 4) Check the units for path available, stop on the nearest unit which can be reached
         * 5) If no unit can be reached, try buildings
         * 6) Return null if nothing found
         * */
        public static void findNearbyTarget(Unit thisunit)
        {
            Dictionary<float, Unit> nearbyTargets = new Dictionary<float, Unit>();
            Dictionary<float, Unit> nearbyBuilding = new Dictionary<float, Unit>();
            thisunit.target = null;

            foreach (Unit u in Game1.map.units)
            {
                if (u.owner != thisunit.owner && thisunit.type != UnitType.Paysan && 
                    (u.type != UnitType.Sousmarin || (u.type == UnitType.Sousmarin && ((Submarine)u).canBeSeenBy(thisunit.owner))))
                {
                    if (thisunit.owner == Game1.currentPLayer || (Game1.map.players[thisunit.owner].network == false && u.owner == Game1.currentPLayer))// its an ai
                        if (Vector2.Distance(u.position, thisunit.position) <= AggroRange ||
                            thisunit.onRangeToAttackTarget(u))
                        {
                            if ((int)u.type >= Unit.littlebuildingindex && u.type != UnitType.BallistaTower && u.type != UnitType.CannonTower) // try to not target buildings if possible
                            {
                                nearbyBuilding.Remove(Vector2.Distance(u.position, thisunit.position));
                                nearbyBuilding.Add(Vector2.Distance(u.position, thisunit.position), u);
                            }
                            else if (thisunit.CanAttackAir && u.flying || thisunit.CanAttackWater && u.sailing || thisunit.CanAttackGround && (!u.sailing && !u.flying))
                            {
                                // if i'm ground and target is flying over unaccessible land, dont target it
                                if (thisunit.onRangeToAttackTarget(u) ||
                                    !(!thisunit.flying && !thisunit.sailing && u.flying && !Game1.map.groundTraversableMapTiles[(int)u.position.X, (int)u.position.Y]))
                                {
                                    if (!(u.type == UnitType.Paysan && ((Harvester)u).ShouldntGetHarmed())) // if its not a harvester untargettable
                                    {
                                        nearbyTargets.Remove(Vector2.Distance(u.position, thisunit.position));
                                        nearbyTargets.Add(Vector2.Distance(u.position, thisunit.position), u);
                                    }
                                }
                            }
                        }
                }
            }


            var items = from pair in nearbyTargets
                        orderby pair.Key ascending
                        select pair;
            foreach (KeyValuePair<float, Unit> u in items)
            {
                if (thisunit.onRangeToAttackTarget(u.Value) || (thisunit.owner == Game1.currentPLayer && Game1.statics.threading.SafePathFindingRequest(thisunit, u.Value.position, Game1.map).Count > 0))
                {
                    thisunit.target = u.Value;
                    break;
                }
                else if (thisunit.owner != Game1.currentPLayer &&  Game1.statics.threading.SafePathFindingRequest(thisunit, u.Value.position, Game1.map, Game1.map.playerZones.getPositions()).Count > 0)
                {
                    thisunit.target = u.Value;
                    break;
                }
            }


            if (thisunit.target == null)
            {
                items = from pair in nearbyBuilding
                        orderby pair.Key ascending
                        select pair;

                foreach (KeyValuePair<float, Unit> u in items)
                {
                    List<Vector2> buildpositions =  Utils.Utils.getAllPositions((int) u.Value.spriteSize.X / 32, (int) u.Value.spriteSize.Y / 32, u.Value.position); // let us walk on the building itself to calculate path

                    if (thisunit.onRangeToAttackTarget(u.Value) || thisunit.owner == Game1.currentPLayer &&  Game1.statics.threading.SafePathFindingRequest(thisunit, u.Value.position, Game1.map, buildpositions).Count > 0)
                    {
                        thisunit.target = u.Value;
                        break;
                    }
                    else if (thisunit.owner != Game1.currentPLayer && Game1.statics.threading.SafePathFindingRequest(thisunit, u.Value.position, Game1.map, Game1.map.playerZones.getPositions()).Count > 0)
                    {
                        thisunit.target = u.Value;
                        break;
                    }
                }
            }
        }


        public bool isAvailableHarvester(Harvester har, bool weWantAPeasant)
        {
            if (((har.type == UnitType.Paysan && weWantAPeasant) || (har.type == UnitType.Petrolier && !weWantAPeasant)) &&
                har.timeharvesting == 0 && har.inbuilding == null && har.disabled <= 0 && har.selectedBuilding == null)
            {
                return true;
            }

            return false;
        }

        private void updateDistanceTownHallToMine(List<Building> myOwnBuildings)
        {
            if (goldminepos != Game1.statics.ImpossiblePos)
            {
                foreach (Building b in myOwnBuildings)
                {
                    if (b.type == UnitType.TownHall || b.type == UnitType.StrongHold || b.type == UnitType.Fortress)
                        if ((int) Vector2.Distance(b.position, goldminepos) < currentDistanceTownHallToGMine)
                            currentDistanceTownHallToGMine = (int) Vector2.Distance(b.position, goldminepos);
                }
            }
        }
             

    }
}
