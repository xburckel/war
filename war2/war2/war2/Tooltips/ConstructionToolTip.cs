﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using war2.Menus;
using war2.Utils;

namespace war2.Tooltips
{
    public class ConstructionToolTip
    {
        private bool mustDraw_;
        private UnitType mustDrawWhat_;
        public ConstructionToolTip()
        {
            mustDraw_ = false;
            mustDrawWhat_ = UnitType.None;
        }

        public void Update()
        {
            mustDrawWhat_ = UnitType.None;
            mustDraw_ = false;
            bool buildPanel = Game1.map.players[Game1.currentPLayer].buildPanelToDraw;
            bool advPanel = Game1.map.players[Game1.currentPLayer].advancedbuildPanelToDraw;

            if (advPanel || buildPanel)
            {
                UnitType mouseOn = LeftPanelUtils.ToBuilding(LeftPanelUtils.MouseOnLeftPanelIcon(Mouse.GetState()), advPanel);
                if (mouseOn != UnitType.None && Game1.map.players[Game1.currentPLayer].playerTechs.hasTech(mouseOn))
                {
                    mustDraw_ = true;
                    mustDrawWhat_ = mouseOn;
                }
            }
            else
                HandleBuildUnitsTooltip();
        }

        private string GetWoodAndGoldAndOilCost()
        {
            return "Gold : " +
                        (((int)mustDrawWhat_ >= Unit.littlebuildingindex) ? UnitProperties.getBuildingProperties(mustDrawWhat_)[(int)BuildingProperties.GOLDCOST].ToString()
                        : UnitProperties.getUnitProperties(mustDrawWhat_)[(int)Properties.GOLDCOST].ToString())
                        +
                        ", Wood : " +
                        (((int)mustDrawWhat_ >= Unit.littlebuildingindex) ? UnitProperties.getBuildingProperties(mustDrawWhat_)[(int)BuildingProperties.WOODCOST].ToString()
                        : UnitProperties.getUnitProperties(mustDrawWhat_)[(int)Properties.WOODCOST].ToString())
                        + ", Oil : " +
                        (((int)mustDrawWhat_ >= Unit.littlebuildingindex) ? UnitProperties.getBuildingProperties(mustDrawWhat_)[(int)BuildingProperties.OILCOST].ToString()
                        : UnitProperties.getUnitProperties(mustDrawWhat_)[(int)Properties.OILCOST].ToString());
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (mustDraw_)
            {
                string theChar = Shortcuts.GetShortCutKey(mustDrawWhat_).ToString().ToUpper();

                string toDisplay = mustDrawWhat_.ToString().ToUpper();
                if (theChar != "NONE") // if there is a shortcut, make it appear on tooltip
                {
                    toDisplay = StringsUtils.ReplaceFirstOccurrence(toDisplay, theChar, "(" + "fdsfdssfdfds)");
                    toDisplay = toDisplay.ToLower();
                    toDisplay = toDisplay.Replace("fdsfdssfdfds)", theChar.ToUpper() + ")");
                }
                toDisplay = "Build " + toDisplay;
                spriteBatch.Begin();
                spriteBatch.DrawString(Game1.statics.font, toDisplay, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 8,
                    0.90f * Game1.graphics.GraphicsDevice.Viewport.Height), Color.White);
                spriteBatch.DrawString(Game1.statics.font, GetWoodAndGoldAndOilCost(), new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 8,
                        0.90f * Game1.graphics.GraphicsDevice.Viewport.Height + 15), Color.Gold);

                spriteBatch.End();


            }
        }

        private void HandleBuildUnitsTooltip()
        {
            if (Game1.map.selected.Count == 1 && (int)Game1.map.selected.First().type >= Unit.littlebuildingindex) // handle unit construction logic for building
            {
                Building b = (Building)Game1.map.selected.First();
                UnitType ut = UnitType.None;


                if (b.isBuilt())
                {
                    if (Game1.mouse.buildingFirstUnit(Game1.statics.CreateUnitIconCoordinate, Mouse.GetState()))
                    {
                        if (b.type == UnitType.TownHall || b.type == UnitType.Fortress || b.type == UnitType.StrongHold)
                            ut = UnitType.Paysan;
                        if (b.type == UnitType.MilitaryBase)
                            ut = UnitType.Fantassin;
                        if (b.type == UnitType.GoblinWorkBench)
                            ut = UnitType.MachineVolante;
                        if (b.type == UnitType.GryffinTower)
                            ut = UnitType.Griffon;
                        if (b.type == UnitType.MageTower)
                            ut = UnitType.Mage;
                        if (b.type == UnitType.NavalChantier)
                            ut = UnitType.Petrolier;
                    }
                    if (Game1.mouse.buildingSecondUnit(Game1.statics.CreateUnitIconCoordinate, Mouse.GetState()))
                    {
                        if (b.type == UnitType.MilitaryBase)
                            ut = UnitType.Archer;
                        if (b.type == UnitType.GoblinWorkBench)
                            ut = UnitType.Nains;
                        if (b.type == UnitType.NavalChantier)
                            ut = UnitType.Destroyer;
                    }
                    if (Game1.mouse.buildingThirdUnit(Game1.statics.CreateUnitIconCoordinate, Mouse.GetState()))
                    {
                        if (b.type == UnitType.MilitaryBase)
                            ut = UnitType.Catapulte;
                        if (b.type == UnitType.NavalChantier)
                            ut = UnitType.Sousmarin;
                    }
                    if (Game1.mouse.buildingFourthUnit(Game1.statics.CreateUnitIconCoordinate, Mouse.GetState()))
                    {
                        if (b.type == UnitType.MilitaryBase)
                            ut = UnitType.Chevalier;
                        if (b.type == UnitType.NavalChantier)
                            ut = UnitType.Battleship;
                    }
                    if (Game1.mouse.buildingFifthUnit(Game1.statics.CreateUnitIconCoordinate, Mouse.GetState()))
                    {
                        if (b.type == UnitType.NavalChantier)
                            ut = UnitType.Transport;
                    }
                }
                if (ut != UnitType.None && Game1.map.players[Game1.currentPLayer].playerTechs.hasTech(ut))
                {
                    this.mustDraw_ = true;
                    this.mustDrawWhat_ = ut;
                }

            }
        }
    }
}
