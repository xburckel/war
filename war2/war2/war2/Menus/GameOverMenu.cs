﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.Menus
{
    public class GameOverMenu
    {
        Song _BGMUSIC;
        MouseState prevMouseState;
        Texture2D menuTexture;
        Texture2D previousTexture = Game1.content.Load<Texture2D>("MainMenus/previous");
        bool isOrc;
        bool itsawin;

        public GameOverMenu(bool isOrc, bool itsawin)
        {

            prevMouseState = Mouse.GetState();
            this.isOrc = isOrc;
            this.itsawin = itsawin;

            if (!itsawin)
                _BGMUSIC = Game1.content.Load<Song>("MainMenus/lose");
            else if (itsawin && !isOrc)
                _BGMUSIC = Game1.content.Load<Song>("MainMenus/HumanVictory");
            else if (itsawin && isOrc)
                _BGMUSIC = Game1.content.Load<Song>("MainMenus/OrcVictory");


            if (!this.isOrc && itsawin)
                menuTexture = Game1.content.Load<Texture2D> ("MainMenus/allianceMenu");
            if (!this.isOrc && !itsawin)
                menuTexture = Game1.content.Load<Texture2D>("MainMenus/allianceMenu");
            if (this.isOrc && itsawin)
                menuTexture = Game1.content.Load<Texture2D>("MainMenus/hordedefeat");
            if (this.isOrc && !itsawin)
                menuTexture = Game1.content.Load<Texture2D>("MainMenus/hordedefeat");

            Game1.soundEngine.StopPlaying();

            MediaPlayer.Play(_BGMUSIC);
        }

        public void Draw(SpriteBatch spbatch)
        {
            spbatch.Begin();
            spbatch.Draw(menuTexture, new Rectangle(0, 0, Game1.graphics.GraphicsDevice.Viewport.Width, Game1.graphics.GraphicsDevice.Viewport.Height), Color.White);
            spbatch.Draw(previousTexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 4 * 3, Game1.graphics.GraphicsDevice.Viewport.Height / 4 * 3),
                new Rectangle(0, 0, 209, 28), Color.White);

            spbatch.End();

        }

        public void Update(GameTime gameTime)
        {
            MouseState ms = Mouse.GetState();
            KeyboardState kstate = Keyboard.GetState();

            if (ms.LeftButton == ButtonState.Released && prevMouseState.LeftButton == ButtonState.Pressed || kstate.IsKeyDown(Keys.P))
            {
                if (kstate.IsKeyDown(Keys.P))
                {
                    Game1.statics.govermenu = null;
                    Game1.isInMainMenu = true;
                    MediaPlayer.Stop();
                }
                else if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 4 * 3 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 4 * 3 + 209
                    && ms.Y > Game1.graphics.GraphicsDevice.Viewport.Height / 4 * 3 && ms.Y < Game1.graphics.GraphicsDevice.Viewport.Height / 4 * 3 + 28)
                {
                    Game1.statics.govermenu = null;
                    Game1.isInMainMenu = true;
                    MediaPlayer.Stop();
                }
            }

            prevMouseState = ms;

        }



    }
}
