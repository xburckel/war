﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2
{
    [Serializable]
    class Shortcuts
    {
        public static Dictionary<string, Keys> keys;

        public static Keys BuildPanel= Keys.B;
        public static Keys  AdvancedBuildPanel= Keys.A;

        /* Buildings */
        private static Keys  Farm= Keys.F ;
        private static Keys  TownHall= Keys.T ;
        private static Keys  MilitaryBase= Keys.M ;
        private static Keys  LumberMill= Keys.L ;
        private static Keys  Forge= Keys.O ;
        private static Keys  Tower= Keys.W ;
        private static Keys  BallistaTower= Keys.B ;
        private static Keys   CannonTower= Keys.C ;
        private static Keys  NavalChantier= Keys.N ;
        private static Keys  Foundry= Keys.D ;
        private static Keys  Raffinery= Keys.Y ;
        private static Keys  Stronghold= Keys.S ;
        private static Keys  Fortress= Keys.F ;
        private static Keys  Stable= Keys.S ;
        private static Keys  GoblinWorkBench = Keys.G;
        private static Keys  Church= Keys.U ;
        private static Keys MageTower= Keys.M ;
        private static Keys  GryffinTower= Keys.R ;
        private static Keys  NavalPlatform = Keys.N;
        /* Units : TODO
        private static Keys Paysan = Keys.P;
        private static Keys Fantassin = Keys.A;
         * */

        static Shortcuts()
        {
          keys = new Dictionary<string, Keys>();

          Type type = typeof(Shortcuts); // MyClass is static class with static properties
          foreach (var p in type.GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic))
          {
                        keys.Add(p.Name.ToString(), (Keys) p.GetValue(type));
          }
        }


        
        /**
         * Check the shortcut for a specific unit type
         * Fill allowed with a list of shortcuts you don't want to get
         */
        public static Keys GetShortCutKey(UnitType t, List<UnitType> allowed = null)
        {
            if (keys.ContainsKey(t.ToString()))
            {
                if (allowed == null || allowed.Contains(t))
                    return keys[t.ToString()];
            }

            return Keys.None;
        }
        /**
         * Return a units unittype from a key pressed
         */
        public static UnitType GetShortcutUnit(Keys k, List<UnitType> allowed = null)
        {
            if (keys.ContainsValue(k))
            {
                string myKey = keys.FirstOrDefault(x => x.Value == k).Key;
                UnitType toReturn = UnitType.None;
                /**
                 * Gets the unit type from its string value 
                 */
                foreach (UnitType ut in Enum.GetValues(typeof(UnitType)))
                {
                    if (ut.ToString() == myKey)
                    {
                        toReturn = ut;
                        break;
                    }
                }

                if (allowed == null || allowed.Contains(toReturn))
                    return toReturn;
            }

            return UnitType.None;
        }

    }
}
