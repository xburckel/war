﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.Utils
{
    public class Debug
    {
        public static string debugString = "";


        public static void DrawDebug(SpriteBatch spriteBatch)
        {

            Texture2D dummyTexture = new Texture2D(Game1.graphics.GraphicsDevice, 1, 1);
            dummyTexture.SetData(new Color[] { Color.White });


            for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                    spriteBatch.Draw(dummyTexture, new Rectangle(i * 32, j * 32, 20, 20), new Color(Game1.map.mapAreas.stGridsGround[i, j] * 10, Game1.map.mapAreas.stGridsGround[i, j] * 5, Game1.map.mapAreas.stGridsGround[i, j] * 2));



        }
        public static void DrawGroundTraversableDebug(SpriteBatch spriteBatch, bool water = false, bool air = false)
        {

            Texture2D dummyTexture = new Texture2D(Game1.graphics.GraphicsDevice, 1, 1);
            dummyTexture.SetData(new Color[] { Color.White });
            int temp = 125;
            bool[,] array = Game1.map.groundTraversableMapTiles;
            if (water)
                array = Game1.map.waterTraversableMapTiles;
            else if (air)
                array = Game1.map.flyingTraversableMapTiles;

            for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                {
                    if (array[i, j])
                        temp = 125;
                    else
                        temp = 0;
                    spriteBatch.Draw(dummyTexture, new Rectangle(i * 32, j * 32, 20, 20), new Color(temp, temp, temp));
                }

        }

        public static void DrawFunctionTimer(SpriteBatch spriteBatch)
        {
            Game1.statics.annouceMessage.UpdateMessage(Debug.debugString);
        }
    }
}
