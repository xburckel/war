﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace war2
{
    [Serializable]
    public class LeftPanel
    {
        Texture2D menuTexture; // 175 * 263 px
        Texture2D HealthBarTexture;
        Texture2D Icons;
        Texture2D ProgressBarTexture;

        public static int LEFTPANEL_WIDTH = 175;
        public static int LEFTPANEL_TOPLEFT_ICON_POSITION_X = 10; // positions of the first icon symbol when something is selected
        public static int LEFTPANEL_TOPLEFT_ICON_POSITION_Y = 500;
        public static int LEFTPANEL_TOPLEFT_BUILD_ICON_POSITION_X = 10; // positions of the first icon symbol when something is to be built
        public static int LEFTPANEL_TOPLEFT_BUILD_ICON_POSITION_Y = 600;
        public static int LEFTPANEL_ICON_SIZE_X = 50;
        public static int LEFTPANEL_ICON_SIZE_Y = 40;

        public LeftPanel(Texture2D menuTexture, Texture2D HealthBarTexture, Texture2D icons, Texture2D progressBarTexture, ContentManager content)
        {

            this.menuTexture = menuTexture;
            this.HealthBarTexture = HealthBarTexture;
            this.Icons = icons;
            ProgressBarTexture = progressBarTexture;

        }

        private void DrawHealthBar(SpriteBatch spriteBatch, GraphicsDevice graphics, int hp, UnitType ut, Vector2 pos)
        {
            float healthpercentagemultiplicator = 1;

            // TODO
            switch (ut)
            {
                case UnitType.Archer:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.ARCHER_HP;
                    break;
                case UnitType.Fantassin:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.FANTASSIN_HP;
                    break;
                case UnitType.Battleship:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.BATLLESHIP_HP;
                    break;
                case UnitType.Paysan:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.PAYSAN_HP;
                    break;
                case UnitType.Catapulte:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.CATAPULTE_HP;
                    break;
                case UnitType.Chevalier:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.KNIGHT_HP;
                    break;
                case UnitType.Destroyer:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.DESTROYER_HP;
                    break;
                case UnitType.Griffon:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.DRAGON_HP;
                    break;
                case UnitType.MachineVolante:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.FLYINGMACHINE_HP;
                    break;
                case UnitType.Mage:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.MAGE_HP;
                    break;
                case UnitType.Nains:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.DWARVES_HP;
                    break;
                case UnitType.Paladin:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.PALADIN_HP;
                    break;
                case UnitType.Petrolier:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.OILTANKER_HP;
                    break;
                case UnitType.Sousmarin:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.SUBMARINE_HP;
                    break;
                case UnitType.Transport:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.TRANSPORT_HP;
                    break;
                // TODO : buildings
                case UnitType.Farm:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.FARM_HP;
                    break;
                case UnitType.BallistaTower:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.BALLISTATOWER_HP;
                    break;
                case UnitType.CannonTower:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.CANNONTOWER_HP;
                    break;
                case UnitType.Church:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.CHURCH_HP;
                    break;
                case UnitType.Forge:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.FORGE_HP;
                    break;
                case UnitType.Fortress:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.FORTRESS_HP;
                    break;
                case UnitType.Foundry:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.FOUNDRY_HP;
                    break;
                case UnitType.GoblinWorkBench:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.GOBLIN_WORKBENCH_HP;
                    break;
                case UnitType.GryffinTower:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.GRYFFINTOWER_HP;
                    break;
                case UnitType.LumberMill:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.LUMBERMILL_HP;
                    break;
                case UnitType.MageTower:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.MAGETOWER_HP;
                    break;
                case UnitType.MilitaryBase:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.MILITARYBASE_HP;
                    break;
                case UnitType.NavalChantier:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.CHANTIERNAVAL_HP;
                    break;
                case UnitType.NavalPlatform:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.NAVALPLATFORM_HP;
                    break;
                case UnitType.Raffinery:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.RAFINNERY_HP;
                    break;
                case UnitType.Stable:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.STABLE_HP;
                    break;
                case UnitType.StrongHold:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.STRONGHOLD_HP;
                    break;
                case UnitType.Tower:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.TOWER_HP;
                    break;
                case UnitType.TownHall:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.TOWNHALL_HP;
                    break;


                default:
                    healthpercentagemultiplicator = (float)hp / (float)UnitProperties.FANTASSIN_HP;
                    break;
            }



            spriteBatch.Begin();
            if (healthpercentagemultiplicator > 0.7)
                spriteBatch.Draw(HealthBarTexture, pos, new Rectangle(0, 0, (int)Math.Round((32 * healthpercentagemultiplicator)), 5), Color.Green);
            else if (healthpercentagemultiplicator > 0.4)
                spriteBatch.Draw(HealthBarTexture, pos, new Rectangle(0, 0, (int)Math.Round((32 * healthpercentagemultiplicator)), 5), Color.Yellow);
            else
                spriteBatch.Draw(HealthBarTexture, pos, new Rectangle(0, 0, (int)Math.Round((32 * healthpercentagemultiplicator)), 5), Color.Red);
            spriteBatch.End();
        }
        // draw leftpanel unit icons
        private void DrawIcons(SpriteBatch spriteBatch, GraphicsDevice graphics, UnitType ut, Vector2 pos, bool isOrc)
        {
            spriteBatch.Begin();
            switch (ut)
            {
                case UnitType.Paysan:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(50, 0, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(0, 0, 46, 38), Color.White); break;
                case UnitType.Archer:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(245, 0, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(196, 0, 46, 38), Color.White);
                    break;
                case UnitType.Catapulte:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(343, 41, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(294, 41, 46, 38), Color.White);
                    break;
                case UnitType.Chevalier:
                    if (!isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(393, 0, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(441, 0, 46, 38), Color.White);
                    break;
                case UnitType.Destroyer:
                    if (!isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(98, 82, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(147, 82, 46, 38), Color.White);
                    break;
                case UnitType.Griffon:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(49, 123, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(0, 123, 46, 38), Color.White);
                    break;
                case UnitType.MachineVolante:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(441, 82, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(392, 82, 46, 38), Color.White);
                    break;
                case UnitType.Mage:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(245, 41, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(196, 41, 46, 38), Color.White);
                    break;

                case UnitType.Nains:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(147, 41, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(98, 41, 46, 38), Color.White);
                    break;

                case UnitType.Paladin:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(49, 41, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(0, 41, 46, 38), Color.White);
                    break;
                case UnitType.Petrolier:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(441, 41, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(392, 41, 46, 38), Color.White);
                    break;
                case UnitType.Transport:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(49, 82, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(0, 82, 46, 38), Color.White);
                    break;
                case UnitType.Sousmarin:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(343, 82, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(294, 82, 46, 38), Color.White);
                    break;
                case UnitType.Battleship:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(245, 82, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(196, 82, 46, 38), Color.White);
                    break;

                case UnitType.Farm:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(441, 123, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(392, 123, 46, 38), Color.White);
                    break;

                case UnitType.TownHall:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(49, 164, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(0, 164, 46, 38), Color.White);
                    break;
                case UnitType.MilitaryBase:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(147, 164, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(98, 164, 46, 38), Color.White);
                    break;
                case UnitType.LumberMill:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(245, 164, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(196, 164, 46, 38), Color.White);
                    break;
                case UnitType.Forge:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(343, 164, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(294, 164, 46, 38), Color.White);
                    break;
                case UnitType.NavalChantier:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(441, 164, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(392, 164, 46, 38), Color.White);
                    break;
                case UnitType.Raffinery:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(49, 205, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(0, 205, 46, 38), Color.White);
                    break;
                case UnitType.Foundry:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(147, 205, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(98, 205, 46, 38), Color.White);
                    break;
                case UnitType.NavalPlatform:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(245, 205, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(196, 205, 46, 38), Color.White);
                    break;
                case UnitType.Stable:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(343, 205, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(294, 205, 46, 38), Color.White);
                    break;
                case UnitType.GoblinWorkBench:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(441, 205, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(392, 205, 46, 38), Color.White);
                    break;
                case UnitType.Tower:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(49, 246, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(0, 246, 46, 38), Color.White);
                    break;
                case UnitType.Church:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(147, 246, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(98, 246, 46, 38), Color.White);
                    break;
                case UnitType.MageTower:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(245, 246, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(196, 246, 46, 38), Color.White);
                    break;
                case UnitType.StrongHold:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(343, 246, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(294, 246, 46, 38), Color.White);
                    break;
                case UnitType.Fortress:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(441, 246, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(392, 246, 46, 38), Color.White);
                    break;


                case UnitType.GryffinTower:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(147, 287, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(98, 287, 46, 38), Color.White);
                    break;
                case UnitType.BallistaTower:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(343, 287, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(245, 287, 46, 38), Color.White);
                    break;
                case UnitType.CannonTower:
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(392, 287, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(294, 287, 46, 38), Color.White);
                    break;





                default: //fantassin
                    if (isOrc)
                        spriteBatch.Draw(Icons, pos, new Rectangle(147, 0, 46, 38), Color.White);
                    else
                        spriteBatch.Draw(Icons, pos, new Rectangle(98, 0, 46, 38), Color.White);
                    break;
            }
            spriteBatch.End();
        }

        public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphics, List<Unit> Selection, List<Player> players)
        {
            for (int i = 0; i <= graphics.Viewport.Height + 263; i += 263)
            {
                Vector2 pos = new Vector2(0, graphics.Viewport.Height - i);
                spriteBatch.Begin();
                spriteBatch.Draw(menuTexture, pos, new Rectangle(0, 0, LeftPanel.LEFTPANEL_WIDTH, 263), Color.White);
                spriteBatch.End();
            }

            if (!Game1.map.minimapHidden)
                for (int i = LeftPanel.LEFTPANEL_WIDTH; i <= 300; i += LeftPanel.LEFTPANEL_WIDTH)
                {
                    Vector2 pos = new Vector2(i, 0);
                    spriteBatch.Begin();
                    spriteBatch.Draw(menuTexture, pos, new Rectangle(0, 0, 88, 263), Color.White);
                    spriteBatch.End();
                }


            int nb = 0; // not more than 9

            foreach (Unit u in Selection)
            {
                if (nb < 9)
                {
                    Vector2 pos = new Vector2(LEFTPANEL_TOPLEFT_ICON_POSITION_X + (nb % 3) * 47,
                        graphics.Viewport.Height - LEFTPANEL_TOPLEFT_ICON_POSITION_Y + 42 + (nb / 3) * 60); // + 42 to set health bar under icon absolute pos

                    DrawHealthBar(spriteBatch, graphics, u.hp, u.type, pos);

                    pos = new Vector2(LEFTPANEL_TOPLEFT_ICON_POSITION_X + (nb % 3) * 50, 
                        graphics.Viewport.Height - LEFTPANEL_TOPLEFT_ICON_POSITION_Y + (nb / 3) * 60);

                    DrawIcons(spriteBatch, graphics, u.type, pos, players[u.owner].isOrc);
                    nb++;

                    if (Selection.Count == 1 && Selection.First().owner == Game1.currentPLayer) // no build panel for ennemies
                    {
                        u.DrawLeftPanelIcons(spriteBatch);

                    }

                }
                else
                    break;
            }


        }


        public void DrawConstructionIcons(SpriteBatch spriteBatch, Vector2 pos, int spritePosx, int spritePosY)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(Icons, pos, new Rectangle(spritePosx, spritePosY, 46, 38), Color.White);
            spriteBatch.End();
        }


        public void DrawBuildIcons(SpriteBatch spriteBatch, UnitType unitType, Vector2 pos, bool isOrc)
        {
            // 343, 228
            spriteBatch.Begin();
            spriteBatch.Draw(Icons, pos, new Rectangle(343, 328, 46, 38), Color.White);
            spriteBatch.Draw(Icons, new Vector2(pos.X + 50, pos.Y), new Rectangle(392, 328, 46, 38), Color.White);
            spriteBatch.End();


        }

        internal void DrawTankerBuildPanel(bool isOrc, Vector2 pos, SpriteBatch spriteBatch, Player player)
        {
            spriteBatch.Begin();

            if (isOrc)
            {
                spriteBatch.Draw(Icons, pos, new Rectangle(245, 205, 46, 38), Color.White);
            }
            else
            {
                spriteBatch.Draw(Icons, pos, new Rectangle(196, 205, 46, 38), Color.White);
            }
            spriteBatch.End();

        }


        internal void DrawBuildPanel(bool isOrc, Vector2 pos, SpriteBatch spriteBatch, Player player)
        {
            spriteBatch.Begin();

            if (isOrc)
            {
                spriteBatch.Draw(Icons, pos, new Rectangle(49, 164, 46, 38), Color.White);
                spriteBatch.Draw(Icons, new Vector2(pos.X + 50, pos.Y), new Rectangle(441, 123, 46, 38), Color.White);
                spriteBatch.Draw(Icons, new Vector2(pos.X + 100, pos.Y), new Rectangle(147, 164, 46, 38), Color.White);
                pos.Y -= 45;

                if (player.playerTechs.hasTech(UnitType.LumberMill)) // if he has it it means he has a town hall thus he can build everything in tier 1
                {
                    spriteBatch.Draw(Icons, pos, new Rectangle(245, 164, 46, 38), Color.White); // scierie
                    spriteBatch.Draw(Icons, new Vector2(pos.X + 50, pos.Y), new Rectangle(343, 164, 46, 38), Color.White); // forge
                    pos.Y -= 45;
                    spriteBatch.Draw(Icons, pos, new Rectangle(49, 246, 46, 38), Color.White); // tour
                }
            }
            else
            {

                spriteBatch.Draw(Icons, pos, new Rectangle(0, 164, 46, 38), Color.White);
                spriteBatch.Draw(Icons, new Vector2(pos.X + 50, pos.Y), new Rectangle(392, 123, 46, 38), Color.White);
                spriteBatch.Draw(Icons, new Vector2(pos.X + 100, pos.Y), new Rectangle(98, 164, 46, 38), Color.White);
                pos.Y -= 45;

                if (player.playerTechs.hasTech(UnitType.LumberMill)) // if he has it it means he has a town hall thus he can build everything in tier 1
                {
                    spriteBatch.Draw(Icons, pos, new Rectangle(196, 164, 46, 38), Color.White); // scierie
                    spriteBatch.Draw(Icons, new Vector2(pos.X + 50, pos.Y), new Rectangle(294, 164, 46, 38), Color.White);
                    pos.Y -= 45;
                    spriteBatch.Draw(Icons, pos, new Rectangle(0, 246, 46, 38), Color.White); // tour
                }
            }
            spriteBatch.End();
        }

        internal void DrawAdvancedBuildPanel(bool isOrc, Vector2 pos, SpriteBatch spriteBatch, Player player)
        {
            spriteBatch.Begin();

            if (isOrc)
            {
                if (player.playerTechs.hasTech(UnitType.NavalChantier))
                {
                    spriteBatch.Draw(Icons, pos, new Rectangle(441, 164, 46, 38), Color.White);
                }
                if (player.playerTechs.hasTech(UnitType.Foundry))
                {
                    spriteBatch.Draw(Icons, new Vector2(pos.X + 50, pos.Y), new Rectangle(147, 205, 46, 38), Color.White);
                    spriteBatch.Draw(Icons, new Vector2(pos.X + 100, pos.Y), new Rectangle(49, 205, 46, 38), Color.White);
                }

                pos.Y -= 45;
                if (player.playerTechs.hasTech(UnitType.Stable))
                {
                    spriteBatch.Draw(Icons, pos, new Rectangle(343, 205, 46, 38), Color.White); // ogre
                    spriteBatch.Draw(Icons, new Vector2(pos.X + 50, pos.Y), new Rectangle(441, 205, 46, 38), Color.White);
                }
                pos.Y -= 45;
                if (player.playerTechs.hasTech(UnitType.Church))
                {
                    spriteBatch.Draw(Icons, pos, new Rectangle(147, 246, 46, 38), Color.White);
                    spriteBatch.Draw(Icons, new Vector2(pos.X + 50, pos.Y), new Rectangle(245, 246, 46, 38), Color.White);
                    spriteBatch.Draw(Icons, new Vector2(pos.X + 100, pos.Y), new Rectangle(147, 287, 46, 38), Color.White);
                }
            }
            else
            {
                if (player.playerTechs.hasTech(UnitType.NavalChantier))
                {
                    spriteBatch.Draw(Icons, pos, new Rectangle(392, 164, 46, 38), Color.White);
                }
                if (player.playerTechs.hasTech(UnitType.Foundry))
                {
                    spriteBatch.Draw(Icons, new Vector2(pos.X + 50, pos.Y), new Rectangle(98, 205, 46, 38), Color.White);
                    spriteBatch.Draw(Icons, new Vector2(pos.X + 100, pos.Y), new Rectangle(0, 205, 46, 38), Color.White);
                }

                pos.Y -= 45;
                if (player.playerTechs.hasTech(UnitType.Stable))
                {
                    spriteBatch.Draw(Icons, pos, new Rectangle(294, 205, 46, 38), Color.White); // ecuries
                    spriteBatch.Draw(Icons, new Vector2(pos.X + 50, pos.Y), new Rectangle(392, 205, 46, 38), Color.White);
                }

                pos.Y -= 45;
                if (player.playerTechs.hasTech(UnitType.Church))
                {
                    spriteBatch.Draw(Icons, pos, new Rectangle(98, 246, 46, 38), Color.White); // eglise
                    spriteBatch.Draw(Icons, new Vector2(pos.X + 50, pos.Y), new Rectangle(196, 246, 46, 38), Color.White); // tour mage
                    spriteBatch.Draw(Icons, new Vector2(pos.X + 100, pos.Y), new Rectangle(98, 287, 46, 38), Color.White); // griffons
                }

            }
            spriteBatch.End();
        }

        internal void DrawBuildProgressBar(SpriteBatch spriteBatch, Building building, GraphicsDevice graphics, List<Player> players, bool upgrading = false)
        {
            int a = 0;
            if (building.unitbuilding != UnitType.None)
                a = UnitProperties.getUnitProperties(building.unitbuilding)[(int)Properties.BUILDTIME]; // time to build the unit that the building is constructing atm

            int percentbuild = building.percentBuilt;
            float constructState = building.constructionState;
            if (upgrading) // if it is an upgrade and not a classic build
            {
                a = UnitProperties.getBuildingProperties(building.upgradingto)[(int)BuildingProperties.BUILDTIME];
                constructState = building.upgrading;
                percentbuild = 100;
            }
            if (percentbuild >= 100 && (upgrading || building.unitbuilding != UnitType.None)) // if the building is finished
            {
                spriteBatch.Begin();
                Vector2 pos = new Vector2(LEFTPANEL_TOPLEFT_ICON_POSITION_X, graphics.Viewport.Height - LEFTPANEL_TOPLEFT_ICON_POSITION_Y + 4 * 60);

                float percentcomplet = (constructState / 1000.0f) / a;

                spriteBatch.Draw(ProgressBarTexture, pos, new Rectangle(9, 5, (int)(130 * percentcomplet), 16), Color.Green); // progressBar
                spriteBatch.DrawString(Game1.statics.font, ((int)(percentcomplet * 100)).ToString() + " % Complete", new Vector2(pos.X + 15, pos.Y + 4), Color.Gold);
                spriteBatch.End();
                if (upgrading)
                    DrawIcons(spriteBatch, graphics, building.upgradingto, new Vector2(115, pos.Y - 15), players[building.owner].isOrc);
                else
                    DrawIcons(spriteBatch, graphics, building.unitbuilding, new Vector2(115, pos.Y - 15), players[building.owner].isOrc);

            }

            else if (percentbuild < 100)
            {
                spriteBatch.Begin();
                Vector2 pos = new Vector2(LEFTPANEL_TOPLEFT_ICON_POSITION_X, graphics.Viewport.Height - LEFTPANEL_TOPLEFT_ICON_POSITION_Y + 4 * 60);

                spriteBatch.Draw(ProgressBarTexture, pos, new Rectangle(9, 5, building.percentBuilt, 16), Color.Green); // progressBar
                spriteBatch.DrawString(Game1.statics.font, (building.percentBuilt).ToString() + " % Complete", new Vector2(pos.X + 15, pos.Y + 4), Color.Gold);
                spriteBatch.End();
                DrawIcons(spriteBatch, graphics, building.type, new Vector2(115, pos.Y - 15), players[building.owner].isOrc);

            }
        }

        internal void DrawInfoPanel(SpriteBatch spbatch, Unit unit, Resource resource = null)
        {
            if (resource != null)
            {
                String Name = "";
                if (resource.type == ResourceType.Gold)
                    Name = "Gold Mine";
                else
                    Name = "Oil patch";

                spbatch.Begin();
                spbatch.DrawString(Game1.statics.font, Name, new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 400), Color.White);
                spbatch.DrawString(Game1.statics.font, "Amount : " + resource.amount, new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 440), Color.White);

                spbatch.End();
            }

            else if (unit != null)
            {
                spbatch.Begin();
                if (Game1.statics.debug)
                { 
                    spbatch.DrawString(Game1.statics.font, "Pos: " + unit.position.ToString(), new Vector2(LEFTPANEL_TOPLEFT_ICON_POSITION_X, LEFTPANEL_TOPLEFT_ICON_POSITION_Y + 150), Color.White);
                    spbatch.DrawString(Game1.statics.font, "Dest: " + unit.destination.ToString(), new Vector2(LEFTPANEL_TOPLEFT_ICON_POSITION_X, LEFTPANEL_TOPLEFT_ICON_POSITION_Y + 170), Color.White);
                    spbatch.DrawString(Game1.statics.font, "path: " + unit.path.Count(), new Vector2(LEFTPANEL_TOPLEFT_ICON_POSITION_X, LEFTPANEL_TOPLEFT_ICON_POSITION_Y + 190), Color.White);
                    spbatch.DrawString(Game1.statics.font, "stuck: " + (unit.type == UnitType.Paysan ? ((Harvester)unit).stuckTime.ToString() : ""), new Vector2(LEFTPANEL_TOPLEFT_ICON_POSITION_X, LEFTPANEL_TOPLEFT_ICON_POSITION_Y + 210), Color.White);
                    spbatch.DrawString(Game1.statics.font, "building: " + ((unit.type == UnitType.Paysan && ((Harvester)unit).constructingBuilding != null) ? ((Harvester)unit).constructingBuilding.type.ToString() : ""), new Vector2(LEFTPANEL_TOPLEFT_ICON_POSITION_X, LEFTPANEL_TOPLEFT_ICON_POSITION_Y + 230), Color.White);
                    spbatch.DrawString(Game1.statics.font, "timeharvesting: " + (unit.type == UnitType.Paysan ? ((Harvester) unit).timeharvesting.ToString() : ""), new Vector2(LEFTPANEL_TOPLEFT_ICON_POSITION_X, LEFTPANEL_TOPLEFT_ICON_POSITION_Y + 250), Color.White);

                }

                spbatch.DrawString(Game1.statics.font, unit.type.ToString(), new Vector2(LEFTPANEL_TOPLEFT_ICON_POSITION_X, LEFTPANEL_TOPLEFT_ICON_POSITION_Y - 15), Color.White);
                spbatch.DrawString(Game1.statics.font, "Health: " + unit.hp.ToString() + " / " + 
                    (unit.isABuilding ? UnitProperties.getBuildingProperties(unit.type) : UnitProperties.getUnitProperties(unit.type, unit.isOrc()))[(int)Properties.HP].ToString(), 
                    new Vector2(LEFTPANEL_TOPLEFT_ICON_POSITION_X, LEFTPANEL_TOPLEFT_ICON_POSITION_Y + 50), Color.White);


                if ((int)unit.type < Unit.littlebuildingindex)
                {
                    spbatch.DrawString(Game1.statics.font, "Armor : " + unit.armor + "(+ " + Game1.map.players[unit.owner].playerAmeliorations.getArmorAmelioration(unit.type) + ")", new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 550), Color.White);
                    spbatch.DrawString(Game1.statics.font, "Damage : " + unit.damage + "(+ " + Game1.map.players[unit.owner].playerAmeliorations.getDamageAmelioration(unit.type) + ")", new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 570), Color.White);
                    spbatch.DrawString(Game1.statics.font, "Sight : " + unit.sight, new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 590), Color.White);
                    spbatch.DrawString(Game1.statics.font, "Speed : " + unit.speed, new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 610), Color.White);
                    spbatch.DrawString(Game1.statics.font, "Range : " + unit.range, new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 630), Color.White);
                }
                else if (unit.type == UnitType.TownHall || unit.type == UnitType.StrongHold || unit.type == UnitType.Fortress)
                {
                    if (Game1.map.players[unit.owner].PlayerHas(UnitType.Fortress) > 0)
                        spbatch.DrawString(Game1.statics.font, "Gold : 100 (+20)", new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 550), Color.White);
                    else if (Game1.map.players[unit.owner].PlayerHas(UnitType.StrongHold) > 0)
                        spbatch.DrawString(Game1.statics.font, "Gold : 100 (+10)", new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 550), Color.White);
                    else
                        spbatch.DrawString(Game1.statics.font, "Gold : 100", new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 550), Color.White);
                    if (Game1.map.players[unit.owner].PlayerHas(UnitType.LumberMill) > 0)
                        spbatch.DrawString(Game1.statics.font, "Wood : 100 (+25)", new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 570), Color.White);
                    else
                        spbatch.DrawString(Game1.statics.font, "Wood : 100", new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 570), Color.White);
                    if (Game1.map.players[unit.owner].PlayerHas(UnitType.Raffinery) > 0)
                        spbatch.DrawString(Game1.statics.font, "Oil : 100 (+25)", new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 590), Color.White);
                    else
                        spbatch.DrawString(Game1.statics.font, "Oil : 100", new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 590), Color.White);
                }


                spbatch.End();


            }
        }

        public void DrawCancelButton(SpriteBatch spbatch)
        {
            spbatch.Begin();
            spbatch.Draw(Icons, new Vector2(120, Game1.graphics.GraphicsDevice.Viewport.Height - 50), new Rectangle(49, 369, 46, 38), Color.White);
            spbatch.End();
        }

        public void DrawUnloadButton(bool isHuman)
        {
            SpriteBatch spbatch = new SpriteBatch(Game1.graphics.GraphicsDevice);

            spbatch.Begin();
            if (isHuman)
                spbatch.Draw(Icons, new Vector2(120, Game1.graphics.GraphicsDevice.Viewport.Height - 50), new Rectangle(98, 656, 46, 38), Color.White);
            else
                spbatch.Draw(Icons, new Vector2(120, Game1.graphics.GraphicsDevice.Viewport.Height - 50), new Rectangle(147, 656, 46, 38), Color.White);
            spbatch.End();
        }

        internal void DrawDemolishButton(bool isHuman)
        {
            SpriteBatch spbatch = new SpriteBatch(Game1.graphics.GraphicsDevice);

            spbatch.Begin();
            if (isHuman)
                spbatch.Draw(Icons, new Vector2(120, Game1.graphics.GraphicsDevice.Viewport.Height - 50), new Rectangle(98, 574, 46, 38), Color.White);
            else
                spbatch.Draw(Icons, new Vector2(120, Game1.graphics.GraphicsDevice.Viewport.Height - 50), new Rectangle(147, 574, 46, 38), Color.White);
            spbatch.End();
        }

        internal void DrawSpells(UnitsRelated.Wizard wizard)
        {
            SpriteBatch spbatch = new SpriteBatch(Game1.graphics.GraphicsDevice);

            float healthpercentagemultiplicator = (float)wizard.mana / (float)UnitProperties.MAGE_MANA; // to draw mana bar

            spbatch.Begin();
            spbatch.Draw(HealthBarTexture, new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 200), new Rectangle(0, 0, 64, 15), Color.WhiteSmoke);

            spbatch.Draw(HealthBarTexture, new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 200), new Rectangle(0, 0, (int)Math.Round((32 * healthpercentagemultiplicator) * 2), 15), Color.Blue);

            // (aMouse.X > 120 && aMouse.X < 166 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 64 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 100) // arcan barrage

            if (Game1.map.players[wizard.owner].isOrc)
            {//147, 410
                spbatch.Draw(Icons, new Vector2(120, Game1.graphics.GraphicsDevice.Viewport.Height - 50), new Rectangle(196, 369, 46, 38), Color.White);
                spbatch.Draw(Icons, new Vector2(65, Game1.graphics.GraphicsDevice.Viewport.Height - 50), new Rectangle(147, 451, 46, 38), Color.White);
                spbatch.Draw(Icons, new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 50), new Rectangle(392, 369, 46, 38), Color.White);
                spbatch.Draw(Icons, new Vector2(120, Game1.graphics.GraphicsDevice.Viewport.Height - 100), new Rectangle(196, 451, 46, 38), Color.White);
                spbatch.Draw(Icons, new Vector2(65, Game1.graphics.GraphicsDevice.Viewport.Height - 100), new Rectangle(147, 410, 46, 38), Color.White);//deathanddecay
                spbatch.Draw(Icons, new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 100), new Rectangle(392, 410, 46, 38), Color.White);

            }
            else
            {
                spbatch.Draw(Icons, new Vector2(120, Game1.graphics.GraphicsDevice.Viewport.Height - 50), new Rectangle(49, 410, 46, 38), Color.White);
                spbatch.Draw(Icons, new Vector2(65, Game1.graphics.GraphicsDevice.Viewport.Height - 50), new Rectangle(0, 410, 46, 38), Color.White);
                spbatch.Draw(Icons, new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 50), new Rectangle(294, 369, 46, 38), Color.White);

                spbatch.Draw(Icons, new Vector2(120, Game1.graphics.GraphicsDevice.Viewport.Height - 100), new Rectangle(196, 451, 46, 38), Color.White);
                spbatch.Draw(Icons, new Vector2(65, Game1.graphics.GraphicsDevice.Viewport.Height - 100), new Rectangle(245, 410, 46, 38), Color.White);
                spbatch.Draw(Icons, new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 100), new Rectangle(245, 451, 46, 38), Color.White);

            }
            spbatch.End();
        }

        internal void DrawAmeliorationProgressBar(SpriteBatch spriteBatch, Building building, GraphicsDevice graphicsDevice, List<Player> list)
        {
            spriteBatch.Begin();
            Vector2 pos = new Vector2(5, graphicsDevice.Viewport.Height - 526 + 4 * 60);

            int a = UnitProperties.AMELIORATION_TIME; // time to build itself
            float percentcomplet = ((float)building.sa.percentage / 1000.0f) / (float)a;

            spriteBatch.Draw(ProgressBarTexture, pos, new Rectangle(9, 5, (int)(130 * percentcomplet), 16), Color.Green); // progressBar
            spriteBatch.DrawString(Game1.statics.font, ((int)(percentcomplet * 100)).ToString() + " % Complete", new Vector2(pos.X + 15, pos.Y + 4), Color.Gold);
            spriteBatch.End();
            DrawAmeliorationIcons(spriteBatch, graphicsDevice, building.type, new Vector2(115, pos.Y - 15), building.sa);

        }

        private void DrawAmeliorationIcons(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice, UnitType unitType, Vector2 pos, UnitsRelated.SingleAmelioration sa)
        {
            spriteBatch.Begin();
            switch (sa.getUnitTypeResearched())
            {
                case UnitType.Fantassin:

                    if (sa.getIncreaseType() == UnitsRelated.Ameliorations.buffs.Damage && Game1.map.players[Game1.currentPLayer].playerAmeliorations.getDamageAmelioration(sa.getUnitTypeResearched()) == 0)
                    {
                        if (Game1.map.players[Game1.currentPLayer].isOrc)
                            spriteBatch.Draw(Icons, pos, new Rectangle(0, 492, 46, 38), Color.White);
                        else
                            spriteBatch.Draw(Icons, pos, new Rectangle(343, 451, 46, 38), Color.White);
                    }
                    else if (sa.getIncreaseType() == UnitsRelated.Ameliorations.buffs.Damage && Game1.map.players[Game1.currentPLayer].playerAmeliorations.getDamageAmelioration(sa.getUnitTypeResearched()) != 0)
                    {
                        if (Game1.map.players[Game1.currentPLayer].isOrc)
                            spriteBatch.Draw(Icons, pos, new Rectangle(48, 492, 46, 38), Color.White);
                        else
                            spriteBatch.Draw(Icons, pos, new Rectangle(392, 451, 46, 38), Color.White);

                    }
                    if (sa.getIncreaseType() == UnitsRelated.Ameliorations.buffs.Armor && Game1.map.players[Game1.currentPLayer].playerAmeliorations.getArmorAmelioration(sa.getUnitTypeResearched()) == 0)
                    {
                        if (Game1.map.players[Game1.currentPLayer].isOrc)
                            spriteBatch.Draw(Icons, pos, new Rectangle(343, 656, 46, 38), Color.White);
                        else
                            spriteBatch.Draw(Icons, pos, new Rectangle(245, 656, 46, 38), Color.White);
                    }
                    else if (sa.getIncreaseType() == UnitsRelated.Ameliorations.buffs.Armor && Game1.map.players[Game1.currentPLayer].playerAmeliorations.getArmorAmelioration(sa.getUnitTypeResearched()) != 0)
                    {
                        if (Game1.map.players[Game1.currentPLayer].isOrc)
                            spriteBatch.Draw(Icons, pos, new Rectangle(392, 492, 46, 38), Color.White);
                        else
                            spriteBatch.Draw(Icons, pos, new Rectangle(294, 656, 46, 38), Color.White);

                    }
                    break;
                case UnitType.Archer:
                    if (sa.getIncreaseType() == UnitsRelated.Ameliorations.buffs.Damage && Game1.map.players[Game1.currentPLayer].playerAmeliorations.getDamageAmelioration(sa.getUnitTypeResearched()) == 0)
                    {
                        if (Game1.map.players[Game1.currentPLayer].isOrc)
                            spriteBatch.Draw(Icons, pos, new Rectangle(392, 492, 46, 38), Color.White);
                        else
                            spriteBatch.Draw(Icons, pos, new Rectangle(245, 492, 46, 38), Color.White);
                    }
                    else if (sa.getIncreaseType() == UnitsRelated.Ameliorations.buffs.Damage && Game1.map.players[Game1.currentPLayer].playerAmeliorations.getDamageAmelioration(sa.getUnitTypeResearched()) != 0)
                    {
                        if (Game1.map.players[Game1.currentPLayer].isOrc)
                            spriteBatch.Draw(Icons, pos, new Rectangle(441, 492, 46, 38), Color.White);
                        else
                            spriteBatch.Draw(Icons, pos, new Rectangle(294, 492, 46, 38), Color.White);
                    }
                    break;
                case UnitType.Paladin:
                    if (sa.getIncreaseType() == UnitsRelated.Ameliorations.buffs.PaladinResearch)
                    {
                        if (Game1.map.players[Game1.currentPLayer].isOrc)
                            spriteBatch.Draw(Icons, pos, new Rectangle(49, 41, 46, 38), Color.White);
                        else
                            spriteBatch.Draw(Icons, pos, new Rectangle(0, 41, 46, 38), Color.White);
                    }
                    break;
            }

            spriteBatch.End();


        }
    }



}
