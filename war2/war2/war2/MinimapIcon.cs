﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using war2.Utils;

namespace war2
{
    public class MinimapIcon
    {
        private string current_;
        public Vector2 minimapPosition; // position where the minimap is displayed on the SELECT MENU (not during game)
        public KeyValuePair<string, Texture2D> currentTexture;


        public MinimapIcon(string current_)
        {
            this.current_ = current_;
            minimapPosition = new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 820, 250);
            currentTexture = new KeyValuePair<string, Texture2D>();
        }

        /** 
         * Draws the minimap to a texture & returns that texture
         * */
        public Texture2D getMinimap(Game1 gameObject, string name = null)
        {
            if (name == null)
                name = current_;

            if (currentTexture.Key == current_ && currentTexture.Value != null)
                return currentTexture.Value;

            RenderTarget2D renderTarget = new RenderTarget2D(
                        Game1.graphics.GraphicsDevice,
                        256,
                        256);
            Game1.graphics.GraphicsDevice.SetRenderTarget(renderTarget);
            Game1.spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp,
                  null, null, null, Game1.mmcam.TransformMinimapAsPNG());
            gameObject.DrawMap(true);
            Game1.map.DrawGoldMinesAsYellowSquares(Game1.spriteBatch, true);
            Game1.map.DrawOilPatchesAsBlackSquares(Game1.spriteBatch, true);
            Game1.spriteBatch.End();
            int w = 256;
            int h = 256;
            Game1.graphics.GraphicsDevice.SetRenderTarget(null);

            Texture2D myMap = new Texture2D(Game1.graphics.GraphicsDevice, w, h);
            Color[] texdata = new Color[myMap.Width * myMap.Height];
            renderTarget.GetData(texdata);
            myMap.SetData(texdata);
            currentTexture = new KeyValuePair<string,Texture2D>(current_, myMap);
            return myMap;

       }

        /** 
         * Draws the minimap on the map select screen
         */
        public void DrawMinimap(Game1 game1, SpriteBatch spbatch = null)
        {
            try
            {
                Texture2D texture = getMinimap(game1);
                spbatch.Draw(texture, new Rectangle((int)minimapPosition.X, (int)minimapPosition.Y, 256, 256), Color.White);
            }
            catch (Exception)
            {
                Game1.statics.menuItem.DrawMenu((int)minimapPosition.X, (int)minimapPosition.Y, "Map preview unavailable", null, spbatch, false);
            }

        }
    

        public void UpdateCurrent(string newPath, string newPathFullPath)
        {
            if (current_ != newPath)
            {
                Game1.map.Load(newPathFullPath);
                currentTexture = new KeyValuePair<string, Texture2D>(current_, null);
            }

            current_ = newPath;
        }

        public bool MinimapIconExist(string name = null)
        {
            if (name == null)
                name = current_;
            return (File.Exists("MinimapIcons\\" + name + ".jpg"));

        }

    }
}
