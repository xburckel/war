﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.UnitsRelated
{
    // reprensents a single amelioration beeing researched
     [Serializable]
    public class SingleAmelioration
    {
        Ameliorations.buffs type;
        UnitType unitAmeliorated;
        private bool isResearching;
        private bool isResearched;
        public int percentage;
        public int playerResearching;
        int increase;

        public SingleAmelioration(Ameliorations.buffs type, UnitType unitAmeliorated, int increase, int playerResearching)
        {
            this.playerResearching = playerResearching;
            isResearching = false;
            percentage = 0;
            this.unitAmeliorated = unitAmeliorated;
            this.type = type;
            this.increase = increase;

        }

        public void Update(GameTime gameTime)
        {
            if (isResearching && ((percentage / 1000) < UnitProperties.AMELIORATION_TIME))
                percentage += gameTime.ElapsedGameTime.Milliseconds;
            if ((percentage / 1000) >= UnitProperties.AMELIORATION_TIME)
            {
                isResearched = true;
                percentage = 100000;
            }
            if (isResearched)
            {
                Game1.map.players[playerResearching].playerAmeliorations.addAmelioration(type, unitAmeliorated, increase);
                Game1.statics.ameliorations.Remove(this);
                if (unitAmeliorated == UnitType.Fantassin) // fantassin amelioration also upgrades knights by the double
                {
                    Game1.map.players[playerResearching].playerAmeliorations.addAmelioration(type, UnitType.Paladin, increase * 2);
                    Game1.map.players[playerResearching].playerAmeliorations.addAmelioration(type, UnitType.Chevalier, increase * 2);
                }

            }
        }

        public bool IsResearched()
        {
            return isResearched;
        }
        public void Research()
        {
            isResearching = true;
        }
        public UnitType getUnitTypeResearched()
        {
            return unitAmeliorated;
        }

        public Ameliorations.buffs getIncreaseType()
        {
            return type;
        }

    }
}
