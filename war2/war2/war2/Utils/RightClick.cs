﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.Utils
{
    // plays the cross animation
    public class RightClick
    {
        Texture2D cross;
        Vector2 position;
        int displayTime;
        UnitType selectedUnit;
        public RightClick()
        {
            displayTime = 0;
            cross = Game1.content.Load<Texture2D>("cross");

        }

        public void doRightClick(Vector2 position, UnitType selectedUnit, bool isOrc)
        {
            displayTime = 2000;
            this.position = position;
            this.selectedUnit = selectedUnit;
            Game1.soundEngine.playMoveUnitSound(selectedUnit, isOrc);
        }

        public void Update(GameTime gameTime)
        {
            displayTime -= gameTime.ElapsedGameTime.Milliseconds;
        }

        public void Draw(SpriteBatch spbatch)
        {
            if (displayTime > 1500)
            {
                spbatch.Begin();

                spbatch.Draw(cross, position, new Rectangle(0, 0, 32, 32), Color.White);
                spbatch.End();
            }
            else if (displayTime > 750)
            {
                spbatch.Begin();
                int percent = (int) (((float) displayTime / (float) 1500) * 255);
                spbatch.Draw(cross, position, new Rectangle(0, 0, 32, 32), new Color(percent, percent, percent));
                spbatch.End();
                
            }
        }

    }
}
