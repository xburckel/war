﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.UnitsRelated
{
    [Serializable]
    public class Wizard : Unit
    {
        public int manaTimer;
        public SpellType selectedSpell;

        public Wizard(int hp, int mana, float speed,
         int damage,
         bool flying,
         bool sailing,
         int goldCost,
         int woodCost,
         int oilCost,
         int range,
         int sight,
         Vector2 position,
        int buildtime,
            UnitType type,
        int armor, int owner)
            : base(hp, mana, speed,
                damage,
                flying,
                sailing,
                goldCost,
                woodCost,
                oilCost,
                range,
                sight,
                position,
                buildtime,
                type,
                armor, owner)
        {
            manaTimer = 0;
            mana = 100;
        }


        public override void Update(Map map, GameTime gameTime)
        {
            base.Update(map, gameTime);

            manaTimer += gameTime.ElapsedGameTime.Milliseconds;
            if (manaTimer > 500)
            {
                manaTimer = 0;
                if (mana < 255)
                    mana++;
            }
        }



        public override void DrawLeftPanelIcons(SpriteBatch spriteBatch)
        {
            Game1.leftPanel.DrawSpells(this);

        }



        internal void SelectSpell(Microsoft.Xna.Framework.Input.MouseState aMouse)
        {
            if (aMouse.X > 120 && aMouse.X < 166 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 14 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 50) // fire explosion
            {
                selectedSpell = SpellType.FireExplosion;
                Game1.spellCursorActive = true;

            }
            else if (aMouse.X > 65 && aMouse.X < 111 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 64 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 100) // blizzard
            {
                if (Game1.map.players[owner].isOrc)
                    selectedSpell = SpellType.DeathAndDecay;
                else
                    selectedSpell = SpellType.Blizzard;
                Game1.spellCursorActive = true;
            }
            else if (aMouse.X > 10 && aMouse.X < 65 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 64 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 100) // arcan barrage
            {
                    selectedSpell = SpellType.ArcaneBarrage;
                Game1.spellCursorActive = true;
            }
            else if (aMouse.X > 120 && aMouse.X < 166 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 64 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 100) // arcan barrage
            {
                selectedSpell = SpellType.Resurrection;
                Game1.spellCursorActive = true;
            }
            else
            {
                selectedSpell = SpellType.None;
                Game1.spellCursorActive = false;
            }

        }

        internal void LaunchSpell(int x, int y)
        {
            if (Vector2.Distance(this.position, new Vector2(x, y)) > 10)
            {
                if (position.X > x + 7)
                    x = (int) position.X - 7;
                if (position.X < x - 7)
                    x = (int) position.X + 7;
                if (position.Y > y + 7)
                    y = (int)position.Y - 7;
                if (position.Y < y - 7)
                    y = (int)position.Y + 7;
            }

            if (Vector2.Distance(this.position, new Vector2(x, y)) <= 10)// dont launch from the other side of the map
            {
                if (selectedSpell == SpellType.Blizzard || selectedSpell == SpellType.DeathAndDecay)
                {
                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                        {
                            if (mana > 25)
                            {
                                Spell spell = new Spell(4000, new Vector2(x + i, y + j), selectedSpell);
                                Game1.map.mapSpells.Add(spell);
                                mana -= 25;
                            }
                            else
                                break;

                        }

                    if (selectedSpell == SpellType.DeathAndDecay)
                        Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.DEATHANDDECAY], new Vector2(x, y));
                    else if (selectedSpell == SpellType.Blizzard)
                        Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.BLIZZARD], new Vector2(x, y));
                }
                if (selectedSpell == SpellType.FireExplosion)
                {
                    if (mana > 125)
                    {
                        Spell spell = new Spell(1500, new Vector2(x, y), selectedSpell);
                        Game1.map.mapSpells.Add(spell);
                        mana -= 125;
                        Game1.soundEngine.PlaySound(Game1.soundEngine.spellSounds[(int)SpellSounds.fireExplosion], new Vector2(x, y));

                    }

                }
                if (selectedSpell == SpellType.ArcaneBarrage)
                {
                    if (mana > 150)
                    {
                        Spell spell = new Spell(10000, new Vector2(x, y), selectedSpell, this);
                        Game1.map.mapSpells.Add(spell);
                        mana -= 150;
                        Game1.soundEngine.PlaySound(Game1.soundEngine.spellSounds[(int)SpellSounds.arcaneBarrage], new Vector2(x, y));
                    }

                }

                if (selectedSpell == SpellType.Resurrection)
                {
                    if (mana >= 255)
                    {
                        Spell spell = new Spell(0, new Vector2(x, y), selectedSpell, this);
                        Game1.map.mapSpells.Add(spell);
                      //  Game1.soundEngine.PlaySound(Game1.soundEngine.spellSounds[(int)SpellSounds.arcaneBarrage], new Vector2(x, y));
                    }
                }

                // network
                if (owner == Game1.currentPLayer)
                {
                    SpellCasted sc = new SpellCasted();
                    sc.casterIndex = index;
                    sc.casterPos = position;
                    sc.spellPos = new Vector2(x, y);
                    sc.spellType = selectedSpell;
                    Game1.map.players[Game1.currentPLayer].playerMoves.spellCasted.Add(sc);
                }
                selectedSpell = SpellType.None;
            }
        }
    }

}

