﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using war2.SpriteSheetCode;
using war2.UnitsRelated;

namespace war2
{
    



    public class Client
    {
        /*
        static NetClient client;
        int refreshtimer;
        // Clients list of characters
        // static List<Character> GameStateList;

        // Create timer that tells client, when to send update
        private int checkPacketsTimer;
        public static int received = 0;
        public static int counter = 0; // nb of packets received
        // Indicates if program is running

        public Client()
        {
            checkPacketsTimer = 0;
            refreshtimer = 0;
            // Ask for IP

            // Read Ip to string
            string hostip;// = "127.0.0.1";

            // Create new instance of configs. Parameter is "application Id". It has to be same on client and server.
            NetPeerConfiguration Config = new NetPeerConfiguration("game");

            // Create new client, with previously created configs
            client = new NetClient(Config);

            // Create new outgoing message
            NetOutgoingMessage outmsg = client.CreateMessage();


            //LoginPacket lp = new LoginPacket("Katu");

            // Start client
            client.Start();

            // Write byte ( first byte informs server about the message type ) ( This way we know, what kind of variables to read )
            outmsg.Write((byte)PacketTypes.LOGIN);

            // Write String "Name" . Not used, but just showing how to do it
            outmsg.Write("MyName");


            StreamReader reader = new StreamReader("ipconfig.txt");
            hostip = reader.ReadLine();//"192.168.1.12"; //"127.0.0.1"; // 

            // Connect client, to ip previously requested from user 
            client.Connect(hostip, 14242, outmsg);


            Console.WriteLine("client Started");

            // Create the list of characters
            // GameStateList = new List<Character>();

            // Set timer to tick every 50ms

            // When time has elapsed ( 50ms in this case ), call "update_Elapsed" funtion

            // Funtion that waits for connection approval info from server
            //    WaitForStartingInfo();

            // Start the timer

        }


        private static void WaitForStartingInfo()
        {
            // When this is set to true, we are approved and ready to go
            // New incomgin message
            NetIncomingMessage inc;

            // Loop untill we are approved


            // If new messages arrived
            if ((inc = client.ReadMessage()) != null)
            {
                // Switch based on the message types
                switch (inc.MessageType)
                {

                    // All manually sent messages are type of "Data"
                    case NetIncomingMessageType.Data:

                        // Read the first byte
                        // This way we can separate packets from each others
                        if (inc.LengthBytes >= 1 && inc.ReadByte() == (byte)PacketTypes.STARTGAME)
                        {


                            int datalength = inc.ReadInt32();
                            byte[] data = new byte[datalength];
                            inc.ReadBytes(data, 0, datalength);
                            SaveGameData s = (SaveGameData)Server.Deserialize(data);

                            Game1.map.players = s.players;
                            Game1.map.Load("Maps.pud");

                            foreach (Unit u in s.units)
                            {
                                if ((int)u.type < Unit.littlebuildingindex && u.currentAnimation == null)
                                    u.currentAnimation = new UnitAnimation(u.position, u);
                            }

                            Game1.map.units = s.units;


                            Game1.currentPLayer = s.currentplayer;
                            NetOutgoingMessage outmsg = client.CreateMessage();

                            Game1.window.Title = "client" + Game1.currentPLayer;

                            // Worldstate packet structure
                            //
                            // int = count of players
                            // character obj * count



                            // Write byte = Set "MOVE" as packet type
                            outmsg.Write((byte)PacketTypes.GOOGTOGO);

                            // Write byte = move direction

                            // Send it to server
                            client.SendMessage(outmsg, NetDeliveryMethod.Unreliable);

                            inc = null;


                            while (true) // we wait a good to go
                            {
                                inc = client.ReadMessage();



                                if (inc != null && inc.LengthBytes == 1 && (PacketTypes)inc.ReadByte() == PacketTypes.GOOGTOGO)
                                {
                                    break;
                                }
                                //freeze
                            }




                            //Console.WriteLine("WorldState Update");

                            // Empty the gamestatelist
                            // new data is coming, so everything we knew on last frame, does not count here
                            // Even if client would manipulate this list ( hack ), it wont matter, becouse server handles the real list
                            //    GameStateList.Clear();

                            // Declare count


                            // When all players are added to list, start the game
                            Game1.isInMainMenu = false;
                        }



                        break;

                }
            }
        }



        public void Disconnect()
        {
            client.Shutdown("byebye");
        }

        public void Update(GameTime gameTime)
        {
            if (Game1.mainMenu.lobbymenu)
                WaitForStartingInfo();
            refreshtimer += gameTime.ElapsedGameTime.Milliseconds;
            checkPacketsTimer += gameTime.ElapsedGameTime.Milliseconds;
            NetIncomingMessage inc;

           if ((inc = client.ReadMessage()) != null)
                    CheckServerMessages(inc);
            if (!Game1.isInMainMenu && refreshtimer > 50)
            {

                refreshtimer = 0;
                PlayerMoves mymoves = Game1.map.players[Game1.currentPLayer].playerMoves;
                SendUnitDatas(mymoves);
            }



        }

        private void SendUnitDatas(PlayerMoves mymoves)
        {
            mymoves.SendNetworkMessagesToHost(client);

            Game1.map.players[Game1.currentPLayer].playerMoves = new PlayerMoves(Game1.currentPLayer);
        }



        /// <summary>
        /// Check for new incoming messages from server
        /// </summary>
        private static void CheckServerMessages(NetIncomingMessage inc)
        {
            // Create new incoming message holder


            if (inc.MessageType == NetIncomingMessageType.Data)
            {
                if (inc.LengthBytes >= 1)
                {
                    byte b = inc.ReadByte();
                    Client.received++;

                    if (b == (byte)PacketTypes.UPDATEUNITS)
                    {
                        UpdateTypes up = (UpdateTypes)inc.ReadByte();
                        switch (up)
                        {
                            case UpdateTypes.UnitPosUpdate:
                                long index = inc.ReadInt64();
                                Vector2 position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
                                Vector2 destination = new Vector2(inc.ReadFloat(), inc.ReadFloat());
                                Vector2 direction = new Vector2(inc.ReadFloat(), inc.ReadFloat());

                                int playernumber = 0;
                                foreach (Unit u in Game1.map.units)
                                {
                                    if (u.index == index)
                                    {
                                        u.destination = destination;
                                        u.position = position;
                                        u.direction = direction;
                                        playernumber = u.owner;
                                        break;
                                    }
                                }
                                break;
                            case UpdateTypes.UnitAttack:
                                index = inc.ReadInt64();
                                position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
                                long targetIndex = inc.ReadInt64();
                                playernumber = 0;
                                foreach (Unit u in Game1.map.units)
                                {
                                    if (u.index == index)
                                    {
                                        u.position = position;
                                        playernumber = u.owner;
                                        foreach (Unit target in Game1.map.units)
                                        {
                                            if (target.index == targetIndex)
                                            {
                                                u.target = target;
                                                if (!u.onRangeForAttack()) // TODO : this is bad
                                                {
                                                    u.Move(u.target.position, Game1.map);
                                                }
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }

                                break;
                            case UpdateTypes.DeadUnits:
                                index = inc.ReadInt64();
                                foreach (Unit u in Game1.map.units)
                                {
                                    if (u.index == index)
                                    {
                                        playernumber = u.owner;
                                        u.hp = -1; // kill it
                                        break;
                                    }
                                }
                                break;
                            case UpdateTypes.NewBuilding:
                                index = inc.ReadInt64();
                                position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
                                long builderIndex = inc.ReadInt64();
                                UnitType ut = (UnitType)inc.ReadInt32();
                                playernumber = 0;
                                foreach (Unit u in Game1.map.units)
                                {
                                    if (u.index == builderIndex)
                                    {
                                        playernumber = u.owner;
                                        Building buil = (Building)Game1.map.CreateUnit(ut, position, playernumber);
                                        buil.percentBuilt = 0;
                                        buil.builder = (Harvester)u;
                                        ((Harvester)u).constructingBuilding = buil;
                                        ((Harvester)u).position = buil.position;
                                        u.destination = buil.position;
                                        u.path.Clear();
                                        ((Harvester)u).buildingConstructPosition = buil.position;
                                        ((Harvester)u).BuildBuilding();
                                        Game1.map.newUnits.Last().index = index;


                                        break;
                                    }
                                }



                                break;
                            case UpdateTypes.UnitUpgrade:
                                index = inc.ReadInt64();
                                ut = (UnitType)inc.ReadByte();
                                playernumber = 0;
                                foreach (Unit u in Game1.map.units)
                                {
                                    if (u.index == index)
                                    {
                                        ((Building)u).upgradingto = ut;
                                        ((Building)u).Upgrade(ut);

                                        break;
                                    }
                                }
                                break;
                            case UpdateTypes.DwarvesDemolish:
                                index = inc.ReadInt64();
                                position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
                                playernumber = 0;
                                foreach (Unit u in Game1.map.units)
                                {
                                    if (u.index == index)
                                    {
                                        u.position = position;
                                        ((Demolisher)u).DemolishRequest();


                                        break;
                                    }
                                }
                                break;
                            case UpdateTypes.TransportLoadRequest:
                                index = inc.ReadInt64();
                                long unitindex = inc.ReadInt64();
                                position = new Vector2(inc.ReadFloat(), inc.ReadFloat());

                                playernumber = 0;
                                foreach (Unit u in Game1.map.units)
                                {
                                    if (u.index == index)
                                    {
                                        foreach (Unit u2 in Game1.map.units)
                                        {
                                            if (u2.index == unitindex)
                                            {
                                                u.position = position;
                                                u2.position = new Vector2(u.position.X, u.position.Y);
                                                ((Transport)u).carryRequest.Add(u2);
                                            }
                                            break;
                                        }

                                        break;
                                    }
                                }

                                break;
                            case UpdateTypes.TransportUnloadRequest:
                                index = inc.ReadInt64();
                                position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
                                playernumber = 0;
                                foreach (Unit u in Game1.map.units)
                                {
                                    if (u.index == index)
                                    {

                                        u.position = position;
                                        ((Transport)u).unloadRequest = true;

                                        break;
                                    }
                                }

                                break;
                            case UpdateTypes.Speull:
                                Client.received = -50000;

                                index = inc.ReadInt64();
                                Vector2 wizardposition = new Vector2(inc.ReadFloat(), inc.ReadFloat());
                                position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
                                SpellType spt = (SpellType)inc.ReadByte();
                                playernumber = 0;
                                foreach (Unit u in Game1.map.units)
                                {
                                    if (u.index == index)
                                    {
                                        playernumber = u.owner;
                                        u.position = wizardposition;
                                        ((Wizard)u).selectedSpell = spt;
                                        ((Wizard)u).LaunchSpell((int)position.X, (int)position.Y);

                                        break;
                                    }
                                }
                                break;
                            case UpdateTypes.CancelBuilding:
                                index = inc.ReadInt64();
                                playernumber = 0;
                                foreach (Unit u in Game1.map.units)
                                {
                                    if (u.index == index)
                                    {

                                        ((Building)u).CancelBuild();

                                        break;
                                    }
                                }

                                break;
                            case UpdateTypes.NewUnit:
                                index = inc.ReadInt64();
                                position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
                                ut = (UnitType)inc.ReadByte();
                                playernumber = inc.ReadInt32();


                                Game1.map.units.Add(Game1.map.CreateUnit(ut, position, playernumber));
                                Game1.map.units.Last().destination = position;
                                Game1.map.units.Last().index = index;
                                break;
                            case UpdateTypes.HarvesterEnteringBuilding:
                                index = inc.ReadInt64();
                                long buildindex = inc.ReadInt64();
                                playernumber = 0;
                                foreach (Unit u in Game1.map.units)
                                {
                                    if (u.index == index)
                                    {
                                        playernumber = u.owner;
                                        foreach (Unit u2 in Game1.map.units)
                                        {
                                            if (buildindex == u2.index)
                                            {
                                                ((Harvester)u).inbuilding = ((Building)u2);
                                                ((Harvester)u).timeharvesting = 2000;
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                                break;

                            case UpdateTypes.HarvesterHarvested:
                                index = inc.ReadInt64();
                                position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
                                playernumber = 0;
                                foreach (Unit u in Game1.map.units)
                                {
                                    if (u.index == index)
                                    {
                                        playernumber = u.owner;
                                        Resource r = Game1.map.resources[(int)position.X, (int)position.Y];
                                        if (r != null)
                                        {
                                            r.amount -= 100;
                                            ((Harvester)u).carrying = r.type;
                                        }
                                        break;
                                    }
                                }
                                break;



                        }
                     


                    } 
                }
            }
        }



        public void Draw(SpriteBatch spbatch) // debug
        {
            spbatch.Begin();
            spbatch.DrawString(Game1.statics.font, "packets received by client : " + client.Statistics.ReceivedPackets + "packets sent by client : " + client.Statistics.SentPackets, new Vector2(500, 500), Color.Green);
            spbatch.DrawString(Game1.statics.font, "messages received by client : " + received + " messages sent : " + counter, new Vector2(500, 600), Color.Red);



            spbatch.End();
        }
        // Get input from player and send it to server
        private static void GetInputAndSendItToServer()
        {
            // If button was pressed and it was some of those movement keys
            /* if (MoveDir != MoveDirection.NONE)
             {
                 // Create new message
                 NetOutgoingMessage outmsg = client.CreateMessage();

                 // Write byte = Set "MOVE" as packet type
                 outmsg.Write((byte)PacketTypes.MOVE);

                 // Write byte = move direction
                 outmsg.Write((byte)MoveDir);

                 // Send it to server
                 Client.SendMessage(outmsg, NetDeliveryMethod.Unreliable);

                 // Reset movedir
                 MoveDir = MoveDirection.NONE;
             } */

        }







    }

