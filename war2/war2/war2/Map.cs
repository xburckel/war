﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using war2.Astar;
using war2.PathFindingRelated;
using war2.UnitsRelated;
using war2.Utils;

namespace war2
{
    public enum MapTextureType { Snow, Summer, Desert }

    public enum MapElement
    {
        Forest = 0, Water, Mud, Grass, Rock,
        FullForest1, FullForest2, FullForest3, FullForest4, FullForest5, FullForest6, FullForest7, FullForest8, FullForest9, FullForest10, FullForest11,
        Grass1, Grass2, Grass3, Grass4, Grass5, Grass6, Grass7, Grass8, Grass9, Grass10, Grass11, Grass12, Grass13, Grass14, Grass15,
        Mud1, Mud2, Mud3, Mud4, Mud5, Mud6, Mud7, Mud8, Mud9, Mud10, Mud11, Mud12, Mud13, Mud14, Mud15,
        Water1, Water2, Water3, Water4, Water5, Water6, Water7, Water8, Water9, Water10, Water11, Water12, Water13, Water14, Water15,
        Water16, Water17, Water18, Water19, Water20, Water21, Water22, Water23, Water24, Water25, Water26, Water27, Water28, Water29, Water30,
        Rock1, Rock2, Rock3, Rock4, Rock5, Rock6, Rock7, Rock8, Rock9, Rock10, Rock11, Rock12, Rock13, Rock14, Rock15,
        Rock16, Rock17, Rock18, Rock19, Rock20,


        Gold, Oil,


        GrassTopLeft, GrassTopRight, GrassTopHalf, GrassBottomLeft, GrassLeftHalf, GrassTopRightBottomLeft, GrassTopHalfBottomLeft, GrassBottomRight, GrassTopLeftBottomRight, GrassRightHalf, GrassTopHalfBottomRight,
        GrassBottomHalf, GrassTopLeftBottomHalf, GrassTopRightBottomHalf,
        // 155
        CoastTopLeft, CoastTopRight, CoastTopHalf, CoastBottomLeft, CoastLeftHalf, CoastTopRightBottomLeft, CoastTopHalfBottomLeft, CoastBottomRight, CoastTopLeftBottomRight, CoastRightHalf, CoastTopHalfBottomRight,
        CoastBottomHalf, CoastTopLeftBottomHalf, CoastTopRightBottomHalf,
        // 170
        RockTopLeft, RockTopRight, RockTopHalf, RockBottomLeft, RockLeftHalf, RockTopRightBottomLeft, RockTopHalfBottomLeft, RockBottomRight, RockTopLeftBottomRight, RockRightHalf, RockTopHalfBottomRight,
        RockBottomHalf, RockTopLeftBottomHalf, RockTopRightBottomHalf, // rock & coast
        // 185
        WaterTopLeft, WaterTopRight, WaterTopHalf, WaterBottomLeft, WaterLeftHalf, WaterTopRightBottomLeft, WaterTopHalfBottomLeft, WaterBottomRight, WaterTopLeftBottomRight, WaterRightHalf, WaterTopHalfBottomRight,
        WaterBottomHalf, WaterTopLeftBottomHalf, WaterTopRightBottomHalf, // water & coast

    };

    public enum TextureCollision
    {
        Left = 0, Top, Right, Bottom

    };




    [Serializable]
    public class Map
    {
        public string name;
        public MapAreas mapAreas;
        public PathFindingRelated.HumanPlayZones playerZones;
        public bool minimapHidden;
        public static int borderIndex = 140;
        public bool[,] groundTraversableMapTiles;
        public bool[,] waterTraversableMapTiles;
        public bool[,] flyingTraversableMapTiles;

        public MapElement[,] array; // array containing the map elements as Forest = 0, Water, Mud, Grass, Rock only
        public static int mapsize = 128;
        public List<Unit> units;
        public List<Unit> selected;
        public List<Projectile> projectiles;
        public List<Cadavre> cadavres;
        public List<Spell> mapSpells;
        public List<Resource> GoldResources;// we need this for collisions refresh
        public List<Resource> OilResources;// we need this to save compute time
        public PackMove packMoves;

        public Resource[,] resources;
        public Resource selectedResource;
        public List<Unit> unitsToRemove;
        public List<Resource> toRemove;


        public List<Projectile> projectilesToRemove;
        public List<Player> players;

        // to load textures, refer to the position in MapElement
        private int foreststartindex = 5;
        private int forestendindex = 15;
        private int grassstartindex;
        private int grassendindex;
        private int mudstartindex;
        private int mudendindex;
        private int waterstartindex;
        private int waterendindex;
        private int rockstartindex;
        private int rockendindex;

        public Vector2[,] mapTextures; // array containing the position of the textures to draw

        public List<Unit> newUnits;



        public Map()
        {
            name = "map.pud";
            minimapHidden = false;
            unitsToRemove = new List<Unit>();
            GoldResources = new List<Resource>();
            OilResources = new List<Resource>();
            cadavres = new List<Cadavre>();
            toRemove = new List<Resource>();
            selected = new List<Unit>();
            array = new MapElement[mapsize, mapsize];
            grassstartindex = forestendindex + 1;
            grassendindex = grassstartindex + 15;
            mudstartindex = grassendindex + 1;
            mudendindex = mudstartindex + 14;
            waterstartindex = mudendindex + 1;
            waterendindex = waterstartindex + 29;
            rockstartindex = waterendindex + 1;
            rockendindex = rockstartindex + 19;

            mapTextures = new Vector2[mapsize, mapsize];
            groundTraversableMapTiles = new bool[mapsize, mapsize];
            waterTraversableMapTiles = new bool[mapsize, mapsize];
            flyingTraversableMapTiles = new bool[mapsize, mapsize];
            newUnits = new List<Unit>();
            resources = new Resource[mapsize, mapsize];
            selectedResource = null;
            projectiles = new List<Projectile>();
            projectilesToRemove = new List<Projectile>();
            mapSpells = new List<Spell>();
            for (int i = 0; i < mapsize; i++)
                for (int j = 0; j < mapsize; j++)
                {
                    groundTraversableMapTiles[i, j] = true;
                    waterTraversableMapTiles[i, j] = true;
                    flyingTraversableMapTiles[i, j] = true;
                    resources[i, j] = null;
                }

        }

        public MapElement[,] getMap()
        {
            return array;
        }


        private MapElement ParseMapElement(string item)
        {
            return (MapElement)(int.Parse(item));
        }

        public int[] GetMaptilesPosition(int posi, int posj)
        {

            int[] array = { 0, 0 };



            array[0] = 33 * (int)Math.Round(mapTextures[posi, posj].X);
            array[1] = 33 * (int)Math.Round(mapTextures[posi, posj].Y);
            return array;

        }
        // Parser
        /*
         * Section 1 = Map Tiles
         * Section 2 = Units
         * Section 3 = Map resources (Gold + oil)
         * 
         * */
        public void Load(string path)
        {
            this.name = path;
            if (Game1.soundEngine != null)
                Game1.soundEngine.ContinuePlaying();

            if (Game1.currentMapTexture == MapTextureType.Snow)
                Game1.currentTile = Game1.wintercurrentTile;
            if (Game1.currentMapTexture == MapTextureType.Summer)
                Game1.currentTile = Game1.maptiles;
            if (Game1.currentMapTexture == MapTextureType.Desert)
                Game1.currentTile = Game1.wastelandcurrentTile;
            Game1.unitTextures = new UnitTexture[16];
            Game1.statics.istryingtobuild = false;
            mapSpells = new List<Spell>();

            foreach (Player p in players) // reset ai
            {
                if (!p.network && p.number != Game1.currentPLayer)
                    p.playerAI = new IA(p.number);
            }
            for (int i = 0; i < 16; i += 2)
            {
                Game1.unitTextures[i] = new UnitTexture((War2Color)(i / 2), false, Game1.content);
                Game1.unitTextures[i + 1] = new UnitTexture((War2Color)(i / 2), true, Game1.content);
            }

            // load unit - texture spritesheet mapping
            Game1.unitTextureAssociated = new Texture2D[Enum.GetNames(typeof(UnitType)).Length, Game1.map.players.Count];

            for (int i = 0; i < 8; i++)
            {
                Unit.loadUnitTextureMapping(Game1.unitTextureAssociated, Game1.unitTextures, Game1.map.players[i]);
            }



            Random rand = new Random();
            // "Saves\\noob1.wsav"
            selected.Clear();

            units = new List<Unit>();
            StreamReader reader = new StreamReader(path);

            string size = reader.ReadLine();
            size = size.Replace("Size:", "");

            try
            {
                Map.mapsize = int.Parse(size);
            }
            catch (Exception)   // map is most probably invalid
            {
                Map.mapsize = 128;
            }



            OilResources = new List<Resource>();

            GoldResources = new List<Resource>();
            Game1.map.players[Game1.currentPLayer].explored = new bool[Map.mapsize, Map.mapsize];
            Game1.map.players[Game1.currentPLayer].hasVision = new bool[Map.mapsize, Map.mapsize];
            resources = new Resource[mapsize, mapsize];
            array = new MapElement[mapsize, mapsize];
            mapTextures = new Vector2[mapsize, mapsize];
            groundTraversableMapTiles = new bool[mapsize, mapsize];
            waterTraversableMapTiles = new bool[mapsize, mapsize];
            flyingTraversableMapTiles = new bool[mapsize, mapsize];
            Game1.mmcam = new MinimapCamera();

                Game1.renderTarget = new RenderTarget2D(
                            Game1.graphics.GraphicsDevice,
                            (4096),
                            (4096));

            string line = "";
            int linenb = 0;
            for (int i = 0; i < mapsize; i++)
                for (int j = 0; j < mapsize; j++)
                    resources[i, j] = null;

            while ((line = reader.ReadLine()) != null && !line.Contains("Units"))
            {
                int columnnb = 0;
                string[] items = line.Split(',');
                foreach (string item in items)
                {

                    if (columnnb < mapsize && linenb < mapsize)
                    {
                        MapElement melemnt = ParseMapElement(item);
                        array[linenb, columnnb] = melemnt;
                    }
                    columnnb++;
                }
                linenb++;
            }


            for (linenb = 0; linenb < mapsize; linenb++)
                for (int columnnb = 0; columnnb < mapsize; columnnb++)
                {
                    MapElement melemnt = array[linenb, columnnb];

                    if (melemnt == MapElement.Forest)
                    {
                        Resource r = new Resource(100, new Vector2(linenb, columnnb), ResourceType.Wood);
                        this.resources[linenb, columnnb] = r;

                        int randomNumber = rand.Next(foreststartindex, forestendindex);
                        melemnt = (MapElement)randomNumber;
                        groundTraversableMapTiles[linenb, columnnb] = false;
                        waterTraversableMapTiles[linenb, columnnb] = false;
                    }

                    else if (melemnt == MapElement.Grass)
                    {
                        groundTraversableMapTiles[linenb, columnnb] = true;
                        waterTraversableMapTiles[linenb, columnnb] = false;
                        int randomNumber = rand.Next(grassstartindex, grassendindex);
                        melemnt = (MapElement)randomNumber;
                    }

                    else if (melemnt == MapElement.Mud)
                    {
                        groundTraversableMapTiles[linenb, columnnb] = true;
                        waterTraversableMapTiles[linenb, columnnb] = false;
                        int randomNumber = rand.Next(mudstartindex, mudendindex);
                        melemnt = (MapElement)randomNumber;
                    }
                    else if (melemnt == MapElement.Rock)
                    {
                        groundTraversableMapTiles[linenb, columnnb] = false;
                        waterTraversableMapTiles[linenb, columnnb] = false;
                        int randomNumber = rand.Next(rockstartindex, rockendindex);
                        melemnt = (MapElement)randomNumber;
                    }
                    else if (melemnt == MapElement.Water)
                    {
                        groundTraversableMapTiles[linenb, columnnb] = false;
                        waterTraversableMapTiles[linenb, columnnb] = true;
                        int randomNumber = rand.Next(waterstartindex, waterendindex);
                        melemnt = (MapElement)randomNumber;
                    }

                    else if (melemnt == MapElement.Gold)
                    {
                        // shouldnt be there
                    }

                    else if (melemnt == MapElement.Oil)
                    {
                        // shouldnt be there either
                    }
                    else if (melemnt >= MapElement.CoastTopLeft && melemnt <= MapElement.CoastTopRightBottomHalf) // border tiles traversable
                    {
                        groundTraversableMapTiles[linenb, columnnb] = true;
                        waterTraversableMapTiles[linenb, columnnb] = false;
                        array[linenb, columnnb] = MapElement.Mud;
                    }
                    else if (melemnt >= MapElement.GrassTopLeft && melemnt <= MapElement.GrassTopRightBottomHalf)
                    {
                        Resource r = new Resource(100, new Vector2(linenb, columnnb), ResourceType.Wood);
                        this.resources[linenb, columnnb] = r;
                        array[linenb, columnnb] = MapElement.Forest;

                        groundTraversableMapTiles[linenb, columnnb] = false;
                        waterTraversableMapTiles[linenb, columnnb] = false;
                    }
                    else
                    {
                        groundTraversableMapTiles[linenb, columnnb] = false;
                        waterTraversableMapTiles[linenb, columnnb] = false;
                        array[linenb, columnnb] = MapElement.Rock;
                    }

                    LoadTextureArray(melemnt, linenb, columnnb);


                }




            mapAreas = new MapAreas(groundTraversableMapTiles, waterTraversableMapTiles, flyingTraversableMapTiles);
            packMoves = new PackMove();
            LoadUnits(units, reader);
            LoadResources(reader);
            cadavres.Clear();
            playerZones = new HumanPlayZones();

            for (int i = 0; i < 8; i++)
            {
                players[i].food = 0;
                players[i].usedFood = 0;
                players[i].playerTechs.Update(players[i].number, units);
            }
            if (Game1.cam != null)
                Game1.cam.setInitialPosition(Game1.map.units);
            Game1.recentPaths = new List<AlreadyComputedPaths>[Map.mapsize, Map.mapsize];
            for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                    Game1.recentPaths[i, j] = new List<AlreadyComputedPaths>() ;
        }



        // called once to load every texture position
        private void LoadTextureArray(MapElement elt, int line, int column)
        {
            int[] array = new int[2];
            switch (elt)
            {
                // dont modify this

                case MapElement.GrassTopLeft: array[0] = 15; array[1] = 6; break;
                case MapElement.GrassTopRight: array[0] = 12; array[1] = 5; break;
                case MapElement.GrassTopHalf: array[0] = 0; array[1] = 7; break;
                case MapElement.GrassBottomLeft: array[0] = 16; array[1] = 6; break;
                case MapElement.GrassLeftHalf: array[0] = 17; array[1] = 6; break;
                case MapElement.GrassTopRightBottomLeft: array[0] = 13; array[1] = 6; break;
                case MapElement.GrassTopHalfBottomLeft: array[0] = 6; array[1] = 7; break;
                case MapElement.GrassBottomRight: array[0] = 3; array[1] = 7; break;
                case MapElement.GrassTopLeftBottomRight: array[0] = 4; array[1] = 7; break;
                case MapElement.GrassRightHalf: array[0] = 11; array[1] = 5; break;
                case MapElement.GrassTopHalfBottomRight: array[0] = 4; array[1] = 7; break;
                case MapElement.GrassBottomHalf: array[0] = 2; array[1] = 7; break;
                case MapElement.GrassTopLeftBottomHalf: array[0] = 3; array[1] = 6; break;
                case MapElement.GrassTopRightBottomHalf: array[0] = 10; array[1] = 5; break; // this



                case MapElement.CoastTopLeft: array[0] = 4; array[1] = 14; break;
                case MapElement.CoastTopRight: array[0] = 12; array[1] = 14; break;
                case MapElement.CoastTopHalf: array[0] = 13; array[1] = 14; break;
                case MapElement.CoastBottomLeft: array[0] = 7; array[1] = 14; break;
                case MapElement.CoastLeftHalf: array[0] = 9; array[1] = 14; break;
                case MapElement.CoastTopRightBottomLeft: array[0] = 1; array[1] = 15; break;
                case MapElement.CoastTopHalfBottomLeft: array[0] = 18; array[1] = 14; break;
                case MapElement.CoastBottomRight: array[0] = 0; array[1] = 15; break;
                case MapElement.CoastTopLeftBottomRight: array[0] = 4; array[1] = 15; break;
                case MapElement.CoastRightHalf: array[0] = 9; array[1] = 15; break;
                case MapElement.CoastTopHalfBottomRight: array[0] = 12; array[1] = 15; break;
                case MapElement.CoastBottomHalf: array[0] = 5; array[1] = 15; break;
                case MapElement.CoastTopLeftBottomHalf: array[0] = 7; array[1] = 15; break;
                case MapElement.CoastTopRightBottomHalf: array[0] = 14; array[1] = 15; break;

                // TODO
                case MapElement.RockTopLeft: array[0] = 2; array[1] = 9; break;
                case MapElement.RockTopRight: array[0] = 14; array[1] = 7; break;
                case MapElement.RockTopHalf: array[0] = 1; array[1] = 9; break;
                case MapElement.RockBottomLeft: array[0] = 9; array[1] = 7; break;
                case MapElement.RockLeftHalf: array[0] = 12; array[1] = 8; break;
                case MapElement.RockTopRightBottomLeft: array[0] = 10; array[1] = 7; break;
                case MapElement.RockTopHalfBottomLeft: array[0] = 8; array[1] = 4; break;
                case MapElement.RockBottomRight: array[0] = 17; array[1] = 8; break;
                case MapElement.RockTopLeftBottomRight: array[0] = 5; array[1] = 8; break;
                case MapElement.RockRightHalf: array[0] = 13; array[1] = 7; break;
                case MapElement.RockTopHalfBottomRight: array[0] = 2; array[1] = 8; break;
                case MapElement.RockBottomHalf: array[0] = 10; array[1] = 7; break;
                case MapElement.RockTopLeftBottomHalf: array[0] = 13; array[1] = 7; break;
                case MapElement.RockTopRightBottomHalf: array[0] = 12; array[1] = 7; break; // case MapElement.Rock & coast
                // 185
                case MapElement.WaterTopLeft: array[0] = 16; array[1] = 10; break;
                case MapElement.WaterTopRight: array[0] = 5; array[1] = 11; break;
                case MapElement.WaterTopHalf: array[0] = 8; array[1] = 11; break;
                case MapElement.WaterBottomLeft: array[0] = 18; array[1] = 10; break;
                case MapElement.WaterLeftHalf: array[0] = 3; array[1] = 11; break;
                case MapElement.WaterTopRightBottomLeft: array[0] = 9; array[1] = 11; break;
                case MapElement.WaterTopHalfBottomLeft: array[0] = 10; array[1] = 11; break;
                case MapElement.WaterBottomRight: array[0] = 13; array[1] = 11; break;
                case MapElement.WaterTopLeftBottomRight: array[0] = 14; array[1] = 11; break;
                case MapElement.WaterRightHalf: array[0] = 2; array[1] = 12; break;
                case MapElement.WaterTopHalfBottomRight: array[0] = 4; array[1] = 12; break;
                case MapElement.WaterBottomHalf: array[0] = 16; array[1] = 11; break;
                case MapElement.WaterTopLeftBottomHalf: array[0] = 18; array[1] = 11; break;
                case MapElement.WaterTopRightBottomHalf: array[0] = 7; array[1] = 12; break; // case MapElement.Water & coast

                case MapElement.FullForest1:
                    array[0] = 8;
                    array[1] = 5;
                    break;
                case MapElement.FullForest2:
                    array[0] = 10;
                    array[1] = 5;
                    break;
                case MapElement.FullForest3:
                    array[0] = 13;
                    array[1] = 5;
                    break;
                case MapElement.FullForest4:
                    array[0] = 14;
                    array[1] = 5;
                    break;
                case MapElement.FullForest5:
                    array[0] = 18;
                    array[1] = 5;
                    break;
                case MapElement.FullForest6:
                    array[0] = 16;
                    array[1] = 5;
                    break;
                case MapElement.FullForest7:
                    array[0] = 17;
                    array[1] = 5;
                    break;
                case MapElement.FullForest8:
                    array[0] = 0;
                    array[1] = 6;
                    break;
                case MapElement.FullForest9:
                    array[0] = 1;
                    array[1] = 6;
                    break;
                case MapElement.FullForest10:
                    array[0] = 2;
                    array[1] = 6;
                    break;
                case MapElement.FullForest11:
                    array[0] = 3;
                    array[1] = 6;
                    break;
                case MapElement.Grass1:
                    array[0] = 0;
                    array[1] = 14;
                    break;
                case MapElement.Grass2:
                    array[0] = 1;
                    array[1] = 14;
                    break;
                case MapElement.Grass3:
                    array[0] = 2;
                    array[1] = 14;
                    break;
                case MapElement.Grass4:
                    array[0] = 3;
                    array[1] = 14;
                    break;
                case MapElement.Grass5:
                    array[0] = 10;
                    array[1] = 13;
                    break;
                case MapElement.Grass6:
                    array[0] = 0;
                    array[1] = 13;
                    break;
                case MapElement.Grass7:
                    array[0] = 1;
                    array[1] = 13;
                    break;
                case MapElement.Grass8:
                    array[0] = 2;
                    array[1] = 13;
                    break;
                case MapElement.Grass9:
                    array[0] = 3;
                    array[1] = 13;
                    break;
                case MapElement.Grass10:
                    array[0] = 4;
                    array[1] = 13;
                    break;
                case MapElement.Grass11:
                    array[0] = 5;
                    array[1] = 13;
                    break;
                case MapElement.Grass12:
                    array[0] = 6;
                    array[1] = 13;
                    break;
                case MapElement.Grass13:
                    array[0] = 7;
                    array[1] = 13;
                    break;
                case MapElement.Grass14:
                    array[0] = 8;
                    array[1] = 13;
                    break;
                case MapElement.Grass15:
                    array[0] = 9;
                    array[1] = 13;
                    break;
                case MapElement.Mud1:
                    array[0] = 10;
                    array[1] = 10;
                    break;
                case MapElement.Mud2:
                    array[0] = 11;
                    array[1] = 10;
                    break;
                case MapElement.Mud3:
                    array[0] = 12;
                    array[1] = 10;
                    break;
                case MapElement.Mud4:
                    array[0] = 13;
                    array[1] = 10;
                    break;
                case MapElement.Mud5:
                    array[0] = 11;
                    array[1] = 10;
                    break;
                case MapElement.Mud6:
                    array[0] = 0;
                    array[1] = 10;
                    break;
                case MapElement.Mud7:
                    array[0] = 1;
                    array[1] = 10;
                    break;
                case MapElement.Mud8:
                    array[0] = 2;
                    array[1] = 10;
                    break;
                case MapElement.Mud9:
                    array[0] = 3;
                    array[1] = 10;
                    break;
                case MapElement.Mud10:
                    array[0] = 4;
                    array[1] = 10;
                    break;
                case MapElement.Mud11:
                    array[0] = 5;
                    array[1] = 10;
                    break;
                case MapElement.Mud12:
                    array[0] = 6;
                    array[1] = 10;
                    break;
                case MapElement.Mud13:
                    array[0] = 7;
                    array[1] = 10;
                    break;
                case MapElement.Mud14:
                    array[0] = 8;
                    array[1] = 10;
                    break;
                case MapElement.Mud15:
                    array[0] = 9;
                    array[1] = 10;
                    break;
                case MapElement.Mud:
                    array[0] = 9;
                    array[1] = 9;
                    break;

                case MapElement.Water1:
                    array[0] = 15;
                    array[1] = 15;
                    break;
                case MapElement.Water2:
                    array[0] = 16;
                    array[1] = 15;
                    break;
                case MapElement.Water3:
                    array[0] = 17;
                    array[1] = 15;
                    break;
                case MapElement.Water4:
                    array[0] = 18;
                    array[1] = 15;
                    break;
                case MapElement.Water5:
                    array[0] = 10;
                    array[1] = 16;
                    break;
                case MapElement.Water6:
                    array[0] = 0;
                    array[1] = 16;
                    break;
                case MapElement.Water7:
                    array[0] = 1;
                    array[1] = 16;
                    break;
                case MapElement.Water8:
                    array[0] = 2;
                    array[1] = 16;
                    break;
                case MapElement.Water9:
                    array[0] = 3;
                    array[1] = 16;
                    break;
                case MapElement.Water10:
                    array[0] = 4;
                    array[1] = 16;
                    break;
                case MapElement.Water11:
                    array[0] = 5;
                    array[1] = 16;
                    break;
                case MapElement.Water12:
                    array[0] = 6;
                    array[1] = 16;
                    break;
                case MapElement.Water13:
                    array[0] = 7;
                    array[1] = 16;
                    break;
                case MapElement.Water14:
                    array[0] = 8;
                    array[1] = 16;
                    break;
                case MapElement.Water15:
                    array[0] = 9;
                    array[1] = 16;
                    break;
                case MapElement.Water16:
                    array[0] = 11;
                    array[1] = 16;
                    break;
                case MapElement.Water17:
                    array[0] = 12;
                    array[1] = 16;
                    break;
                case MapElement.Water18:
                    array[0] = 13;
                    array[1] = 16;
                    break;
                case MapElement.Water19:
                    array[0] = 14;
                    array[1] = 16;
                    break;
                case MapElement.Water20:
                    array[0] = 14;
                    array[1] = 16;
                    break;
                case MapElement.Water21:
                    array[0] = 15;
                    array[1] = 16;
                    break;
                case MapElement.Water22:
                    array[0] = 16;
                    array[1] = 16;
                    break;
                case MapElement.Water23:
                    array[0] = 17;
                    array[1] = 16;
                    break;
                case MapElement.Water24:
                    array[0] = 18;
                    array[1] = 16;
                    break;
                case MapElement.Water25:
                    array[0] = 1;
                    array[1] = 17;
                    break;
                case MapElement.Water26:
                    array[0] = 2;
                    array[1] = 17;
                    break;
                case MapElement.Water27:
                    array[0] = 3;
                    array[1] = 17;
                    break;
                case MapElement.Water28:
                    array[0] = 4;
                    array[1] = 17;
                    break;
                case MapElement.Water29:
                    array[0] = 5;
                    array[1] = 17;
                    break;
                case MapElement.Water30:
                    array[0] = 6;
                    array[1] = 17;
                    break;
                case MapElement.Rock1:
                    array[0] = 0;
                    array[1] = 8;
                    break;
                case MapElement.Rock2:
                    array[0] = 1;
                    array[1] = 8;
                    break;
                case MapElement.Rock3:
                    array[0] = 2;
                    array[1] = 8;
                    break;
                case MapElement.Rock4:
                    array[0] = 3;
                    array[1] = 8;
                    break;
                case MapElement.Rock5:
                    array[0] = 4;
                    array[1] = 8;
                    break;
                case MapElement.Rock6:
                    array[0] = 5;
                    array[1] = 8;
                    break;
                case MapElement.Rock7:
                    array[0] = 6;
                    array[1] = 8;
                    break;
                case MapElement.Rock8:
                    array[0] = 7;
                    array[1] = 8;
                    break;
                case MapElement.Rock9:
                    array[0] = 8;
                    array[1] = 8;
                    break;
                case MapElement.Rock10:
                    array[0] = 7;
                    array[1] = 9;
                    break;
                case MapElement.Rock11:
                    array[0] = 12;
                    array[1] = 7;
                    break;
                case MapElement.Rock12:
                    array[0] = 6;
                    array[1] = 9;
                    break;
                case MapElement.Rock13:
                    array[0] = 8;
                    array[1] = 9;
                    break;
                case MapElement.Rock14:
                    array[0] = 13;
                    array[1] = 7;
                    break;
                case MapElement.Rock15:
                    array[0] = 16;
                    array[1] = 7;
                    break;
                case MapElement.Rock16:
                    array[0] = 15;
                    array[1] = 7;
                    break;
                case MapElement.Rock17:
                    array[0] = 16;
                    array[1] = 8;
                    break;
                case MapElement.Rock18:
                    array[0] = 18;
                    array[1] = 7;
                    break;
                case MapElement.Rock19:
                    array[0] = 18;
                    array[1] = 8;
                    break;
                case MapElement.Rock20:
                    array[0] = 0;
                    array[1] = 9;
                    break;
                case MapElement.Rock:
                    array[0] = 6;
                    array[1] = 9;
                    break;
                case MapElement.Water:
                    array[0] = 0;
                    array[1] = 16;
                    break;
                default: // grass
                    array[0] = 0;
                    array[1] = 14;
                    break;
            }

            mapTextures[line, column].X = array[0];
            mapTextures[line, column].Y = array[1];


        }


        // load units from file to list
        private void LoadUnits(List<Unit> units, StreamReader reader)
        {
            string line;
            string[] unit;
            int posx = 0;
            int posy = 0;
            while ((line = reader.ReadLine()) != null && !line.Contains("Resources"))
            {
                posx = 0;
                unit = line.Split(',');

                foreach (string tmp in unit)
                {
                    ParseUnit(units, tmp, new Vector2(posx, posy));
                    posx++;

                }

                posy++;
            }

        }


        // format : - = rien,7;1 (//= fantassin) ,1-15, = fntassin + truc volant
        // unitées separé par virgules, 2 unités meme case on met un tiret, numéro de joueur ; unité
        private void ParseUnit(List<Unit> units, string unit, Vector2 position)
        {
            if (!unit.Equals("-") && !unit.Equals("")) // not empty
            {
                Unit NewUnit1 = null;
                Unit NewUnit2 = null;

                if (unit.Contains("-")) // two units
                {
                    string[] tmp = unit.Split('-');
                    string[] owningplayerandunit = tmp[0].Split(';');
                    string[] owningplayerandunit2 = tmp[1].Split(';');

                    NewUnit1 = (CreateUnit((UnitType)(int.Parse(owningplayerandunit[1])), position, int.Parse(owningplayerandunit[0]), true));
                    NewUnit2 = (CreateUnit((UnitType)(int.Parse(owningplayerandunit2[1])), position, int.Parse(owningplayerandunit2[0]), true));

                }
                else
                {
                    string[] owningplayerandunit = unit.Split(';');

                    NewUnit1 = (CreateUnit((UnitType)(int.Parse(owningplayerandunit[1])), position, int.Parse(owningplayerandunit[0]), true));
                }
                // TODO : uncomment the following to handle collision

                if (NewUnit1 != null)
                {
                    if (NewUnit1.sailing)
                        waterTraversableMapTiles[(int)Math.Round(position.X), (int)Math.Round(position.Y)] = false;
                    else if (NewUnit1.flying)
                        flyingTraversableMapTiles[(int)Math.Round(position.X), (int)Math.Round(position.Y)] = false;
                    else
                        groundTraversableMapTiles[(int)Math.Round(position.X), (int)Math.Round(position.Y)] = false;

                    units.Add(NewUnit1);

                }
                if (NewUnit2 != null)
                {

                    if (NewUnit2.sailing)
                        waterTraversableMapTiles[(int)Math.Round(position.X), (int)Math.Round(position.Y)] = false;
                    else if (NewUnit2.flying)
                        flyingTraversableMapTiles[(int)Math.Round(position.X), (int)Math.Round(position.Y)] = false;
                    else
                        groundTraversableMapTiles[(int)Math.Round(position.X), (int)Math.Round(position.Y)] = false;

                    units.Add(NewUnit2);

                }
            }
        }


        private void LoadResources(StreamReader reader)
        {
            string line;
            string[] resourceslist;
            int posx = 0;
            int posy = 0;
            while ((line = reader.ReadLine()) != null && !line.Contains("Currentplayer"))
            {
                posx = 0;
                resourceslist = line.Split(',');

                foreach (string tmp in resourceslist)
                {
                    ParseResource(tmp, new Vector2(posx, posy));
                    posx++;

                }

                posy++;
            }

            while ((line = reader.ReadLine()) != null) // player resource info
            { //0;G2000;O2000;W1000
                if (line.Contains('G'))
                {
                    int pnumber = int.Parse(line.First().ToString());

                    string[] infos = line.Split(';');
                    pnumber = int.Parse(infos[0]);
                    players[pnumber].gold = int.Parse(infos[1].Replace("G", ""));
                    players[pnumber].oil = int.Parse(infos[2].Replace("O", ""));
                    players[pnumber].wood = int.Parse(infos[3].Replace("W", ""));
                }

            }
        }

        private void ParseResource(string resource, Vector2 pos)
        {
            if (resource != "" && !resource.Equals("-"))
            {
                if (resource[0] == 'G')
                {
                    resource = resource.Replace("G", "");
                    Resource r = new Resource(int.Parse(resource), pos, ResourceType.Gold);
                    GoldResources.Add(r);
                    this.resources[(int)pos.X, (int)pos.Y] = r;
                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                            this.groundTraversableMapTiles[(int)pos.X + i, (int)pos.Y + j] = false;
                }
                else if (resource[0] == 'O')
                {
                    resource = resource.Replace("O", "");
                    Resource r = new Resource(int.Parse(resource), pos, ResourceType.Oil);
                    this.resources[(int)pos.X, (int)pos.Y] = r;
                    OilResources.Add(r);
                }
            }


        }

        // for minimap rendering
        public void DrawGoldMinesAsYellowSquares(SpriteBatch spriteBatch, bool disableFogOfWar = false)
        {
            Color[] data = new Color[32 * 32];

            for (int i = 0; i < data.Length; ++i) data[i] = Color.Yellow;
            Texture2D coloredRectangle = new Texture2D(Game1.graphics.GraphicsDevice, 32, 32);
            coloredRectangle.SetData(data);

            foreach (Resource r in GoldResources)
            {
                if (r != null && r.type == ResourceType.Gold && r.amount > 0 && (disableFogOfWar || (!Game1.statics.fogOfWarEnabled || (Game1.statics.fogOfWarEnabled && players[Game1.currentPLayer].explored[(int)r.Position.X + 1, (int)r.Position.Y + 1]))))
                {
                    if (disableFogOfWar || (!Game1.statics.fogOfWarEnabled || players[Game1.currentPLayer].hasVision[(int)r.Position.X + 1, (int)r.Position.Y + 1]))
                        spriteBatch.Draw(coloredRectangle, new Vector2(r.Position.X * 32, r.Position.Y * 32), new Rectangle(12, 105, 96, 96), Color.White);
                    else
                        spriteBatch.Draw(coloredRectangle, new Vector2(r.Position.X * 32, r.Position.Y * 32), new Rectangle(12, 105, 96, 96), Color.Gray);


                }
            }
        }

        public void DrawOilPatchesAsBlackSquares(SpriteBatch spriteBatch, bool disableFogOfWar = false)
        {
            Texture2D blackRectangle = new Texture2D(Game1.graphics.GraphicsDevice, 32, 32);
            Color[] data = new Color[32 * 32];
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Black;
            blackRectangle.SetData(data);

            foreach (Resource r in OilResources)
            {
                if (r != null && (disableFogOfWar || (!Game1.statics.fogOfWarEnabled || (Game1.statics.fogOfWarEnabled && players[Game1.currentPLayer].explored[(int)r.Position.X + 1, (int)r.Position.Y + 1]))))
                {
                    if (disableFogOfWar || (!Game1.statics.fogOfWarEnabled || players[Game1.currentPLayer].hasVision[(int)r.Position.X + 1, (int)r.Position.Y + 1]))
                        spriteBatch.Draw(blackRectangle, new Vector2(r.Position.X * 32, r.Position.Y * 32), new Rectangle(12, 105, 96, 96), Color.White);
                    else
                        spriteBatch.Draw(blackRectangle, new Vector2(r.Position.X * 32, r.Position.Y * 32), new Rectangle(12, 105, 96, 96), Color.Gray);
                }
            }
        }

        public Unit CreateUnit(UnitType ut, Vector2 position, int owningPlayer, bool createBuildingCollision = true)
        {
            Unit unit = new Unit();
            if ((int)ut < Unit.littlebuildingindex) // not a building
            {
                switch (ut)
                {
                    case UnitType.Fantassin:

                        unit = new Unit(UnitProperties.FANTASSIN_HP, UnitProperties.FANTASSIN_MANA, UnitProperties.FANTASSIN_SPEED, UnitProperties.FANTASSIN_DAMAGE, false, false, UnitProperties.FANTASSIN_GOLD_COST, UnitProperties.FANTASSIN_WOOD_COST,
                            UnitProperties.FANTASSIN_OIL_COST, UnitProperties.FANTASSIN_RANGE, UnitProperties.FANTASSIN_SIGHT, position, UnitProperties.FANTASSIN_BUILD_TIME, ut, UnitProperties.FANTASSIN_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.FANTASSIN_SPRITE_SIZE;
                        break;

                    case UnitType.Archer:
                        unit = new Unit(UnitProperties.ARCHER_HP, UnitProperties.ARCHER_MANA, UnitProperties.ARCHER_SPEED, UnitProperties.ARCHER_DAMAGE, false, false, UnitProperties.ARCHER_GOLD_COST, UnitProperties.ARCHER_WOOD_COST,
                            UnitProperties.ARCHER_OIL_COST, UnitProperties.ARCHER_RANGE, UnitProperties.ARCHER_SIGHT, position, UnitProperties.ARCHER_BUILD_TIME, ut, UnitProperties.ARCHER_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.ARCHER_SPRITE_SIZE;

                        break;
                    case UnitType.Paysan:
                        unit = new Harvester(UnitProperties.PAYSAN_HP, UnitProperties.PAYSAN_MANA, UnitProperties.PAYSAN_SPEED, UnitProperties.PAYSAN_DAMAGE, false, false, UnitProperties.PAYSAN_GOLD_COST, UnitProperties.PAYSAN_WOOD_COST,
                            UnitProperties.PAYSAN_OIL_COST, UnitProperties.PAYSAN_RANGE, UnitProperties.PAYSAN_SIGHT, position, UnitProperties.PAYSAN_BUILD_TIME, ut, UnitProperties.PAYSAN_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.PAYSAN_SPRITE_SIZE;

                        break;
                    case UnitType.Catapulte:
                        unit = new Unit(UnitProperties.CATAPULTE_HP, UnitProperties.CATAPULTE_MANA, UnitProperties.CATAPULTE_SPEED, UnitProperties.CATAPULTE_DAMAGE, false, false, UnitProperties.CATAPULTE_GOLD_COST, UnitProperties.CATAPULTE_WOOD_COST,
                            UnitProperties.CATAPULTE_OIL_COST, UnitProperties.CATAPULTE_RANGE, UnitProperties.CATAPULTE_SIGHT, position, UnitProperties.CATAPULTE_BUILD_TIME, ut, UnitProperties.CATAPULTE_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.CATAPULTE_SPRITE_SIZE;

                        break;
                    case UnitType.Chevalier:
                        unit = new Unit(UnitProperties.KNIGHT_HP, UnitProperties.KNIGHT_MANA, UnitProperties.KNIGHT_SPEED, UnitProperties.KNIGHT_DAMAGE, false, false, UnitProperties.KNIGHT_GOLD_COST, UnitProperties.KNIGHT_WOOD_COST,
                            UnitProperties.KNIGHT_OIL_COST, UnitProperties.KNIGHT_RANGE, UnitProperties.KNIGHT_SIGHT, position, UnitProperties.KNIGHT_BUILD_TIME, ut, UnitProperties.KNIGHT_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.KNHIGHT_SPRITE_SIZE;
                        break;
                    case UnitType.Destroyer:
                        unit = new Unit(UnitProperties.DESTROYER_HP, UnitProperties.DESTROYER_MANA, UnitProperties.DESTROYER_SPEED, UnitProperties.DESTROYER_DAMAGE, false, true, UnitProperties.DESTROYER_GOLD_COST, UnitProperties.DESTROYER_WOOD_COST,
                            UnitProperties.DESTROYER_OIL_COST, UnitProperties.DESTROYER_RANGE, UnitProperties.DESTROYER_SIGHT, position, UnitProperties.DESTROYER_BUILD_TIME, ut, UnitProperties.DESTROYER_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.DESTROYER_SPRITE_SIZE;
                        break;
                    case UnitType.Griffon:
                        unit = new Unit(UnitProperties.DRAGON_HP, UnitProperties.DRAGON_MANA, UnitProperties.DRAGON_SPEED, UnitProperties.DRAGON_DAMAGE, true, false, UnitProperties.DRAGON_GOLD_COST, UnitProperties.DRAGON_WOOD_COST,
                            UnitProperties.DRAGON_OIL_COST, UnitProperties.DRAGON_RANGE, UnitProperties.DRAGON_SIGHT, position, UnitProperties.DRAGON_BUILD_TIME, ut, UnitProperties.DRAGON_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.DRAGON_SPRITE_SIZE;
                        break;
                    case UnitType.MachineVolante:
                        unit = new Unit(UnitProperties.FLYINGMACHINE_HP, UnitProperties.FLYINGMACHINE_MANA, UnitProperties.FLYINGMACHINE_SPEED, UnitProperties.FLYINGMACHINE_DAMAGE, true, false, UnitProperties.FLYINGMACHINE_GOLD_COST, UnitProperties.FLYINGMACHINE_WOOD_COST,
                            UnitProperties.FLYINGMACHINE_OIL_COST, UnitProperties.FLYINGMACHINE_RANGE, UnitProperties.FLYINGMACHINE_SIGHT, position, UnitProperties.FLYINGMACHINE_BUILD_TIME, ut, UnitProperties.FLYINGMACHINE_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.FLYINGMACHINE_SPRITE_SIZE;
                        break;
                    case UnitType.Mage:
                        unit = new Wizard(UnitProperties.MAGE_HP, UnitProperties.MAGE_MANA, UnitProperties.MAGE_SPEED, UnitProperties.MAGE_DAMAGE, false, false, UnitProperties.MAGE_GOLD_COST, UnitProperties.MAGE_WOOD_COST,
                            UnitProperties.MAGE_OIL_COST, UnitProperties.MAGE_RANGE, UnitProperties.MAGE_SIGHT, position, UnitProperties.MAGE_BUILD_TIME, ut, UnitProperties.MAGE_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.MAGE_SPRITE_SIZE;
                        break;
                    case UnitType.Nains:
                        unit = new war2.UnitsRelated.Demolisher(UnitProperties.DWARVES_HP, UnitProperties.DWARVES_MANA, UnitProperties.DWARVES_SPEED, UnitProperties.DWARVES_DAMAGE, false, false, UnitProperties.DWARVES_GOLD_COST, UnitProperties.DWARVES_WOOD_COST,
                            UnitProperties.DWARVES_OIL_COST, UnitProperties.DWARVES_RANGE, UnitProperties.DWARVES_SIGHT, position, UnitProperties.DWARVES_BUILD_TIME, ut, UnitProperties.DWARVES_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.DWARVES_SPRITE_SIZE;
                        break;
                    case UnitType.Paladin:
                        unit = new Unit(UnitProperties.PALADIN_HP, UnitProperties.PALADIN_MANA, UnitProperties.PALADIN_SPEED, UnitProperties.PALADIN_DAMAGE, false, false, UnitProperties.PALADIN_GOLD_COST, UnitProperties.PALADIN_WOOD_COST,
                            UnitProperties.PALADIN_OIL_COST, UnitProperties.PALADIN_RANGE, UnitProperties.PALADIN_SIGHT, position, UnitProperties.PALADIN_BUILD_TIME, ut, UnitProperties.PALADIN_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.PALADIN_SPRITE_SIZE;
                        break;
                    case UnitType.Petrolier:
                        unit = new Harvester(UnitProperties.OILTANKER_HP, UnitProperties.OILTANKER_MANA, UnitProperties.OILTANKER_SPEED, UnitProperties.OILTANKER_DAMAGE, false, true, UnitProperties.OILTANKER_GOLD_COST, UnitProperties.OILTANKER_WOOD_COST,
                            UnitProperties.OILTANKER_OIL_COST, UnitProperties.OILTANKER_RANGE, UnitProperties.OILTANKER_SIGHT, position, UnitProperties.OILTANKER_BUILD_TIME, ut, UnitProperties.OILTANKER_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.OILTANKER_SPRITE_SIZE;
                        break;
                    case UnitType.Sousmarin:
                        unit = new Submarine(UnitProperties.SUBMARINE_HP, UnitProperties.SUBMARINE_MANA, UnitProperties.SUBMARINE_SPEED, UnitProperties.SUBMARINE_DAMAGE, false, true, UnitProperties.SUBMARINE_GOLD_COST, UnitProperties.SUBMARINE_WOOD_COST,
                            UnitProperties.SUBMARINE_OIL_COST, UnitProperties.SUBMARINE_RANGE, UnitProperties.SUBMARINE_SIGHT, position, UnitProperties.SUBMARINE_BUILD_TIME, ut, UnitProperties.SUBMARINE_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.SUBMARINE_SPRITE_SIZE;
                        break;
                    case UnitType.Transport:
                        unit = new Transport(UnitProperties.TRANSPORT_HP, UnitProperties.TRANSPORT_MANA, UnitProperties.TRANSPORT_SPEED, UnitProperties.TRANSPORT_DAMAGE, false, true, UnitProperties.TRANSPORT_GOLD_COST, UnitProperties.TRANSPORT_WOOD_COST,
                            UnitProperties.TRANSPORT_OIL_COST, UnitProperties.TRANSPORT_RANGE, UnitProperties.TRANSPORT_SIGHT, position, UnitProperties.TRANSPORT_BUILD_TIME, ut, UnitProperties.TRANSPORT_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.TRANSPORT_SPRITE_SIZE;
                        break;
                    case UnitType.Battleship:
                        unit = new Unit(UnitProperties.BATLLESHIP_HP, UnitProperties.BATLLESHIP_MANA, UnitProperties.BATLLESHIP_SPEED, UnitProperties.BATLLESHIP_DAMAGE, false, true, UnitProperties.BATLLESHIP_GOLD_COST, UnitProperties.BATLLESHIP_WOOD_COST,
                            UnitProperties.BATLLESHIP_OIL_COST, UnitProperties.BATLLESHIP_RANGE, UnitProperties.BATLLESHIP_SIGHT, position, UnitProperties.BATLLESHIP_BUILD_TIME, ut, UnitProperties.BATLLESHIP_ARMOR, owningPlayer);
                        unit.spriteSize = UnitProperties.BATTLESHIP_SPRITE_SIZE;
                        break;
                    default: // fantassin if not found
                        unit = new Unit(UnitProperties.FANTASSIN_HP, UnitProperties.FANTASSIN_MANA, UnitProperties.FANTASSIN_SPEED, UnitProperties.FANTASSIN_DAMAGE, false, false, UnitProperties.FANTASSIN_GOLD_COST, UnitProperties.FANTASSIN_WOOD_COST,
                            UnitProperties.FANTASSIN_OIL_COST, UnitProperties.FANTASSIN_RANGE, UnitProperties.FANTASSIN_SIGHT, position, UnitProperties.FANTASSIN_BUILD_TIME, ut, UnitProperties.FANTASSIN_ARMOR, owningPlayer);
                        break;
                }
            }
            else
            {
                // TODO

                switch (ut)
                {
                    case UnitType.BallistaTower:

                        unit = new Building(UnitProperties.BALLISTATOWER_HP, -1, UnitProperties.BALLISTATOWER_SPEED, 0, false, false, UnitProperties.BALLISTATOWER_GOLD_COST, UnitProperties.BALLISTATOWER_WOOD_COST,
         UnitProperties.BALLISTATOWER_OIL_COST, 6, UnitProperties.BALLISTATOWER_SIGHT, position, UnitProperties.BALLISTATOWER_BUILD_TIME, ut, UnitProperties.BALLISTATOWER_ARMOR, owningPlayer);
                        break;
                    case UnitType.CannonTower:

                        unit = new Building(UnitProperties.CANNONTOWER_HP, -1, UnitProperties.CANNONTOWER_SPEED, 0, false, false, UnitProperties.CANNONTOWER_GOLD_COST, UnitProperties.CANNONTOWER_WOOD_COST,
         UnitProperties.CANNONTOWER_OIL_COST, 7, UnitProperties.CANNONTOWER_SIGHT, position, UnitProperties.CANNONTOWER_BUILD_TIME, ut, UnitProperties.CANNONTOWER_ARMOR, owningPlayer);
                        break;
                    case UnitType.Church:

                        unit = new Building(UnitProperties.CHURCH_HP, -1, UnitProperties.CHURCH_SPEED, 0, false, false, UnitProperties.CHURCH_GOLD_COST, UnitProperties.CHURCH_WOOD_COST,
         UnitProperties.CHURCH_OIL_COST, 0, UnitProperties.CHURCH_SIGHT, position, UnitProperties.CHURCH_BUILD_TIME, ut, UnitProperties.CHURCH_ARMOR, owningPlayer);
                        break;
                    case UnitType.Farm:

                        unit = new Building(UnitProperties.FARM_HP, -1, UnitProperties.FARM_SPEED, 0, false, false, UnitProperties.FARM_GOLD_COST, UnitProperties.FARM_WOOD_COST,
         UnitProperties.FARM_OIL_COST, 0, UnitProperties.FARM_SIGHT, position, UnitProperties.FARM_BUILD_TIME, ut, UnitProperties.FARM_ARMOR, owningPlayer);
                        break;
                    case UnitType.Forge:

                        unit = new Building(UnitProperties.FORGE_HP, -1, UnitProperties.FORGE_SPEED, 0, false, false, UnitProperties.FORGE_GOLD_COST, UnitProperties.FORGE_WOOD_COST,
         UnitProperties.FORGE_OIL_COST, 0, UnitProperties.FORGE_SIGHT, position, UnitProperties.FORGE_BUILD_TIME, ut, UnitProperties.FORGE_ARMOR, owningPlayer);
                        break;
                    case UnitType.Fortress:

                        unit = new Building(UnitProperties.FORTRESS_HP, -1, UnitProperties.FORTRESS_SPEED, 0, false, false, UnitProperties.FORTRESS_GOLD_COST, UnitProperties.FORTRESS_WOOD_COST,
         UnitProperties.FORTRESS_OIL_COST, 0, UnitProperties.FORTRESS_SIGHT, position, UnitProperties.FORTRESS_BUILD_TIME, ut, UnitProperties.FORTRESS_ARMOR, owningPlayer);
                        break;
                    case UnitType.Foundry:

                        unit = new Building(UnitProperties.FOUNDRY_HP, -1, UnitProperties.FOUNDRY_SPEED, 0, false, false, UnitProperties.FOUNDRY_GOLD_COST, UnitProperties.FOUNDRY_WOOD_COST,
         UnitProperties.FOUNDRY_OIL_COST, 0, UnitProperties.FOUNDRY_SIGHT, position, UnitProperties.FOUNDRY_BUILD_TIME, ut, UnitProperties.FOUNDRY_ARMOR, owningPlayer);
                        break;
                    case UnitType.GoblinWorkBench:

                        unit = new Building(UnitProperties.GOBLIN_WORKBENCH_HP, -1, UnitProperties.GOBLIN_WORKBENCH_SPEED, 0, false, false, UnitProperties.GOBLIN_WORKBENCH_GOLD_COST, UnitProperties.GOBLIN_WORKBENCH_WOOD_COST,
         UnitProperties.GOBLIN_WORKBENCH_OIL_COST, 0, UnitProperties.GOBLIN_WORKBENCH_SIGHT, position, UnitProperties.GOBLIN_WORKBENCH_BUILD_TIME, ut, UnitProperties.GOBLIN_WORKBENCH_ARMOR, owningPlayer);
                        break;
                    case UnitType.GryffinTower:

                        unit = new Building(UnitProperties.GRYFFINTOWER_HP, -1, UnitProperties.GRYFFINTOWER_SPEED, 0, false, false, UnitProperties.GRYFFINTOWER_GOLD_COST, UnitProperties.GRYFFINTOWER_WOOD_COST,
         UnitProperties.GRYFFINTOWER_OIL_COST, 0, UnitProperties.GRYFFINTOWER_SIGHT, position, UnitProperties.GRYFFINTOWER_BUILD_TIME, ut, UnitProperties.GRYFFINTOWER_ARMOR, owningPlayer);
                        break;
                    case UnitType.LumberMill:

                        unit = new Building(UnitProperties.LUMBERMILL_HP, -1, UnitProperties.LUMBERMILL_SPEED, 0, false, false, UnitProperties.LUMBERMILL_GOLD_COST, UnitProperties.LUMBERMILL_WOOD_COST,
         UnitProperties.LUMBERMILL_OIL_COST, 0, UnitProperties.LUMBERMILL_SIGHT, position, UnitProperties.LUMBERMILL_BUILD_TIME, ut, UnitProperties.LUMBERMILL_ARMOR, owningPlayer);
                        break;
                    case UnitType.MageTower:

                        unit = new Building(UnitProperties.MAGETOWER_HP, -1, UnitProperties.MAGETOWER_SPEED, 0, false, false, UnitProperties.MAGETOWER_GOLD_COST, UnitProperties.MAGETOWER_WOOD_COST,
         UnitProperties.MAGETOWER_OIL_COST, 0, UnitProperties.MAGETOWER_SIGHT, position, UnitProperties.MAGETOWER_BUILD_TIME, ut, UnitProperties.MAGETOWER_ARMOR, owningPlayer);
                        break;
                    case UnitType.MilitaryBase:

                        unit = new Building(UnitProperties.MILITARYBASE_HP, -1, UnitProperties.MILITARYBASE_SPEED, 0, false, false, UnitProperties.MILITARYBASE_GOLD_COST, UnitProperties.MILITARYBASE_WOOD_COST,
         UnitProperties.MILITARYBASE_OIL_COST, 0, UnitProperties.MILITARYBASE_SIGHT, position, UnitProperties.MILITARYBASE_BUILD_TIME, ut, UnitProperties.MILITARYBASE_ARMOR, owningPlayer);
                        break;
                    case UnitType.NavalChantier:

                        unit = new Building(UnitProperties.CHANTIERNAVAL_HP, -1, UnitProperties.CHANTIERNAVAL_SPEED, 0, false, false, UnitProperties.CHANTIERNAVAL_GOLD_COST, UnitProperties.CHANTIERNAVAL_WOOD_COST,
         UnitProperties.CHANTIERNAVAL_OIL_COST, 0, UnitProperties.CHANTIERNAVAL_SIGHT, position, UnitProperties.CHANTIERNAVAL_BUILD_TIME, ut, UnitProperties.CHANTIERNAVAL_ARMOR, owningPlayer);
                        break;
                    case UnitType.NavalPlatform:

                        unit = new Building(UnitProperties.NAVALPLATFORM_HP, -1, UnitProperties.NAVALPLATFORM_SPEED, 0, false, false, UnitProperties.NAVALPLATFORM_GOLD_COST, UnitProperties.NAVALPLATFORM_WOOD_COST,
         UnitProperties.NAVALPLATFORM_OIL_COST, 0, UnitProperties.NAVALPLATFORM_SIGHT, position, UnitProperties.NAVALPLATFORM_BUILD_TIME, ut, UnitProperties.NAVALPLATFORM_ARMOR, owningPlayer);
                        break;
                    case UnitType.Raffinery:

                        unit = new Building(UnitProperties.RAFINNERY_HP, -1, UnitProperties.RAFINNERY_SPEED, 0, false, false, UnitProperties.RAFINNERY_GOLD_COST, UnitProperties.RAFINNERY_WOOD_COST,
         UnitProperties.RAFINNERY_OIL_COST, 0, UnitProperties.RAFINNERY_SIGHT, position, UnitProperties.RAFINNERY_BUILD_TIME, ut, UnitProperties.RAFINNERY_ARMOR, owningPlayer);
                        break;
                    case UnitType.Stable:

                        unit = new Building(UnitProperties.STABLE_HP, -1, UnitProperties.STABLE_SPEED, 0, false, false, UnitProperties.STABLE_GOLD_COST, UnitProperties.STABLE_WOOD_COST,
         UnitProperties.STABLE_OIL_COST, 0, UnitProperties.STABLE_SIGHT, position, UnitProperties.STABLE_BUILD_TIME, ut, UnitProperties.STABLE_ARMOR, owningPlayer);
                        break;
                    case UnitType.StrongHold:

                        unit = new Building(UnitProperties.STRONGHOLD_HP, -1, UnitProperties.STRONGHOLD_SPEED, 0, false, false, UnitProperties.STRONGHOLD_GOLD_COST, UnitProperties.STRONGHOLD_WOOD_COST,
         UnitProperties.STRONGHOLD_OIL_COST, 0, UnitProperties.STRONGHOLD_SIGHT, position, UnitProperties.STRONGHOLD_BUILD_TIME, ut, UnitProperties.STRONGHOLD_ARMOR, owningPlayer);
                        break;
                    case UnitType.Tower:

                        unit = new Building(UnitProperties.TOWER_HP, -1, UnitProperties.TOWER_SPEED, 0, false, false, UnitProperties.TOWER_GOLD_COST, UnitProperties.TOWER_WOOD_COST,
         UnitProperties.TOWER_OIL_COST, 0, UnitProperties.TOWER_SIGHT, position, UnitProperties.TOWER_BUILD_TIME, ut, UnitProperties.TOWER_ARMOR, owningPlayer);
                        break;
                    case UnitType.TownHall:

                        unit = new Building(UnitProperties.TOWNHALL_HP, -1, UnitProperties.TOWNHALL_SPEED, 0, false, false, UnitProperties.TOWNHALL_GOLD_COST, UnitProperties.TOWNHALL_WOOD_COST,
         UnitProperties.TOWNHALL_OIL_COST, 0, UnitProperties.TOWNHALL_SIGHT, position, UnitProperties.TOWNHALL_BUILD_TIME, ut, UnitProperties.TOWNHALL_ARMOR, owningPlayer);
                        break;

                    default:
                        unit = new Building(UnitProperties.FARM_HP, UnitProperties.FANTASSIN_MANA, UnitProperties.FANTASSIN_SPEED, UnitProperties.FANTASSIN_DAMAGE, false, false, UnitProperties.FANTASSIN_GOLD_COST, UnitProperties.FANTASSIN_WOOD_COST,
        UnitProperties.FANTASSIN_OIL_COST, 0, UnitProperties.FANTASSIN_SIGHT, position, UnitProperties.FANTASSIN_BUILD_TIME, ut, UnitProperties.FANTASSIN_ARMOR, owningPlayer);

                        break;
                }

            }

            return unit;
        }



        internal void updateCollisions(GameTime gameTime)
        {
            GoldResources.RemoveAll(g => g.amount <= 0);
            OilResources.RemoveAll(g => g.amount <= 0);

            foreach (Resource r in GoldResources)
            {
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        groundTraversableMapTiles[(int)Math.Round(Math.Round(r.Position.X) + i), (int)Math.Round(Math.Round(r.Position.Y) + j)] = false;
                    }
                }
            
            }
            for (int i = 0; i < mapsize; i++)
                for (int j = 0; j < mapsize; j++)
                {

                    waterTraversableMapTiles[i, j] = mapAreas.stGridsWater[i,j] != 0;
                    groundTraversableMapTiles[i, j] = mapAreas.stGridsGround[i,j] != 0;
                    flyingTraversableMapTiles[i, j] = true;
                }
        

            foreach (Unit u in units)
            {

                if ((int)(u.type) >= Unit.largebuildingindex)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        for (int j = 0; j < 4; j++)
                        {
                            if (Map.Inbounds((int)Math.Round(u.position.X) + i, (int)Math.Round(u.position.Y) + j))
                            {
                                groundTraversableMapTiles[(int)Math.Round(u.position.X) + i, (int)Math.Round(u.position.Y) + j] = false;
                            }
                        }
                    }
                }
                else if ((int)(u.type) >= Unit.mediumbuildingindex)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            if (Map.Inbounds((int)Math.Round(u.position.X) + i, (int)Math.Round(u.position.Y) + j))
                            {
                                groundTraversableMapTiles[(int)Math.Round(Math.Round(u.position.X) + i), (int)Math.Round(Math.Round(u.position.Y) + j)] = false;
                                waterTraversableMapTiles[(int)Math.Round(u.position.X) + i, (int)Math.Round(u.position.Y) + j] = false;
                            }
                        }
                    }
                }
                else if ((int)u.type >= Unit.littlebuildingindex)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            if (Map.Inbounds((int)Math.Round(u.position.X) + i, (int)Math.Round(u.position.Y) + j))
                                groundTraversableMapTiles[(int)Math.Round(u.position.X) + i, (int)Math.Round(u.position.Y) + j] = false
                                    ;
                        }
                    }
                }
                else if (u.sailing && u.path.Count == 0)
                {
                    // TODO : check bounds
                    if (Map.Inbounds((int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)))
                        waterTraversableMapTiles[(int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)] = false;
                    if (Map.Inbounds((int)Math.Round(u.position.X) + 1, (int)Math.Round(u.position.Y)))
                        waterTraversableMapTiles[(int)Math.Round(u.position.X) + 1, (int)Math.Round(u.position.Y)] = false;
                    if (Map.Inbounds((int)Math.Round(u.position.X), (int)Math.Round(u.position.Y) + 1))
                        waterTraversableMapTiles[(int)Math.Round(u.position.X), (int)Math.Round(u.position.Y) + 1] = false;
                    if (Map.Inbounds((int)Math.Round(u.position.X + 1), (int)Math.Round(u.position.Y) + 1))
                        waterTraversableMapTiles[(int)Math.Round(u.position.X) + 1, (int)Math.Round(u.position.Y) + 1] = false;



                }
                else if (u.flying && u.path.Count == 0)
                {
                    if (Map.Inbounds((int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)))
                        flyingTraversableMapTiles[(int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)] = false;
                    if (Map.Inbounds((int)Math.Round(u.position.X) + 1, (int)Math.Round(u.position.Y)))
                        flyingTraversableMapTiles[(int)Math.Round(u.position.X) + 1, (int)Math.Round(u.position.Y)] = false;
                    if (Map.Inbounds((int)Math.Round(u.position.X), (int)Math.Round(u.position.Y) + 1))
                        flyingTraversableMapTiles[(int)Math.Round(u.position.X), (int)Math.Round(u.position.Y) + 1] = false;
                    if (Map.Inbounds((int)Math.Round(u.position.X + 1), (int)Math.Round(u.position.Y) + 1))
                        flyingTraversableMapTiles[(int)Math.Round(u.position.X) + 1, (int)Math.Round(u.position.Y) + 1] = false;

                }
                else if (u.path.Count == 0)
                {
                    if (Map.Inbounds((int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)))
                        groundTraversableMapTiles[(int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)] = false;

                }
            }

            playerZones.Update();

        }

        internal static bool Inbounds(int posx, int posy)
        {
            if (posx >= 0 && posy >= 0 && posx < mapsize && posy < mapsize)
                return true;
            return false;
        }
    }




}


