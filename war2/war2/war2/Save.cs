﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using war2.UnitsRelated;
namespace war2
{
    [Serializable]
    public class SaveGameData
    {
        public bool[,] groundTraversableMapTiles;
        public bool[,] waterTraversableMapTiles;
        public bool[,] flyingTraversableMapTiles;
        public MapElement[,] array; // array containing the map elements as Forest = 0, Water, Mud, Grass, Rock only
        public int mapsize;
        public List<Unit> units;
        public List<Unit> selected;
        public List<Projectile> projectiles;
        public Resource[,] resources;
        public Resource selectedResource;
        public List<Player> players;
        public Vector2[,] mapTextures; // array containing the position of the textures to draw
        public int currentplayer;
        public List<Spell> spells;

    }

    class Save
    {
        SaveGameData saves;

        public Save()
        {}

        public Save(bool[,] groundTraversableMapTiles,
         bool[,] waterTraversableMapTiles,
         bool[,] flyingTraversableMapTiles,
         MapElement[,] array, // array containing the map elements as Forest = 0, Water, Mud, Grass, Rock only
         int mapsize,
         List<Unit> units,
         List<Unit> selected,
         List<Projectile> projectiles,
         Resource[,] resources,
         Resource selectedResource,
         List<Player> players,
         Vector2[,] mapTextures, 
            List<Spell> spells)
        {
            
            saves = new SaveGameData();
            saves.groundTraversableMapTiles = groundTraversableMapTiles;
           saves.waterTraversableMapTiles = waterTraversableMapTiles;
            saves.flyingTraversableMapTiles = flyingTraversableMapTiles;
            saves.array = array;
            saves.mapsize = mapsize;
            saves.mapTextures = mapTextures;
            saves.players = players;
            saves.projectiles = projectiles;
            saves.resources = resources;
            saves.selected = selected;
            saves.selectedResource = selectedResource;
            saves.units = units;
            saves.currentplayer = Game1.currentPLayer;
            saves.spells = spells;
        }

        public void doSave(String name)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(name, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, saves);
            stream.Close();

        }

        public static SaveGameData Load(String name)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(name, FileMode.Open, FileAccess.Read, FileShare.Read);
            SaveGameData svg = (SaveGameData)formatter.Deserialize(stream);
            stream.Close();
            return svg;
        }

    }
}
