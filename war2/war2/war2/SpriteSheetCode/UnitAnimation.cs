﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.UnitsRelated;

namespace war2.SpriteSheetCode
{
    public class UnitAnimation
    {
        private AllSpritePositions currentAnime;
        public Unit un;
        public bool isOrc;
        public Texture2D texture;
        public int intervalBetween;
        Vector2 mapPos;
        private int timer;
        bool fliphorizontal;

        public AllSpritePositions getCurrentAnime()
        {
            return currentAnime;
        }

        public void SetNoAnimation()
        {
            this.currentAnime.changeAction(DisplayAction.Stand);

            if (un.type == UnitType.Paysan)
            {
                if (((Harvester)un).carrying != ResourceType.None)
                {
                    if (((Harvester)un).carrying == ResourceType.Gold)
                        this.currentAnime.changeAction(DisplayAction.StandWithGold);
                    if (((Harvester)un).carrying == ResourceType.Wood)
                        this.currentAnime.changeAction(DisplayAction.StandWithWood);
                }
            }
            else if (un.type == UnitType.Petrolier)
            {
                if (((Harvester)un).carrying != ResourceType.None)
                {
                    if (((Harvester)un).carrying == ResourceType.Oil)
                        this.currentAnime.changeAction(DisplayAction.CarryOil);
                }
            }
        }

        public UnitAnimation(Vector2 mappos, Unit u)
        {

            texture = Game1.unitTextureAssociated[(int)u.type, u.owner]; // unitexture[unitType, OwningPlayer.number] -> spritesheet associée;
            this.mapPos = mappos;
            this.mapPos = new Vector2(mappos.X * 32, mappos.Y * 32);
            this.un = u;
            isOrc = Game1.map.players[u.owner].isOrc;
            timer = 0;
            currentAnime = new AllSpritePositions(u.type, DisplayAction.Stand, isOrc, Orientation.Top);
            if (currentAnime.nextSprites != null)
                intervalBetween = u.attackCooldown / currentAnime.nextSprites.Count();
            else
                intervalBetween = 999999;
        }

        public void Update(GameTime gametime, Unit u)
        {
            int decallage = 0;
            un = u;
            if (currentAnime.nextSprites != null)
                intervalBetween = u.attackCooldown / currentAnime.nextSprites.Count();


            if (currentAnime.action != DisplayAction.Stand && currentAnime.action != DisplayAction.Attack) // cuz attack is updated on (attack() function)
            {
                fliphorizontal = false;


                if (Math.Abs(u.direction.X) < 0.2)
                    u.direction.X = 0;
                if (Math.Abs(u.direction.Y) < 0.2)
                    u.direction.Y = 0;

                if (u.direction.X < 0 && u.direction.Y < 0) // northwest
                {
                    decallage = 1;
                    fliphorizontal = true;

                }
                else if (u.direction.X > 0 && u.direction.Y > 0) // southeast
                {
                    decallage = 3;
                }
                else if (u.direction.X > 0 && u.direction.Y < 0) // northeast
                {
                    decallage = 1;
                }
                else if (u.direction.X < 0 && u.direction.Y > 0) // southwest
                {
                    if (un.type != UnitType.Battleship)
                    {
                        decallage = 3;
                        fliphorizontal = true;

                    }
                }
                else if (u.direction.X < 0) // west
                {
                    decallage = 2;
                    fliphorizontal = true;
                }
                else if (u.direction.X > 0) // east
                {
                    decallage = 2;
                }
                else if (u.direction.Y > 0) // south
                {
                   decallage = 4;
                }
                else if (u.direction.Y < 0) // north
                {
                    decallage = 0;
                }

                if (!(u.direction.X == 0 && u.direction.Y == 0))
                {
                    currentAnime.setFlip(fliphorizontal);

                switch (decallage)
                {

                    case 0: // top
                        currentAnime.changeOrientation(Orientation.Top, fliphorizontal);
                        break;
                    case 1: // topright
                        currentAnime.changeOrientation(Orientation.TopRight, fliphorizontal);
                        break;
                    case 2: // right
                        currentAnime.changeOrientation(Orientation.Right, fliphorizontal);
                        break;
                    case 3: // bottomright
                        currentAnime.changeOrientation(Orientation.BottomRight, fliphorizontal);
                        break;
                    case 4: // bottom
                        currentAnime.changeOrientation(Orientation.Bottom, fliphorizontal);
                        break;
                    default:
                        break;
                }
            }

            }


            mapPos = new Vector2(u.position.X * 32, u.position.Y * 32);

            timer += gametime.ElapsedGameTime.Milliseconds;



            if (timer > intervalBetween)
            {
                timer = 0;
                currentAnime.goToNextStep();

            }

        }

        

        public void Draw(SpriteBatch spbatch, Texture2D textureCadavreSheet = null)
        {
            if (textureCadavreSheet == null)
                currentAnime.Draw(texture, spbatch, mapPos, (int) un.spriteSize.X, (int) un.spriteSize.Y);
            else
                currentAnime.Draw(textureCadavreSheet, spbatch, mapPos, (int)un.spriteSize.X, (int)un.spriteSize.Y);

        }
    }
      
}