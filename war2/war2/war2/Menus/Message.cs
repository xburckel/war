﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.Menus
{
    public class Message
    {
        protected string textBuffer;
        protected string text;
        protected int duration;
        protected KeyboardState prevKstate;
        protected bool isEnterPressed;

        public Message(string text)
        {
            duration = 5000;
            this.text = text;
            prevKstate = Keyboard.GetState();
            isEnterPressed = false;
            textBuffer = "";
        }


        public virtual void Update(GameTime gameTime)
        {
            if (!Game1.isInMainMenu && !Game1.isInMenu)
            {
                KeyboardState kstate = Keyboard.GetState();
                if (!isEnterPressed && kstate.IsKeyDown(Keys.Enter) && prevKstate.IsKeyUp(Keys.Enter)) // start a new message
                {
                    isEnterPressed = true;
                    textBuffer = "";
                }
                else if (isEnterPressed && kstate.IsKeyDown(Keys.Enter) && prevKstate.IsKeyUp(Keys.Enter)) // end a new message
                {
                    if (Game1.statics.allCheatCodesList.isACheatCode(textBuffer))
                    {
                        Game1.statics.allCheatCodesList.applyCheat(textBuffer);
                        textBuffer = "Cheat Enabled";
                    }
                    UpdateMessage(textBuffer);
                    textBuffer = "";
                    isEnterPressed = false;
                }
                else if (kstate.GetPressedKeys().Count() > 0)
                {
                    bool shiftPressed = false;

                    foreach (Keys key in kstate.GetPressedKeys())
                    {
                        if (key == Keys.LeftShift || key == Keys.RightShift)
                        {
                            shiftPressed = true;
                        }
                    }

                    foreach (Keys key in kstate.GetPressedKeys())
                    {
                        if (!prevKstate.GetPressedKeys().Contains(key))
                        {
                            if (key >= Keys.D0 && key <= Keys.D9 && shiftPressed)
                            {
                                    textBuffer += (key.ToString().Remove(0, 1)); // remove the "D"
                                // Number keys pressed so need to so special processing
                                // also check if shift pressed
                            }
                             else if (key >= Keys.NumPad0 && key <= Keys.NumPad9)
                            {
                                 textBuffer += (key.ToString().Remove(0, 6)); // remove the "D"
                            }
                             else if (key == Keys.Space)
                            {
                                textBuffer += " ";
                            }
                            else if (key >= Keys.A && key <= Keys.Z)
                            {
                                if (shiftPressed)
                                    textBuffer += (key.ToString()).ToUpper();
                                else
                                    textBuffer += key.ToString().ToLower();
                            }
                            else if (key == Keys.Back && textBuffer.Length != 0)
                            {
                                textBuffer = textBuffer.Remove(textBuffer.Length - 1);
                            }
                        }
                    }
                }


                if (duration > 0)
                    duration -= gameTime.ElapsedGameTime.Milliseconds;


                prevKstate = kstate;
            }
        }

        public virtual void Draw(SpriteBatch spbatch)
        {
            if (!Game1.isInMainMenu && !Game1.isInMenu)
            {
                if (duration > 0)
                    spbatch.DrawString(TopPanel.font, text, new Vector2(LeftPanel.LEFTPANEL_WIDTH + 100, Game1.graphics.PreferredBackBufferHeight - 100), Color.Gold);
                if (isEnterPressed)
                    spbatch.DrawString(TopPanel.font, textBuffer + "_", new Vector2(LeftPanel.LEFTPANEL_WIDTH + 100, Game1.graphics.PreferredBackBufferHeight - 80), Color.Gold);
            }
        }

        public virtual void UpdateMessage(string text)
        {
            duration = 5000;
            this.text = text;
        }



    }
}
