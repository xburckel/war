﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.PathFindingRelated
{
    public class HandleCollisions
    {
        public List<Unit>[,] groundUnits;
        public List<Unit>[,] airUnits;
        public List<Unit>[,] sailUnits;
        Dictionary<Unit, int> unitsToAgents;

        public HandleCollisions()
        {
            groundUnits = new List<Unit>[Map.mapsize, Map.mapsize];
            airUnits = new List<Unit>[Map.mapsize, Map.mapsize];
            sailUnits = new List<Unit>[Map.mapsize, Map.mapsize];
            unitsToAgents = new Dictionary<Unit, int>();
        }

        /**
         * Returns list of units at pos or empty list if nothing
         * */
        public static List<Unit> UnitsAtPos(int posX, int posY, bool ground = true, bool sail = false, bool fly = false)
        {
            if (Map.Inbounds(posX, posY))
            {

                if (ground)
                    return Game1.statics.collisionUpdates.groundUnits[posX, posY];
                if (sail)
                    return Game1.statics.collisionUpdates.sailUnits[posX, posY];
                if (fly)
                    return Game1.statics.collisionUpdates.airUnits[posX, posY];
            }

            return new List<Unit>();
        }

        /** Returns the biggest unit at pos that is not [thisunit]
         * Or null if nothing found
         * */
        public static Unit BiggestUnitAtPos(int posX, int posY, Unit thisUnit = null, bool ground = true, bool sail = false, bool fly = false)
        {
            List<Unit> units = UnitsAtPos(posX, posY, ground, sail, fly);
            if (units.Count == 0)
                return null;
            units.Sort((x, y) => x.spriteSize.X.CompareTo(y.spriteSize.X));
            if (units.First() != thisUnit)
                return units.First();
            if (units.Count == 1)
                return null;
            return units[1];
        }

        private int RoundToOne(float t)
        {
            if (t > 0)
                return 1;
            if (t < 0)
                return -1;
            return 0;
        }

    /**
     * Returns the unit at specified position, or null if the only unit is thisOwnUnit or there is nothing
     */
         public static Unit UnitAtPos(int i, int j, Unit thisOwnUnit = null, bool ground = true, bool sailing = false, bool air = false)
        {
            if (!Map.Inbounds(i, j))
                return null;

            List<Unit> atPos = UnitsAtPos(i, j, ground, sailing, air);

            foreach (Unit u in atPos)
            {
                if (u != thisOwnUnit)
                    return u;
            }

            return null;
        }


         /**
          * Checks if something is blocking a tile
          */
         public static bool SomethingBlockingAtDest(int i, int j, bool ground = true, bool sailing = false, bool air = false)
         {
             Unit toret = UnitAtPos(i, j, null, ground, sailing, air);

             if (toret != null)
                 return true;

             if (ground)
             {
                 foreach (Resource r in Game1.map.GoldResources)
                 {
                     for (int k = 0; k < 3; k++)
                     {
                         for (int v = 0; v < 3; v++)
                         {
                             if (Map.Inbounds(i - k, j - v))
                             {
                                 /*
                                  * -3 -2 -1 0
                                  * -2 -2 -1 0
                                  * -1 -1 -1 0
                                  *  0  0  0 0
                                  */
                                 if ((int)r.Position.X == (i - k) && (int)r.Position.Y == (j - v))
                                 {
                                     return true;
                                 }
                             }

                         }
                     }
                 }
                 if (Game1.map.resources[i, j] != null && Game1.map.resources[i, j].type == ResourceType.Wood)
                     return true;
             }
             return false;
         }



        /**
         * Foreach unit:
         * Check if it is about to collide with another unit
         * Update direction appropriately, taking into account next grid pos
         */
        private void HandleAllunitsCollisions(GameTime gameTime)
        {
             foreach (Unit u in Game1.map.units)
             {
                 u.stopMove += gameTime.ElapsedGameTime.Milliseconds;
                 if (u.path.Count > 0 && u.stopMove >= 0) // stopMove negative means collisionning so not moving until its solved
                 {
                     int directionX = RoundToOne(u.path.First().x - u.position.X);
                     int directionY = RoundToOne(u.path.First().y - u.position.Y);

                     double movement =  (u.speed * gameTime.ElapsedGameTime.Milliseconds) / 1000d;

                     if (Math.Abs(u.position.X - u.path.First().x) <= movement)
                         directionX = 0;
                     if (Math.Abs(u.position.Y - u.path.First().y) <= movement)
                         directionY = 0;

                     if (Math.Round(u.position.X) != Math.Round(u.position.X + directionX * movement) // If the unit is gonna change tile, check collisions
                         || Math.Round(u.position.Y) != Math.Round(u.position.Y + directionY * movement))
                     {
                         // Check for a unit on the next tiles to be reached
                         Unit bottomOrTop = (HandleCollisions.BiggestUnitAtPos((int)Math.Round(u.position.X), (int)Math.Round(u.position.Y + directionY * movement), u));
                         Unit rightOrLeft = (HandleCollisions.BiggestUnitAtPos((int)Math.Round(u.position.X + directionX * movement), (int)Math.Round(u.position.Y), u));
                         Unit corner = (HandleCollisions.BiggestUnitAtPos((int)Math.Round(u.position.X + directionX * movement), (int)Math.Round(u.position.Y + directionY * movement), u));

                         // IF in one direction is blocked, don't move in that direction (contourner)
                         // Diagonal move
                         if (u.type != UnitType.Paysan && u.type != UnitType.Petrolier) // They ignore collisions with other units
                         {
                             if (corner == null && directionY != 0 && directionX != 0)
                             {
                                 // just keep moving in diagonal then
                             }
                             else
                             {
                                 if (rightOrLeft != null)
                                     directionX = 0;
                                 if (bottomOrTop != null)
                                     directionY = 0;
                             }
                         }
                         else // special case for harvesters : ignore collision with other units because it's soooo messy
                         {
                             if ((corner == null || !corner.isABuilding) && directionY != 0 && directionX != 0)
                             {
                                 // just keep moving in diagonal then
                             }
                             else
                             {
                                 if (rightOrLeft != null && rightOrLeft.isABuilding)
                                     directionX = 0;
                                 if (bottomOrTop != null && bottomOrTop.isABuilding)
                                     directionY = 0;
                             }
                             if (directionY == 0 && directionX == 0 && u.owner != Game1.currentPLayer) // if it is an AI harvester, allow it to go through buildings sometimes 
                             {
                                 directionX = RoundToOne(u.path.First().x - u.position.X);
                                 directionY = RoundToOne(u.path.First().y - u.position.Y);
                             }
                         }
                     }
                     u.direction = new Vector2(directionX, directionY);
                 }
             }
        }
        


        /**
         * Updates each array with the list of each units on which map tile 
         */
        public void Update(GameTime gameTime)
        {
            groundUnits = Initialize(this.groundUnits);
            sailUnits = Initialize(this.sailUnits);
            airUnits = Initialize(this.airUnits);

            foreach (Unit u in Game1.map.units)
            {
                if (u.isABuilding)
                {
                    for (int i = 0; i < u.spriteSize.X / 32; i++)
                        for (int j = 0; j < u.spriteSize.X / 32; j++)
                    {
                        groundUnits[i + (int) u.position.X, j + (int) u.position.Y].Add(u);
                    }
                }
                else
                {
                    if (u.sailing)
                        AddToList(u, false, true);
                    else if (u.flying)
                        AddToList(u, false, false, true);
                    else
                        AddToList(u);
                }
            }

            HandleAllunitsCollisions(gameTime);
        }

        /**
         * Fills all arrays with empty lists
         */
        private List<Unit>[,] Initialize(List<Unit>[,] toInit)
        {
            for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                {
                    toInit[i, j] = new List<Unit>();
                }

            return toInit;
       }

        /**
         * Adds a unit to an array
         */
        private void AddToList(Unit u, bool ground = true, bool sail = false, bool air = false)
        {
            if (ground)
                groundUnits[(int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)].Add(u);
            else if (sail)
                sailUnits[(int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)].Add(u);
            else if (air)
                airUnits[(int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)].Add(u);
        }

        /**
         * Returns true if a & b should collide
         * */
        public bool ShouldCollision(Unit a, Unit b)
        {
            /** 1   2   3   4
             *1 b  X
             *2 X  X   a 
             *3 
             *
             * a(3.3, 2 veut aller en 2.2, 3) ; un batiment de 64 prend 1 et 2 
             * On check que a.x > b.x + b.ss && a.x + a.ss < b.x
             * On check que a.y > b.y +b.ss && a.y + a.ss > b.y
             * 
             */
            bool condition = ShouldCollisionX(a, b) && ShouldCollisionY(a, b);
            return condition;

        }

        private bool ShouldCollisionX(Unit a, Unit b)
        {
            return (a.position.X + a.spriteSize.X / 32 >= b.position.X &&
                a.position.X <= (b.position.X + b.spriteSize.X / 32));
        }

        private bool ShouldCollisionY(Unit a, Unit b)
        {
            return (a.position.Y + a.spriteSize.Y / 32) >= b.position.Y &&
                a.position.Y <= b.position.Y + b. spriteSize.Y / 32;
        }
    }
}
