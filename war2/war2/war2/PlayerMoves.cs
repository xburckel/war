﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.UnitsRelated;

namespace war2
{
    enum UpdateTypes
    {
        UnitPosUpdate, UnitUpgrade, UnitAttack, NewBuilding, BuildingCreateUnit, DeadUnits, CancelBuilding, DwarvesDemolish, TransportUnloadRequest, TransportLoadRequest, NewUnit,
        HarvesterEnteringBuilding, HarvesterHarvested,
        Speull
    };


    [Serializable]
    public struct UnitPosUpdate
    {
        public Int64 index;
        public Vector2 position;
        public Vector2 destination;
        public Vector2 direction;
    }
    [Serializable]
    public struct UnitAttack
    {
        public Int64 index;
        public Vector2 position;
        public Int64 targetindex;
    }

    [Serializable]
    public struct NewUnit
    {
        public Int64 index;
        public Vector2 position;
        public UnitType type;
    }

    [Serializable]
    public struct UnitUpgrade
    {
        public Int64 buildingindex;
        public UnitType upgradeType;
    }

    [Serializable]
    public struct NewBuilding
    {
        public Int64 index;
        public Int64 builderIndex;
        public UnitType constructionType;
        public Vector2 position;
    }

    [Serializable]
    public struct HarvestersInBuilding
    {
        public Int64 hindex;
        public Int64 buildingindex;
    }


    [Serializable]
    public struct DeadUnits
    {
        public Int64 index;
    }

    [Serializable]
    public struct CancelBuilding
    {
        public Int64 CancelledBuildingClickIndex;
    }

    [Serializable]
    public struct DwarvesDemolish
    {
        public Int64 dwarfindex;
        public Vector2 dwarfposition;
    }


    [Serializable]
    public struct TransportUnloadRequest
    {
        public Int64 index;
        public Vector2 transportPosition;
    }

    [Serializable]
    public struct TransportLoadRequest
    {
        public Int64 transportindex;
        public Int64 unitIndex;
        public Vector2 unitpos;
        public Vector2 trpos;
    }
    [Serializable]
    public struct SpellCasted
    {
        public Int64 casterIndex;
        public SpellType spellType;
        public Vector2 spellPos;
        public Vector2 casterPos;
    }

    [Serializable]

    public struct HarvesterHarvested
    {
        public Int64 hindex;
        public Vector2 resourcePos;
    }

    [Serializable]
    public class PlayerMoves
    {
        private int pindex;
        public List<UnitPosUpdate> upos = new List<UnitPosUpdate>();
        public List<UnitAttack> attacks = new List<UnitAttack>();
        public List<DeadUnits> deadunits = new List<DeadUnits>();
        public List<NewBuilding> newBuildings = new List<NewBuilding>();
        public List<UnitUpgrade> uupgrades = new List<UnitUpgrade>();
        public List<DwarvesDemolish> dwarvesDemolish = new List<DwarvesDemolish>();
        public List<TransportLoadRequest> trloads = new List<TransportLoadRequest>();
        public List<TransportUnloadRequest> trunload = new List<TransportUnloadRequest>();
        public List<CancelBuilding> cancelBuilding = new List<CancelBuilding>();
        public List<NewUnit> newunits = new List<NewUnit>();
        public List<HarvestersInBuilding> harvestersinbuilding = new List<HarvestersInBuilding>();
        public List<HarvesterHarvested> harvesterHarvesting = new List<HarvesterHarvested>();
        public List<SpellCasted> spellCasted = new List<SpellCasted>();

        public PlayerMoves(int playerIndex)
        {
            pindex = playerIndex;
        }

        /* public void SendNetworkMessagesToHost(NetClient nc, NetServer server = null)
        {

            foreach (UnitPosUpdate up in upos)
            {
                if (server != null)
                    Game1.server.sentcounter++;
                if (nc != null)
                    Client.counter++;

                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte) UpdateTypes.UnitPosUpdate);
                outmsg.Write(up.index);
                outmsg.Write(up.position.X);
                outmsg.Write(up.position.Y);
                outmsg.Write(up.destination.X);
                outmsg.Write(up.destination.Y);
                outmsg.Write(up.direction.X);
                outmsg.Write(up.direction.Y);
                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.Unreliable);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.Unreliable, 0);
            }
            foreach (UnitAttack ua in attacks)
            {
                if (server != null)
                    Game1.server.sentcounter++;
                if (nc != null)
                    Client.counter++;

                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte)UpdateTypes.UnitAttack);
                outmsg.Write(ua.index);
                outmsg.Write(ua.position.X);
                outmsg.Write(ua.position.Y);
                outmsg.Write(ua.targetindex);


                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.Unreliable);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.Unreliable, 0);
            }
            foreach (DeadUnits du in deadunits)
            {
                if (server != null)
                    Game1.server.sentcounter++;
                if (nc != null)
                    Client.counter++;

                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte)UpdateTypes.DeadUnits);
                outmsg.Write(du.index);

                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.ReliableUnordered, 0);

            }

            foreach (NewBuilding nb in newBuildings)
            {
                if (server != null)
                    Game1.server.sentcounter++;
                if (nc != null)
                Client.counter++;

                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte)UpdateTypes.NewBuilding);
                outmsg.Write(nb.index);
                outmsg.Write(nb.position.X);
                outmsg.Write(nb.position.Y);
                outmsg.Write(nb.builderIndex);
                outmsg.Write((int) nb.constructionType);

                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.ReliableUnordered, 0);


            }

            foreach (UnitUpgrade uu in uupgrades)
            {
                if (server != null)
                    Game1.server.sentcounter++;
                if (nc != null)
                Client.counter++;

                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte)UpdateTypes.UnitUpgrade);
                outmsg.Write(uu.buildingindex);
                outmsg.Write((byte) uu.upgradeType);

                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.ReliableUnordered, 0);


            }

            foreach (CancelBuilding cb in cancelBuilding)
            {
                if (server != null)
                    Game1.server.sentcounter++;
                if (nc != null)
                    Client.counter++;

                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte)UpdateTypes.CancelBuilding);
                outmsg.Write(cb.CancelledBuildingClickIndex);

                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.ReliableUnordered, 0);
            }


            foreach (DwarvesDemolish dd in dwarvesDemolish)
            {
                if (nc != null)
                    Client.counter++;
                if (server != null)
                    Game1.server.sentcounter++;
                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte)UpdateTypes.DwarvesDemolish);
                outmsg.Write(dd.dwarfindex);
                outmsg.Write(dd.dwarfposition.X);
                outmsg.Write(dd.dwarfposition.Y);

                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.ReliableUnordered, 0);

            }

            foreach (TransportLoadRequest tlr in trloads)
            {
                if (nc != null)
                    Client.counter++;
                if (server != null)
                    Game1.server.sentcounter++;
                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte)UpdateTypes.TransportLoadRequest);
                outmsg.Write(tlr.transportindex);
                outmsg.Write(tlr.unitIndex);
                outmsg.Write(tlr.trpos.X);
                outmsg.Write(tlr.trpos.Y);


                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.ReliableUnordered, 0);
            }
            foreach (TransportUnloadRequest tur in trunload)
            {
                if (nc != null)
                    Client.counter++;
                if (server != null)
                    Game1.server.sentcounter++;
                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte)UpdateTypes.TransportUnloadRequest);
                outmsg.Write(tur.index);
                outmsg.Write(tur.transportPosition.X);
                outmsg.Write(tur.transportPosition.Y);

                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.ReliableUnordered, 0);
            }

            foreach (NewUnit nu in newunits)
            {
                if (nc != null)
                    Client.counter++;
                if (server != null)
                    Game1.server.sentcounter++;
                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte)UpdateTypes.NewUnit);
                outmsg.Write(nu.index);
                outmsg.Write(nu.position.X);
                outmsg.Write(nu.position.Y);
                outmsg.Write((byte) nu.type);
                outmsg.Write(Game1.currentPLayer);

                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.ReliableUnordered, 0);

            }

            foreach (HarvestersInBuilding hu in harvestersinbuilding)
            {
                if (nc != null)
                    Client.counter++;
                if (server != null)
                    Game1.server.sentcounter++;
                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte)UpdateTypes.HarvesterEnteringBuilding);
                outmsg.Write(hu.hindex);
                outmsg.Write(hu.buildingindex);

                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.ReliableUnordered, 0);

            }

            foreach (SpellCasted sc in spellCasted)
            {
                if (server != null)
                    Game1.server.sentcounter++;
                if (nc != null)
                    Client.counter++;

                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte)UpdateTypes.Speull);
                outmsg.Write(sc.casterIndex);
                outmsg.Write(sc.casterPos.X);
                outmsg.Write(sc.casterPos.Y);
                outmsg.Write(sc.spellPos.X);
                outmsg.Write(sc.spellPos.Y);

                outmsg.Write((byte)sc.spellType);

                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.ReliableUnordered, 0);

            }
            foreach (HarvesterHarvested hh in harvesterHarvesting)
            {
                if (nc != null)
                    Client.counter++;
                if (server != null)
                    Game1.server.sentcounter++;
                NetOutgoingMessage outmsg;
                if (server == null)
                    outmsg = nc.CreateMessage();
                else
                    outmsg = server.CreateMessage();
                outmsg.Write((byte)PacketTypes.UPDATEUNITS);
                outmsg.Write((byte)UpdateTypes.HarvesterHarvested);
                outmsg.Write(hh.hindex);
                outmsg.Write(hh.resourcePos.X);
                outmsg.Write(hh.resourcePos.Y);

                if (server == null && nc != null)
                    nc.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
                else
                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.ReliableUnordered, 0);

            }



        }*/
    }
}
