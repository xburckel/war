﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.UnitsRelated;

namespace war2
{
    [Serializable]
    public class Building : Unit
    {
        public Vector2 spriteSheetPosition;
        public int constructionState;
        // bool canAttack;
        public UnitType unitbuilding;
        public int percentBuilt; // divide by 1000 to use ; TODO: doesnt look like a percent but like total time ; looks bugged
        // seems like it equals 100000 if built, else it equals number of seconds from start...

        public int currentTimeSpentBuilding;


        
        public int upgrading; // over 0 means its currently upgrading, over or equal 100 = finished
        public UnitType upgradingto;
        public Animation animation;
        public Harvester builder;
        public UnitsRelated.SingleAmelioration sa;
        int buildStartTime = 0;

        public Building(int hp, int mana, float speed,
         int damage,
         bool flying,
         bool sailing,
         int goldCost,
         int woodCost,
         int oilCost,
         int range,
         int sight,
         Vector2 position,
        int buildtime,
            UnitType type,
        int armor, int owner)
            : base(hp, mana, speed,
                damage,
                flying,
                sailing,
                goldCost,
                woodCost,
                oilCost,
                range,
                sight,
                position,
                buildtime,
                type,
                armor, owner)
        {
            isABuilding = true;
           
            sa = null;
            builder = null;
            upgradingto = UnitType.None;
            upgrading = 0;
            percentBuilt = 100;
            this.unitbuilding = UnitType.None;
            // this.canAttack = false;
            // if (type.Equals(UnitType.BallistaTower) || type.Equals(UnitType.CannonTower))
            //   this.canAttack = true;

            if (Game1.statics.basicSailingBuildings.Contains(type) || type == UnitType.NavalPlatform)
                this.sailing = true;

            if ((int)type >= littlebuildingindex)
            {
                spriteSize.X = 64;
                spriteSize.Y = 64;
            }
            if ((int)type >= mediumbuildingindex)
            {
                spriteSize.X = 96;
                spriteSize.Y = 96;
            }
            if ((int)type >= largebuildingindex)
            {
                spriteSize.X = 128;
                spriteSize.Y = 128;
            }

            if (!Game1.map.players[owner].isOrc)
            {
                spriteSheetPosition.X = UnitProperties.getBuildingProperties(type)[(int)BuildingProperties.HUMANSPRITEPOSX];
                spriteSheetPosition.Y = UnitProperties.getBuildingProperties(type)[(int)BuildingProperties.HUMANSPRITEPOSY];
            }
            else
            {
                spriteSheetPosition.X = UnitProperties.getBuildingProperties(type)[(int)BuildingProperties.ORCSPRITEPOSX];
                spriteSheetPosition.Y = UnitProperties.getBuildingProperties(type)[(int)BuildingProperties.ORCSPRITEPOSY];
            }

            if (this.type != UnitType.Tower && this.type != UnitType.CannonTower && this.type != UnitType.BallistaTower)
                this.sight = 4;
            else
                this.sight = 9;
            animation = new Animation(AnimationType.None, this.position * 32);

            CanAttackGround = false;
            CanAttackAir = false;
            CanAttackWater = false;
            if (this.type == UnitType.BallistaTower)
            {
                CanAttackGround = true;
                CanAttackWater = true;
                CanAttackAir = true;
            }
            if (this.type == UnitType.CannonTower)
            {
                CanAttackGround = true;
                CanAttackWater = true;
            }

            currentTimeSpentBuilding = 0;
        }

        public override void Draw(SpriteBatch spriteBatch, Texture2D texture, Vector2 pos)
        {
            Vector2 pos2 = pos; // pos = pos + leger decallage
            if (!isBuilt()) // trying to center construction texture
            {
                if ((int)type >= largebuildingindex)
                {
                    pos2.X += spriteSize.X / 3;
                    pos2.Y += spriteSize.X / 3;
                }
                else if ((int)type >= mediumbuildingindex)
                {
                    pos2.X += spriteSize.X / 3;
                    pos2.Y += spriteSize.X / 3;
                }
                else if ((int)type >= littlebuildingindex)
                {
                    pos2.X += spriteSize.X / 5;
                    pos2.Y += spriteSize.X / 5;
                }
            }
            if (isBuilt())
            {
                spriteBatch.Draw(Game1.unitTextureAssociated[(int)type, owner], pos2, new Rectangle((int)Math.Round(this.spriteSheetPosition.X), (int)Math.Round(this.spriteSheetPosition.Y), (int)Math.Round(this.spriteSize.X), (int)Math.Round(this.spriteSize.Y)), Color.White);
            }
            else if (percentBuilt <= 70)
            {
                if (type != UnitType.Foundry && type != UnitType.NavalChantier && type != UnitType.Raffinery && type != UnitType.NavalPlatform)
                {
                    spriteBatch.Draw(Game1.ResourceTexture, pos2, new Rectangle(590, 143, 64, 64), Color.White);
                }
                else
                    spriteBatch.Draw(Game1.ResourceTexture, pos2, new Rectangle(443, 205, 64, 64), Color.White);

            }
            else if (percentBuilt <= 45)
            {
                if (type != UnitType.Foundry && type != UnitType.NavalChantier && type != UnitType.Raffinery && type != UnitType.NavalPlatform)
                {
                    spriteBatch.Draw(Game1.ResourceTexture, pos2, new Rectangle(580, 200, 64, 64), Color.White);
                }
                else
                    spriteBatch.Draw(Game1.ResourceTexture, pos2, new Rectangle(437, 134, 64, 64), Color.White);
            }
            else
            {

                if (Game1.map.players[owner].isOrc)
                    spriteBatch.Draw(Game1.unitTextureAssociated[(int)this.type, owner], pos, new Rectangle(UnitProperties.getBuildingProperties(this.type)[(int)BuildingProperties.ORCHALFBUILDSPRITEPOSX],
                        UnitProperties.getBuildingProperties(this.type)[(int)BuildingProperties.ORCHALFBUILDSPRITEPOSY], (int)spriteSize.X, (int)spriteSize.Y), Color.White);
                else
                    spriteBatch.Draw(Game1.unitTextureAssociated[(int)this.type, owner], pos, new Rectangle(UnitProperties.getBuildingProperties(this.type)[(int)BuildingProperties.HUMANHALFBUILDSPRITEPOSX],
                        UnitProperties.getBuildingProperties(this.type)[(int)BuildingProperties.HUMANHALFBUILDSPRITEPOSY], (int)spriteSize.X, (int)spriteSize.Y), Color.White);
            }

            if (animation.ant != AnimationType.None)
                animation.Draw(spriteBatch);

        }
        public override void Update(Map map, GameTime gameTime)
        {
            if (this.hp < UnitProperties.getBuildingProperties(type)[(int)BuildingProperties.HP] / 3 && isBuilt())
            {
                if (animation.ant == AnimationType.None || animation.ant == AnimationType.littleFire)
                {
                    animation = new Animation(AnimationType.bigFire, this.position * 32 + (this.spriteSize / 4));
                }
            }
            else if (this.hp <= (UnitProperties.getBuildingProperties(type)[(int)BuildingProperties.HP] / 4) * 3 && isBuilt())
            {
                if (animation.ant == AnimationType.None || animation.ant == AnimationType.bigFire)
                {
                    animation = new Animation(AnimationType.littleFire, this.position * 32 + (this.spriteSize / 4));
                }
            }
            else if (animation != null)
                animation.ant = AnimationType.None;


            base.Update(map, gameTime);
            if (currentTimeSpentBuilding / 1000 < this.buildtime && percentBuilt < 100)
            {
                if (builder != null)
                    builder.disabled = 1000; // disable him
                sight = (int)spriteSize.X / 32;
                currentTimeSpentBuilding += gameTime.ElapsedGameTime.Milliseconds;
                buildStartTime += gameTime.ElapsedGameTime.Milliseconds;
                if (buildStartTime > 1000) // update every second
                {
                    buildStartTime = 0;
                    hp += UnitProperties.getBuildingProperties(type)[(int)(BuildingProperties.HP)] / UnitProperties.getBuildingProperties(type)[(int)(BuildingProperties.BUILDTIME)];
                }
                percentBuilt = currentTimeSpentBuilding * 100 / (this.buildtime * 1000);
                if (percentBuilt == 100)
                    percentBuilt = 101; // so if by chance its just 100%, we will still go to the following else loop :
            }
            else if (percentBuilt != 100) // we finish the building
            {
                FinishConstrution(gameTime);
            }
            if (upgradingto != UnitType.None)
            {
                HandleUpgrade(gameTime);
            }

            if (sa != null)
            {
                // build complete

                sa.Update(gameTime);
                if (sa.IsResearched())
                    sa = null;
            }



            animation.Update(gameTime);

        }

        private void HandleUpgrade(GameTime gameTime)
        {
            if (upgrading / 1000 <= this.buildtime) // TODO : not this.buildtime
                upgrading += gameTime.ElapsedGameTime.Milliseconds;
            else // build complete
            {
                upgrading = 0;
                this.type = upgradingto;
                List<int> tmp = UnitProperties.getBuildingProperties(upgradingto);
                this.hp = tmp[(int)BuildingProperties.HP];
                this.sight = tmp[(int)BuildingProperties.SIGHT];
                this.damage = 0;
                this.range = 0;
                this.armor = tmp[(int)BuildingProperties.ARMOR];
                if (!Game1.map.players[owner].isOrc)
                {
                    this.spriteSheetPosition.X = tmp[(int)BuildingProperties.HUMANSPRITEPOSX];
                    this.spriteSheetPosition.Y = tmp[(int)BuildingProperties.HUMANSPRITEPOSY];
                }
                else
                {
                    this.spriteSheetPosition.X = tmp[(int)BuildingProperties.ORCSPRITEPOSX];
                    this.spriteSheetPosition.Y = tmp[(int)BuildingProperties.ORCSPRITEPOSY];
                }
                upgradingto = UnitType.None;

                if (this.type == UnitType.BallistaTower)
                {
                    CanAttackAir = true;
                    CanAttackWater = true;
                    CanAttackGround = true;

                    damage = 9;
                    penetratingDamage = 8;
                    range = 6;
                }
                if (this.type == UnitType.CannonTower)
                {
                    CanAttackGround = true;
                    CanAttackWater = true;
                    damage = 30;
                    penetratingDamage = 20;
                    range = 7;
                }

                Game1.map.players[owner].playerTechs.Update(owner, Game1.map.units);

            }
        }

        private void FinishConstrution(GameTime gametime)
        {
            if (hp > UnitProperties.getBuildingProperties(type)[(int)(BuildingProperties.HP)]) // never know with the block above
                hp = UnitProperties.getBuildingProperties(type)[(int)(BuildingProperties.HP)];
            // free the builder
            if (builder != null)
            {
                builder.hp = UnitProperties.PAYSAN_HP;
                builder.position = builder.findClearSortieSpot(builder.position);
                builder.currentAnimation.Update(gametime, builder);
                builder.destination = builder.position;
                builder.path.Clear();
                builder.disabled = 0;
                builder = null;
            }

            List<int> tmp = UnitProperties.getBuildingProperties(type);
            this.sight = tmp[(int)BuildingProperties.SIGHT];

            this.percentBuilt = 100;
            Game1.map.players[owner].playerTechs.Update(owner, Game1.map.units);
            if (Game1.map.players[owner].isOrc && owner == Game1.currentPLayer)
                Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.orcSE[(int)OrcSE.WorkComplete]);
            else if (owner == Game1.currentPLayer)
                Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.humanSE[(int)HumanSE.WorkComplete]);
        }

        public bool isBuilt()
        {
            return (this.percentBuilt == 100);
        }
        public bool isNotUpgrading()
        {
            return (this.upgradingto == UnitType.None);
        }

        public UnitType Built(GameTime gametime)
        {
            if (unitbuilding != UnitType.None && constructionState / 1000 >= UnitProperties.getUnitProperties(this.unitbuilding, Game1.map.players[owner].isOrc)[(int)Properties.BUILDTIME]) // construction finished
            {
                constructionState = 0;

                return this.unitbuilding;

            }
            else if (unitbuilding != UnitType.None)
            {
                constructionState += gametime.ElapsedGameTime.Milliseconds;
                return UnitType.None;
            }




            return UnitType.None;


        }

        public void Upgrade(UnitType upgradeTo)
        {
            if (Game1.map.players[this.owner].playerTechs.hasTech(upgradeTo) && upgradingto == UnitType.None) // we got the tech for this
            {
                if (this.type == UnitType.Tower && upgradeTo == UnitType.BallistaTower || (this.type == UnitType.Tower && upgradeTo == UnitType.CannonTower)
                    || (this.type == UnitType.StrongHold && upgradeTo == UnitType.Fortress)
                    || (this.type == UnitType.TownHall && upgradeTo == UnitType.StrongHold))
                {

                    if ((owner != Game1.currentPLayer) || 
                        (Game1.map.players[owner].gold >= UnitProperties.getBuildingProperties(upgradeTo)[(int)BuildingProperties.GOLDCOST]
                        && Game1.map.players[owner].wood >= UnitProperties.getBuildingProperties(upgradeTo)[(int)BuildingProperties.WOODCOST]
                    && Game1.map.players[owner].oil >= UnitProperties.getBuildingProperties(upgradeTo)[(int)BuildingProperties.OILCOST]))
                    {
                        Game1.map.players[owner].gold -= UnitProperties.getBuildingProperties(upgradeTo)[(int)BuildingProperties.GOLDCOST];
                        Game1.map.players[owner].wood -= UnitProperties.getBuildingProperties(upgradeTo)[(int)BuildingProperties.WOODCOST];
                        Game1.map.players[owner].oil -= UnitProperties.getBuildingProperties(upgradeTo)[(int)BuildingProperties.OILCOST];
                        upgradingto = upgradeTo;
                        upgrading = 0;

                        buildtime = UnitProperties.getBuildingProperties(upgradingto)[(int)BuildingProperties.BUILDTIME];
                        List<int> tmp = UnitProperties.getBuildingProperties(upgradingto);

                        if (!Game1.map.players[owner].isOrc)
                        {
                            this.spriteSheetPosition.X = tmp[(int)BuildingProperties.HUMANHALFBUILDSPRITEPOSX];
                            this.spriteSheetPosition.Y = tmp[(int)BuildingProperties.HUMANHALFBUILDSPRITEPOSY];
                        }
                        else
                        {
                            this.spriteSheetPosition.X = tmp[(int)BuildingProperties.ORCHALFBUILDSPRITEPOSX];
                            this.spriteSheetPosition.Y = tmp[(int)BuildingProperties.ORCHALFBUILDSPRITEPOSY];
                        }
                    }
                    else if (owner == Game1.currentPLayer)
                    {
                        if ((Game1.map.players[owner].gold < UnitProperties.getBuildingProperties(upgradeTo)[(int)BuildingProperties.GOLDCOST]))
                            Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.SentinelNoGold1]);

                        else if (Game1.map.players[owner].wood < UnitProperties.getBuildingProperties(upgradeTo)[(int)BuildingProperties.WOODCOST])
                            Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.SentinelNoLumber1]);

                        else if (Game1.map.players[owner].oil < UnitProperties.getBuildingProperties(upgradeTo)[(int)BuildingProperties.OILCOST])
                            Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.Error]);
                    }


                }
            }
        }

        private bool isAmeliorating()
        {
            return sa != null;
        }

        public void Ameliorate(int ameliorationIndex) // launch amelioration
        {
            if (isBuilt() && isNotUpgrading() && sa == null)
            {
                bool thisAmeliorationResearching = false;

                foreach (Unit b in Game1.map.units)
                {
                    if (b != this && b.type == this.type && b.owner == owner)
                        if (((Building)b).sa != null)
                        {
                            thisAmeliorationResearching = true;
                            break;
                        }
                }
                if (!thisAmeliorationResearching) // if this amelioration isnt under research
                {
                    switch (type)
                    {
                        case UnitType.Forge:
                            if (ameliorationIndex == 0 && (Game1.map.players[owner].playerAmeliorations.getDamageAmelioration(UnitType.Fantassin) < UnitProperties.FANTASSIN_MAX_AMELIORATION_DAMAGE))
                            {
                                sa = new UnitsRelated.SingleAmelioration(UnitsRelated.Ameliorations.buffs.Damage, UnitType.Fantassin, UnitProperties.FANTASSIN_MAX_AMELIORATION_DAMAGE / 2, owner);
                                sa.Research();
                            }
                            else if (ameliorationIndex == 1 && Game1.map.players[owner].playerAmeliorations.getArmorAmelioration(UnitType.Fantassin) < UnitProperties.FANTASSIN_MAX_AMELIORATION_ARMOR)
                            {
                                sa = new UnitsRelated.SingleAmelioration(UnitsRelated.Ameliorations.buffs.Armor, UnitType.Fantassin, UnitProperties.FANTASSIN_MAX_AMELIORATION_ARMOR / 2, owner);
                                sa.Research();
                            }

                            break;
                        case UnitType.LumberMill:
                            if (ameliorationIndex == 0 && (Game1.map.players[owner].playerAmeliorations.getDamageAmelioration(UnitType.Archer) < UnitProperties.ARCHER_MAX_AMELIORATION_DAMAGE))
                            {
                                sa = new UnitsRelated.SingleAmelioration(UnitsRelated.Ameliorations.buffs.Damage, UnitType.Archer, UnitProperties.ARCHER_MAX_AMELIORATION_DAMAGE / 2, owner);
                                sa.Research();
                            }
                            break;
                        case UnitType.Foundry:
                            break;
                        case UnitType.Church:
                            if (!Game1.map.players[owner].playerAmeliorations.hasPaladin)
                            {
                                sa = new UnitsRelated.SingleAmelioration(UnitsRelated.Ameliorations.buffs.PaladinResearch, UnitType.Paladin, 0, owner);
                                sa.Research();
                            }
                            break;
                    }
                }
            }
        }


        public void CancelBuild()
        {
            if (this.owner == Game1.currentPLayer) // network
            {
                CancelBuilding c = new CancelBuilding();
                c.CancelledBuildingClickIndex = index;
                if (builder != null)
                    builder.cancelledBuild = true;
                Game1.map.players[Game1.currentPLayer].playerMoves.cancelBuilding.Add(c);
            }


            UnitType refund = UnitType.None;

            if (this.unitbuilding != UnitType.None)
            {
                refund = this.unitbuilding;
                this.unitbuilding = UnitType.None;
                this.constructionState = 0;
            }
            else if (this.upgradingto != UnitType.None)
            {
                refund = this.upgradingto;
                this.upgradingto = UnitType.None;
                this.upgrading = 0;
                List<int> tmp = UnitProperties.getBuildingProperties(type);

                if (!Game1.map.players[owner].isOrc)
                {

                    this.spriteSheetPosition.X = tmp[(int)BuildingProperties.HUMANSPRITEPOSX];
                    this.spriteSheetPosition.Y = tmp[(int)BuildingProperties.HUMANSPRITEPOSY];
                }
                else
                {
                    this.spriteSheetPosition.X = tmp[(int)BuildingProperties.ORCSPRITEPOSX];
                    this.spriteSheetPosition.Y = tmp[(int)BuildingProperties.ORCSPRITEPOSY];
                }
            }
            else if (this.percentBuilt < 100)
            {
                refund = this.type;
                this.hp = -1; // lol this is ugly ; should work fine though

                if (builder != null)
                {
                    ((Harvester)builder).constructingBuilding = null;
                    ((Harvester)builder).buildingConstructPosition = Game1.statics.ImpossiblePos;
                    builder.disabled = 0;
                    builder.position = builder.findClearSortieSpot(Game1.statics.ImpossiblePos);
                }

                Game1.map.selected.Remove(this);
            }
            else if (this.sa != null)
            {
                sa = null;
            }
            if (refund != UnitType.None)
            {
                if ((int)refund >= littlebuildingindex) // you lose a littlebit as you cancelled it
                {
                    Game1.map.players[owner].gold += (int) (UnitProperties.getBuildingProperties(refund)[(int)BuildingProperties.GOLDCOST] * 0.9);
                    Game1.map.players[owner].wood += (int) (UnitProperties.getBuildingProperties(refund)[(int)BuildingProperties.WOODCOST] * 0.9);
                    Game1.map.players[owner].oil += (int) (UnitProperties.getBuildingProperties(refund)[(int)BuildingProperties.OILCOST] * 0.9);
                }
                else // unit cancel so full refund
                {
                    Game1.map.players[owner].gold += UnitProperties.getUnitProperties(refund)[(int)Properties.GOLDCOST];
                    Game1.map.players[owner].wood += UnitProperties.getUnitProperties(refund)[(int)Properties.WOODCOST];
                    Game1.map.players[owner].oil += UnitProperties.getUnitProperties(refund)[(int)Properties.OILCOST];
                }

            }

        }




        public override void DrawLeftPanelIcons(SpriteBatch spriteBatch)
        {

            if ((int)type >= littlebuildingindex && (isBuilt() && isNotUpgrading()) && ((unitbuilding == UnitType.None)) && sa == null)
            {
                switch (type)
                {
                    case UnitType.MilitaryBase:
                        if (Game1.map.players[owner].isOrc)
                        {

                            Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 147, 0);
                            if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Archer)) // archer
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 245, 0);
                            if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Catapulte))
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 100, Game1.statics.CreateUnitIconCoordinate.Y), 343, 41);
                            if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Chevalier))
                            {
                                if (Game1.map.players[owner].playerAmeliorations.hasPaladin)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X, Game1.statics.CreateUnitIconCoordinate.Y + 50), 0, 41);
                                else
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X, Game1.statics.CreateUnitIconCoordinate.Y + 50), 441, 0);
                            }
                        }
                        else
                        {
                            Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 98, 0);
                            if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Archer)) // troll
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 196, 0);
                            if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Catapulte))
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 100, Game1.statics.CreateUnitIconCoordinate.Y), 294, 41);
                            if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Chevalier))
                            {
                                if (Game1.map.players[owner].playerAmeliorations.hasPaladin)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X, Game1.statics.CreateUnitIconCoordinate.Y + 50), 49, 41);
                                else
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X, Game1.statics.CreateUnitIconCoordinate.Y + 50), 392, 0);
                            }
                        }
                        break;
                    case UnitType.Forge:
                        if (!isAmeliorating())
                        {
                            if (Game1.map.players[owner].isOrc)
                            {
                                if (Game1.map.players[Game1.currentPLayer].playerAmeliorations.getDamageAmelioration(UnitType.Fantassin) == 0)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 0, 492);
                                else if (Game1.map.players[Game1.currentPLayer].playerAmeliorations.getDamageAmelioration(UnitType.Fantassin) < UnitProperties.FANTASSIN_MAX_AMELIORATION_DAMAGE)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X, Game1.statics.CreateUnitIconCoordinate.Y), 48, 492);

                                if (Game1.map.players[Game1.currentPLayer].playerAmeliorations.getArmorAmelioration(UnitType.Fantassin) == 0)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 343, 656);
                                else if (Game1.map.players[Game1.currentPLayer].playerAmeliorations.getArmorAmelioration(UnitType.Fantassin) < UnitProperties.FANTASSIN_MAX_AMELIORATION_ARMOR)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 392, 656);
                            }

                            else
                            {
                                if (Game1.map.players[Game1.currentPLayer].playerAmeliorations.getDamageAmelioration(UnitType.Fantassin) == 0)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 343, 451);
                                else if (Game1.map.players[Game1.currentPLayer].playerAmeliorations.getDamageAmelioration(UnitType.Fantassin) < UnitProperties.FANTASSIN_MAX_AMELIORATION_DAMAGE)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X, Game1.statics.CreateUnitIconCoordinate.Y), 392, 451);
                                if (Game1.map.players[Game1.currentPLayer].playerAmeliorations.getArmorAmelioration(UnitType.Fantassin) == 0)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 245, 656);
                                else if (Game1.map.players[Game1.currentPLayer].playerAmeliorations.getArmorAmelioration(UnitType.Fantassin) < UnitProperties.FANTASSIN_MAX_AMELIORATION_ARMOR)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 294, 656);
                            }
                        }
                        break;
                    case UnitType.LumberMill:
                        if (!isAmeliorating())
                        {
                            if (Game1.map.players[owner].isOrc)
                            {
                                if (Game1.map.players[Game1.currentPLayer].playerAmeliorations.getDamageAmelioration(UnitType.Archer) == 0)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 392, 492);
                                else if (Game1.map.players[Game1.currentPLayer].playerAmeliorations.getDamageAmelioration(UnitType.Archer) < UnitProperties.ARCHER_MAX_AMELIORATION_DAMAGE)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X, Game1.statics.CreateUnitIconCoordinate.Y), 441, 492);
                            }

                            else
                            {
                                if (Game1.map.players[Game1.currentPLayer].playerAmeliorations.getDamageAmelioration(UnitType.Archer) == 0)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 245, 492);
                                else if (Game1.map.players[Game1.currentPLayer].playerAmeliorations.getDamageAmelioration(UnitType.Archer) < UnitProperties.ARCHER_MAX_AMELIORATION_DAMAGE)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X, Game1.statics.CreateUnitIconCoordinate.Y), 294, 492);
                            }
                        }
                        break;
                    case UnitType.GoblinWorkBench:
                        if (Game1.map.players[owner].isOrc)
                        {
                            Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 147, 47);
                            Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 441, 82);
                        }
                        else
                        {
                            Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 392, 82);
                            Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 98, 41);

                        }

                        break;
                    case UnitType.GryffinTower:
                        if (Game1.map.players[owner].isOrc)
                        {
                            Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 49, 123);
                        }
                        else
                        {
                            Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 0, 123);
                        }
                        break;
                    case UnitType.MageTower:
                        {
                            if (Game1.map.players[owner].isOrc)
                            {
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 245, 41);
                            }
                            else
                            {
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 196, 41);
                            }
                        }
                        break;
                    case UnitType.Church:
                        {
                            if (Game1.map.players[owner].isOrc)
                            {
                                if (!Game1.map.players[owner].playerAmeliorations.hasPaladin)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 49, 41);
                            }
                            else
                            {
                                if (!Game1.map.players[owner].playerAmeliorations.hasPaladin)
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 0, 41);
                            }
                        }
                        break;
                    case UnitType.NavalChantier: // TODO: other naval units
                        if (Game1.map.players[owner].isOrc)
                        {

                            Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 441, 41);
                            Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 147, 82);
                            if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Sousmarin))
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 100, Game1.statics.CreateUnitIconCoordinate.Y), 343, 82);
                            if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Battleship))
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X, Game1.statics.CreateUnitIconCoordinate.Y + 50), 245, 82);
                            if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Transport))
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y + 50), 49, 82);
                        }
                        else
                        {
                            Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 392, 41);
                            Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 98, 82);
                            if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Sousmarin))
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 100, Game1.statics.CreateUnitIconCoordinate.Y), 294, 82);
                            if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Battleship))
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X, Game1.statics.CreateUnitIconCoordinate.Y + 50), 196, 82);
                            if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Transport))
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y + 50), 0, 82);
                        }
                        break;
                    case UnitType.Tower:
                        {
                            if (isBuilt()) // if finished building
                            {
                                if (Game1.map.players[owner].isOrc)
                                {
                                    if (Game1.map.players[owner].playerTechs.hasTech(UnitType.BallistaTower)) // tech to get tower ballista
                                        Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 343, 287);
                                    if (Game1.map.players[owner].playerTechs.hasTech(UnitType.CannonTower))
                                        Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 392, 287);
                                }
                                else
                                {
                                    if (Game1.map.players[owner].playerTechs.hasTech(UnitType.BallistaTower)) // tech to get tower ballista
                                        Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 245, 287);
                                    if (Game1.map.players[owner].playerTechs.hasTech(UnitType.CannonTower))
                                        Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 294, 287);


                                }
                            }
                        }
                        break;
                    case UnitType.TownHall:
                        {
                            if (isBuilt()) // if finished building
                            {
                                if (Game1.map.players[owner].isOrc)
                                {

                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 49, 0);
                                    if (Game1.map.players[owner].playerTechs.hasTech(UnitType.StrongHold)) // tech to get stronghold
                                        Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 343, 246);

                                }
                                else
                                {
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 0, 0);
                                    if (Game1.map.players[owner].playerTechs.hasTech(UnitType.StrongHold)) // tech to get stronghold
                                        Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 294, 246);


                                }
                            }
                        }
                        break;
                    case UnitType.StrongHold:
                        {
                            if (Game1.map.players[owner].isOrc)
                            {

                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 49, 0);
                                if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Fortress)) // tech to get fortress
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 441, 246);

                            }
                            else
                            {
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 0, 0);
                                if (Game1.map.players[owner].playerTechs.hasTech(UnitType.Fortress)) // tech to get fortress
                                    Game1.leftPanel.DrawConstructionIcons(spriteBatch, new Vector2(Game1.statics.CreateUnitIconCoordinate.X + 50, Game1.statics.CreateUnitIconCoordinate.Y), 392, 246);


                            }
                        }
                        break;
                    default: // town/stronghold/fortress
                        if ((int)type >= largebuildingindex)
                        {
                            if (Game1.map.players[owner].isOrc)
                            {

                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 49, 0);
                            }
                            else
                            {
                                Game1.leftPanel.DrawConstructionIcons(spriteBatch, Game1.statics.CreateUnitIconCoordinate, 0, 0);

                            }
                        }
                        break;

                }
            }
            else if ((int)type >= littlebuildingindex) // upgrading or under construction
            {
                Game1.leftPanel.DrawCancelButton(spriteBatch);
            }

        }



        /**
         * Builds a unit : Checks tech available, gold wood and oil costs
         * Checks if we are not already building something
         * If all checks are ok, diminish the players resources by the amount
         */
        internal void BuildUnit(UnitType unitbuilding)
        {
            if (!isBuilt()) // this building isn't even done yet
                return;
            if (this.unitbuilding != UnitType.None) // we are already building something
                return;
            if (!(Game1.map.players[owner].playerAI != null && Game1.map.players[owner].usedFood >= Game1.map.players[owner].food)) // recheck food for ai because of potential food field
            {
                if (unitbuilding != UnitType.None && Game1.map.players[owner].playerTechs.hasTech(unitbuilding)) // handle gold, wood, oil costs IF we have the tech to build it
                {
                    this.unitbuilding = unitbuilding;

                    if (UnitProperties.getUnitProperties(unitbuilding)[(int)Properties.GOLDCOST] > Game1.map.players[owner].gold)
                    {
                        if (owner == Game1.currentPLayer)
                            Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.KnightNoGold1]);
                        this.unitbuilding = UnitType.None;
                    }
                    else if (UnitProperties.getUnitProperties(unitbuilding)[(int)Properties.WOODCOST] > Game1.map.players[owner].wood)
                    {
                        if (owner == Game1.currentPLayer)
                            Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.KnightNoGold1]);
                        this.unitbuilding = UnitType.None;
                    }
                    else if (UnitProperties.getUnitProperties(unitbuilding)[(int)Properties.OILCOST] > Game1.map.players[owner].oil)
                    {
                        if (owner == Game1.currentPLayer)
                            Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.Error]);
                        this.unitbuilding = UnitType.None;
                    }

                }
                else if (!Game1.map.players[owner].playerTechs.hasTech(unitbuilding)) // not the tech to build it
                    this.unitbuilding = UnitType.None;

                if (Game1.map.players[owner].usedFood < Game1.map.players[owner].food && this.unitbuilding != UnitType.None)
                {
                    Game1.map.players[owner].gold -= UnitProperties.getUnitProperties(unitbuilding)[(int)Properties.GOLDCOST];
                    Game1.map.players[owner].wood -= UnitProperties.getUnitProperties(unitbuilding)[(int)Properties.WOODCOST];
                    Game1.map.players[owner].oil -= UnitProperties.getUnitProperties(unitbuilding)[(int)Properties.OILCOST];
                }
            }
        }
    }

}


