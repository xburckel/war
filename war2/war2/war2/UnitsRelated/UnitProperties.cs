﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2
{
    public enum Properties { HP = 0, MANA, SIGHT, DAMAGE, BUILDTIME, GOLDCOST, WOODCOST, OILCOST, RANGE, ARMOR, SPEED, SPRITESIZEX, SPRITESIZEY };
    public enum BuildingProperties { HP = 0, SIGHT, BUILDTIME, GOLDCOST, WOODCOST, OILCOST, ARMOR, SPEED, ORCSPRITEPOSX, ORCSPRITEPOSY, HUMANSPRITEPOSX, HUMANSPRITEPOSY, ORCHALFBUILDSPRITEPOSX, ORCHALFBUILDSPRITEPOSY, HUMANHALFBUILDSPRITEPOSX, HUMANHALFBUILDSPRITEPOSY };


    public static class UnitProperties
    {
        // TODO : complete


        static UnitProperties()
        {
            /* DEPRECATED
            // HUMANS
            #region attacks
            List<Vector2> fattacks = new List<Vector2>();
            FANTASSIN_ATTACKS = fattacks;
            fattacks.Add(new Vector2(0, 0));
            fattacks.Add(new Vector2(0, 281));
            fattacks.Add(new Vector2(0, 333));
            fattacks.Add(new Vector2(0, 403));
            fattacks.Add(new Vector2(0, 451));

            List<Vector2> aattacks = new List<Vector2>();
            ARCHER_ATTACKS = aattacks;
            aattacks.Add(new Vector2(0, 225));
            aattacks.Add(new Vector2(0, 303));
            aattacks.Add(new Vector2(0, 370));
            aattacks.Add(new Vector2(0, 447));
            List<Vector2> battacks = new List<Vector2>();
            battacks.Add(new Vector2(0, 0));
            battacks.Add(new Vector2(0, 64));
            battacks.Add(new Vector2(0, 128));
            battacks.Add(new Vector2(0, 195));
            BALLISTA_ATTACKS = battacks;
            List<Vector2> mattacks = new List<Vector2>();
            mattacks.Add(new Vector2(0, 310));
            mattacks.Add(new Vector2(0, 368));
            mattacks.Add(new Vector2(0, 428));
            mattacks.Add(new Vector2(0, 430));
            mattacks.Add(new Vector2(0, 490));


            MAGE_ATTACKS = mattacks;
            List<Vector2> pattacks = new List<Vector2>();
            pattacks.Add(new Vector2(0, 192));
            pattacks.Add(new Vector2(0, 228));
            pattacks.Add(new Vector2(0, 261));
            pattacks.Add(new Vector2(0, 306));
            pattacks.Add(new Vector2(0, 352));
            PEASANT_ATTACKS = pattacks;
            List<Vector2> kattacks = new List<Vector2>();

            kattacks.Add(new Vector2(1, 302));
            kattacks.Add(new Vector2(1, 379));
            kattacks.Add(new Vector2(1, 450));
            kattacks.Add(new Vector2(1, 528));
            kattacks.Add(new Vector2(1, 599));
            
            KNIGHT_ATTACKS = kattacks;
            List<Vector2> gattacks = new List<Vector2>();
            gattacks.Add(new Vector2(1, 253));
            gattacks.Add(new Vector2(1, 340));
            gattacks.Add(new Vector2(1, 415));
            gattacks.Add(new Vector2(1, 497));
            GRYFFON_ATTACKS = gattacks;
            List<Vector2> sattacks = new List<Vector2>();
            sattacks.Add(new Vector2(1, 6));
            sattacks.Add(new Vector2(1, 80));
            sattacks.Add(new Vector2(1, 151));
            SUBMARINE_ATTACKS = sattacks;

            #endregion


            List<Vector2> fmoves = new List<Vector2>();
            FANTASSIN_MOVES = fmoves;
            fmoves.Add(new Vector2(0, 0));
            fmoves.Add(new Vector2(0, 63));
            fmoves.Add(new Vector2(0, 119));
            fmoves.Add(new Vector2(0, 173));
            fmoves.Add(new Vector2(0, 226));

            List<Vector2> amoves = new List<Vector2>();
            ARCHER_MOVES = amoves;
            amoves.Add(new Vector2(0, 6));
            amoves.Add(new Vector2(0, 77));
            amoves.Add(new Vector2(0, 151));
            amoves.Add(new Vector2(0, 225));

            List<Vector2> bmoves = new List<Vector2>();
            bmoves.Add(new Vector2(0, 0));
            BALLISTA_MOVES = bmoves;
            List<Vector2> mmoves = new List<Vector2>();
            mmoves.Add(new Vector2(0, 0));
            mmoves.Add(new Vector2(0, 63));
            mmoves.Add(new Vector2(0, 126));
            mmoves.Add(new Vector2(0, 186));
            mmoves.Add(new Vector2(0, 250));

            MAGE_MOVES = mmoves;
            List<Vector2> pmoves = new List<Vector2>();
            pmoves.Add(new Vector2(0, 0));
            pmoves.Add(new Vector2(0, 37));
            pmoves.Add(new Vector2(0, 74));
            pmoves.Add(new Vector2(0, 111));
            pmoves.Add(new Vector2(0, 148));
            PEASANT_MOVES = pmoves;
            List<Vector2> kmoves = new List<Vector2>();

            kmoves.Add(new Vector2(1, 8));
            kmoves.Add(new Vector2(1, 81));
            kmoves.Add(new Vector2(1, 155));
            kmoves.Add(new Vector2(1, 228));
            kmoves.Add(new Vector2(1, 302));

            KNIGHT_MOVES = kmoves;
            List<Vector2> gmoves = new List<Vector2>();
            gmoves.Add(new Vector2(1, 0));
            gmoves.Add(new Vector2(1, 90));
            gmoves.Add(new Vector2(1, 167));
            gmoves.Add(new Vector2(1, 255));
            GRYFFON_MOVES = gmoves;
            List<Vector2> smoves = new List<Vector2>();
            smoves.Add(new Vector2(1, 6));

            SUBMARINE_MOVES = smoves;

            ORCDESTROYERMOVES = new List<Vector2>();
            ORCDESTROYERMOVES.Add(new Vector2(0, 0));

            ORCOILHARVESTERMOVES = new List<Vector2>();
            ORCOILHARVESTERMOVES.Add(new Vector2(0, 0));

            ORCTRANSPORTMOVES = new List<Vector2>();
            ORCTRANSPORTMOVES.Add(new Vector2(0, 0));

            ORCFLYMACHINEMOVES = new List<Vector2>();
            ORCFLYMACHINEMOVES.Add(new Vector2(0, 0));
            ORCFLYMACHINEMOVES.Add(new Vector2(0, 75));

            OGREMAGEMOVES = OGRE_MOVES;

            GOBLINSMOVES = new List<Vector2>();
            GOBLINSMOVES.Add(new Vector2(0, 0));
            GOBLINSMOVES.Add(new Vector2(0, 47));
            GOBLINSMOVES.Add(new Vector2(0, 94));
            GOBLINSMOVES.Add(new Vector2(0, 133));
            GOBLINSMOVES.Add(new Vector2(0, 175));
            GOBLINSMOVES.Add(new Vector2(0, 224));
            GOBLINSMOVES.Add(new Vector2(0, 270));

            JUGGERNAUTMOVES = new List<Vector2>();
            JUGGERNAUTMOVES.Add(new Vector2(0, 0));

            HUMANDESTROYERMOVES = new List<Vector2>();
            HUMANDESTROYERMOVES.Add(new Vector2(0, 0));

            HUMANOILHARVESTERMOVES = new List<Vector2>();
            HUMANOILHARVESTERMOVES.Add(new Vector2(0, 0));

            HUMANTRANSPORTMOVES = new List<Vector2>();
            HUMANTRANSPORTMOVES.Add(new Vector2(0, 0));

            HUMANFLYMACHINEMOVES = new List<Vector2>();
            HUMANFLYMACHINEMOVES.Add(new Vector2(0, 0));
            HUMANFLYMACHINEMOVES.Add(new Vector2(0, 85));

            PALADINMOVES = KNIGHT_MOVES;

            DWARVESMOVES = new List<Vector2>();
            DWARVESMOVES.Add(new Vector2(0, 0));
            DWARVESMOVES.Add(new Vector2(0, 47));
            DWARVESMOVES.Add(new Vector2(0, 94));
            DWARVESMOVES.Add(new Vector2(0, 133));
            DWARVESMOVES.Add(new Vector2(0, 175));
            DWARVESMOVES.Add(new Vector2(0, 224));
            DWARVESMOVES.Add(new Vector2(0, 270));

            BATTLESHIPMOVES = new List<Vector2>();
            BATTLESHIPMOVES.Add(new Vector2(0, 0));







            // TODO : ORCS
            List<Vector2> Ofattacks = new List<Vector2>();



            
            Ofattacks.Add(new Vector2(0, 258));
            Ofattacks.Add(new Vector2(0, 308));
            Ofattacks.Add(new Vector2(0, 360));
            Ofattacks.Add(new Vector2(0, 406));
            GRUNT_ATTACKS = Ofattacks;
            List<Vector2> Oaattacks = new List<Vector2>();
            Oaattacks.Add(new Vector2(0, 260));
            Oaattacks.Add(new Vector2(0, 313));
            Oaattacks.Add(new Vector2(0, 363));
            Oaattacks.Add(new Vector2(0, 418));
            TROLL_ATTACKS = Oaattacks;
            List<Vector2> Obattacks = new List<Vector2>();
            CATAPULTE_ATTACKS = battacks;
            List<Vector2> Omattacks = new List<Vector2>();
            SORCERER_ATTACKS = mattacks;
            List<Vector2> Okattacks = new List<Vector2>();
            OGRE_ATTACKS = kattacks;
            List<Vector2> Ogattacks = new List<Vector2>();
            Ogattacks.Add(new Vector2(0, 287));
            Ogattacks.Add(new Vector2(0, 342));
            Ogattacks.Add(new Vector2(0, 393));
            Ogattacks.Add(new Vector2(0, 457));
            DRAGON_ATTACKS = Ogattacks;
            List<Vector2> Osattacks = new List<Vector2>();
            TURTLE_ATTACKS = sattacks;


            List<Vector2> Ofmoves = new List<Vector2>();

            Ofmoves.Add(new Vector2(0, 0));
            Ofmoves.Add(new Vector2(0, 49));
            Ofmoves.Add(new Vector2(0, 104));
            Ofmoves.Add(new Vector2(0, 154));
            Ofmoves.Add(new Vector2(0, 205));

            GRUNT_MOVES = Ofmoves;
            List<Vector2> Oamoves = new List<Vector2>();
            Oamoves.Add(new Vector2(0, 0));
            Oamoves.Add(new Vector2(0, 52));
            Oamoves.Add(new Vector2(0, 106));
            Oamoves.Add(new Vector2(0, 158));
            Oamoves.Add(new Vector2(0, 210));
            TROLL_MOVES = Oamoves;
            List<Vector2> Obmoves = new List<Vector2>();
            CATAPULTE_MOVES = bmoves;
            List<Vector2> Ommoves = new List<Vector2>();
            SORCERER_MOVES = mmoves;
            List<Vector2> Opmoves = new List<Vector2>();
            Opmoves.Add(new Vector2(0, 0));
            Opmoves.Add(new Vector2(0, 44));
            Opmoves.Add(new Vector2(0, 92));
            Opmoves.Add(new Vector2(0, 140));
            Opmoves.Add(new Vector2(0, 187));
            PEON_MOVES = Opmoves;
            List<Vector2> Okmoves = new List<Vector2>();

            Okmoves.Add(new Vector2(0, 0));
            Okmoves.Add(new Vector2(0, 54));
            Okmoves.Add(new Vector2(0, 114));
            Okmoves.Add(new Vector2(0, 170));
            Okmoves.Add(new Vector2(0, 225));
            OGRE_MOVES = Okmoves;
            List<Vector2> Ogmoves = new List<Vector2>();
            DRAGON_MOVES = gmoves;
            List<Vector2> Osmoves = new List<Vector2>();
            TURTLE_MOVES = smoves;



            FANTASSIN_STILL = new Vector2(0, 0);
            ARCHER_STILL = new Vector2(0, 0);
            BALLISTA_STILL = new Vector2(0, 0);
            MAGE_STILL = new Vector2(0, 0);
            PEASANT_STILL = new Vector2(0, 0);
            KNIGHT_STILL = new Vector2(0, 0);
            GRYFFON_STILL = new Vector2(0, 0);
            SUBMARINE_STILL = new Vector2(0, 0);

            // ORCS
            GRUNT_STILL = new Vector2(0, 0);
            TROLL_STILL = new Vector2(0, 0);
            CATAPULTE_STILL = new Vector2(0, 0);
            SORCERER_STILL = new Vector2(0, 0);
            PEON_STILL = new Vector2(0, 0);
            OGRE_STILL = new Vector2(0, 0); ;
            DRAGON_STILL = new Vector2(0, 0);
            TURTLE_STILL = new Vector2(0, 0);

            ORCTRANSPORT_STILL = new Vector2(5, 1);
            ORCOILHARVESTER_STILL = new Vector2(7, 2);
            ORCDESTROYER_STILL = new Vector2(15, 6);
            ORCFLYMACHINE_STILL = new Vector2(0, 0);
            OGREMAGE_STILL = new Vector2(5, 5);
            GOBLINS_STILL = new Vector2(5, 5);
            JUGGERNAUT_STILL = new Vector2(0, 0);

            HUMANTRANSPORT_STILL = new Vector2(0, 0);
            HUMANOILHARVESTER_STILL = new Vector2(15, 0);
            HUMANDESTROYER_STILL = new Vector2(0, 0);
            HUMANFLYMACHINE_STILL = new Vector2(0, 0);
            PALADIN_STILL = new Vector2(21, 7);
            DWARVES_STILL = new Vector2(7, 6);
            BATTLESHIP_STILL = new Vector2(16, 5);


            List<Vector2> pharvestwood = new List<Vector2>();
            pharvestwood.Add(new Vector2(0, 189));
            pharvestwood.Add(new Vector2(0, 229));
            pharvestwood.Add(new Vector2(0, 263));
            pharvestwood.Add(new Vector2(0, 303));
            pharvestwood.Add(new Vector2(0, 341));
            PEASANT_COLLECT_WOOD = pharvestwood;

            List<Vector2> pcarrygold = new List<Vector2>();
            pcarrygold.Add(new Vector2(0, 459));
            pcarrygold.Add(new Vector2(0, 494));
            pcarrygold.Add(new Vector2(0, 531));
            pcarrygold.Add(new Vector2(0, 569));
            pcarrygold.Add(new Vector2(0, 607));
            PEASANT_CARRY_GOLD = pcarrygold;
            List<Vector2> pcarrywood = new List<Vector2>();
            pcarrywood.Add(new Vector2(0, 644));
            pcarrywood.Add(new Vector2(0, 678));
            pcarrywood.Add(new Vector2(0, 718));
            pcarrywood.Add(new Vector2(0, 757));
            pcarrywood.Add(new Vector2(0, 794));
            PEASANT_CARRY_WOOD = pcarrywood;
            List<Vector2> tcarryoil = new List<Vector2>();
            tcarryoil.Add(new Vector2(0, 220));
            OTANKER_FULL = tcarryoil;
            TANKER_FULL = tcarryoil;





            List<Vector2> opharvestwood = new List<Vector2>();
            opharvestwood.Add(new Vector2(0, 235));
            opharvestwood.Add(new Vector2(0, 286));
            opharvestwood.Add(new Vector2(0, 328));
            opharvestwood.Add(new Vector2(0, 375));
            opharvestwood.Add(new Vector2(0, 426));
            OPEASANT_COLLECT_WOOD = opharvestwood;

            List<Vector2> opcarrygold = new List<Vector2>();
            opcarrygold.Add(new Vector2(0, 573));
            opcarrygold.Add(new Vector2(0, 620));
            opcarrygold.Add(new Vector2(0, 669));
            opcarrygold.Add(new Vector2(0, 715));
            opcarrygold.Add(new Vector2(0, 761));
            OPEASANT_CARRY_GOLD = opcarrygold;
            List<Vector2> opcarrywood = new List<Vector2>();
            opcarrywood.Add(new Vector2(0, 809));
            opcarrywood.Add(new Vector2(0, 857));
            opcarrywood.Add(new Vector2(0, 904));
            opcarrywood.Add(new Vector2(0, 950));
            opcarrywood.Add(new Vector2(0, 1000));
            OPEASANT_CARRY_WOOD = opcarrywood;
            PEON_ATTACKS = opharvestwood;
             * */

        }

        /* DEPRECATED
        public static List<Vector2> getUnitAnimationsProperties(UnitAnimationType uat)
        {
            List<Vector2> tmp = new List<Vector2>();


            switch (uat)
            {
                case UnitAnimationType.ArcherAttack:
                    return ARCHER_ATTACKS;
                case UnitAnimationType.BallistaAttack:
                    return BALLISTA_ATTACKS;
                case UnitAnimationType.CatapulteAttack:
                    return CATAPULTE_ATTACKS;
                case UnitAnimationType.DragonAttack:
                    return DRAGON_ATTACKS;
                case UnitAnimationType.FantassinAttack:
                    return FANTASSIN_ATTACKS;
                case UnitAnimationType.GruntAttack:
                    return GRUNT_ATTACKS;
                case UnitAnimationType.GryffonAttack:
                    return GRYFFON_ATTACKS;
                case UnitAnimationType.KnightAttack:
                    return KNIGHT_ATTACKS;
                case UnitAnimationType.MageAttack:
                    return MAGE_ATTACKS;
                case UnitAnimationType.OgreAttack:
                    return OGRE_ATTACKS;
                case UnitAnimationType.PeasantAttack:
                    return PEASANT_ATTACKS;
                case UnitAnimationType.PeonAttack:
                    return PEON_ATTACKS;
                case UnitAnimationType.SorcererAttack:
                    return DRAGON_ATTACKS;
                case UnitAnimationType.SousMarinAttack:
                    return SUBMARINE_ATTACKS;
                case UnitAnimationType.TrollAttack:
                    return TROLL_ATTACKS;
                case UnitAnimationType.TurtleAttack:
                    return TURTLE_ATTACKS;
                case UnitAnimationType.ArcherMove:
                    return ARCHER_MOVES;
                case UnitAnimationType.BallistaMove:
                    return BALLISTA_MOVES;
                case UnitAnimationType.CatapulteMove:
                    return CATAPULTE_MOVES;
                case UnitAnimationType.DragonMove:
                    return DRAGON_MOVES;
                case UnitAnimationType.FantassinMove:
                    return FANTASSIN_MOVES;
                case UnitAnimationType.GruntMove:
                    return GRUNT_MOVES;
                case UnitAnimationType.GryffonMove:
                    return GRYFFON_MOVES;
                case UnitAnimationType.KnightMove:
                    return KNIGHT_MOVES;
                case UnitAnimationType.MageMove:
                    return MAGE_MOVES;
                case UnitAnimationType.OgreMove:
                    return OGRE_MOVES;
                case UnitAnimationType.OgreMageMove:
                    return OGRE_MOVES;
                case UnitAnimationType.PeasantMove:
                    return PEASANT_MOVES;
                case UnitAnimationType.PeonMove:
                    return PEON_MOVES;
                case UnitAnimationType.SorcererMove:
                    return SORCERER_MOVES;
                case UnitAnimationType.SousMarinMove:
                    return SUBMARINE_MOVES;
                case UnitAnimationType.TrollMove:
                    return TROLL_MOVES;
                case UnitAnimationType.TurtleMove:
                    return TURTLE_MOVES;

                case UnitAnimationType.JuggernautMove: return JUGGERNAUTMOVES;
                case UnitAnimationType.OrcDestroyerMove: return ORCDESTROYERMOVES;
                case UnitAnimationType.OrcFlyMachineMove: return ORCFLYMACHINEMOVES;
                case UnitAnimationType.GoblinsMove: return GOBLINSMOVES;
                case UnitAnimationType.OrcOilHarvesterMove: return ORCOILHARVESTERMOVES;
                case UnitAnimationType.OrcTransportMove: return ORCTRANSPORTMOVES;

                case UnitAnimationType.BattleShipMove: return BATTLESHIPMOVES;
                case UnitAnimationType.HumanDestroyerMove: return HUMANDESTROYERMOVES;
                case UnitAnimationType.HumanFlyMachineMove: return HUMANFLYMACHINEMOVES;
                case UnitAnimationType.DwarvesMove: return DWARVESMOVES;
                case UnitAnimationType.PalladinMove: return PALADINMOVES;
                case UnitAnimationType.HumanOilHarvesterMove: return HUMANOILHARVESTERMOVES;
                case UnitAnimationType.HumanTransportMove: return HUMANTRANSPORTMOVES;

                case UnitAnimationType.HarvestCarryingGold: return PEASANT_CARRY_GOLD;
                case UnitAnimationType.HarvestCarryingWood: return PEASANT_CARRY_WOOD;
                case UnitAnimationType.HarvestCollectingWood: return PEASANT_COLLECT_WOOD;
                case UnitAnimationType.HarvesterRepairing: return PEASANT_COLLECT_WOOD; // same as wood collect
                case UnitAnimationType.OHarvestCarryingGold: return OPEASANT_CARRY_GOLD;
                case UnitAnimationType.OHarvestCarryingWood: return OPEASANT_CARRY_WOOD;
                case UnitAnimationType.OHarvestCollectingWood: return OPEASANT_COLLECT_WOOD;
                case UnitAnimationType.OHarvesterRepairing: return OPEASANT_COLLECT_WOOD; // same as wood collect

                case UnitAnimationType.OTankerFull: return OTANKER_FULL; 
                case UnitAnimationType.TankerFull: return TANKER_FULL; 
            }


            List<Vector2> unitAnimationPosition = new List<Vector2>();
            unitAnimationPosition.Add(new Vector2(0, 0));

            return unitAnimationPosition;

        }
        */

        // retun thel ist of unit properties :

        public static List<int> getBuildingProperties(UnitType type)
        {
            List<int> properties = new List<int>();

            switch (type)
            {
                case (UnitType.BallistaTower):
                    properties.Add(BALLISTATOWER_HP);
                    properties.Add(BALLISTATOWER_SIGHT);
                    properties.Add(BALLISTATOWER_BUILD_TIME);
                    properties.Add(BALLISTATOWER_GOLD_COST);
                    properties.Add(BALLISTATOWER_WOOD_COST);
                    properties.Add(BALLISTATOWER_OIL_COST);
                    properties.Add(BALLISTATOWER_ARMOR);
                    properties.Add((int)BALLISTATOWER_SPEED);
                    properties.Add(409);
                    properties.Add(602);
                    properties.Add(394);
                    properties.Add(133);
                    // half build building sprite position
                    properties.Add(478);
                    properties.Add(537);
                    properties.Add(394);
                    properties.Add(199);
                    break;
                #region other buildings
                case (UnitType.CannonTower):
                    properties.Add(CANNONTOWER_HP);
                    properties.Add(CANNONTOWER_SIGHT);
                    properties.Add(CANNONTOWER_BUILD_TIME);
                    properties.Add(CANNONTOWER_GOLD_COST);
                    properties.Add(CANNONTOWER_WOOD_COST);
                    properties.Add(CANNONTOWER_OIL_COST);
                    properties.Add(CANNONTOWER_ARMOR);
                    properties.Add((int)CANNONTOWER_SPEED);
                    properties.Add(477);
                    properties.Add(602);
                    properties.Add(464);
                    properties.Add(134);
                    // half build building sprite position
                    properties.Add(478);
                    properties.Add(537);
                    properties.Add(394);
                    properties.Add(199);
                    break;
                case (UnitType.Church):
                    properties.Add(CHURCH_HP);
                    properties.Add(CHURCH_SIGHT);
                    properties.Add(CHURCH_BUILD_TIME);
                    properties.Add(CHURCH_GOLD_COST);
                    properties.Add(CHURCH_WOOD_COST);
                    properties.Add(CHURCH_OIL_COST);
                    properties.Add(CHURCH_ARMOR);
                    properties.Add((int)CHURCH_SPEED);
                    properties.Add(305);
                    properties.Add(240);
                    properties.Add(304);
                    properties.Add(257);
                    // half build building sprite position
                    properties.Add(215);
                    properties.Add(247);
                    properties.Add(304);
                    properties.Add(366);
                    break;
                case (UnitType.Farm):
                    properties.Add(FARM_HP);
                    properties.Add(FARM_SIGHT);
                    properties.Add(FARM_BUILD_TIME);
                    properties.Add(FARM_GOLD_COST);
                    properties.Add(FARM_WOOD_COST);
                    properties.Add(FARM_OIL_COST);
                    properties.Add(FARM_ARMOR);
                    properties.Add((int)FARM_SPEED);
                    properties.Add(336);
                    properties.Add(597);
                    properties.Add(394);
                    properties.Add(0);
                    // half build building sprite position
                    properties.Add(269);
                    properties.Add(602);
                    properties.Add(394);
                    properties.Add(72);
                    break;

                case (UnitType.Forge):
                    properties.Add(FORGE_HP);
                    properties.Add(FORGE_SIGHT);
                    properties.Add(FORGE_BUILD_TIME);
                    properties.Add(FORGE_GOLD_COST);
                    properties.Add(FORGE_WOOD_COST);
                    properties.Add(FORGE_OIL_COST);
                    properties.Add(FORGE_ARMOR);
                    properties.Add((int)FORGE_SPEED);
                    properties.Add(107);
                    properties.Add(346);
                    properties.Add(0);
                    properties.Add(260);
                    // half build building sprite position
                    properties.Add(14);
                    properties.Add(346);
                    properties.Add(3);
                    properties.Add(360);
                    break;

                case (UnitType.Fortress):
                    properties.Add(FORTRESS_HP);
                    properties.Add(FORTRESS_SIGHT);
                    properties.Add(FORTRESS_BUILD_TIME);
                    properties.Add(FORTRESS_GOLD_COST);
                    properties.Add(FORTRESS_WOOD_COST);
                    properties.Add(FORTRESS_OIL_COST);
                    properties.Add(FORTRESS_ARMOR);
                    properties.Add((int)FORTRESS_SPEED);
                    properties.Add(397);
                    properties.Add(13);
                    properties.Add(129);
                    properties.Add(0);
                    // half build building sprite position
                    properties.Add(265);
                    properties.Add(16);
                    properties.Add(130);
                    properties.Add(133);
                    break;

                case (UnitType.Foundry):
                    properties.Add(FOUNDRY_HP);
                    properties.Add(FOUNDRY_SIGHT);
                    properties.Add(FOUNDRY_BUILD_TIME);
                    properties.Add(FOUNDRY_GOLD_COST);
                    properties.Add(FOUNDRY_WOOD_COST);
                    properties.Add(FOUNDRY_OIL_COST);
                    properties.Add(FOUNDRY_ARMOR);
                    properties.Add((int)FOUNDRY_SPEED);
                    properties.Add(503);
                    properties.Add(342);
                    properties.Add(101);
                    properties.Add(455);
                    // half build building sprite position
                    properties.Add(414);
                    properties.Add(342);
                    properties.Add(105);
                    properties.Add(556);
                    break;



                case (UnitType.GoblinWorkBench):
                    properties.Add(GOBLIN_WORKBENCH_HP);
                    properties.Add(GOBLIN_WORKBENCH_SIGHT);
                    properties.Add(GOBLIN_WORKBENCH_BUILD_TIME);
                    properties.Add(GOBLIN_WORKBENCH_GOLD_COST);
                    properties.Add(GOBLIN_WORKBENCH_WOOD_COST);
                    properties.Add(GOBLIN_WORKBENCH_OIL_COST);
                    properties.Add(GOBLIN_WORKBENCH_ARMOR);
                    properties.Add((int)GOBLIN_WORKBENCH_SPEED);
                    properties.Add(502);
                    properties.Add(146);
                    properties.Add(403);
                    properties.Add(455);
                    // half build building sprite position
                    properties.Add(413);
                    properties.Add(154);
                    properties.Add(394);
                    properties.Add(558);
                    break;

                case (UnitType.GryffinTower):
                    properties.Add(GRYFFINTOWER_HP);
                    properties.Add(GRYFFINTOWER_SIGHT);
                    properties.Add(GRYFFINTOWER_BUILD_TIME);
                    properties.Add(GRYFFINTOWER_GOLD_COST);
                    properties.Add(GRYFFINTOWER_WOOD_COST);
                    properties.Add(GRYFFINTOWER_OIL_COST);
                    properties.Add(GRYFFINTOWER_ARMOR);
                    properties.Add((int)GRYFFINTOWER_SPEED);
                    properties.Add(307);
                    properties.Add(142);
                    properties.Add(511);
                    properties.Add(302);
                    // half build building sprite position
                    properties.Add(210);
                    properties.Add(154);
                    properties.Add(511);
                    properties.Add(399);
                    break;


                case (UnitType.LumberMill):
                    properties.Add(LUMBERMILL_HP);
                    properties.Add(LUMBERMILL_SIGHT);
                    properties.Add(LUMBERMILL_BUILD_TIME);
                    properties.Add(LUMBERMILL_GOLD_COST);
                    properties.Add(LUMBERMILL_WOOD_COST);
                    properties.Add(LUMBERMILL_OIL_COST);
                    properties.Add(LUMBERMILL_ARMOR);
                    properties.Add((int)LUMBERMILL_SPEED);
                    properties.Add(505);
                    properties.Add(244);
                    properties.Add(103);
                    properties.Add(264);
                    // half build building sprite position
                    properties.Add(409);
                    properties.Add(243);
                    properties.Add(104);
                    properties.Add(360);
                    break;

                case (UnitType.MageTower):
                    properties.Add(MAGETOWER_HP);
                    properties.Add(MAGETOWER_SIGHT);
                    properties.Add(MAGETOWER_BUILD_TIME);
                    properties.Add(MAGETOWER_GOLD_COST);
                    properties.Add(MAGETOWER_WOOD_COST);
                    properties.Add(MAGETOWER_OIL_COST);
                    properties.Add(MAGETOWER_ARMOR);
                    properties.Add((int)MAGETOWER_SPEED);
                    properties.Add(103);
                    properties.Add(149);
                    properties.Add(400);
                    properties.Add(266);
                    // half build building sprite position
                    properties.Add(8);
                    properties.Add(148);
                    properties.Add(403);
                    properties.Add(361);
                    break;

                case (UnitType.MilitaryBase):
                    properties.Add(MILITARYBASE_HP);
                    properties.Add(MILITARYBASE_SIGHT);
                    properties.Add(MILITARYBASE_BUILD_TIME);
                    properties.Add(MILITARYBASE_GOLD_COST);
                    properties.Add(MILITARYBASE_WOOD_COST);
                    properties.Add(MILITARYBASE_OIL_COST);
                    properties.Add(MILITARYBASE_ARMOR);
                    properties.Add((int)MILITARYBASE_SPEED);
                    properties.Add(106);
                    properties.Add(238);
                    properties.Add(303);
                    properties.Add(459);
                    // half build building sprite position
                    properties.Add(14);
                    properties.Add(250);
                    properties.Add(308);
                    properties.Add(564);
                    break;

                case (UnitType.NavalChantier):
                    properties.Add(CHANTIERNAVAL_HP);
                    properties.Add(CHANTIERNAVAL_SIGHT);
                    properties.Add(CHANTIERNAVAL_BUILD_TIME);
                    properties.Add(CHANTIERNAVAL_GOLD_COST);
                    properties.Add(CHANTIERNAVAL_WOOD_COST);
                    properties.Add(CHANTIERNAVAL_OIL_COST);
                    properties.Add(CHANTIERNAVAL_ARMOR);
                    properties.Add((int)CHANTIERNAVAL_SPEED);
                    properties.Add(107);
                    properties.Add(440);
                    properties.Add(0);
                    properties.Add(455);
                    // half build building sprite position
                    properties.Add(10);
                    properties.Add(446);
                    properties.Add(0);
                    properties.Add(554);
                    break;

                case (UnitType.NavalPlatform):
                    properties.Add(NAVALPLATFORM_HP);
                    properties.Add(NAVALPLATFORM_SIGHT);
                    properties.Add(NAVALPLATFORM_BUILD_TIME);
                    properties.Add(NAVALPLATFORM_GOLD_COST);
                    properties.Add(NAVALPLATFORM_WOOD_COST);
                    properties.Add(NAVALPLATFORM_OIL_COST);
                    properties.Add(NAVALPLATFORM_ARMOR);
                    properties.Add((int)NAVALPLATFORM_SPEED);
                    properties.Add(410);
                    properties.Add(441);
                    properties.Add(526);
                    properties.Add(0);
                    // half build building sprite position
                    properties.Add(411);
                    properties.Add(444);
                    properties.Add(533);
                    properties.Add(102);
                    break;
                case (UnitType.Raffinery):
                    properties.Add(RAFINNERY_HP);
                    properties.Add(RAFINNERY_SIGHT);
                    properties.Add(RAFINNERY_BUILD_TIME);
                    properties.Add(RAFINNERY_GOLD_COST);
                    properties.Add(RAFINNERY_WOOD_COST);
                    properties.Add(RAFINNERY_OIL_COST);
                    properties.Add(RAFINNERY_ARMOR);
                    properties.Add((int)RAFINNERY_SPEED);
                    properties.Add(311);
                    properties.Add(440);
                    properties.Add(202);
                    properties.Add(457);
                    // half build building sprite position
                    properties.Add(209);
                    properties.Add(450);
                    properties.Add(210);
                    properties.Add(554);
                    break;

                case (UnitType.Stable):
                    properties.Add(STABLE_HP);
                    properties.Add(STABLE_SIGHT);
                    properties.Add(STABLE_BUILD_TIME);
                    properties.Add(STABLE_GOLD_COST);
                    properties.Add(STABLE_WOOD_COST);
                    properties.Add(STABLE_OIL_COST);
                    properties.Add(STABLE_ARMOR);
                    properties.Add((int)STABLE_SPEED);
                    properties.Add(300);
                    properties.Add(340);
                    properties.Add(199);
                    properties.Add(261);
                    // half build building sprite position
                    properties.Add(211);
                    properties.Add(341);
                    properties.Add(201);
                    properties.Add(358);
                    break;

                case (UnitType.StrongHold):
                    properties.Add(STRONGHOLD_HP);
                    properties.Add(STRONGHOLD_SIGHT);
                    properties.Add(STRONGHOLD_BUILD_TIME);
                    properties.Add(STRONGHOLD_GOLD_COST);
                    properties.Add(STRONGHOLD_WOOD_COST);
                    properties.Add(STRONGHOLD_OIL_COST);
                    properties.Add(STRONGHOLD_ARMOR);
                    properties.Add((int)STRONGHOLD_SPEED);
                    properties.Add(142);
                    properties.Add(17);
                    properties.Add(0);
                    properties.Add(0);
                    // half build building sprite position
                    properties.Add(13);
                    properties.Add(19);
                    properties.Add(3);
                    properties.Add(142);
                    break;

                case (UnitType.Tower):
                    properties.Add(TOWER_HP);
                    properties.Add(TOWER_SIGHT);
                    properties.Add(TOWER_BUILD_TIME);
                    properties.Add(TOWER_GOLD_COST);
                    properties.Add(TOWER_WOOD_COST);
                    properties.Add(TOWER_OIL_COST);
                    properties.Add(TOWER_ARMOR);
                    properties.Add((int)TOWER_SPEED);
                    properties.Add(542);
                    properties.Add(600);
                    properties.Add(466);
                    properties.Add(0);
                    // half build building sprite position
                    properties.Add(410);
                    properties.Add(549);
                    properties.Add(467);
                    properties.Add(74);

                    break;

                case (UnitType.TownHall):
                    properties.Add(TOWNHALL_HP);
                    properties.Add(TOWNHALL_SIGHT);
                    properties.Add(TOWNHALL_BUILD_TIME);
                    properties.Add(TOWNHALL_GOLD_COST);
                    properties.Add(TOWNHALL_WOOD_COST);
                    properties.Add(TOWNHALL_OIL_COST);
                    properties.Add(TOWNHALL_ARMOR);
                    properties.Add((int)TOWNHALL_SPEED);
                    properties.Add(138);
                    properties.Add(540);
                    properties.Add(268);
                    properties.Add(0);
                    // half build building sprite position
                    properties.Add(10);
                    properties.Add(550);
                    properties.Add(267);
                    properties.Add(135);
                    break;
                #endregion

            }
            return properties;

        }
        public static List<int> getUnitProperties(UnitType type, bool isOrc = false)
        {
            #region getUnitProperties
            List<int> properties = new List<int>();

            switch (type)
            {
                case (UnitType.Archer):
                    properties.Add(ARCHER_HP);
                    properties.Add(ARCHER_MANA);
                    properties.Add(ARCHER_SIGHT);
                    properties.Add(ARCHER_DAMAGE);
                    properties.Add(ARCHER_BUILD_TIME);
                    properties.Add(ARCHER_GOLD_COST);
                    properties.Add(ARCHER_WOOD_COST);
                    properties.Add(ARCHER_OIL_COST);
                    properties.Add(ARCHER_RANGE);
                    properties.Add(ARCHER_ARMOR);
                    properties.Add((int)ARCHER_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)ARCHER_SPRITE_SIZE.X);
                        properties.Add((int)ARCHER_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)OARCHER_SPRITE_SIZE.X);
                        properties.Add((int)OARCHER_SPRITE_SIZE.Y);
                    }
                    break;

                case (UnitType.Battleship):
                    properties.Add(BATLLESHIP_HP);
                    properties.Add(BATLLESHIP_MANA);
                    properties.Add(BATLLESHIP_SIGHT);
                    properties.Add(BATLLESHIP_DAMAGE);
                    properties.Add(BATLLESHIP_BUILD_TIME);
                    properties.Add(BATLLESHIP_GOLD_COST);
                    properties.Add(BATLLESHIP_WOOD_COST);
                    properties.Add(BATLLESHIP_OIL_COST);
                    properties.Add(BATLLESHIP_RANGE);
                    properties.Add(BATLLESHIP_ARMOR);
                    properties.Add((int)BATLLESHIP_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)BATTLESHIP_SPRITE_SIZE.X);
                        properties.Add((int)BATTLESHIP_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)OBATTLESHIP_SPRITE_SIZE.X);
                        properties.Add((int)OBATTLESHIP_SPRITE_SIZE.Y);
                    }
                    break;

                case (UnitType.Catapulte):
                    properties.Add(CATAPULTE_HP);
                    properties.Add(CATAPULTE_MANA);
                    properties.Add(CATAPULTE_SIGHT);
                    properties.Add(CATAPULTE_DAMAGE);
                    properties.Add(CATAPULTE_BUILD_TIME);
                    properties.Add(CATAPULTE_GOLD_COST);
                    properties.Add(CATAPULTE_WOOD_COST);
                    properties.Add(CATAPULTE_OIL_COST);
                    properties.Add(CATAPULTE_RANGE);
                    properties.Add(CATAPULTE_ARMOR);
                    properties.Add((int)CATAPULTE_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)CATAPULTE_SPRITE_SIZE.X);
                        properties.Add((int)CATAPULTE_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)OCATAPULTE_SPRITE_SIZE.X);
                        properties.Add((int)OCATAPULTE_SPRITE_SIZE.Y);
                    }
                    break;
                case (UnitType.Chevalier):
                    properties.Add(KNIGHT_HP);
                    properties.Add(KNIGHT_MANA);
                    properties.Add(KNIGHT_SIGHT);
                    properties.Add(KNIGHT_DAMAGE);
                    properties.Add(KNIGHT_BUILD_TIME);
                    properties.Add(KNIGHT_GOLD_COST);
                    properties.Add(KNIGHT_WOOD_COST);
                    properties.Add(KNIGHT_OIL_COST);
                    properties.Add(KNIGHT_RANGE);
                    properties.Add(KNIGHT_ARMOR);
                    properties.Add((int)KNIGHT_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)KNHIGHT_SPRITE_SIZE.X);
                        properties.Add((int)KNHIGHT_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)OKNHIGHT_SPRITE_SIZE.X);
                        properties.Add((int)OKNHIGHT_SPRITE_SIZE.Y);
                    }
                    break;
                case (UnitType.Destroyer):
                    properties.Add(DESTROYER_HP);
                    properties.Add(DESTROYER_MANA);
                    properties.Add(DESTROYER_SIGHT);
                    properties.Add(DESTROYER_DAMAGE);
                    properties.Add(DESTROYER_BUILD_TIME);
                    properties.Add(DESTROYER_GOLD_COST);
                    properties.Add(DESTROYER_WOOD_COST);
                    properties.Add(DESTROYER_OIL_COST);
                    properties.Add(DESTROYER_RANGE);
                    properties.Add(DESTROYER_ARMOR);
                    properties.Add((int)DESTROYER_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)DESTROYER_SPRITE_SIZE.X);
                        properties.Add((int)DESTROYER_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)ODESTROYER_SPRITE_SIZE.X);
                        properties.Add((int)ODESTROYER_SPRITE_SIZE.Y);
                    }
                    break;
                case (UnitType.Fantassin):
                    properties.Add(FANTASSIN_HP);
                    properties.Add(FANTASSIN_MANA);
                    properties.Add(FANTASSIN_SIGHT);
                    properties.Add(FANTASSIN_DAMAGE);
                    properties.Add(FANTASSIN_BUILD_TIME);
                    properties.Add(FANTASSIN_GOLD_COST);
                    properties.Add(FANTASSIN_WOOD_COST);
                    properties.Add(FANTASSIN_OIL_COST);
                    properties.Add(FANTASSIN_RANGE);
                    properties.Add(FANTASSIN_ARMOR);
                    properties.Add((int)FANTASSIN_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)FANTASSIN_SPRITE_SIZE.X);
                        properties.Add((int)FANTASSIN_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)OFANTASSIN_SPRITE_SIZE.X);
                        properties.Add((int)OFANTASSIN_SPRITE_SIZE.Y);
                    }
                    break;
                case (UnitType.Griffon):
                    properties.Add(DRAGON_HP);
                    properties.Add(DRAGON_MANA);
                    properties.Add(DRAGON_SIGHT);
                    properties.Add(DRAGON_DAMAGE);
                    properties.Add(DRAGON_BUILD_TIME);
                    properties.Add(DRAGON_GOLD_COST);
                    properties.Add(DRAGON_WOOD_COST);
                    properties.Add(DRAGON_OIL_COST);
                    properties.Add(DRAGON_RANGE);
                    properties.Add(DRAGON_ARMOR);
                    properties.Add((int)DRAGON_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)DRAGON_SPRITE_SIZE.X);
                        properties.Add((int)DRAGON_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)ODRAGON_SPRITE_SIZE.X);
                        properties.Add((int)ODRAGON_SPRITE_SIZE.Y);
                    }
                    break;
                case (UnitType.Mage):
                    properties.Add(MAGE_HP);
                    properties.Add(MAGE_MANA);
                    properties.Add(MAGE_SIGHT);
                    properties.Add(MAGE_DAMAGE);
                    properties.Add(MAGE_BUILD_TIME);
                    properties.Add(MAGE_GOLD_COST);
                    properties.Add(MAGE_WOOD_COST);
                    properties.Add(MAGE_OIL_COST);
                    properties.Add(MAGE_RANGE);
                    properties.Add(MAGE_ARMOR);
                    properties.Add((int)MAGE_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)MAGE_SPRITE_SIZE.X);
                        properties.Add((int)MAGE_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)OMAGE_SPRITE_SIZE.X);
                        properties.Add((int)OMAGE_SPRITE_SIZE.Y);
                    }
                    break;
                case (UnitType.Nains):
                    properties.Add(DWARVES_HP);
                    properties.Add(DWARVES_MANA);
                    properties.Add(DWARVES_SIGHT);
                    properties.Add(DWARVES_DAMAGE);
                    properties.Add(DWARVES_BUILD_TIME);
                    properties.Add(DWARVES_GOLD_COST);
                    properties.Add(DWARVES_WOOD_COST);
                    properties.Add(DWARVES_OIL_COST);
                    properties.Add(DWARVES_RANGE);
                    properties.Add(DWARVES_ARMOR);
                    properties.Add((int)DWARVES_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)DWARVES_SPRITE_SIZE.X);
                        properties.Add((int)DWARVES_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)ODWARVES_SPRITE_SIZE.X);
                        properties.Add((int)ODWARVES_SPRITE_SIZE.Y);
                    }
                    break;

                case (UnitType.Paladin):
                    properties.Add(PALADIN_HP);
                    properties.Add(PALADIN_MANA);
                    properties.Add(PALADIN_SIGHT);
                    properties.Add(PALADIN_DAMAGE);
                    properties.Add(PALADIN_BUILD_TIME);
                    properties.Add(PALADIN_GOLD_COST);
                    properties.Add(PALADIN_WOOD_COST);
                    properties.Add(PALADIN_OIL_COST);
                    properties.Add(PALADIN_RANGE);
                    properties.Add(PALADIN_ARMOR);
                    properties.Add((int)PALADIN_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)PALADIN_SPRITE_SIZE.X);
                        properties.Add((int)PALADIN_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)OPALADIN_SPRITE_SIZE.X);
                        properties.Add((int)OPALADIN_SPRITE_SIZE.Y);
                    }
                    break;

                case (UnitType.Paysan):
                    properties.Add(PAYSAN_HP);
                    properties.Add(PAYSAN_MANA);
                    properties.Add(PAYSAN_SIGHT);
                    properties.Add(PAYSAN_DAMAGE);
                    properties.Add(PAYSAN_BUILD_TIME);
                    properties.Add(PAYSAN_GOLD_COST);
                    properties.Add(PAYSAN_WOOD_COST);
                    properties.Add(PAYSAN_OIL_COST);
                    properties.Add(PAYSAN_RANGE);
                    properties.Add(PAYSAN_ARMOR);
                    properties.Add((int)PAYSAN_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)PAYSAN_SPRITE_SIZE.X);
                        properties.Add((int)PAYSAN_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)OPAYSAN_SPRITE_SIZE.X);
                        properties.Add((int)OPAYSAN_SPRITE_SIZE.Y);
                    }
                    break;

                case (UnitType.Petrolier):
                    properties.Add(OILTANKER_HP);
                    properties.Add(OILTANKER_MANA);
                    properties.Add(OILTANKER_SIGHT);
                    properties.Add(OILTANKER_DAMAGE);
                    properties.Add(OILTANKER_BUILD_TIME);
                    properties.Add(OILTANKER_GOLD_COST);
                    properties.Add(OILTANKER_WOOD_COST);
                    properties.Add(OILTANKER_OIL_COST);
                    properties.Add(OILTANKER_RANGE);
                    properties.Add(OILTANKER_ARMOR);
                    properties.Add((int)OILTANKER_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)OILTANKER_SPRITE_SIZE.X);
                        properties.Add((int)OILTANKER_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)OOILTANKER_SPRITE_SIZE.X);
                        properties.Add((int)OOILTANKER_SPRITE_SIZE.Y);
                    }
                    break;

                case (UnitType.Sousmarin):
                    properties.Add(SUBMARINE_HP);
                    properties.Add(SUBMARINE_MANA);
                    properties.Add(SUBMARINE_SIGHT);
                    properties.Add(SUBMARINE_DAMAGE);
                    properties.Add(SUBMARINE_BUILD_TIME);
                    properties.Add(SUBMARINE_GOLD_COST);
                    properties.Add(SUBMARINE_WOOD_COST);
                    properties.Add(SUBMARINE_OIL_COST);
                    properties.Add(SUBMARINE_RANGE);
                    properties.Add(SUBMARINE_ARMOR);
                    properties.Add((int)SUBMARINE_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)OSUBMARINE_SPRITE_SIZE.X);
                        properties.Add((int)OSUBMARINE_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)SUBMARINE_SPRITE_SIZE.X);
                        properties.Add((int)SUBMARINE_SPRITE_SIZE.Y);
                    }
                    break;
                case (UnitType.Transport):
                    properties.Add(TRANSPORT_HP);
                    properties.Add(TRANSPORT_MANA);
                    properties.Add(TRANSPORT_SIGHT);
                    properties.Add(TRANSPORT_DAMAGE);
                    properties.Add(TRANSPORT_BUILD_TIME);
                    properties.Add(TRANSPORT_GOLD_COST);
                    properties.Add(TRANSPORT_WOOD_COST);
                    properties.Add(TRANSPORT_OIL_COST);
                    properties.Add(TRANSPORT_RANGE);
                    properties.Add(TRANSPORT_ARMOR);
                    properties.Add((int)TRANSPORT_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)TRANSPORT_SPRITE_SIZE.X);
                        properties.Add((int)TRANSPORT_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)OTRANSPORT_SPRITE_SIZE.X);
                        properties.Add((int)OTRANSPORT_SPRITE_SIZE.Y);
                    }
                    break;
                case (UnitType.MachineVolante):
                    properties.Add(FLYINGMACHINE_HP);
                    properties.Add(FLYINGMACHINE_MANA);
                    properties.Add(FLYINGMACHINE_SIGHT);
                    properties.Add(FLYINGMACHINE_DAMAGE);
                    properties.Add(FLYINGMACHINE_BUILD_TIME);
                    properties.Add(FLYINGMACHINE_GOLD_COST);
                    properties.Add(FLYINGMACHINE_WOOD_COST);
                    properties.Add(FLYINGMACHINE_OIL_COST);
                    properties.Add(FLYINGMACHINE_RANGE);
                    properties.Add(FLYINGMACHINE_ARMOR);
                    properties.Add((int)FLYINGMACHINE_SPEED);
                    if (!isOrc)
                    {
                        properties.Add((int)FLYINGMACHINE_SPRITE_SIZE.X);
                        properties.Add((int)FLYINGMACHINE_SPRITE_SIZE.Y);
                    }
                    else
                    {
                        properties.Add((int)OFLYINGMACHINE_SPRITE_SIZE.X);
                        properties.Add((int)OFLYINGMACHINE_SPRITE_SIZE.Y);
                    }
                    break;
            }
            #endregion


            return properties;
        }
        #region unit stats

        // second sprite size is for orc equivalent
        public static int FANTASSIN_HP = 60;
        public static int FANTASSIN_MANA = -1;
        public static int FANTASSIN_SIGHT = 4;
        public static int FANTASSIN_DAMAGE = 6;
        public static int FANTASSIN_BUILD_TIME = 15;
        public static int FANTASSIN_GOLD_COST = 600;
        public static int FANTASSIN_WOOD_COST = 0;
        public static int FANTASSIN_OIL_COST = 0;
        public static int FANTASSIN_RANGE = 1;
        public static int FANTASSIN_ARMOR = 2;
        public static float FANTASSIN_SPEED = 5;
        public static Vector2 FANTASSIN_SPRITE_SIZE = new Vector2(40, 40);
        public static Vector2 OFANTASSIN_SPRITE_SIZE = new Vector2(40, 40);


        public static int ARCHER_HP = 40;
        public static int ARCHER_MANA = -1;
        public static int ARCHER_SIGHT = 5;
        public static int ARCHER_DAMAGE = 6;
        public static int ARCHER_BUILD_TIME = 20;
        public static int ARCHER_GOLD_COST = 500;
        public static int ARCHER_WOOD_COST = 50;
        public static int ARCHER_OIL_COST = 0;
        public static int ARCHER_RANGE = 4;
        public static int ARCHER_ARMOR = 0;
        public static float ARCHER_SPEED = 5;
        public static Vector2 ARCHER_SPRITE_SIZE = new Vector2(43, 43);
        public static Vector2 OARCHER_SPRITE_SIZE = new Vector2(38, 45);


        public static int PAYSAN_HP = 30;
        public static int PAYSAN_MANA = -1;
        public static int PAYSAN_SIGHT = 4;
        public static int PAYSAN_DAMAGE = 3;
        public static int PAYSAN_BUILD_TIME = 10;
        public static int PAYSAN_GOLD_COST = 400;
        public static int PAYSAN_WOOD_COST = 0;
        public static int PAYSAN_OIL_COST = 0;
        public static int PAYSAN_RANGE = 1;
        public static int PAYSAN_ARMOR = 0;
        public static float PAYSAN_SPEED = 4;
        public static Vector2 PAYSAN_SPRITE_SIZE = new Vector2(26, 26);
        public static Vector2 OPAYSAN_SPRITE_SIZE = new Vector2(30, 30);



        public static int CATAPULTE_HP = 110;
        public static int CATAPULTE_MANA = -1;
        public static int CATAPULTE_SIGHT = 9;
        public static int CATAPULTE_DAMAGE = 80;
        public static int CATAPULTE_BUILD_TIME = 75;
        public static int CATAPULTE_GOLD_COST = 900;
        public static int CATAPULTE_WOOD_COST = 100;
        public static int CATAPULTE_OIL_COST = 0;
        public static int CATAPULTE_RANGE = 8;
        public static int CATAPULTE_ARMOR = 0;
        public static float CATAPULTE_SPEED = 3;
        public static Vector2 CATAPULTE_SPRITE_SIZE = new Vector2(58, 59);
        public static Vector2 OCATAPULTE_SPRITE_SIZE = new Vector2(57, 58);


        public static int MAGE_HP = 60;
        public static int MAGE_MANA = 255;
        public static int MAGE_SIGHT = 9;
        public static int MAGE_DAMAGE = 5;
        public static int MAGE_BUILD_TIME = 40;
        public static int MAGE_GOLD_COST = 1200;
        public static int MAGE_WOOD_COST = 0;
        public static int MAGE_OIL_COST = 0;
        public static int MAGE_RANGE = 2;
        public static int MAGE_ARMOR = 0;
        public static float MAGE_SPEED = 7;
        public static Vector2 MAGE_SPRITE_SIZE = new Vector2(40, 40);
        public static Vector2 OMAGE_SPRITE_SIZE = new Vector2(45, 55);


        public static int FLYINGMACHINE_HP = 150;
        public static int FLYINGMACHINE_MANA = -1;
        public static int FLYINGMACHINE_SIGHT = 9;
        public static int FLYINGMACHINE_DAMAGE = 0;
        public static int FLYINGMACHINE_BUILD_TIME = 22;
        public static int FLYINGMACHINE_GOLD_COST = 500;
        public static int FLYINGMACHINE_WOOD_COST = 100;
        public static int FLYINGMACHINE_OIL_COST = 0;
        public static int FLYINGMACHINE_RANGE = 0;
        public static int FLYINGMACHINE_ARMOR = 0;
        public static float FLYINGMACHINE_SPEED = 9;
        public static Vector2 FLYINGMACHINE_SPRITE_SIZE = new Vector2(64, 64);
        public static Vector2 OFLYINGMACHINE_SPRITE_SIZE = new Vector2(64, 64);




        public static int KNIGHT_HP = 90;
        public static int KNIGHT_MANA = -1;
        public static int KNIGHT_SIGHT = 4;
        public static int KNIGHT_DAMAGE = 8;
        public static int KNIGHT_BUILD_TIME = 30;
        public static int KNIGHT_GOLD_COST = 800;
        public static int KNIGHT_WOOD_COST = 100;
        public static int KNIGHT_OIL_COST = 0;
        public static int KNIGHT_RANGE = 1;
        public static int KNIGHT_ARMOR = 4;
        public static float KNIGHT_SPEED = 7;
        public static Vector2 KNHIGHT_SPRITE_SIZE = new Vector2(50, 50);
        public static Vector2 OKNHIGHT_SPRITE_SIZE = new Vector2(42, 50);



        public static int PALADIN_HP = 90;
        public static int PALADIN_MANA = -1;
        public static int PALADIN_SIGHT = 5;
        public static int PALADIN_DAMAGE = 12;
        public static int PALADIN_BUILD_TIME = 30;
        public static int PALADIN_GOLD_COST = 800;
        public static int PALADIN_WOOD_COST = 100;
        public static int PALADIN_OIL_COST = 0;
        public static int PALADIN_RANGE = 1;
        public static int PALADIN_ARMOR = 6;
        public static float PALADIN_SPEED = 7;
        public static Vector2 PALADIN_SPRITE_SIZE = new Vector2(50, 50);
        public static Vector2 OPALADIN_SPRITE_SIZE = new Vector2(42, 50);


        public static int DRAGON_HP = 100;
        public static int DRAGON_MANA = -1;
        public static int DRAGON_SIGHT = 6;
        public static int DRAGON_DAMAGE = 16;
        public static int DRAGON_BUILD_TIME = 85;
        public static int DRAGON_GOLD_COST = 2500;
        public static int DRAGON_WOOD_COST = 0;
        public static int DRAGON_OIL_COST = 0;
        public static int DRAGON_RANGE = 4;
        public static int DRAGON_ARMOR = 10;
        public static float DRAGON_SPEED = 5;
        public static Vector2 DRAGON_SPRITE_SIZE = new Vector2(70, 70);
        public static Vector2 ODRAGON_SPRITE_SIZE = new Vector2(75 , 75);


        public static int OILTANKER_HP = 90;
        public static int OILTANKER_MANA = -1;
        public static int OILTANKER_SIGHT = 4;
        public static int OILTANKER_DAMAGE = 0;
        public static int OILTANKER_BUILD_TIME = 18;
        public static int OILTANKER_GOLD_COST = 500;
        public static int OILTANKER_WOOD_COST = 100;
        public static int OILTANKER_OIL_COST = 0;
        public static int OILTANKER_RANGE = 1;
        public static int OILTANKER_ARMOR = 0;
        public static float OILTANKER_SPEED = 7;
        public static Vector2 OILTANKER_SPRITE_SIZE = new Vector2(58, 58);
        public static Vector2 OOILTANKER_SPRITE_SIZE = new Vector2(58, 55);


        public static int DESTROYER_HP = 100;
        public static int DESTROYER_MANA = -1;
        public static int DESTROYER_SIGHT = 8;
        public static int DESTROYER_DAMAGE = 35;
        public static int DESTROYER_BUILD_TIME = 30;
        public static int DESTROYER_GOLD_COST = 700;
        public static int DESTROYER_WOOD_COST = 350;
        public static int DESTROYER_OIL_COST = 700;
        public static int DESTROYER_RANGE = 4;
        public static int DESTROYER_ARMOR = 10;
        public static float DESTROYER_SPEED = 8;
        public static Vector2 DESTROYER_SPRITE_SIZE = new Vector2(69, 69);
        public static Vector2 ODESTROYER_SPRITE_SIZE = new Vector2(66, 66);



        public static int BATLLESHIP_HP = 150;
        public static int BATLLESHIP_MANA = -1;
        public static int BATLLESHIP_SIGHT = 8;
        public static int BATLLESHIP_DAMAGE = 130;
        public static int BATLLESHIP_BUILD_TIME = 50;
        public static int BATLLESHIP_GOLD_COST = 1000;
        public static int BATLLESHIP_WOOD_COST = 500;
        public static int BATLLESHIP_OIL_COST = 1000;
        public static int BATLLESHIP_RANGE = 6;
        public static int BATLLESHIP_ARMOR = 15;
        public static float BATLLESHIP_SPEED = 4;
        public static Vector2 BATTLESHIP_SPRITE_SIZE = new Vector2(75, 75);
        public static Vector2 OBATTLESHIP_SPRITE_SIZE = new Vector2(85, 85);

        public static int TRANSPORT_HP = 150;
        public static int TRANSPORT_MANA = -1;
        public static int TRANSPORT_SIGHT = 4;
        public static int TRANSPORT_DAMAGE = 0;
        public static int TRANSPORT_BUILD_TIME = 25;
        public static int TRANSPORT_GOLD_COST = 400;
        public static int TRANSPORT_WOOD_COST = 0;
        public static int TRANSPORT_OIL_COST = 0;
        public static int TRANSPORT_RANGE = 1;
        public static int TRANSPORT_ARMOR = 0;
        public static float TRANSPORT_SPEED = 7;
        public static Vector2 TRANSPORT_SPRITE_SIZE = new Vector2(65, 65);
        public static Vector2 OTRANSPORT_SPRITE_SIZE = new Vector2(59, 59);

        public static int SUBMARINE_HP = 60;
        public static int SUBMARINE_MANA = -1;
        public static int SUBMARINE_SIGHT = 6;
        public static int SUBMARINE_DAMAGE = 35;
        public static int SUBMARINE_BUILD_TIME = 30;
        public static int SUBMARINE_GOLD_COST = 400;
        public static int SUBMARINE_WOOD_COST = 0;
        public static int SUBMARINE_OIL_COST = 0;
        public static int SUBMARINE_RANGE = 4;
        public static int SUBMARINE_ARMOR = 0;
        public static float SUBMARINE_SPEED = 6;
        public static Vector2 SUBMARINE_SPRITE_SIZE = new Vector2(60, 60);
        public static Vector2 OSUBMARINE_SPRITE_SIZE = new Vector2(71, 65);

        public static int DWARVES_HP = 60;
        public static int DWARVES_MANA = -1;
        public static int DWARVES_SIGHT = 6;
        public static int DWARVES_DAMAGE = 5;
        public static int DWARVES_BUILD_TIME = 20;
        public static int DWARVES_GOLD_COST = 400;
        public static int DWARVES_WOOD_COST = 0;
        public static int DWARVES_OIL_COST = 0;
        public static int DWARVES_RANGE = 1;
        public static int DWARVES_ARMOR = 0;
        public static float DWARVES_SPEED = 7;
        public static Vector2 DWARVES_SPRITE_SIZE = new Vector2(40, 40);
        public static Vector2 ODWARVES_SPRITE_SIZE = new Vector2(35, 35);

        public static int FARM_HP = 400;
        public static int FARM_SIGHT = 3;
        public static int FARM_BUILD_TIME = 5;
        public static int FARM_GOLD_COST = 500;
        public static int FARM_WOOD_COST = 250;
        public static int FARM_OIL_COST = 0;
        public static int FARM_ARMOR = 0;
        public static int FARM_SPEED = 0;


        public static int MILITARYBASE_HP = 800;
        public static int MILITARYBASE_SIGHT = 4;
        public static int MILITARYBASE_BUILD_TIME = 15;
        public static int MILITARYBASE_GOLD_COST = 700;
        public static int MILITARYBASE_WOOD_COST = 450;
        public static int MILITARYBASE_OIL_COST = 0;
        public static int MILITARYBASE_ARMOR = 0;
        public static int MILITARYBASE_SPEED = 0;

        public static int LUMBERMILL_HP = 600;
        public static int LUMBERMILL_SIGHT = 4;
        public static int LUMBERMILL_BUILD_TIME = 20;
        public static int LUMBERMILL_GOLD_COST = 700;
        public static int LUMBERMILL_WOOD_COST = 450;
        public static int LUMBERMILL_OIL_COST = 0;
        public static int LUMBERMILL_ARMOR = 0;
        public static int LUMBERMILL_SPEED = 0;

        public static int FORGE_HP = 800;
        public static int FORGE_SIGHT = 4;
        public static int FORGE_BUILD_TIME = 20;
        public static int FORGE_GOLD_COST = 800;
        public static int FORGE_WOOD_COST = 450;
        public static int FORGE_OIL_COST = 100;
        public static int FORGE_ARMOR = 0;
        public static int FORGE_SPEED = 0;

        public static int CHANTIERNAVAL_HP = 1100;
        public static int CHANTIERNAVAL_SIGHT = 4;
        public static int CHANTIERNAVAL_BUILD_TIME = 45;
        public static int CHANTIERNAVAL_GOLD_COST = 700;
        public static int CHANTIERNAVAL_WOOD_COST = 350;
        public static int CHANTIERNAVAL_OIL_COST = 0;
        public static int CHANTIERNAVAL_ARMOR = 0;
        public static int CHANTIERNAVAL_SPEED = 0;

        public static int TOWER_HP = 100;
        public static int TOWER_SIGHT = 9;
        public static int TOWER_BUILD_TIME = 15;
        public static int TOWER_GOLD_COST = 500;
        public static int TOWER_WOOD_COST = 200;
        public static int TOWER_OIL_COST = 0;
        public static int TOWER_ARMOR = 3;
        public static int TOWER_SPEED = 0;

        public static int FOUNDRY_HP = 750;
        public static int FOUNDRY_SIGHT = 4;
        public static int FOUNDRY_BUILD_TIME = 45;
        public static int FOUNDRY_GOLD_COST = 700;
        public static int FOUNDRY_WOOD_COST = 450;
        public static int FOUNDRY_OIL_COST = 350;
        public static int FOUNDRY_ARMOR = 0;
        public static int FOUNDRY_SPEED = 0;

        public static int RAFINNERY_HP = 800;
        public static int RAFINNERY_SIGHT = 4;
        public static int RAFINNERY_BUILD_TIME = 45;
        public static int RAFINNERY_GOLD_COST = 700;
        public static int RAFINNERY_WOOD_COST = 350;
        public static int RAFINNERY_OIL_COST = 350;
        public static int RAFINNERY_ARMOR = 0;
        public static int RAFINNERY_SPEED = 0;

        public static int CANNONTOWER_HP = 160;
        public static int CANNONTOWER_SIGHT = 9;
        public static int CANNONTOWER_BUILD_TIME = 45;
        public static int CANNONTOWER_GOLD_COST = 1000;
        public static int CANNONTOWER_WOOD_COST = 300;
        public static int CANNONTOWER_OIL_COST = 0;
        public static int CANNONTOWER_ARMOR = 0;
        public static int CANNONTOWER_SPEED = 0;

        public static int TOWNHALL_HP = 1200;
        public static int TOWNHALL_SIGHT = 5;
        public static int TOWNHALL_BUILD_TIME = 15;
        public static int TOWNHALL_GOLD_COST = 1200;
        public static int TOWNHALL_WOOD_COST = 800;
        public static int TOWNHALL_OIL_COST = 0;
        public static int TOWNHALL_ARMOR = 0;
        public static int TOWNHALL_SPEED = 0;

        public static int STRONGHOLD_HP = 400;
        public static int STRONGHOLD_SIGHT = 5;
        public static int STRONGHOLD_BUILD_TIME = 45;
        public static int STRONGHOLD_GOLD_COST = 2000;
        public static int STRONGHOLD_WOOD_COST = 1000;
        public static int STRONGHOLD_OIL_COST = 200;
        public static int STRONGHOLD_ARMOR = 0;
        public static int STRONGHOLD_SPEED = 0;

        public static int FORTRESS_HP = 1400;
        public static int FORTRESS_SIGHT = 5;
        public static int FORTRESS_BUILD_TIME = 45;
        public static int FORTRESS_GOLD_COST = 2500;
        public static int FORTRESS_WOOD_COST = 1200;
        public static int FORTRESS_OIL_COST = 500;
        public static int FORTRESS_ARMOR = 0;
        public static int FORTRESS_SPEED = 0;

        public static int BALLISTATOWER_HP = 130;
        public static int BALLISTATOWER_SIGHT = 9;
        public static int BALLISTATOWER_BUILD_TIME = 20;
        public static int BALLISTATOWER_GOLD_COST = 500;
        public static int BALLISTATOWER_WOOD_COST = 100;
        public static int BALLISTATOWER_OIL_COST = 0;
        public static int BALLISTATOWER_ARMOR = 3;
        public static int BALLISTATOWER_SPEED = 0;

        public static int STABLE_HP = 800;
        public static int STABLE_SIGHT = 4;
        public static int STABLE_BUILD_TIME = 45;
        public static int STABLE_GOLD_COST = 1000;
        public static int STABLE_WOOD_COST = 450;
        public static int STABLE_OIL_COST = 0;
        public static int STABLE_ARMOR = 0;
        public static int STABLE_SPEED = 0;

        public static int GOBLIN_WORKBENCH_HP = 1100;
        public static int GOBLIN_WORKBENCH_SIGHT = 4;
        public static int GOBLIN_WORKBENCH_BUILD_TIME = 45;
        public static int GOBLIN_WORKBENCH_GOLD_COST = 1000;
        public static int GOBLIN_WORKBENCH_WOOD_COST = 450;
        public static int GOBLIN_WORKBENCH_OIL_COST = 0;
        public static int GOBLIN_WORKBENCH_ARMOR = 0;
        public static int GOBLIN_WORKBENCH_SPEED = 0;


        public static int NAVALPLATFORM_HP = 700;
        public static int NAVALPLATFORM_SIGHT = 4;
        public static int NAVALPLATFORM_BUILD_TIME = 45;
        public static int NAVALPLATFORM_GOLD_COST = 700;
        public static int NAVALPLATFORM_WOOD_COST = 450;
        public static int NAVALPLATFORM_OIL_COST = 0;
        public static int NAVALPLATFORM_ARMOR = 0;
        public static int NAVALPLATFORM_SPEED = 0;

        public static int CHURCH_HP = 1000;
        public static int CHURCH_SIGHT = 4;
        public static int CHURCH_BUILD_TIME = 45;
        public static int CHURCH_GOLD_COST = 1000;
        public static int CHURCH_WOOD_COST = 450;
        public static int CHURCH_OIL_COST = 0;
        public static int CHURCH_ARMOR = 0;
        public static int CHURCH_SPEED = 0;

        public static int GRYFFINTOWER_HP = 1000;
        public static int GRYFFINTOWER_SIGHT = 4;
        public static int GRYFFINTOWER_BUILD_TIME = 45;
        public static int GRYFFINTOWER_GOLD_COST = 1000;
        public static int GRYFFINTOWER_WOOD_COST = 450;
        public static int GRYFFINTOWER_OIL_COST = 0;
        public static int GRYFFINTOWER_ARMOR = 0;
        public static int GRYFFINTOWER_SPEED = 0;

        public static int MAGETOWER_HP = 600;
        public static int MAGETOWER_SIGHT = 4;
        public static int MAGETOWER_BUILD_TIME = 45;
        public static int MAGETOWER_GOLD_COST = 1000;
        public static int MAGETOWER_WOOD_COST = 450;
        public static int MAGETOWER_OIL_COST = 0;
        public static int MAGETOWER_ARMOR = 0;
        public static int MAGETOWER_SPEED = 0;
        #endregion

        // unit animations stats
        // HUMANS

        /* DEPRECATED
        public static List<Vector2> FANTASSIN_ATTACKS;
        public static List<Vector2> ARCHER_ATTACKS;
        public static List<Vector2> BALLISTA_ATTACKS;
        public static List<Vector2> MAGE_ATTACKS;
        public static List<Vector2> PEASANT_ATTACKS;
        public static List<Vector2> KNIGHT_ATTACKS;
        public static List<Vector2> GRYFFON_ATTACKS;
        public static List<Vector2> SUBMARINE_ATTACKS;

        public static List<Vector2> FANTASSIN_MOVES;
        public static List<Vector2> ARCHER_MOVES;
        public static List<Vector2> BALLISTA_MOVES;
        public static List<Vector2> MAGE_MOVES;
        public static List<Vector2> PEASANT_MOVES;
        public static List<Vector2> KNIGHT_MOVES;
        public static List<Vector2> GRYFFON_MOVES;
        public static List<Vector2> SUBMARINE_MOVES;

        public static List<Vector2> PEASANT_COLLECT_WOOD;
        public static List<Vector2> PEASANT_CARRY_GOLD;
        public static List<Vector2> PEASANT_CARRY_WOOD;
        public static List<Vector2> TANKER_FULL;
        public static List<Vector2> OPEASANT_COLLECT_WOOD;
        public static List<Vector2> OPEASANT_CARRY_GOLD;
        public static List<Vector2> OPEASANT_CARRY_WOOD;
        public static List<Vector2> OTANKER_FULL;


        public static Vector2 FANTASSIN_STILL;
        public static Vector2 ARCHER_STILL;
        public static Vector2 BALLISTA_STILL;
        public static Vector2 MAGE_STILL;
        public static Vector2 PEASANT_STILL;
        public static Vector2 KNIGHT_STILL;
        public static Vector2 GRYFFON_STILL;
        public static Vector2 SUBMARINE_STILL;
        public static Vector2 HUMANTRANSPORT_STILL;
        public static Vector2 HUMANOILHARVESTER_STILL;
        public static Vector2 HUMANDESTROYER_STILL;
        public static Vector2 HUMANFLYMACHINE_STILL;
        public static Vector2 PALADIN_STILL;
        public static Vector2 DWARVES_STILL;
        public static Vector2 BATTLESHIP_STILL;

        // ORCS
        public static List<Vector2> GRUNT_ATTACKS;
        public static List<Vector2> TROLL_ATTACKS;
        public static List<Vector2> CATAPULTE_ATTACKS;
        public static List<Vector2> SORCERER_ATTACKS;
        public static List<Vector2> PEON_ATTACKS;
        public static List<Vector2> OGRE_ATTACKS;
        public static List<Vector2> DRAGON_ATTACKS;
        public static List<Vector2> TURTLE_ATTACKS;

        public static List<Vector2> GRUNT_MOVES;
        public static List<Vector2> TROLL_MOVES;
        public static List<Vector2> CATAPULTE_MOVES;
        public static List<Vector2> SORCERER_MOVES;
        public static List<Vector2> PEON_MOVES;
        public static List<Vector2> OGRE_MOVES;
        public static List<Vector2> DRAGON_MOVES;
        public static List<Vector2> TURTLE_MOVES;


        public static List<Vector2> ORCTRANSPORTMOVES;
        public static List<Vector2> ORCOILHARVESTERMOVES;
        public static List<Vector2> ORCDESTROYERMOVES;
        public static List<Vector2> ORCFLYMACHINEMOVES;
        public static List<Vector2> OGREMAGEMOVES;
        public static List<Vector2> GOBLINSMOVES;
        public static List<Vector2> JUGGERNAUTMOVES;

        public static List<Vector2> HUMANTRANSPORTMOVES;
        public static List<Vector2> HUMANOILHARVESTERMOVES;
        public static List<Vector2> HUMANDESTROYERMOVES;
        public static List<Vector2> HUMANFLYMACHINEMOVES;
        public static List<Vector2> PALADINMOVES;
        public static List<Vector2> DWARVESMOVES;
        public static List<Vector2> BATTLESHIPMOVES;

        */

        /* DEPRECATED
        public static Vector2 ORCTRANSPORT_STILL;
        public static Vector2 ORCOILHARVESTER_STILL;
        public static Vector2 ORCDESTROYER_STILL;
        public static Vector2 ORCFLYMACHINE_STILL;
        public static Vector2 OGREMAGE_STILL;
        public static Vector2 GOBLINS_STILL;
        public static Vector2 JUGGERNAUT_STILL;
        public static Vector2 GRUNT_STILL;
        public static Vector2 TROLL_STILL;
        public static Vector2 CATAPULTE_STILL;
        public static Vector2 SORCERER_STILL;
        public static Vector2 PEON_STILL;
        public static Vector2 OGRE_STILL;
        public static Vector2 DRAGON_STILL;
        public static Vector2 TURTLE_STILL;
        */




        public static int FANTASSIN_MAX_AMELIORATION_DAMAGE = 4;

        public static int FANTASSIN_MAX_AMELIORATION_ARMOR = 2;
        public static int AMELIORATION_TIME = 180;
        public static int ARCHER_MAX_AMELIORATION_DAMAGE = 6;
    }

}
