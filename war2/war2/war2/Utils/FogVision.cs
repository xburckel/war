﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.Utils
{
    class FogVision
    {
        public UnitType unitType;
        public ResourceType resourceType;
        public Color color;
        public Vector2 position;
        public int size;

        public FogVision(Color c, Vector2 position, int size, UnitType type = UnitType.None, ResourceType rt = ResourceType.None)
        {
            unitType = type;
            color = c;
            this.position = position;
            this.size = size;
            resourceType = rt;
        }

        public void Draw(bool toMinimap)
        {

        }




    }
}
