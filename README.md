# README #

This is my attempt at reprogramming as a hobby the 1995 Blizzard game warcraft 2. It is not as clean as the original game because I don't have advanced graphical skills, but most features are already there and some things may even work better. There is still some performance issues related to pathfinding in specific situations to be fixed :)
 If you want to know more or have any issues, feel free to contact me.
 Pudconverter is used to convert the original game map format to my specific format, just launch it in a repository with .pud maps and it will create altrernate versions.

### What is this repository for? ###

That's my war 2 + the pud converter : just launch war2.exe; 
Requires Xna framework 4.0 redistributable + the dotnet 4 framework.
1.0.0

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Copyright ###

I don't pretend to own the rights for warcraft II, so if you are a blizzard employee and think this is breaching some kind of copyright, feel free to contact me and I'll remove this repo.
But honestly I just do this as a hobby, don't intend to monetize it and this is not finished enough to be fully playable.
