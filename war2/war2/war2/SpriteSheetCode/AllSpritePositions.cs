﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.SpriteSheetCode;

namespace war2.UnitsRelated
{
    public enum DisplayAction { Stand, Attack, Move, Collect, Repair, CarryWood, CarryGold, CarryOil, StandWithGold, StandWithWood, Dead, Decomposing, Sinking };
    public enum Orientation { Top, TopRight, Right, BottomRight, Bottom };



    public class AllSpritePositions
    {

        public SingleSpritePosition[] nextSprites;
        private int currentIndex = 0;
        private UnitType unitType;
        public DisplayAction action;
        private Orientation orientation;
        private bool isOrc;
        private bool flipHorizontal;
               
        public bool isStartStep()
        {
            return currentIndex == 0;
        }

        public bool isAtAttackStep()
        {
            return currentIndex == nextSprites.Count() - 1;
        }
        public void Draw(Texture2D spriteSheet, SpriteBatch spriteBatch, Vector2 mapPos, int selectBoxSizeX = 32, int selectBoxSizeY = 32)
        {
            // should not happen
            if (nextSprites == null)
                return;

            // below to remove units "jumping" because of sprite diff
            int diffX = (nextSprites[currentIndex].sizeX - selectBoxSizeX);
            int diffY = (nextSprites[currentIndex].sizeY - selectBoxSizeY);
            mapPos.X -= diffX;
            mapPos.Y -= diffY;

            if (flipHorizontal)
            {
                spriteBatch.Draw(spriteSheet, mapPos, new Rectangle(nextSprites[currentIndex].posX, nextSprites[currentIndex].posY, nextSprites[currentIndex].sizeX, nextSprites[currentIndex].sizeY),
                    Color.White, 0, Game1.statics.ImpossiblePos, 1, SpriteEffects.FlipHorizontally, 1);
            }
            else
            {
                spriteBatch.Draw(spriteSheet, mapPos, new Rectangle(nextSprites[currentIndex].posX, nextSprites[currentIndex].posY, nextSprites[currentIndex].sizeX, nextSprites[currentIndex].sizeY), Color.White);
            }
        }

        public void setFlip(bool flip = false)
        {
            flipHorizontal = flip;
        }

        public void changeOrientation(Orientation orientation, bool flip)
        {
            if (this.orientation == orientation && flipHorizontal == flip)
                return;
            this.orientation = orientation;
            this.flipHorizontal = flip;
            BuildStance();
        }

        public void changeAction(DisplayAction da)
        {
            if (action == da)
                return;
            action = da;
            BuildStance();
            currentIndex = currentIndex % nextSprites.Count();
        }

        public void goToNextStep()
        {
            currentIndex++;
            currentIndex = currentIndex % (nextSprites.Count());
        }

        // build a stance that does not move
        private void buildStand(bool isFiveStances, int posx1, int posY1, int sizeX1, int sizeY1)
        {
            if (isFiveStances)
            {
                nextSprites = new SingleSpritePosition[5];
                nextSprites[4] = new SingleSpritePosition(posx1, posY1, sizeX1, sizeY1);
            }
            else
                nextSprites = new SingleSpritePosition[4];

            nextSprites[0] = nextSprites[1] = nextSprites[2] = nextSprites[3] = new SingleSpritePosition(posx1, posY1, sizeX1, sizeY1);
        }
        // default if round not set is coordinates of first rank * rank number
        private void build(int numberOfStances, int posx1, int posY1, int sizeX1, int sizeY1,
                            int posx2 = 0, int posY2 = 0, int sizeX2 = 0, int sizeY2 = 0,
                            int posx3 = 0, int posY3 = 0, int sizeX3 = 0, int sizeY3 = 0,
                            int posx4 = 0, int posY4 = 0, int sizeX4 = 0, int sizeY4 = 0,
                            int posx5 = 0, int posY5 = 0, int sizeX5 = 0, int sizeY5 = 0
                            )
        {

            if (numberOfStances == 5)
            {
                nextSprites = new SingleSpritePosition[5];
                nextSprites[4] = new SingleSpritePosition(posx5 != 0 ? posx5 : posx1, posY5 != 0 ? posY5 : posY1 + sizeY1 * 4, sizeX5 != 0 ? sizeX5 : sizeX1, sizeY5 != 0 ? sizeY5 : sizeY1);
                nextSprites[1] = new SingleSpritePosition(posx2, posY2, sizeX2, sizeY2);
                nextSprites[2] = new SingleSpritePosition(posx3, posY3, sizeX3, sizeY3);
                nextSprites[3] = new SingleSpritePosition(posx4, posY4, sizeX4, sizeY4);
            }
            else if (numberOfStances == 4)
            {
                nextSprites = new SingleSpritePosition[4];
                nextSprites[1] = new SingleSpritePosition(posx2, posY2, sizeX2, sizeY2);
                nextSprites[2] = new SingleSpritePosition(posx3, posY3, sizeX3, sizeY3);
                nextSprites[3] = new SingleSpritePosition(posx4, posY4, sizeX4, sizeY4);
            }
            else if (numberOfStances == 3)
            {
                nextSprites = new SingleSpritePosition[3];
                nextSprites[1] = new SingleSpritePosition(posx2 != 0 ? posx2 : posx1, posY2 != 0 ? posY2 : posY1 + sizeY1, sizeX2 != 0 ? sizeX2 : sizeX1, sizeY2 != 0 ? sizeY2 : sizeY1);
                nextSprites[2] = new SingleSpritePosition(posx3 != 0 ? posx3 : posx1, posY3 != 0 ? posY3 : posY1 + sizeY1 * 2, sizeX3 != 0 ? sizeX3 : sizeX1, sizeY3 != 0 ? sizeY3 : sizeY1);
            }
            else
            {
                nextSprites = new SingleSpritePosition[2];
                nextSprites[1] = new SingleSpritePosition(posx2 != 0 ? posx2 : posx1, posY2 != 0 ? posY2 : posY1 + sizeY1, sizeX2 != 0 ? sizeX2 : sizeX1, sizeY2 != 0 ? sizeY2 : sizeY1);
            }

            nextSprites[0] = new SingleSpritePosition(posx1, posY1, sizeX1, sizeY1);
            /* if (posY2 + posx2 + sizeX2 + sizeY2 == 0)
             {
                 nextSprites[1] = new SingleSpritePosition(posx2 != 0 ? posx2 : posx1, posY2 != 0 ? posY2 : posY1 + sizeY1, sizeX2 != 0 ? sizeX2 : sizeX1, sizeY2 != 0 ? sizeY2 : sizeY1);
                 nextSprites[2] = new SingleSpritePosition(posx3 != 0 ? posx3 : posx1, posY3 != 0 ? posY3 : posY1 + sizeY1 * 2, sizeX3 != 0 ? sizeX3 : sizeX1, sizeY3 != 0 ? sizeY3 : sizeY1);
                 nextSprites[3] = new SingleSpritePosition(posx4 != 0 ? posx4 : posx1, posY4 != 0 ? posY4 : posY1 + sizeY1 * 3, sizeX4 != 0 ? sizeX4 : sizeX1, sizeY4 != 0 ? sizeY4 : sizeY1);
             }
             else
             {
                 nextSprites[0] = new SingleSpritePosition(posx1, posY1, sizeX1, sizeY1);
                 nextSprites[1] = new SingleSpritePosition(posx2, posY2, sizeX2, sizeY2);
                 nextSprites[2] = new SingleSpritePosition(posx3, posY3, sizeX3, sizeY3);
                 nextSprites[3] = new SingleSpritePosition(posx4, posY4, sizeX4, sizeY4);
                 if (isFiveStances)
                    nextSprites[3] = new SingleSpritePosition(posx5, posY5, sizeX5, sizeY5);

             }*/
        }
        // this is gonna be big
        public AllSpritePositions(UnitType unitType, DisplayAction action, bool isOrc, Orientation orientation)
        {
            this.unitType = unitType;
            this.action = action;
            this.isOrc = isOrc;
            this.orientation = orientation;
            flipHorizontal = false;
            BuildStance();
        }

        public void BuildStance()
        {
            if (action == DisplayAction.Decomposing)
            {    
                build(5, 15, 10, 39, 36, 14, 57, 37, 39, 13, 110, 35, 31, 13, 149, 35, 34, 13, 190, 33, 34);
                return;
            }
            if (action == DisplayAction.Sinking)
            {
                if (Game1.statics.ra.Next(2) == 1)
                    flipHorizontal = true;
                buildStand(true, 40, 276, 57, 37);
                return;
            }
            if (action == DisplayAction.Dead)
            {
                if (Game1.statics.ra.Next(2) == 1)
                    flipHorizontal = true;
            }

            #region Human
            if (!isOrc)
            {
                switch (unitType)
                {
                    case UnitType.Paysan:
                        #region human peasant : Done

                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 73, 379, 38, 32, 148, 378, 37, 34);
                                break;

                            case DisplayAction.Attack:
                            case DisplayAction.Repair:
                            case DisplayAction.Collect:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(5, 2, 188, 32, 32, 41, 229, 32, 32, 3, 262, 29, 41, 4, 300, 30, 39, 2, 338, 31, 39);

                                        break;
                                    case Orientation.TopRight:
                                        build(5, 40, 189, 32, 32, 41, 228, 32, 32, 38, 265, 32, 32, 40, 302, 32, 32, 39, 342, 32, 32);

                                        break;
                                    case Orientation.Right:
                                        build(5, 78, 187, 32, 32, 78, 227, 32, 32, 79, 262, 32, 32, 73, 303, 40, 32, 74, 341, 40, 32);

                                        break;
                                    case Orientation.BottomRight:
                                        build(5, 116, 189, 32, 32, 116, 228, 32, 32, 117, 264, 32, 32, 111, 303, 32, 32, 114, 342, 35, 32);

                                        break;
                                    case Orientation.Bottom:
                                        build(5, 152, 191, 32, 32, 152, 227, 32, 32, 152, 267, 32, 32, 155, 301, 32, 32, 155, 344, 32, 32);

                                        break;
                                }
                                break;
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 2, 35, 32, 32, 1, 75, 32, 32, 2, 113, 32, 32, 3, 151, 32, 32);
                                        break;
                                    case Orientation.TopRight:
                                        build(4,  41, 35, 32, 32,  38, 75, 32, 32, 41, 112, 32, 32, 41, 151, 32, 32);

                                        break;
                                    case Orientation.Right:
                                        build(4, 78, 34, 32, 32, 79, 74, 32, 32, 80, 112, 32, 32, 79, 151, 32, 32);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 115, 36, 32, 32, 114, 76, 32, 32, 114, 112, 32, 32, 114, 152, 32, 32);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 150, 38, 32, 32, 153, 75, 32, 32, 152, 114, 32, 32, 152, 152, 32, 32);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                       buildStand(true, 0, 0, 32, 32);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 36, 0, 32, 32);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 78, 0, 32, 32);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 115, 0, 32, 32);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 148, 0, 32, 32);

                                        break;
                                }
                                break;
                            case DisplayAction.StandWithWood:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 0, 644, 32, 32);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 36, 643, 32, 32);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 78, 643, 32, 32);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 115, 643, 32, 32);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 148, 643, 32, 32);

                                        break;
                                }
                                break;
                            case DisplayAction.StandWithGold:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 0, 456, 32, 32);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 36, 456, 32, 32);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 78, 456, 32, 32);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 115, 456, 32, 32);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 155, 452, 23, 38);

                                        break;
                                }
                                break;
                            case DisplayAction.CarryWood:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 0, 686, 33, 28, 0, 726, 34, 26, 0, 761, 33, 30, 0, 800, 33, 27);

                                        break;
                                    case Orientation.TopRight:
                                        build(4, 43, 680, 28, 38, 42, 719, 28, 37, 40, 759, 31, 33, 41, 797, 29, 33);

                                        break;
                                    case Orientation.Right:
                                        build(4, 78, 681, 26, 35, 82, 719, 21, 37, 78, 758, 26, 34, 82, 796, 21, 34);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 115, 685, 30, 29, 118, 723, 28, 29, 117, 761, 28, 29, 116, 798, 28, 29);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 151, 687, 34, 27, 151, 724, 34, 28, 151, 764, 34, 24, 151, 802, 34, 25);

                                        break;
                                }
                                break;
                            case DisplayAction.CarryGold:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 6, 494, 23, 30, 5, 533, 23, 28, 6, 569, 24, 32, 5, 608, 24, 30);

                                        break;
                                    case Orientation.TopRight:
                                        build(4, 40, 492, 25, 34, 41, 531, 25, 33, 42, 571, 25, 30, 41, 608, 24, 30);

                                        break;
                                    case Orientation.Right:
                                        build(4, 81, 494, 24, 30, 79, 532, 23, 30, 82, 569, 22, 32, 79, 607, 23, 31);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 121, 492, 22, 34, 121, 528, 22, 38, 121, 566, 22, 37, 122, 604, 21, 37);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 156, 491, 23, 36, 156, 528, 23, 36, 156, 568, 24, 34, 156, 605, 24, 36);

                                        break;
                                }
                                break;
                        }
                        break;
                    #endregion 
                    case UnitType.Archer:
                        #region human Archer : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 181, 529, 44, 44, 9, 587, 46, 45);
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(3, 6, 11, 40, 47, 10, 373, 45, 52, 10, 452, 44, 48);
                                        break;

                                    case Orientation.TopRight:
                                        build(3, 67, 12, 42, 45, 67, 382, 43, 47, 71, 457, 38, 46);

                                        break;
                                    case Orientation.Right:
                                        build(3, 128, 16, 38, 40, 128, 380, 46, 42, 130, 455, 39, 40);

                                        break;
                                    case Orientation.BottomRight:
                                        build(3, 186, 17, 42, 37, 189, 389, 36, 39, 184, 463, 41, 37);

                                        break;
                                    case Orientation.Bottom:
                                        build(3, 241, 17, 44, 42, 238, 388, 45, 36, 237, 460, 45, 39);

                                        break;

                                }
                                break;
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 5, 85, 42 ,49, 5, 159, 41 ,47, 8, 234, 36, 47, 5, 308, 41, 46);
                                        break;
                                    case Orientation.TopRight:
                                        build(4, 70, 88, 38, 41, 68, 161, 40, 44, 65, 230, 41, 46, 67, 306, 40, 43);

                                        break;
                                    case Orientation.Right:
                                        build(4, 128, 92, 40, 41, 129, 164, 38, 41, 129, 240, 35, 38, 130, 312, 35, 41);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 183, 92, 38, 37, 184, 165, 40, 38, 187, 237, 43, 36, 187, 312, 42, 37);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 239, 92, 43, 41, 241, 165, 43, 42, 247, 238, 38, 41, 244, 312, 41, 41);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                       buildStand(true, 6, 11, 40, 47);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 67, 12, 42, 45);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 128, 16, 38, 40);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 186, 17, 42, 37);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 241, 17, 44, 42);

                                        break;
                                }
                                break;
                        }
                        break;
#endregion
                    case UnitType.Battleship:
                        #region Battleship : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 190, 103, 73, 75, 279, 220, 74, 48);
                                break;
                            case DisplayAction.Attack:case DisplayAction.Move:case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 19, 6, 52, 82);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 101, 4, 68, 84);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 186, 7, 84, 70);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true,  281, 0, 80, 88);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 18, 93, 52, 88);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Petrolier:
                        #region Petrolier : Done
                     switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 149, 87, 71, 49, 84, 161, 56, 55 );
                                break;
                            case DisplayAction.Attack:case DisplayAction.Move:case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 19, 0, 29, 72);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 80, 8, 58, 58);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 148, 20, 72, 32);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true,  226, 13, 59, 50);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 315, 1, 32, 69);

                                        break;
                                }
                                break;
                            case DisplayAction.CarryOil:
                                     switch (orientation)
                                   {
                                    case Orientation.Top:
                                        buildStand(true, 23, 221, 33, 72);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 84, 229, 58, 58);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 150, 239, 72, 34);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true,  311, 222, 35, 69);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 226, 234, 59, 50);

                                        break;
                                }
                                break;
                        }
                        break;
#endregion
                    case UnitType.Catapulte:
                        #region Catapulte : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(1, 0, 0, 0, 0);
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(3, 2, 199, 60, 57, 3, 66, 58, 58, 6, 133, 52, 57);

                                        break;
                                    case Orientation.TopRight:
                                        build(3,  68, 198, 62, 60, 68, 67, 61, 59, 68, 133, 61, 59);

                                        break;
                                    case Orientation.Right:
                                        build(3, 133, 198, 62, 55, 133, 67, 62, 54, 133, 135, 62, 52);

                                        break;
                                    case Orientation.BottomRight:
                                        build(3, 200, 198, 62, 62, 200, 66, 62, 62, 200, 132, 62, 62);

                                        break;
                                    case Orientation.Bottom:
                                        build(3, 268, 198, 57, 59, 269, 66, 55, 60, 269, 132, 55, 59);

                                        break;
                                }
                                break;
                            case DisplayAction.Move: case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 3, 0, 58, 59);
                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 68, 1, 61, 60);
                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 133, 1, 62, 54);
                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 200, 0, 62, 62);
                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 269, 0, 55, 60);
                                        break;
                                }
                            
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Paladin: case UnitType.Chevalier:
                        #region Chevalier / Paladin : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(4, 155, 679, 59, 56, 309, 686, 50, 43, 88, 752, 50, 43);
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 14, 383, 43, 60, 20, 456, 39, 61, 12, 530, 45, 61, 25, 601, 29, 64);

                                        break;
                                    case Orientation.TopRight:
                                        build(4, 91, 379, 46, 59, 90, 460, 44, 52, 94, 527, 43, 59, 95, 601, 49, 59);

                                        break;
                                    case Orientation.Right:
                                        build(4, 158, 379, 61, 52, 157, 453, 60, 52, 155, 527, 61, 52, 154, 608, 66, 45);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 232, 379, 48, 48, 226, 453, 52, 48, 233, 527, 48, 48, 236, 608, 56, 47);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 319, 381, 32, 53, 320, 455, 29, 53, 320, 529, 37, 54, 320, 608, 33, 57);

                                        break;
                                }
                                break;
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 21, 83, 37, 64, 18, 157, 40, 62, 21, 233, 37, 62, 24, 305, 34, 64);
                                        break;
                                    case Orientation.TopRight:
                                        build(4,91, 83, 49, 54, 89, 159, 50, 50, 90, 233, 49, 56, 91, 305, 50, 60);

                                        break;
                                    case Orientation.Right:
                                        build(4, 160, 93, 59, 40, 156, 168, 63, 39, 156, 242, 64, 41, 158, 315, 62, 41);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 240, 93, 48, 41, 240, 167, 46, 43, 236, 240, 48, 53, 239, 315, 47, 47);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 318, 92, 34, 48, 318, 164, 34, 49, 317, 238, 35, 57, 317, 312, 35, 59);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                       buildStand(true, 24, 9, 34, 64);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 94, 9, 47, 58);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 160, 19, 61, 42);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 241, 21, 47, 40);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 318, 18, 34, 49);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Destroyer:
                        #region Destroyer : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 69, 108, 62, 63, 73, 196, 57, 54);
                                break;
                            case DisplayAction.Attack:case DisplayAction.Move:case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 4, 5, 51, 83);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 64, 14, 68, 70);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 136, 26, 80, 46);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 226, 20, 64, 70);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 298, 7, 49, 82);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Fantassin:
                        #region Fantassin : DOne
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 136, 498, 37, 47, 256, 506, 40, 34);
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 1, 281, 47, 37, 10, 328, 32, 56, 1, 399, 56, 37, 20, 442, 28, 50);

                                        break;
                                    case Orientation.TopRight:
                                        build(4, 62, 286, 52, 38, 64, 337, 50, 40, 64, 389, 55, 49, 72, 443, 45, 48);

                                        break;
                                    case Orientation.Right:
                                        build(4, 137, 277, 33, 49, 123, 338, 58, 42, 131, 388, 40, 49, 133, 454, 47, 35);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 201, 278, 22, 48, 197, 332, 35, 48, 201, 387, 35, 51, 181, 453, 62, 33);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 257, 283, 45, 38, 258, 334, 31, 47, 250, 388, 51, 47, 259, 440, 32, 53);

                                        break;
                                }
                                break;
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 15, 55, 30, 52, 16, 112, 30, 49, 15, 174, 29, 37, 15, 227, 31, 37);
                                        break;
                                    case Orientation.TopRight:
                                        build(4, 73, 61, 39, 41, 75, 116, 32, 40, 78, 173, 33, 38, 77, 225, 31, 39);

                                        break;
                                    case Orientation.Right:
                                        build(4, 135, 65, 45, 37, 139, 118, 39, 38, 137, 174, 32, 33, 133, 227, 35, 34);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 194, 62, 43, 40, 194, 119, 41, 37, 197, 175, 36, 35, 193, 230, 38, 31);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 259, 60, 29, 42, 257, 116, 30, 44, 258, 170, 31, 42, 255, 225, 32, 45);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                       buildStand(true, 14, 6, 31, 40);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 78, 8, 25, 39);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 142, 11, 32, 37);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 197, 10, 40, 35);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 256, 5, 32, 46);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Griffon:
                        #region Griffon: Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(5, 9, 586, 69, 61, 89, 588, 78, 60, 170, 593, 65, 55, 327, 592, 69, 65, 10, 657, 72, 37);
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(3, 11, 339, 65, 72, 5, 416, 80, 79, 5, 498, 80, 79);

                                        break;
                                    case Orientation.TopRight:
                                        build(3, 89, 348, 70, 59, 93, 416, 65, 74, 97, 498, 64, 79);

                                        break;
                                    case Orientation.Right:
                                        build(3, 169, 338, 78, 60, 172, 416, 76, 73, 173, 512, 76,66);

                                        break;
                                    case Orientation.BottomRight:
                                        build(3, 259, 337, 58, 71, 254, 419, 77,73, 255, 521, 76, 54);

                                        break;
                                    case Orientation.Bottom:
                                        build(3, 341, 334, 63, 73, 333, 416, 80, 80, 333, 499 , 78, 79);

                                        break;
                                }
                                break;
                            case DisplayAction.Move: case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(3, 13, 95, 65, 71, 16, 176, 61, 74, 5, 256, 80, 76);
                                        break;
                                    case Orientation.TopRight:
                                        build(3, 89, 88, 70, 73, 94, 170, 63, 74, 96, 252, 61, 80);

                                        break;
                                    case Orientation.Right:
                                        build(3, 169, 88, 78, 63, 171, 171, 76, 62, 172, 253, 74, 79);

                                        break;
                                    case Orientation.BottomRight:
                                        build(3, 258, 89, 67, 74, 259, 178, 68, 64, 255, 275, 72, 54);

                                        break;
                                    case Orientation.Bottom:
                                        build(3, 343, 90, 62, 73, 342, 171, 63, 73, 333, 253, 79, 72);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.MachineVolante:
                        #region Machine Volante: Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 11, 158, 59, 62, 77, 156, 77, 72);
                                break;
                            case DisplayAction.Attack:
                            case DisplayAction.Move:
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(2, 11, 11, 60, 64, 11, 89, 60, 62);

                                        break;
                                    case Orientation.TopRight:
                                        build(2, 90, 11, 56, 64, 86, 92, 60, 52);

                                        break;
                                    case Orientation.Right:
                                        build(2, 159, 11, 68, 63, 159, 92, 68, 58);

                                        break;
                                    case Orientation.BottomRight:
                                        build(2, 234, 11, 64, 64, 234, 88, 64, 63);

                                        break;
                                    case Orientation.Bottom:
                                        build(2, 304, 5, 67, 64, 304, 81, 67, 64);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Mage:
                        #region Mage: Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(5, 11, 559, 47, 49, 77, 559, 50, 52, 209, 566, 278, 594, 44, 16, 72, 634, 44, 10);
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 19, 309, 38, 48, 18, 368, 36, 52, 13, 430, 37, 52, 14, 496, 41, 52);

                                        break;
                                    case Orientation.TopRight:
                                        build(4,98, 314, 39, 45, 97, 368, 37, 54, 94, 430, 47, 53, 97, 500, 37, 48);

                                        break;
                                    case Orientation.Right:
                                        build(4, 176, 323, 33, 38, 176, 378, 42, 45, 174, 433, 46, 52, 175, 510, 40, 42);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 245, 324, 40, 33, 245, 386, 39, 34, 248, 444, 46, 39, 246, 514, 40, 36);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 312, 321, 36,35, 317, 385, 34, 34, 319, 447, 35, 43, 315, 513, 38, 34);

                                        break;
                                }
                                break;
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 22, 67, 31, 54, 22, 128, 31, 53, 23, 189, 31, 51, 21, 249, 35, 51);
                                        break;
                                    case Orientation.TopRight:
                                        build(4, 95, 75, 37, 44, 95, 136, 37, 45, 97, 201, 38, 38, 97, 261, 41, 39 );

                                        break;
                                    case Orientation.Right:
                                        build(4, 170, 85, 36, 38, 171, 146, 34, 37, 173, 205, 33, 37, 175, 266, 34, 37);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 246, 87, 27, 35, 246, 147, 26, 35, 247, 207, 27, 34, 246, 268, 25, 34);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 315, 86, 31, 36, 315, 146, 31, 36, 314, 206, 31, 34, 315, 267, 31, 32);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                       buildStand(true, 22, 7, 31, 51);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 97, 16, 37, 42);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 176, 24, 32, 38);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 246, 25, 24, 34);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 315, 24, 32, 33);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Nains:
                        #region Nains : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(5, 4, 414, 51, 44, 64, 415, 51, 43, 123, 413, 47, 46, 179, 410, 55, 51, 238, 429, 50, 32);
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(3, 12, 258, 33, 40, 15, 307, 33, 40, 13, 366, 35, 39);

                                        break;
                                    case Orientation.TopRight:
                                        build(3, 69, 260, 37, 40, 68, 312, 47, 36, 69, 368, 38, 40);

                                        break;
                                    case Orientation.Right:
                                        build(3, 124, 253, 41, 39, 127, 304, 46, 42, 125, 362, 39, 41);

                                        break;
                                    case Orientation.BottomRight:
                                        build(3, 183, 254, 38, 42, 183, 306, 48, 48, 181, 363, 43, 42);

                                        break;
                                    case Orientation.Bottom:
                                        build(3, 243, 255, 37, 43, 244, 308, 33, 50, 244, 364, 37, 42);

                                        break;
                                }
                                break;
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 11, 58, 38, 40, 5, 106, 42, 40, 10, 159, 38,37, 14, 202, 32, 45);
                                        break;
                                    case Orientation.TopRight:
                                        build(4, 64, 61, 44, 37, 63, 110, 42, 37, 67, 160, 37, 37, 69, 212, 36, 37);

                                        break;
                                    case Orientation.Right:
                                        build(4, 120, 54, 51, 43, 126, 104, 33, 41, 122, 153, 48, 43, 125, 203, 36, 43);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 179, 56, 45, 41, 182, 107, 36, 39, 179, 155, 44, 44, 181, 206, 38, 41);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 245, 57, 32, 43, 243, 107, 41, 42, 243, 157, 37, 42, 243, 209, 40, 41);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                       buildStand(true, 12, 8, 34, 37);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 67, 9, 38, 39);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 126, 3, 41, 39);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 182, 5, 44, 39);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 244, 6, 35, 42);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                   
                    case UnitType.Sousmarin:
                        #region Sousmarin: Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(1, 0, 0, 0, 0);
                                break;
                            case DisplayAction.Stand:case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 9, 13, 55, 58);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 78, 20, 59, 52);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 154, 19, 63, 51);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 229, 20, 57, 52);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 302, 11, 59, 61);

                                        break;
                                }
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(2, 9, 84, 55, 58, 8, 153, 58, 67);
                                        break;
                                    case Orientation.TopRight:
                                        build(2, 76, 92, 61, 53, 74, 160, 62, 56);

                                        break;
                                    case Orientation.Right:
                                        build(2, 149, 87, 68, 55, 148, 158, 70, 54);

                                        break;
                                    case Orientation.BottomRight:
                                        build(2, 228, 91, 58, 55, 226, 158, 61, 54);

                                        break;
                                    case Orientation.Bottom:
                                        build(2, 302, 79, 59, 65, 301, 154, 61, 63);

                                        break;
                                }
                                break;
                        }
                        break;
#endregion
                    case UnitType.Transport:
                        #region Transport : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(0, 75, 97, 71, 39, 75, 178, 70, 30);
                                break;
                            case DisplayAction.Attack:case DisplayAction.Move:case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 7, 3, 55, 69);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 76, 8, 67, 62);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 148, 9, 70, 53);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 222, 7, 67, 60);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 305, 4, 54, 66);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion

                }
            }
            #endregion
            #region Orc
            else
                switch (unitType)
                {
                    case UnitType.Paysan:
                        #region Peon orc : OK
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 84, 477, 29, 36, 162, 486, 34, 30);
                                break;
                            case DisplayAction.Attack:
                            case DisplayAction.Repair:
                            case DisplayAction.Collect:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(5, 2, 244, 33, 30, 8, 294, 27, 29, 1, 340, 31, 34, 3, 379, 20, 43, 3, 427, 20, 38);

                                        break;
                                    case Orientation.TopRight:
                                        build(5, 42, 242, 31, 34, 41, 290, 36, 34, 40, 332, 30, 44, 43, 379, 26, 41, 42, 427, 23, 39);

                                        break;
                                    case Orientation.Right:
                                        build(5, 91, 235, 21, 41, 81, 289, 35, 36, 85, 330, 21, 47, 83, 387, 35, 31, 82, 434, 36, 32);

                                        break;
                                    case Orientation.BottomRight:
                                        build(5, 128, 236, 29, 40, 131, 284, 22, 42, 123, 334, 35, 39, 124, 391, 33, 26, 124, 437, 32, 29);

                                        break;
                                    case Orientation.Bottom:
                                        build(5, 166, 243, 32, 32, 167, 284, 26, 43, 167, 341, 28, 32, 164, 391, 23, 33, 163, 440, 23, 28);

                                        break;
                                }
                                break;
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 8, 48, 24, 34, 7, 97, 25, 33, 4, 146, 31, 33, 6, 194, 28, 33);
                                        break;
                                    case Orientation.TopRight:
                                        build(4, 47, 51, 26, 31, 47, 98, 25, 31, 41, 145, 37, 36, 46, 192, 29, 36);

                                        break;
                                    case Orientation.Right:
                                        build(4, 86, 51, 28, 31, 88, 99, 24, 32, 86, 146, 28, 32, 88, 194, 22, 34);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 124, 51, 33, 32, 124, 100, 30, 31, 127, 148, 25, 30, 129, 199, 25, 27);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 167, 54, 25, 29, 167, 103, 26, 28, 165, 149, 31, 29, 165, 198, 30, 28);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 5, 1, 29, 32);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 48, 2, 26, 34);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 90, 2, 18, 33);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 129, 6, 23, 29);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 165, 7, 30, 25);

                                        break;
                                }
                                break;
                            case DisplayAction.StandWithWood:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 2, 816, 37, 32);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 46, 811, 33, 39);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 90, 813, 23, 35);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 125, 819, 32, 29);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 163, 823, 34, 25);

                                        break;
                                }
                                break;
                            case DisplayAction.StandWithGold:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 9, 576, 27, 34);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 51, 577, 24, 34);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 87, 579, 25, 32);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 125, 583, 28, 28);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 162, 577, 30, 34);

                                        break;
                                }
                                break;
                            case DisplayAction.CarryWood:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 1, 863, 37, 33, 2, 911, 37, 33, 2, 959, 37, 33, 2, 1007, 37, 33);
                                        break;
                                    case Orientation.TopRight:
                                        build(4, 43, 859, 36, 38, 43, 907, 37, 37, 47, 954, 30, 42, 46, 1004, 30, 41);

                                        break;
                                    case Orientation.Right:
                                        build(4, 85, 863, 27, 35, 87, 911, 27, 35, 86, 959, 28, 34, 87, 1006, 28, 35);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 125, 819, 32, 29, 127, 866, 30, 32, 126, 913, 30, 33, 126, 965, 32, 29, 123, 1011, 35, 29);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 163, 869, 34, 29, 163, 918, 34, 28, 163, 963, 34, 30, 164, 1013, 34, 28);

                                        break;
                                }
                                break;
                            case DisplayAction.CarryGold:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 11, 623, 26, 36, 10, 671, 27, 35, 8, 719, 27, 35, 8, 766, 28, 35);
                                        break;
                                    case Orientation.TopRight:
                                        build(4, 47, 626, 27, 31, 46, 673, 27, 32, 49, 719, 22, 36, 49, 766, 22, 36);

                                        break;
                                    case Orientation.Right:
                                        build(4, 87, 629, 26, 30, 88, 674, 25, 32, 88, 723, 24, 31, 88, 769, 25, 30);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 125, 627, 29, 32, 124, 676, 29, 31, 124, 726, 29, 29, 125, 774, 28, 26);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 164, 622, 29, 38, 164, 671, 29, 37, 164, 719, 29, 38, 164, 766, 29, 37);

                                        break;
                                }
                                break;


                        }
                        break;
                        #endregion
                    case UnitType.Archer:
                        #region Archer : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 177, 470, 44, 49, 6, 522, 45, 50);
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 7, 262, 36, 46, 15, 314, 29, 49, 2, 367, 51, 45, 15, 419, 34, 47);

                                        break;
                                    case Orientation.TopRight:
                                        build(4, 67, 264, 37, 45, 59, 312, 50, 50, 59, 360, 50, 50, 69, 419, 32, 42);

                                        break;
                                    case Orientation.Right:
                                        build(4, 127, 262, 42, 49, 114, 316, 55, 45, 129, 364, 34, 49, 126, 420, 34, 43);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 183, 261, 36, 51, 177, 313, 46, 51, 183, 365, 35, 50, 174, 419, 41, 45);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 237, 262, 43, 47, 240, 315, 26, 48, 230, 372, 53, 39, 233, 421, 38, 44);

                                        break;
                                }
                                break;
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 14, 54, 29, 48, 13, 108, 32, 46, 9, 159, 38, 47, 11, 211, 37, 47);
                                        break;
                                    case Orientation.TopRight:
                                        build(4, 66, 57, 34, 42, 65, 111, 32, 41, 62, 157, 47, 50, 67, 210, 37, 48);

                                        break;
                                    case Orientation.Right:
                                        build(4, 121, 57, 39, 42, 121, 111, 38, 42, 124, 162, 36, 42, 126, 213, 31, 41);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 177, 55, 41, 45, 179, 108, 38, 44, 179, 161, 33, 39, 181, 216, 34, 37);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 242, 56, 32, 44, 241, 109, 33, 42, 238, 160, 39, 44, 240, 214, 33, 41);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 11, 2, 34, 46);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 70, 4, 28, 44);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 131, 5, 32, 43);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 182, 7, 39, 42);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 241, 6, 34, 43);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Battleship:
                        #region Battleship : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 7, 104, 73, 69, 191, 121, 74, 48);
                                break;
                            case DisplayAction.Attack:
                            case DisplayAction.Move:
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 6, 6, 88, 87);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 97, 9, 87, 83);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 186, 5, 88, 88);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 279, 8, 82, 82);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 369, 5, 82, 88);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Catapulte:
                        #region Catapulte:  Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(1, 0, 0, 0, 0);
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(3, 7, 73, 47, 58, 7, 139, 47, 60, 7, 211, 47, 57);

                                        break;
                                    case Orientation.TopRight:
                                        build(3, 72, 72, 57, 58, 72, 139, 57, 60, 72, 210, 57, 58);

                                        break;
                                    case Orientation.Right:
                                        build(3, 139, 75, 61, 45, 143, 144, 57, 45, 143, 213, 57, 45);

                                        break;
                                    case Orientation.BottomRight:
                                        build(3, 210, 72, 59, 59, 210, 141, 59, 59, 210, 210, 59, 59);

                                        break;
                                    case Orientation.Bottom:
                                        build(3, 284, 70, 47, 61, 284, 143, 47, 57, 284, 212, 47, 57);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 7, 4, 47, 58);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 72, 3, 57, 58);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 139, 6, 61, 45);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 210, 3, 59, 59);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 284, 1, 47, 61);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Chevalier:
                    case UnitType.Paladin:
                        #region Ogre: Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(4, 136, 511, 47, 55, 263, 513, 50,52, 66, 570, 50, 52, 209, 577, 36, 38);
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 8, 289, 50, 43, 1, 345, 59, 42, 6, 396, 46, 54, 4, 458, 53, 48);

                                        break;
                                    case Orientation.TopRight:
                                        build(4, 77, 285, 36, 50, 71, 339, 49, 54, 61, 397, 65, 55, 71, 455, 53, 54);

                                        break;
                                    case Orientation.Right:
                                        build(4, 137, 286, 47, 48, 142, 339, 38, 54, 134, 396, 57, 56, 140, 455, 41, 52);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 194, 289, 62, 46, 198, 343, 56, 51, 207, 397, 45, 55, 211, 454, 39, 52);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 264, 283, 54, 53, 261, 344, 60, 48, 269, 395, 46, 57, 264, 462, 53, 44);

                                        break;
                                }
                                break;
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 12, 55, 41, 53, 8, 115, 46, 49, 9, 170, 41, 52, 8, 227, 45, 49);
                                        break;
                                    case Orientation.TopRight:
                                        build(4, 75, 53, 39, 57, 79, 111, 33, 55, 66, 168, 59, 55, 71, 227, 51, 52);

                                        break;
                                    case Orientation.Right:
                                        build(4, 139, 59, 49, 45, 140, 115, 40, 48, 136, 173, 53, 47, 140, 229, 36, 48);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 198, 55, 58, 52, 201, 114, 52, 49, 207, 171, 43, 50, 211, 229, 36, 48);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 274, 55, 39, 53, 271, 113, 45, 51, 270, 167, 42, 57, 270, 226, 45, 52);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 6, 2, 50, 46);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 76, 0, 39, 51);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 143, 1, 33, 49);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 208, 6, 39, 44);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 266, 3, 51, 46);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Destroyer:
                        #region Destroyer : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 9, 96, 71, 63, 185, 101, 69, 60);
                                break;
                            case DisplayAction.Attack:
                            case DisplayAction.Move:
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 15, 7, 61, 80);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 100, 17, 66, 64);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 180, 12, 87, 62);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 279, 13, 69, 67);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 378, 5, 53, 84);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Fantassin:
                        #region Gruntinou : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 167, 460, 55, 49, 12, 510, 44, 51);
                                break;
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 13, 51, 33, 49, 12, 105, 35, 44, 11, 155, 38, 44, 11, 207, 37, 43);

                                        break;
                                    case Orientation.TopRight:
                                        build(4, 67, 54, 37, 41, 67, 105, 37, 42, 62, 153, 46, 47, 63, 204, 39, 47);

                                        break;
                                    case Orientation.Right:
                                        build(4, 122, 57, 43, 40, 122, 107, 42, 41, 122, 158, 36, 41, 126, 208, 28, 39);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 178, 58, 39, 40, 177, 108, 38, 41, 180, 156, 31, 41, 180, 210, 34, 37);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 233, 61, 33, 36, 234, 113, 34, 37, 233, 158, 37, 40, 234, 213, 37, 35);

                                        break;
                                }
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 3, 259, 53, 44, 12, 308, 41, 45, 5, 362, 49, 42, 23, 407, 29, 51);
                                        break;
                                    case Orientation.TopRight:
                                        build(4, 66, 260, 45, 43, 57, 310, 55, 43, 56, 359, 55, 44, 65, 411, 46, 47);

                                        break;
                                    case Orientation.Right:
                                        build(4, 126, 254, 36, 51, 112, 306, 53, 49, 132, 356, 30, 51, 114, 417, 52, 39);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 182, 255, 28, 50, 177, 306, 33, 50, 177, 356, 33, 50, 169, 417, 52, 40);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 223, 264, 50, 36, 223, 306, 35, 48, 223, 357, 49, 49, 231, 407, 35, 51);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 11, 2, 38, 43);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 66, 2, 35, 45);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 128, 5, 28, 42);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 180, 8, 32, 38);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 232, 13, 39, 36);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Griffon:
                        #region Dragon: Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(5, 11, 414, 71, 80, 98, 420, 75, 74, 184, 420, 79, 80, 273, 420, 80, 80, 363, 422, 80, 78);
                                break;
                            case DisplayAction.Move:
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(3, 5, 5, 80, 79, 25, 86, 40, 78, 12, 169, 67, 73);

                                        break;
                                    case Orientation.TopRight:
                                        build(3, 95, 6, 80, 75, 106, 89, 69, 72, 106, 170, 69, 70);

                                        break;
                                    case Orientation.Right:
                                        build(3, 185, 5, 88, 70, 185, 86, 88, 47, 185, 184, 88, 62);

                                        break;
                                    case Orientation.BottomRight:
                                        build(3, 279, 8, 74, 73, 286, 90, 67, 74, 279, 181, 74, 65);

                                        break;
                                    case Orientation.Bottom:
                                        build(3, 365, 4, 80, 79, 385, 88, 40, 78, 372, 174, 66, 73);

                                        break;
                                }
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(3, 12, 169, 67, 73, 25, 252, 40, 67, 5, 351, 80, 57);
                                        break;
                                    case Orientation.TopRight:
                                        build(3, 106, 170, 69, 70, 106, 251, 69, 70, 95, 333, 80, 79);

                                        break;
                                    case Orientation.Right:
                                        build(3, 185, 184, 88, 62, 185, 270, 88, 48, 190, 332, 60, 77);

                                        break;
                                    case Orientation.BottomRight:
                                        build(3, 279, 181, 74, 65, 279, 262, 73, 67, 275, 332, 80, 80);

                                        break;
                                    case Orientation.Bottom:
                                        build(3, 372, 174, 66, 73, 385, 251, 40, 77, 365, 348, 80, 52);

                                        break;
                                }
                                break;

                        }
                        break;
                        #endregion
                    case UnitType.MachineVolante:
                        #region Machine Volante: Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(1, 0, 0, 0, 0);
                                break;
                            case DisplayAction.Attack:
                            case DisplayAction.Move:
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(2, 3, 2, 66, 71, 3, 76, 66, 71);

                                        break;
                                    case Orientation.TopRight:
                                        build(2, 79, 8, 60, 66, 79, 82, 60, 66);

                                        break;
                                    case Orientation.Right:
                                        build(2, 148, 10, 72, 59, 148, 84, 72, 59);

                                        break;
                                    case Orientation.BottomRight:
                                        build(2, 225, 3, 63, 71, 225, 77, 63, 71);

                                        break;
                                    case Orientation.Bottom:
                                        build(2, 298, 2, 65, 72, 298, 76, 65, 72);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Mage:
                        #region Mage: Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(4, 10, 602, 56, 52, 73, 602, 60, 57, 140, 602, 61, 61, 224, 603, 55, 60);
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 19, 340, 40, 62, 20, 406, 34, 62, 19, 471, 40 ,62, 22, 536, 33, 62);

                                        break;
                                    case Orientation.TopRight:
                                        build(4, 73, 344, 55, 52, 78, 406, 50, 56, 76, 471, 52, 56, 79, 537, 49, 55);

                                        break;
                                    case Orientation.Right:
                                        build(4, 138, 340, 62, 53, 137, 407, 64, 52, 136, 483, 66, 41, 141, 551, 61, 38);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 209, 340, 49, 53, 208, 413, 61, 44, 210, 483, 59, 42, 210, 550, 59, 40);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 279, 340, 39, 63, 283, 412, 36, 53, 276, 481, 43, 50, 285, 551, 36, 44);

                                        break;
                                }
                                break;
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 16, 73, 35, 60, 14, 139, 40, 61, 13, 204, 47, 62, 16, 272, 41, 61);
                                        break;
                                    case Orientation.TopRight:
                                        build(4, 82, 72, 44, 55, 77, 138, 49, 55, 76, 204, 49, 58, 77, 271, 49, 61);

                                        break;
                                    case Orientation.Right:
                                        build(4, 141, 72, 59, 50, 138, 138, 62, 51, 140, 204, 59, 52, 140, 271, 60,52);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 209, 72, 49, 54, 208, 138, 48, 53, 206, 204, 50, 63, 209, 271, 49, 58);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 279, 75, 42, 52, 283, 139, 44, 53, 283, 204, 42, 63, 276, 276, 46, 60);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 23, 8, 29, 59);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 81, 6, 45, 55);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 141, 7, 59, 50);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 208, 12, 50, 47);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 283, 16, 35, 49);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Nains:
                        #region Demolishers : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(5, 8, 408, 40, 30, 56, 404, 45, 37, 109, 401, 53, 41, 169, 398, 56, 46, 232, 401, 54, 43);
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 11, 227, 40, 37, 10, 271, 38, 37, 10, 313, 40, 35, 8, 359, 38, 36);

                                        break;
                                    case Orientation.TopRight:
                                        build(4, 71, 229, 33, 33, 71, 272, 35, 35, 69, 314, 36, 35, 68, 359, 38, 34);

                                        break;
                                    case Orientation.Right:
                                        build(4, 130, 229, 35, 33, 130, 272, 41, 33, 127, 313, 35, 36, 129, 357, 34, 34);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 186, 228, 39, 32, 188, 269, 39, 34, 185, 313, 38, 32, 185, 357, 35, 33);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 241, 228, 41, 33, 244, 271, 36, 38, 242, 314, 39, 34, 240, 359, 39, 33);

                                        break;
                                }
                                break;
                            case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(4, 11, 48, 37, 38, 12, 96, 37, 33, 12, 134, 40, 36, 11, 174, 40, 44);
                                        break;
                                    case Orientation.TopRight:
                                        build(4, 71, 51, 32, 32, 71, 90, 36, 39, 72, 135, 35,32, 70, 175, 40, 42);

                                        break;
                                    case Orientation.Right:
                                        build(4, 129, 49, 35, 35, 129, 91, 36, 36, 130, 136, 36, 31, 129, 180, 46, 35);

                                        break;
                                    case Orientation.BottomRight:
                                        build(4, 186, 49, 38, 31, 185, 93, 34, 32, 188, 136, 37, 32, 187, 178, 46, 35);

                                        break;
                                    case Orientation.Bottom:
                                        build(4, 242, 48, 38, 35, 239, 93, 44, 34, 240, 134, 40, 34, 241, 177, 40, 46);

                                        break;
                                }
                                break;
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 12, 8, 34, 37);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 71, 8, 33, 36);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 129, 8, 33, 34);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 186, 8, 36, 32);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 244, 8, 37, 34);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Petrolier:
                        #region Petrolier : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 5, 71, 63, 56, 146, 79, 61, 50);
                                break;
                            case DisplayAction.Attack:
                            case DisplayAction.Move:
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 12, 4, 45, 64);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 83, 9, 58, 54);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 152, 16, 64, 38);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 229, 11, 58, 53);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 312, 4, 40, 62);

                                        break;
                                }
                                break;
                            case DisplayAction.CarryOil:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 12, 134, 45, 64);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 81, 139, 59, 54);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 150, 146, 64, 40);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 226, 141, 58, 53);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 305, 134, 44, 62);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Sousmarin:
                        #region Sousmarin: Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(1, 0, 0, 0, 0);
                                break;
                            case DisplayAction.Stand:case DisplayAction.Move:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true,  7, 6, 72, 66);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 82, 8, 71, 64);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 157, 9, 69, 63);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 233, 11, 64, 61);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 303, 12, 72, 60);

                                        break;
                                }
                                break;
                            case DisplayAction.Attack:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        build(2, 7, 77, 72, 66, 7, 148, 72, 67);
                                        break;
                                    case Orientation.TopRight:
                                        build(2, 82, 78, 71, 64, 81, 148, 72, 66);

                                        break;
                                    case Orientation.Right:
                                        build(2, 157, 80, 69, 63, 156, 150, 71, 65);

                                        break;
                                    case Orientation.BottomRight:
                                        build(2, 233, 82, 64, 62, 229, 148, 72, 68);

                                        break;
                                    case Orientation.Bottom:
                                        build(2, 303, 82, 72, 60, 304, 148, 71, 64);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion
                    case UnitType.Transport:
                        #region Transport : Done
                        switch (action)
                        {
                            case DisplayAction.Dead:
                                build(2, 5, 81, 72, 58, 149, 90, 70, 42);
                                break;
                            case DisplayAction.Attack:
                            case DisplayAction.Move:
                            case DisplayAction.Stand:
                                switch (orientation)
                                {
                                    case Orientation.Top:
                                        buildStand(true, 9, 1, 53, 69);

                                        break;
                                    case Orientation.TopRight:
                                        buildStand(true, 80, 9, 59, 59);

                                        break;
                                    case Orientation.Right:
                                        buildStand(true, 148, 10, 72, 49);

                                        break;
                                    case Orientation.BottomRight:
                                        buildStand(true, 225, 7, 61, 55);

                                        break;
                                    case Orientation.Bottom:
                                        buildStand(true, 305, 4, 50, 67);

                                        break;
                                }
                                break;
                        }
                        break;
                        #endregion

                }
                #endregion

            #region Buildings
            if (nextSprites == null)
            {
                if (unitType == UnitType.NavalChantier || unitType == UnitType.Raffinery || unitType == UnitType.Foundry || unitType == UnitType.NavalPlatform)
                    build(2, 443, 142, 62, 58, 444, 207, 61, 57);
                else
                    build(5, 513, 10, 63, 61, 514, 79, 59, 53, 443, 12, 63, 59, 519, 150, 32, 32, 521, 184, 28, 26);

            }
            #endregion

        }

        public int NumberOfStances()
        {
            return nextSprites.Count();
        }
    }
}

