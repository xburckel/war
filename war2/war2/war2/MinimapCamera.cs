﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2
{
    public class MinimapCamera
    {
        /*
         * Disclaimer : I lost this file and have no idea what I made it for (was two years ago since I last worked on this)
         * So I just built this nearly empty thing for the transformation, though its probably broken if resolution changes
         * To be changed when I understand the purpose it had
         */
        public Matrix Transform () {
            return Matrix.CreateRotationZ(0) *
                   Matrix.CreateScale(new Vector3(1, 1, 1) * (128f / Map.mapsize)) *
                   Matrix.CreateTranslation(1, 1, 0);
        }

        public Matrix TransformMinimapAsPNG ()
        {
            return Matrix.CreateRotationZ(0) *
               Matrix.CreateScale(256f / (Map.mapsize * 32f)) *
               Matrix.CreateTranslation(1, 1, 0);
        }
    }
}
