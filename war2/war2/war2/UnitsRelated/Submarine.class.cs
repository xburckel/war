﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.UnitsRelated
{

    class Submarine : Unit
    {
        private HashSet<int> visibleBy;

        public Submarine (int hp, int mana, float speed,
         int damage,
         bool flying,
         bool sailing,
         int goldCost,
         int woodCost,
         int oilCost,
         int range,
         int sight,
         Vector2 position,
        int buildtime,
            UnitType type,
        int armor, int owner)
            : base(hp, mana, speed,
                damage,
                flying,
                sailing,
                goldCost,
                woodCost,
                oilCost,
                range,
                sight,
                position,
                buildtime,
                type,
                armor, owner)
        {
            visibleBy = new HashSet<int>();
        }

        public override void Update(Map map, GameTime gameTime)
        {
            base.Update(Game1.map, gameTime);

            visibleBy.Clear();

            foreach(Unit u in Game1.map.units)
            {
                if (Game1.statics.canSeeSubmarines.Contains(u.type) && u.sight >= Vector2.Distance(u.position, this.position))
                    visibleBy.Add(u.owner);

            }
        }

        public override void Draw(SpriteBatch spriteBatch, Texture2D texture, Vector2 pos)
        {
            if (canBeSeenBy(Game1.currentPLayer))
                base.Draw(spriteBatch, texture, pos);
        }

        public bool canBeSeenBy(int owner)
        {
            if (visibleBy.Contains(owner))
                return true;

            return false;
        }


    }
}
