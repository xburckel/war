﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.Menus;
using war2.UnitsRelated;

namespace war2
{
    [Serializable]
    public class MyMouse
    {

        Rectangle mSelectionBox;
        public MouseState mPreviousMouseState;
        public MouseState ms;
        Texture2D mDottedLine;
        Texture2D Cursor;
        Texture2D spellCursor;

        public MyMouse(Texture2D dottedLine, Texture2D cursor, Texture2D spellCursor)
        {
            mSelectionBox = new Rectangle(-1, -1, 0, 0);
            mPreviousMouseState = Mouse.GetState();
            ms = Mouse.GetState();
            mDottedLine = dottedLine;
            this.Cursor = cursor;
            this.spellCursor = spellCursor;
        }


        public void Update(Map map, Camera2D cam,  LeftPanel leftPanel)
        {

            ms = Mouse.GetState();
            if (!Game1.map.minimapHidden && (ms.X > 0) && ms.X <= 250 && ms.Y > 0 && ms.Y <= 250) // check minimap click
            #region minimapclick

            {
                // if left click, update location on map
                if (ms.LeftButton == ButtonState.Pressed && (mPreviousMouseState.LeftButton.Equals(ButtonState.Released)))
                {
                    cam.setPosition(new Vector2((((float)(((250 - (ms.X * cam.Zoom)))) / 250)) * (Map.mapsize * 32) - Map.mapsize * 32 + Game1.graphics.GraphicsDevice.Viewport.Width / 2,
                    (((float)((250 - (ms.Y  * cam.Zoom)))) / 250) * (Map.mapsize * 32) - Map.mapsize * 32 + Game1.graphics.GraphicsDevice.Viewport.Height / 2));

                }
                else if (ms.RightButton == ButtonState.Pressed && (mPreviousMouseState.RightButton.Equals(ButtonState.Released))) // minimap click - move
                {
                    Game1.statics.istryingtobuild = false;

                    foreach (Unit u in map.selected) // move
                    {
                        if (u.owner == Game1.currentPLayer)
                        {
                            Vector2 destination = new Vector2();
                            map.units.Remove(u);
                            destination.X = Map.mapsize - (((float)(((250 - ms.X))) / 250)) * (Map.mapsize);
                            destination.Y = Map.mapsize - (((float)((250 - ms.Y))) / 250) * (Map.mapsize);
                            u.target = null;
                            u.Move(destination, map);
                            map.units.Add(u);
                        }
                    }
                    if (map.selected.Count != 0 && map.selected.First().owner == Game1.currentPLayer)
                        Game1.rclick.doRightClick(new Vector2(ms.X, ms.Y), map.selected.Count == 0 ? UnitType.None : map.selected.First().type, Game1.map.players[Game1.currentPLayer].isOrc);


                }
            }
            #endregion

            else
            #region mapclick
            {
                if (ms.LeftButton == ButtonState.Pressed && ms.X > LeftPanel.LEFTPANEL_WIDTH && mPreviousMouseState.LeftButton == ButtonState.Released)
                {
                    Vector2 truePos = (new Vector2(ms.X, ms.Y) - Game1.cam.Pos) / Game1.cam.Zoom;

                    int x =(int) Math.Round(truePos.X);
                    int y = (int)Math.Round(truePos.Y); 

                    if (Game1.spellCursorActive && map.selected.Count == 1 && map.selected.First().type == UnitType.Mage
                        && ms.X >= LeftPanel.LEFTPANEL_WIDTH) // TODO : check bounds
                    {
                        ((Wizard)map.selected.First()).LaunchSpell(x / 32, y / 32);
                        Game1.spellCursorActive = false;
                    }



   
                    foreach (Unit u in map.units)
                    {
                        if ((x < u.position.X * 32 + 32 && u.position.X * 32 < x) && (y < u.position.Y * 32 + 32 && u.position.Y * 32 < y))
                        {

                            map.selected.Clear();
                            map.selected.Add(u);
                            map.selectedResource = null;
                        }
                    }

                }
                if (ms.RightButton == ButtonState.Pressed && mPreviousMouseState.RightButton == ButtonState.Released)
                {
                    #region moves

                    bool yourUnitsSelected = true;
                    foreach (Unit u in map.selected)
                    {
                        if (u.owner != Game1.currentPLayer)
                        {
                            yourUnitsSelected = false;
                            break;
                        }
                    }

                    if (yourUnitsSelected)
                    {
                    Game1.statics.istryingtobuild = false;
                    Game1.spellCursorActive = false;
                    int x = (int)((ms.X - (int)Math.Round(cam.Pos.X)) / cam.Zoom);
                    int y = (int)((ms.Y - (int)Math.Round(cam.Pos.Y)) / cam.Zoom);

                    foreach (Unit u in map.selected) // move
                    {
                        map.units.Remove(u);
                        Vector2 destination = new Vector2();
                        destination.X = x / 32;
                        destination.Y = y / 32;
                        u.target = null;

                        // go to nearest case
                        if (Map.Inbounds((int)Math.Round(destination.X), (int)Math.Round(destination.Y)))
                        {
                            if (u.flying)
                            {
                                if (map.flyingTraversableMapTiles[(int)Math.Round(destination.X), (int)Math.Round(destination.Y)] == false)
                                    destination = u.findClearSortieSpot(destination);
                            }
                            else if (u.sailing && u.type != UnitType.Petrolier) // so we can go to platforms
                            {
                                if (map.waterTraversableMapTiles[(int)Math.Round(destination.X), (int)Math.Round(destination.Y)] == false)
                                    destination = u.findClearSortieSpot(destination);
                            }
                            else if (u.type != UnitType.Paysan && u.type != UnitType.Petrolier)
                            {
                                if (map.groundTraversableMapTiles[(int)Math.Round(destination.X), (int)Math.Round(destination.Y)] == false)
                                    destination = u.findClearSortieSpot(destination);
                            }

                            u.Move(destination, map);
                        }
                    }
                    

                    #region right click on another unit
                    Unit tmp = null;


                    foreach (Unit u in map.units)
                    {
                        if ((x < u.position.X * 32 + u.spriteSize.X && u.position.X * 32 + u.spriteSize.X < x + u.spriteSize.X) && (y < u.position.Y * 32 + u.spriteSize.Y && u.position.Y * 32 + u.spriteSize.Y < y + u.spriteSize.Y)) // there is something where we click
                        {
                            tmp = u; // transport here
                            break;

                        }
                    }
                    if (tmp != null)
                    {
                        foreach (Unit u2 in map.selected) // attack move detected
                        {
                            if (u2.owner != tmp.owner) // not attack oneself
                            {
                                u2.target = tmp;
                            }
                            else if ( u2.owner == tmp.owner && tmp.type == UnitType.Transport)
                            {
                                // network

                                ((Transport)tmp).carryRequest.Add(u2);
                            }
                        }
                    }


                    foreach (Unit u in map.selected)
                    {
                        map.units.Add(u);
                    }

                    Game1.rclick.doRightClick(new Vector2(ms.X, ms.Y), map.selected.Count == 0 ? UnitType.None : map.selected.First().type, Game1.map.players[Game1.currentPLayer].isOrc);
                    }
                    #endregion

                    #endregion
                }

                   

            }
            #endregion
            // selectbox


            MouseState aMouse = Mouse.GetState();

            // click on disable minimap button
            if (ms.LeftButton == ButtonState.Pressed && (mPreviousMouseState.LeftButton.Equals(ButtonState.Released)) &&
                ((Game1.map.minimapHidden && aMouse.X > Game1.topPanel.minimapShowButtonXPos && aMouse.X < Game1.topPanel.minimapShowButtonXPos + 25 && aMouse.Y > 0 && aMouse.Y < 26)
                || (!Game1.map.minimapHidden && aMouse.X > Game1.topPanel.minimapHideButtonXPos && aMouse.X < Game1.topPanel.minimapHideButtonXPos + 25 && aMouse.Y > 0 && aMouse.Y < 26)))
            {
                Game1.map.minimapHidden = !Game1.map.minimapHidden;
                Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.BigButtonClick]);
            }
            if (aMouse.X > LeftPanel.LEFTPANEL_WIDTH && (aMouse.X > 250 || aMouse.Y > 250) && (!(Game1.prevKstate.GetPressedKeys().Count() > 0))) // not in menubar
            #region click on game map
            {
                Vector2 truePos = (new Vector2(aMouse.X, aMouse.Y) - Game1.cam.Pos) / Game1.cam.Zoom;

                if (map.selected.Count == 1 && (map.selected.First().type == UnitType.Paysan || map.selected.First().type == UnitType.Petrolier) &&
                   ((Harvester)map.selected.First()).selectedBuilding != null && aMouse.LeftButton == ButtonState.Pressed && mPreviousMouseState.LeftButton == ButtonState.Released)
                {
                    if (Game1.map.players[map.selected.First().owner].gold >= ((Harvester)map.selected.First()).selectedBuilding.goldCost && Game1.map.players[map.selected.First().owner].wood >= ((Harvester)map.selected.First()).selectedBuilding.woodCost
                        && Game1.map.players[map.selected.First().owner].oil >= ((Harvester)map.selected.First()).selectedBuilding.oilCost)
                    { // tell harvester to build new building
                        #region harvester creating building : position etc checking
                        // the mouse is centered to the middle of the building so take this into account
                        truePos = (new Vector2(aMouse.X, aMouse.Y) - Game1.cam.Pos) / Game1.cam.Zoom - new Vector2((((Harvester)map.selected.First()).selectedBuilding).spriteSize.X / 2, (((Harvester)map.selected.First()).selectedBuilding).spriteSize.Y/ 2);


                        bool correctSpot = true; // check if spot is valid
                        //bool savePreviousTile = Game1.map.groundTraversableMapTiles[(int)Math.Round(map.selected.First().position.X), (int)Math.Round(map.selected.First().position.Y)];

                        //Game1.map.groundTraversableMapTiles[(int)Math.Round(map.selected.First().position.X), (int)Math.Round(map.selected.First().position.Y)] = true; // ignore ourself on construction collision

                        if (!Harvester.IsCorrectBuildPosition(truePos / 32, ((Harvester)map.selected.First()).selectedBuilding, map.selected.First()))
                                correctSpot = false;

                        if (correctSpot)
                        {
                            Vector2 buildPos = new Vector2((float)Math.Round((truePos.X) / 32), (float)Math.Round((truePos.Y) / 32));
                            ((Harvester)map.selected.First()).GiveBuildingConstructOrder(buildPos);
                        }
                        #endregion
                    }

                    else
                    {
                        if (map.selected.First().owner == Game1.currentPLayer)
                        {
                            if (Game1.map.players[Game1.currentPLayer].gold < UnitProperties.getBuildingProperties(((Harvester)map.selected.First()).selectedBuilding.type)[(int)BuildingProperties.GOLDCOST])
                            {
                                Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.SentinelNoGold1]);
                                Game1.statics.annouceMessage.UpdateMessage("Not enough gold");
                            }
                            else if (Game1.map.players[Game1.currentPLayer].wood < UnitProperties.getBuildingProperties(((Harvester)map.selected.First()).selectedBuilding.type)[(int)BuildingProperties.WOODCOST])
                            {
                                Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.SentinelNoLumber1]);
                                Game1.statics.annouceMessage.UpdateMessage("Not enough Lumber");
                            }
                            else if (Game1.map.players[Game1.currentPLayer].oil < UnitProperties.getBuildingProperties(((Harvester)map.selected.First()).selectedBuilding.type)[(int)BuildingProperties.OILCOST])
                            {   Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.Error]);
                                Game1.statics.annouceMessage.UpdateMessage("Not enough Lumber");
                            }
                        }
                    }
                }

                truePos = (new Vector2(aMouse.X, aMouse.Y) - Game1.cam.Pos) / Game1.cam.Zoom; // we reset truePos if ever we changed it in the construct building check if

                if (aMouse.LeftButton == ButtonState.Pressed && mPreviousMouseState.LeftButton == ButtonState.Released && !Game1.statics.istryingtobuild)
                {
                    //Set the starting location for the selection box to the current location
                    //where the Left button was initially clicked.
                    mSelectionBox = new Rectangle(aMouse.X, aMouse.Y, 1, 1);
                }

                if (aMouse.LeftButton == ButtonState.Pressed && !Game1.statics.istryingtobuild)
                {
                    mSelectionBox = new Rectangle(mSelectionBox.X, mSelectionBox.Y, aMouse.X - mSelectionBox.X, aMouse.Y - mSelectionBox.Y);
                }

                //If the user has released the left mouse button, then reset the selection square
                if (aMouse.LeftButton == ButtonState.Released && !Game1.statics.istryingtobuild)
                {

                    #region select units from selectionbox
                    if (!mSelectionBox.Equals(new Rectangle(-1, -1, 0, 0)))
                    {
                        if (aMouse.X - mSelectionBox.X > 0 && aMouse.Y - mSelectionBox.Y > 0) // top left to bottom right
                            mSelectionBox = new Rectangle(mSelectionBox.X, mSelectionBox.Y, aMouse.X - mSelectionBox.X, aMouse.Y - mSelectionBox.Y);
                        else if (aMouse.X - mSelectionBox.X < 0 && aMouse.Y - mSelectionBox.Y < 0) // bottom right to top left
                            mSelectionBox = new Rectangle(aMouse.X, aMouse.Y, mSelectionBox.X - aMouse.X, mSelectionBox.Y - aMouse.Y);
                        else if (aMouse.X - mSelectionBox.X > 0 && aMouse.Y - mSelectionBox.Y < 0) // bottom left to top right
                            mSelectionBox = new Rectangle(mSelectionBox.X, aMouse.Y, aMouse.X - mSelectionBox.X, mSelectionBox.Y - aMouse.Y);
                        else // top right to bottom left
                            mSelectionBox = new Rectangle(aMouse.X, mSelectionBox.Y, mSelectionBox.X - aMouse.X, aMouse.Y - mSelectionBox.Y);
                        map.selected.Clear();
                        map.selectedResource = null;
                        bool oneUnitSelected = false; // true if at least one selected unit isnt a building
                        bool ownerUnitSelected = false;

                        if (mSelectionBox.Width < 32) // we require at least a tile for select
                            mSelectionBox.Width = 32;
                        if (mSelectionBox.Height < 32) // we require at least a tile for select
                            mSelectionBox.Height = 32;


                        /**
                        * Select units logic goes there
                        * */
                        foreach (Unit u in map.units)
                        {
                            if (u.disabled <= 0) // not forinstance a harvester in a gold mine
                            {
                                Rectangle rec = new Rectangle((int)cam.ToMousePosition(u.position).X, (int)cam.ToMousePosition(u.position).Y, 
                                    (int)(u.spriteSize.X * cam.Zoom), (int)(u.spriteSize.Y * cam.Zoom));
                                if (rec.Intersects(mSelectionBox))
                                    {
                                        if (!((u.type == UnitType.Paysan || u.type == UnitType.Petrolier) && ((((Harvester)u).disabled > 0) || (((Harvester)u).inbuilding != null)
                                            || (((Harvester)u).currentResource != null && ((Harvester)u).currentResource.type == ResourceType.Gold && ((Harvester)u).timeharvesting != 0)))) // if its not a disabled harvester, add it
                                        {
                                            // Game1.statics.annouceMessage.UpdateMessage("camera:" + cam.Pos.ToString() + "zoom:" + cam.Zoom + "mouse:X:" + aMouse.X + " Y:" + aMouse.Y + "upos:" + u.position.ToString());
                                            // Game1.statics.annouceMessage.UpdateMessage(mSelectionBox.ToString());
                                            map.selected.Add(u);
                                            if ((int)u.type < Unit.littlebuildingindex)
                                                oneUnitSelected = true;
                                            if (u.owner == Game1.currentPLayer)
                                                ownerUnitSelected = true;
                                        }
                                    }
                            
                            }
                            
                        }
                        if (map.selected.Count > 0)
                            map.selected.RemoveAll(unit => (unit.type == UnitType.Sousmarin && (!((Submarine)unit).canBeSeenBy(Game1.currentPLayer)))); // if is an invisible submarine remove it


                        if (ownerUnitSelected) // if a unit from player is selected we remove AI units from selection
                            map.selected.RemoveAll(x => x.owner != Game1.currentPLayer);
                        if (oneUnitSelected) // if a unit is selected we remove the buildings from the selection
                            map.selected.RemoveAll(x => (int)x.type >= Unit.littlebuildingindex);
                        else if (!oneUnitSelected && map.selected.Count > 0)
                        {

                            Unit u2 = map.selected.First();
                            Game1.soundEngine.PlaySelectedSound(u2);
                            map.selected.Clear();
                            map.selected.Add(u2);
                        }
                        if (map.selected.Count == 1)
                            Game1.soundEngine.PlaySelectedSound(map.selected.First());

                        /**
                         * Select resource logic goes there
                         * */
                        if (map.selected.Count == 0)
                        {
                            foreach (Resource r in map.resources)
                                if (r != null && (r.type == ResourceType.Gold || r.type == ResourceType.Oil)) 
                                {
                                   if (new Rectangle((int)(cam.ToMousePosition(r.Position).X), (int)cam.ToMousePosition(r.Position).Y, 
                                    (int)(96 * cam.Zoom), (int)(96 * cam.Zoom))
                                    .Intersects(mSelectionBox))
                                        {
                                            if (map.selectedResource == null && r.type == ResourceType.Gold)
                                                Game1.soundEngine.PlaySound(Game1.soundEngine.selectedSE[(int)SelectedSE.GoldMine], new Vector2(r.Position.X, r.Position.Y));
                                            if (map.selectedResource == null && r.type == ResourceType.Oil)
                                                Game1.soundEngine.PlaySound(Game1.soundEngine.selectedSE[(int)SelectedSE.OilPatch], new Vector2(r.Position.X, r.Position.Y));
                                            map.selectedResource = r;
                                            break;         
                                    
                                 }
                             }                                                              
                          
                    }
                     

                        mSelectionBox = new Rectangle(-1, -1, 0, 0);
                    }
                    #endregion


                    //Reset the selection square to no position with no height and width
                }
            }
            #endregion
            else // in menubar OR pressed shortcut
            {
                Vector2 SelectUnitIconCoordinate = new Vector2(LeftPanel.LEFTPANEL_TOPLEFT_ICON_POSITION_X, LeftPanel.LEFTPANEL_TOPLEFT_ICON_POSITION_Y); // + 50 in X or + 50 in Y to get next unit
                Vector2 CreateUnitIconCoordinate = Game1.statics.CreateUnitIconCoordinate;

                #region harvester click on panel
                if (map.selected.Count == 1 && map.selected.First().type == UnitType.Paysan) // peasant building logic
                {
                    if (Game1.prevKstate.IsKeyDown(Shortcuts.BuildPanel) || aMouse.LeftButton == ButtonState.Pressed && aMouse.X > 10
                        && aMouse.X < 56 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 45 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height + 38)
                    {
                        Game1.map.players[map.selected.First().owner].buildPanelToDraw = true;
                        Game1.map.players[map.selected.First().owner].advancedbuildPanelToDraw = false;
                    }
                    else if (Game1.prevKstate.IsKeyDown(Shortcuts.AdvancedBuildPanel) || aMouse.LeftButton == ButtonState.Pressed && aMouse.X > 60
                   && aMouse.X < 60 + 46 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 45 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height + 38)
                    {
                        Game1.map.players[map.selected.First().owner].buildPanelToDraw = false;
                        Game1.map.players[map.selected.First().owner].advancedbuildPanelToDraw = true;
                    }

                    else if ((aMouse.LeftButton == ButtonState.Pressed || Game1.prevKstate.GetPressedKeys().Count() >= 1) && Game1.map.players[Game1.currentPLayer].buildPanelToDraw)
                    {
                        #region Simple build panel click and shortcuts handling

                        /**
                         * Shortcuts
                         * */

                        List<UnitType> baseBuildPanelAllowed = TechTree.GetBasePanelBuildings();
                        // filter on what the player can actually build
                        baseBuildPanelAllowed = baseBuildPanelAllowed.Intersect(Game1.map.players[Game1.currentPLayer].PlayerCanBuild()).ToList();


                        foreach (Keys key in Game1.prevKstate.GetPressedKeys())
                        {
                            if ((Shortcuts.GetShortcutUnit(key, baseBuildPanelAllowed) != UnitType.None))
                            {
                                ((Harvester)map.selected.First()).selectedBuilding = (Building)map.CreateUnit(Shortcuts.GetShortcutUnit(key, baseBuildPanelAllowed), Game1.statics.ImpossiblePos, map.selected.First().owner);
                                break;
                            }
                        }

                        /**
                         * Mouse
                         */
                        UnitType buildingToBeSelected = LeftPanelUtils.ToBuilding(LeftPanelUtils.MouseOnLeftPanelIcon(aMouse));

                        if (buildingToBeSelected != UnitType.None)
                            ((Harvester)map.selected.First()).selectedBuilding = (Building)map.CreateUnit(buildingToBeSelected, Game1.statics.ImpossiblePos, map.selected.First().owner);
                     #endregion

                    }
                    else if ((aMouse.LeftButton == ButtonState.Pressed || Game1.prevKstate.GetPressedKeys().Count() >= 1) && Game1.map.players[Game1.currentPLayer].advancedbuildPanelToDraw)
                    {
                        #region advanced build panel click and shortcuts handling
                        List<UnitType> advBuildPanelAllowed = TechTree.GetAdvancedPanelBuildings();
                        // filter on what the player can actually build
                        advBuildPanelAllowed = advBuildPanelAllowed.Intersect(Game1.map.players[Game1.currentPLayer].PlayerCanBuild()).ToList();

                        /**
                         * Shortcuts
                         * */
                        foreach (Keys key in Game1.prevKstate.GetPressedKeys())
                        {
                            if ((Shortcuts.GetShortcutUnit(key, advBuildPanelAllowed) != UnitType.None))
                            {
                                ((Harvester)map.selected.First()).selectedBuilding = (Building)map.CreateUnit(Shortcuts.GetShortcutUnit(key, advBuildPanelAllowed), Game1.statics.ImpossiblePos, map.selected.First().owner);
                                break;
                            }
                        }

                        /**
                         * Check mouse click
                         * */

                        UnitType buildingToBeSelected = LeftPanelUtils.ToBuilding(LeftPanelUtils.MouseOnLeftPanelIcon(aMouse), true);

                        if (buildingToBeSelected != UnitType.None)
                            ((Harvester)map.selected.First()).selectedBuilding = (Building)map.CreateUnit(buildingToBeSelected, Game1.statics.ImpossiblePos, map.selected.First().owner);
                        #endregion
                    }
                    // if we are trying to build something we don't have the tech for, cancel it
                    if (((Harvester)map.selected.First()).selectedBuilding != null && !Game1.map.players[Game1.currentPLayer].playerTechs.hasTech(((Harvester)map.selected.First()).selectedBuilding.type))
                        ((Harvester)map.selected.First()).selectedBuilding = null;

                #endregion
                    if (!Game1.statics.istryingtobuild && map.selected.First().type == UnitType.Paysan && ((Harvester)map.selected.First()).selectedBuilding != null)
                    {
                        Game1.map.players[map.selected.First().owner].buildPanelToDraw = false;
                        Game1.map.players[map.selected.First().owner].advancedbuildPanelToDraw = false;
                        Game1.statics.istryingtobuild = true;
                        Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.TICK]);
                    }
                }
                else if (map.selected.Count == 1 && map.selected.First().type == UnitType.Petrolier) // tanker building logic
                {
                    if (!Game1.statics.istryingtobuild && (Game1.prevKstate.IsKeyDown(Shortcuts.GetShortCutKey(UnitType.NavalPlatform)) || 
                        (aMouse.LeftButton == ButtonState.Pressed  && aMouse.X > 10 && aMouse.X < 56 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 45 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 90)))
                        {
                            ((Harvester)map.selected.First()).selectedBuilding = (Building)map.CreateUnit(UnitType.NavalPlatform, Game1.statics.ImpossiblePos, map.selected.First().owner);
                            Game1.map.players[map.selected.First().owner].buildPanelToDraw = false;
                            Game1.map.players[map.selected.First().owner].advancedbuildPanelToDraw = false;
                            Game1.statics.istryingtobuild = true;
                            Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.TICK]);
                        }

             
                }

                else if (map.selected.Count == 1 && (int)map.selected.First().type >= Unit.littlebuildingindex) // handle unit construction logic for building
                {
                    #region building creating units
                    Building b = (Building)map.selected.First();
                    UnitType toBuild = UnitType.None;
                    if (BuildingUnitMouseConditions(aMouse, b))
                    {  
                        switch (map.selected.First().type)
                        {
                            case UnitType.MilitaryBase:
                                {
                                    if (buildingFirstUnit(CreateUnitIconCoordinate, aMouse))
                                        toBuild = UnitType.Fantassin;
                                    else if (buildingSecondUnit(CreateUnitIconCoordinate, aMouse))
                                        toBuild = UnitType.Archer;
                                    else if (buildingThirdUnit(CreateUnitIconCoordinate, aMouse))
                                        toBuild = UnitType.Catapulte;
                                    else if (buildingFourthUnit(CreateUnitIconCoordinate, aMouse))
                                        toBuild = UnitType.Chevalier;
                                }
                                break;
                            case UnitType.GoblinWorkBench:
                                if (buildingFirstUnit(CreateUnitIconCoordinate, aMouse))
                                    toBuild = UnitType.MachineVolante;
                                else if (buildingSecondUnit(CreateUnitIconCoordinate, aMouse))
                                    toBuild = UnitType.Nains;
                                break;
                            case UnitType.GryffinTower:
                                if (buildingFirstUnit(CreateUnitIconCoordinate, aMouse))
                                    toBuild = UnitType.Griffon;

                                break;
                            case UnitType.MageTower:
                                if (buildingFirstUnit(CreateUnitIconCoordinate, aMouse))
                                    toBuild = UnitType.Mage;

                                break;
                            case UnitType.NavalChantier:
                                if (buildingFirstUnit(CreateUnitIconCoordinate, aMouse))
                                    toBuild = UnitType.Petrolier;
                                if (buildingSecondUnit(CreateUnitIconCoordinate, aMouse))
                                    toBuild = UnitType.Destroyer;
                                if (buildingThirdUnit(CreateUnitIconCoordinate, aMouse))
                                    toBuild = UnitType.Sousmarin;
                                if (buildingFourthUnit(CreateUnitIconCoordinate, aMouse))
                                    toBuild = UnitType.Battleship;
                                if (buildingFifthUnit(CreateUnitIconCoordinate, aMouse))
                                    toBuild = UnitType.Transport;
                                break;
                            case UnitType.TownHall:
                            case UnitType.StrongHold:
                            case UnitType.Fortress:
                                if (buildingFirstUnit(CreateUnitIconCoordinate, aMouse))
                                    toBuild = UnitType.Paysan;
                                

                                break;
                        }
                    #endregion

                        if (Game1.map.players[map.selected.First().owner].usedFood < Game1.map.players[map.selected.First().owner].food && toBuild != UnitType.None)
                        {

                            b.BuildUnit(toBuild);
                            map.selected.Clear();
                            map.selected.Add(b);
                        }
                        else if (toBuild != UnitType.None)
                        {
                            Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.KnightNoFood1]);
                            Game1.statics.annouceMessage.UpdateMessage("Not enough food. Build more farms.");
                            
                            b.unitbuilding = UnitType.None;
                        }
                    }

            

                }


                if (aMouse.LeftButton == ButtonState.Pressed && mPreviousMouseState.LeftButton == ButtonState.Released && map.selected.Count == 1 && (map.selected.First().type == UnitType.Forge)) // check building upgrades / normal upgrades
                {
                    if (buildingFirstUnit(CreateUnitIconCoordinate, aMouse))
                       ((Building) map.selected.First()).Ameliorate(0);
                    else if (buildingSecondUnit(CreateUnitIconCoordinate, aMouse))
                        ((Building)map.selected.First()).Ameliorate(1);
                }
                if (aMouse.LeftButton == ButtonState.Pressed && mPreviousMouseState.LeftButton == ButtonState.Released && map.selected.Count == 1 && (
                    map.selected.First().type == UnitType.LumberMill || map.selected.First().type == UnitType.Church)) // check building upgrades / normal upgrades
                {
                    if (buildingFirstUnit(CreateUnitIconCoordinate, aMouse))
                        ((Building)map.selected.First()).Ameliorate(0);
                }
                if (aMouse.LeftButton == ButtonState.Pressed && mPreviousMouseState.LeftButton == ButtonState.Released && map.selected.Count == 1 && (int)map.selected.First().type >= Unit.littlebuildingindex
                    && ((Building)map.selected.First()).upgradingto == UnitType.None && ((Building)map.selected.First()).unitbuilding == UnitType.None) // check building upgrades / normal upgrades
                {
                    #region buildingUpgrades
                    Building b = (Building)map.selected.First();
                    if (b.isBuilt()) // building finished
                    {
                        switch (b.type)
                        {
                            case UnitType.TownHall:

                                if (buildingSecondUnit(CreateUnitIconCoordinate, aMouse))
                                    b.Upgrade(UnitType.StrongHold);
                                break;

                            case UnitType.StrongHold:

                                if (buildingSecondUnit(CreateUnitIconCoordinate, aMouse))
                                    b.Upgrade(UnitType.Fortress);
                                break;
                            case UnitType.Tower:
                                if (buildingFirstUnit(CreateUnitIconCoordinate, aMouse))
                                    b.Upgrade(UnitType.BallistaTower);
                                else if (buildingSecondUnit(CreateUnitIconCoordinate, aMouse))
                                    b.Upgrade(UnitType.CannonTower);
                                break;
                        }
                    }
                    #endregion
                }
                if (aMouse.LeftButton == ButtonState.Pressed && map.selected.Count == 1 && (int)map.selected.First().type >= Unit.littlebuildingindex
                    && aMouse.X > 120 && aMouse.X < 166 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 14 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 50) // check CANCEL build
                    ((Building)map.selected.First()).CancelBuild();
                if (aMouse.LeftButton == ButtonState.Pressed && map.selected.Count == 1 && map.selected.First().type == UnitType.Transport
    && aMouse.X > 120 && aMouse.X < 166 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 14 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 50) // check unload transport build
                    ((Transport)map.selected.First()).unload();
                if (aMouse.LeftButton == ButtonState.Pressed && map.selected.Count == 1 && map.selected.First().type == UnitType.Nains
&& aMouse.X > 120 && aMouse.X < 166 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 14 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 50) // check demolish
                    ((Demolisher)map.selected.First()).DemolishRequest();
                if (aMouse.LeftButton == ButtonState.Pressed && map.selected.Count == 1 && map.selected.First().type == UnitType.Mage
&& aMouse.X > 10 && aMouse.X < 166 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 14 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 100) // check spell
                    ((Wizard)map.selected.First()).SelectSpell(aMouse);

                for (int i = 0; i < map.selected.Count && i < 9; i++)
                {
                   if (aMouse.LeftButton == ButtonState.Pressed && mPreviousMouseState.LeftButton == ButtonState.Released &&
                            aMouse.X > LeftPanel.LEFTPANEL_TOPLEFT_ICON_POSITION_X + (LeftPanel.LEFTPANEL_ICON_SIZE_X * (i % 3))
                            && aMouse.X < LeftPanel.LEFTPANEL_TOPLEFT_ICON_POSITION_X + (LeftPanel.LEFTPANEL_ICON_SIZE_X * ((i % 3) + 1)) && aMouse.Y > LeftPanel.LEFTPANEL_TOPLEFT_ICON_POSITION_Y + (i / 3) * 50 &&
                            aMouse.Y < LeftPanel.LEFTPANEL_TOPLEFT_ICON_POSITION_Y + LeftPanel.LEFTPANEL_ICON_SIZE_Y + (((i / 3) + 1) * 50))
                        {
                       if (map.selected.Count == 1)
                       {
                           // set camera on unit if only one unit selected and icon clicked
                           cam.setPosition(new Vector2(- map.selected.First().position.X * 32 * cam.Zoom + Game1.graphics.GraphicsDevice.Viewport.Width / 2,
                            - map.selected.First().position.Y * cam.Zoom * 32 + Game1.graphics.GraphicsDevice.Viewport.Height / 2));
                       }
                       else
                       {
                           // simply select the unit through the mutliple ones
                            Unit u = map.selected[i];
                            map.selected.Clear();
                            map.selected.Add(u);
                       }
                       Game1.soundEngine.playTickSound();


                    }

                }
            }

            //Store the previous mouse state
            mPreviousMouseState = aMouse;
        }
        /**
         * Returns true if mouse has been clicked and building doesnt build yet
         */
        private bool BuildingUnitMouseConditions(MouseState aMouse, Building b)
        {
            return aMouse.LeftButton == ButtonState.Pressed && b.unitbuilding == UnitType.None && mPreviousMouseState.LeftButton == ButtonState.Released && b.isBuilt();
        }
        public bool buildingFirstUnit(Vector2 CreateUnitIconCoordinate, MouseState aMouse)
        {
            if (aMouse.X > CreateUnitIconCoordinate.X && aMouse.Y > CreateUnitIconCoordinate.Y && aMouse.X < CreateUnitIconCoordinate.X + 50 && aMouse.Y < CreateUnitIconCoordinate.Y + 50)
                return true;
            return false;
        }
        public bool buildingSecondUnit(Vector2 CreateUnitIconCoordinate, MouseState aMouse)
        {                                
            if (aMouse.X > CreateUnitIconCoordinate.X + 50 && aMouse.Y > CreateUnitIconCoordinate.Y && aMouse.X < CreateUnitIconCoordinate.X + 100 && aMouse.Y < CreateUnitIconCoordinate.Y + 50)
                return true;
            return false;
        }
        public bool buildingThirdUnit(Vector2 CreateUnitIconCoordinate, MouseState aMouse)
        {
            if (aMouse.X > CreateUnitIconCoordinate.X + 100 && aMouse.Y > CreateUnitIconCoordinate.Y && aMouse.X < CreateUnitIconCoordinate.X + 150 && aMouse.Y < CreateUnitIconCoordinate.Y + 50) 
                return true;
            return false;
        }
        public bool buildingFourthUnit(Vector2 CreateUnitIconCoordinate, MouseState aMouse)
        {
            if (aMouse.X > CreateUnitIconCoordinate.X && aMouse.Y > CreateUnitIconCoordinate.Y + 50 && aMouse.X < CreateUnitIconCoordinate.X + 50 && aMouse.Y < CreateUnitIconCoordinate.Y + 100)
                return true;
            return false;
        }
        public bool buildingFifthUnit(Vector2 CreateUnitIconCoordinate, MouseState aMouse)
        {
            if (aMouse.X > CreateUnitIconCoordinate.X + 50 && aMouse.Y > CreateUnitIconCoordinate.Y + 50 && aMouse.X < CreateUnitIconCoordinate.X + 100 && aMouse.Y < CreateUnitIconCoordinate.Y + 100)
                return true;
            return false;
        }

        public void Draw(SpriteBatch mSpriteBatch)
        {
            if (!mSelectionBox.Equals(new Rectangle(-1, -1, 0, 0)))
            {
                DrawHorizontalLine(mSelectionBox.Y, mSpriteBatch);
                DrawHorizontalLine(mSelectionBox.Y + mSelectionBox.Height, mSpriteBatch);

                //Draw the vertical portions of the selection box 
                DrawVerticalLine(mSelectionBox.X, mSpriteBatch);
                DrawVerticalLine(mSelectionBox.X + mSelectionBox.Width, mSpriteBatch);
            }
        }

        public void DrawCursor(SpriteBatch mSpriteBatch)
        {
            mSpriteBatch.Begin();

            MouseState ms = Mouse.GetState();
            if (Game1.spellCursorActive)
            {
                mSpriteBatch.Draw(spellCursor, new Vector2(ms.X - 5, ms.Y - 5), new Rectangle(0, 0, 48, 48), Color.White);
            }
            else
                mSpriteBatch.Draw(Cursor, new Vector2(ms.X - 5, ms.Y - 5), new Rectangle(0, 0, 48, 48), Color.White);
            mSpriteBatch.End();
        }

        private void DrawHorizontalLine(int thePositionY, SpriteBatch mSpriteBatch)
        {
            //When the width is greater than 0, the user is selecting an area to the right of the starting point
            if (mSelectionBox.Width > 0)
            {
                //Draw the line starting at the starting location and moving to the right
                for (int aCounter = 0; aCounter <= mSelectionBox.Width - 10; aCounter += 10)
                {
                    if (mSelectionBox.Width - aCounter >= 0)
                    {
                        mSpriteBatch.Draw(mDottedLine, new Rectangle(mSelectionBox.X + aCounter, thePositionY, 10, 1), Color.Gold);
                    }
                }
            }
            //When the width is less than 0, the user is selecting an area to the left of the starting point
            else if (mSelectionBox.Width < 0)
            {
                //Draw the line starting at the starting location and moving to the left
                for (int aCounter = -10; aCounter >= mSelectionBox.Width; aCounter -= 10)
                {
                    if (mSelectionBox.Width - aCounter <= 0)
                    {
                        mSpriteBatch.Draw(mDottedLine, new Rectangle(mSelectionBox.X + aCounter, thePositionY, 10, 1), Color.Gold);
                    }
                }
            }
        }
        private void DrawVerticalLine(int thePositionX, SpriteBatch mSpriteBatch)
        {
            //When the height is greater than 0, the user is selecting an area below the starting point
            if (mSelectionBox.Height > 0)
            {
                //Draw the line starting at the starting location and moving down
                for (int aCounter = -2; aCounter <= mSelectionBox.Height; aCounter += 10)
                {
                    if (mSelectionBox.Height - aCounter >= 0)
                    {
                        mSpriteBatch.Draw(mDottedLine, new Rectangle(thePositionX, mSelectionBox.Y + aCounter, 10, 1),
 new Rectangle(0, 0, mDottedLine.Width, mDottedLine.Height), Color.Gold, MathHelper.ToRadians(90), new Vector2(0, 0), SpriteEffects.None, 0);
                    }
                }
            }
            //When the height is less than 0, the user is selecting an area above the starting point
            else if (mSelectionBox.Height < 0)
            {
                //Draw the line starting at the start location and moving up
                for (int aCounter = 0; aCounter >= mSelectionBox.Height; aCounter -= 10)
                {
                    if (mSelectionBox.Height - aCounter <= 0)
                    {
                        mSpriteBatch.Draw(mDottedLine, new Rectangle(thePositionX - 10, mSelectionBox.Y + aCounter, 10, 1), Color.Gold);
                    }
                }
            }
        }

    }
}
