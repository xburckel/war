﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2
{
    public enum MiscSoundEffect
    {
        AXE,
        BLDEXPL1,
        BLDEXPL2,
        BLDEXPL3,
        BOWFIRE,
        BOWHIT,
        BURNING,
        CATAPULT,
        CATYESSR,
        CONSTRCT,
        DOCK,
        ERROR,
        EXPLODE,
        FIREBALL,
        FIREHIT,
        FIST,
        PEONATAK,
        PIG,
        PIGPISSD,
        PUNCH,
        SEAL,
        SEALPISD,
        SHEEP,
        SHPISD1,
        SWORD1,
        SWORD2,
        SWORD3,
        THUNK,
        TREE1,
        TREE2,
        TREE3,
        TREE4,

        FOGHORN,
        SHIPSINK,
        SUBPISS1,
        SUBPISS2,
        SUBPISS3,
        SUBPISS4,
        BLIZZARD,
        DEATHANDDECAY,
        TICK

    }

    public enum OrcSE
    {
        Dead, WorkComplete, MageBasicAttack
    }

    public enum SpellSounds { fireExplosion, arcaneBarrage, RESURRECTION }

    public enum HumanSE { Dead, WorkComplete, MageBasicAttack }

    public enum MoveSE { Fantassin = 0, Peon = 3, Archer = 6, Knight = 9, MachineVolante = 12, Dragon = 13, Ship = 15, Tanker = 18};



    public enum SelectedSE
    {
        Foundry, LumbMill, Mine, OilPlat, OilRef, ShipBell, Smith, OAlchemis, ODragon, ODthTower, OChant, OFarm, OOgreCamp, HInventor, HAviary,
        HWZTower, HChant, HFarm, HStables, GoldMine, OilPatch
    }
    public enum UnitSelectedSE
    {
        FootmanSelected1, FootmanSelected2, FootmanSelected3, FootmanSelected4, FootmanSelected5, FootmanSelected6, Dwarves1, Dwarves2, Archer1, Archer2, Archer3, Archer4,
        Knight1, Knight2, Knight3, Knight4, Mage1, Mage2, Mage3, Peasant1, Peasant2, Peasant3, Peasant4, Ship1, Ship2, Ship3
    }


    public enum NewGameUnitSE
    {
        Catapult, Drake, Grunt, Ogre, Peon,
        Archer, Goblin
    }         


    public enum TriviaSE
    {
        AxeMissile1,
        AxeMissile2,
        AxeMissileLaunch1,
        BattleNetDoorsStereo2,
        BigButtonClick,
        Error,
        goldmineboom,
        KnightGoldMineCollapsed1,
        KnightGoldMineLow1,
        KnightNoEnergy1,
        KnightNoFood1,
        KnightNoGold1,
        KnightNoLumber1,
        KnightTownAttack1,
        KnightUnitAttack1,
        PeasantCannotBuildThere1,
        SentinelNoGold1,
        SentinelNoLumber1
    }

    public class SoundEngine
    {
        public bool stopplaying = false;
        ContentManager content;
        List<Song> musics;
        public List<Song> rareMusics; // a low chance of beeing played at each rerandom
        public List<SoundEffect> miscallieanousSE;
        public List<SoundEffect> humanSE;
        public List<SoundEffect> orcSE;
        public List<SoundEffect> selectedSE;
        public List<SoundEffect> orcSelectedSE; // orc selected unit
        public List<SoundEffect> humanSelectedSE; // human selected unit
        public List<SoundEffect> spellSounds; // spell launch sounds
        public List<SoundEffect> triviaSounds; // announcements etc
        public List<SoundEffect> newUnit; // announcements etc
        public List<SoundEffect> moveSoundsOrc; // move units
        public List<SoundEffect> moveSoundsHuman; 

        public SoundEngine(ContentManager content)
        {
            //   SoundEffect soundEffect;
            this.content = content;
            musics = new List<Song>();
            rareMusics = new List<Song>();
            miscallieanousSE = new List<SoundEffect>();
            humanSE = new List<SoundEffect>();
            orcSE = new List<SoundEffect>();
            selectedSE = new List<SoundEffect>();
            orcSelectedSE = new List<SoundEffect>();
            humanSelectedSE = new List<SoundEffect>();
            spellSounds = new List<SoundEffect>();
            triviaSounds = new List<SoundEffect>();
            newUnit = new List<SoundEffect>();
            moveSoundsOrc = new List<SoundEffect>();
            moveSoundsHuman = new List<SoundEffect>();

            // dont change the order here ofr in the enums
            #region sound effect load

            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/AxeMissile1"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/AxeMissile2"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/AxeMissileLaunch1"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/BattleNetDoorsStereo2"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/BigButtonClick"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/Error"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/goldmineboom"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/KnightGoldMineCollapsed1"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/KnightGoldMineLow1"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/KnightNoEnergy1"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/KnightNoFood1"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/KnightNoGold1"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/KnightNoLumber1"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/KnightTownAttack1"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/KnightUnitAttack1"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/PeasantCannotBuildThere1"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/SentinelNoGold1"));
            triviaSounds.Add(content.Load<SoundEffect>("Sounds/Trivia/SentinelNoLumber1"));

            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Basic Human Voices/HWHAT1"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Basic Human Voices/HWHAT2"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Basic Human Voices/HWHAT3"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Basic Human Voices/HWHAT4"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Basic Human Voices/HWHAT5"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Basic Human Voices/HWHAT6"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Dwarven Demolition Squad/DWHAT1"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Dwarven Demolition Squad/DWHAT2"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Elven Archer_Ranger/EWHAT1"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Elven Archer_Ranger/EWHAT2"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Elven Archer_Ranger/EWHAT3"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Elven Archer_Ranger/EWHAT4"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Knight/KNWHAT1"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Knight/KNWHAT2"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Knight/KNWHAT3"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Knight/KNWHAT4"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Mage/WZWHAT1"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Mage/WZWHAT2"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Mage/WZWHAT3"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Peasant/PSWHAT1"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Peasant/PSWHAT2"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Peasant/PSWHAT3"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Peasant/PSWHAT4"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Ships/HSHPWHT1"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Ships/HSHPWHT2"));
            humanSelectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Ships/HSHPWHT3"));



            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OPISSED3"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OPISSED1"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OWHAT1"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OWHAT2"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OWHAT3"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OWHAT4"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Goblin Sappers/GOWHAT1"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Goblin Sappers/GOWHAT2"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Troll Axethrower_Berserker/TRWHAT1"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Troll Axethrower_Berserker/TRWHAT2"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Troll Axethrower_Berserker/TRWHAT3"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Troll Axethrower_Berserker/TRPISSD2"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ogre/OGWHAT1"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ogre/OGWHAT2"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ogre/OGWHAT3"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ogre/OGWHAT4"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Death Knight/DKWHAT1"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Death Knight/DKWHAT2"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Death Knight/DKYESSR3"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OWHAT3"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OWHAT4"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OWHAT5"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OWHAT6"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ships/OSHPWHT1"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ships/OSHPWHT2"));
            orcSelectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ships/OSHPWHT3"));


            selectedSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Buildings/FOUNDRY"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Buildings/LUMBMILL"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Buildings/MINE"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Buildings/OILPLAT"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Buildings/OILREFIN"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Buildings/SHIPBELL"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Buildings/SMITH"));


            selectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Buildings/ALCHEMST"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Buildings/DRAGON"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Buildings/DTHTOWER"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Buildings/OCHANT"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Buildings/OFARM"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Buildings/OGRECAMP"));

            selectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Buildings/INVENTOR"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Buildings/AVIARY"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Buildings/WZRDTOWR"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Buildings/HCHANT"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Buildings/HFARM"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Buildings/STABLES"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Buildings/MINE"));
            selectedSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Buildings/OILPLAT"));

            humanSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Basic Human Voices/HDEAD"));
            humanSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Basic Human Voices/HWRKDONE"));
            humanSE.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Spells/THUNDER"));

            orcSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/ODEAD"));
            orcSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OWRKDONE"));
            orcSE.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Spells/TOUCHDRK"));

            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/AXE"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/BLDEXPL1"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/BLDEXPL2"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/BLDEXPL3"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/BOWFIRE"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/BOWHIT"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/BURNING"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/CATAPULT"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/CATYESSR"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/CONSTRCT"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/DOCK"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/ERROR"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/EXPLODE"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/FIREBALL"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/FIREHIT"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/FIST"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/PEONATAK"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/PIG"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/PIGPISSD"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/PUNCH"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/SEAL"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/SEALPISD"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/SHEEP"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/SHPISD1"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/SWORD1"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/SWORD2"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/SWORD3"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/THUNK"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/TREE1"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/TREE2"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/TREE3"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/TREE4"));


            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Ships/FOGHORN"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Ships/SHIPSINK"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Ships/SUBPISS1"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Ships/SUBPISS2"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Ships/SUBPISS3"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Ships/SUBPISS4"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/blizzard"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/death-and-decay"));
            miscallieanousSE.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Miscellaneous/Tick"));


            newUnit.Add(content.Load<SoundEffect>("Sounds/Human Sounds/New/catapult"));
            newUnit.Add(content.Load<SoundEffect>("Sounds/Human Sounds/New/drake"));
            newUnit.Add(content.Load<SoundEffect>("Sounds/Human Sounds/New/grunt"));
            newUnit.Add(content.Load<SoundEffect>("Sounds/Human Sounds/New/ogre"));
            newUnit.Add(content.Load<SoundEffect>("Sounds/Human Sounds/New/peon"));
            newUnit.Add(content.Load<SoundEffect>("Sounds/Human Sounds/New/archer"));
            newUnit.Add(content.Load<SoundEffect>("Sounds/Human Sounds/New/goblin"));
            // 6
            newUnit.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/New/catapult"));
            newUnit.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/New/drake"));
            newUnit.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/New/grunt"));
            newUnit.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/New/ogre"));
            newUnit.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/New/peon"));
            newUnit.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/New/archer"));
            newUnit.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/New/goblin"));

            spellSounds.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Spells/flame-shield"));
            spellSounds.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Spells/arcanbarrage"));
            spellSounds.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Spells/ResurrectTarget"));

            // Warning : l'ordre est important
            // human
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Basic Human Voices/HYESSIR1"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Basic Human Voices/HYESSIR2"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Basic Human Voices/HYESSIR3"));

            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Peasant/PSYESSR1"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Peasant/PSYESSR2"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Peasant/PSYESSR3"));

            //moveSounds.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Basic Human Voices/HYESSIR4"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Elven Archer_Ranger/EYESSIR1"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Elven Archer_Ranger/EYESSIR2"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Elven Archer_Ranger/EYESSIR3"));
           // moveSounds.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Elven Archer_Ranger/EYESSIR4"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Knight/KNYESSR1"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Knight/KNYESSR2"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Knight/KNYESSR3"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Gnomish Flying Machine/GNYESSR1"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Gryphon Rider/GRIFFON1"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Gryphon Rider/GRIFFON2"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Ships/HSHPYES1"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Ships/HSHPYES2"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Human Sounds/Ships/HSHPYES3"));
            moveSoundsHuman.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Ships/FOGHORN"));

            // orcs
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OYESSIR1"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OYESSIR2"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OYESSIR3"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OYESSIR1"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OYESSIR2"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OYESSIR3"));
            // moveSounds.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Basic Orc Voices/OYESSIR4"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Troll Axethrower_Berserker/TRYESSR1"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Troll Axethrower_Berserker/TRYESSR2"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Troll Axethrower_Berserker/TRYESSR3"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ogre/OGYESSR1"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ogre/OGYESSR2"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ogre/OGYESSR3"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Goblin Zeppelin/GBYESSR1"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Dragon/DRYESSR1"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Dragon/DRYESSR2"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ships/OSHPYES1"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ships/OSHPYES2"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Orc Sounds/Ships/OSHPYES3"));
            moveSoundsOrc.Add(content.Load<SoundEffect>("Sounds/Shared Sounds/Ships/FOGHORN"));



            #endregion

            for (int i = 1; i < 18; i++)
            {
                Song music = content.Load<Song>("Musics/Others/Other (" + i + ")");
                rareMusics.Add(music);
            }

            for (int i = 1; i < 10; i++)
            {
                Song music = content.Load<Song>("Musics/Human/human" + i);
                musics.Add(music);
                music = content.Load<Song>("Musics/Orc/orc" + i);
                musics.Add(music);
            }
            musics.Add(content.Load<Song>("Musics/Human/human11"));
        }

        public void PlaySound(SoundEffect se, Vector2 position)
        {
            if (withinCameraBounds(position) && (!Game1.statics.fogOfWarEnabled || (Map.Inbounds((int) position.X, (int) position.Y) && Game1.map.players[Game1.currentPLayer].hasVision[(int) position.X, (int) position.Y]))) 
                se.Play();

        }

        private bool withinCameraBounds(Vector2 position)
        {
           // if (!Map.Inbounds((int)position.X, (int)position.Y)) // fuck this lets be generic
             //   position = position / 32;
            float realposx = Game1.cam.Pos.X / Game1.cam.Zoom;
            float realposy = Game1.cam.Pos.Y / Game1.cam.Zoom; // ex : - 1458.5

            int width = Game1.graphics.GraphicsDevice.Viewport.Width;
            int height = Game1.graphics.GraphicsDevice.Viewport.Width;



            if (realposx + position.X * 32 > 0 && realposx + position.X * 32 < width
                && realposy + position.Y * 32 > 0 && realposy + position.Y * 32 < height)
                return true;

            return false;
        }

        public void Update(GameTime gameTime)
        {
            if (!stopplaying)
            {
                Random r = new Random();

                int playRareMusic = r.Next(10);

                if (playRareMusic == 6 && MediaPlayer.State != MediaState.Playing) // one chance out of 10 to play a rare music
                {
                    int i = r.Next(1, rareMusics.Count - 1);
                    MediaPlayer.Play(rareMusics[i]);

                }
                else if (playRareMusic != 6 && MediaPlayer.State != MediaState.Playing)
                {
                    int i = r.Next(musics.Count - 1);
                    MediaPlayer.Play(musics[i]);
                }


                foreach (Unit u in Game1.map.units)
                {
                    if (withinCameraBounds(u.position))
                    {
                        if (u.hp <= 0) // DEATH sounds
                        {
                            if ((int)u.type >= Unit.littlebuildingindex)
                            {
                                int rand = r.Next(3);
                                miscallieanousSE[(int)MiscSoundEffect.BLDEXPL1 + rand].Play();
                            }
                            else if (u.sailing)
                            {
                                miscallieanousSE[(int)MiscSoundEffect.SHIPSINK].Play();

                            }
                            else if (!u.flying)
                            {
                                if (Game1.map.players[u.owner].isOrc)
                                    orcSE[(int)OrcSE.Dead].Play();
                                else
                                    humanSE[(int)OrcSE.Dead].Play();


                            }
                        }

                    }

                }
            }


        }

        // TODO : Complete
        public void playMoveUnitSound(UnitType ut, bool isOrc)
        {
            Random r = new Random();
            int rand = r.Next(3);
            List<SoundEffect> moveSounds = new List<SoundEffect>();

            if (isOrc)
                moveSounds = moveSoundsOrc;
            else
                moveSounds = moveSoundsHuman;
            switch (ut)
            {
                case UnitType.Archer:
                    moveSounds[(int)MoveSE.Archer + rand].Play();
                    break;
                case (UnitType.Fantassin):
                    moveSounds[(int)MoveSE.Fantassin + rand].Play();
                    break;
                case (UnitType.Paysan):
                    moveSounds[(int)MoveSE.Peon + rand].Play();
                    break;
                case (UnitType.MachineVolante):
                    moveSounds[(int)MoveSE.MachineVolante].Play();
                    break;
                case UnitType.Chevalier:case UnitType.Paladin:
                    moveSounds[(int)MoveSE.Knight + rand].Play();
                    break;
                case UnitType.Griffon:
                    rand = r.Next(2);
                    moveSounds[(int)MoveSE.Dragon + rand].Play();
                    break;
                case (UnitType.Catapulte):
                    miscallieanousSE[(int)MiscSoundEffect.CATYESSR].Play();
                    break;
                case UnitType.Destroyer:case UnitType.Battleship:case UnitType.Sousmarin:
                    moveSounds[(int)MoveSE.Ship + rand].Play();

                    break;
                case UnitType.Petrolier:
                    moveSounds[(int)MoveSE.Tanker].Play();
                    break;
                default:
                    break;
            }

        }

        public void playTickSound()
        {
            miscallieanousSE[(int)MiscSoundEffect.TICK].Play();
        }

        internal void PlaySelectedSound(Unit u2)
        {
            if (u2.isABuilding && u2.hp < (UnitProperties.getBuildingProperties(u2.type)[(int)BuildingProperties.HP] / 3) && ((Building) u2).isBuilt())
            {
                miscallieanousSE[(int)MiscSoundEffect.BURNING].Play();
                return;
            }


            if (u2.owner != Game1.currentPLayer)
            {
                playTickSound();
                return;
            }


            if ((int)u2.type >= Unit.littlebuildingindex)
            {

                if (((Building)u2).isBuilt())
                {

                    switch (u2.type)
                    {

                        case UnitType.LumberMill:
                            selectedSE[(int)SelectedSE.LumbMill].Play();
                            break;
                        case UnitType.Foundry:
                            selectedSE[(int)SelectedSE.Foundry].Play();
                            break;
                        case UnitType.Raffinery:
                            selectedSE[(int)SelectedSE.OilRef].Play();
                            break;
                        case UnitType.NavalChantier:
                            selectedSE[(int)SelectedSE.ShipBell].Play();
                            break;
                        case UnitType.Forge:
                            selectedSE[(int)SelectedSE.Smith].Play();
                            break;
                        case UnitType.Church:
                            if (Game1.map.players[u2.owner].isOrc)
                                selectedSE[(int)SelectedSE.OChant].Play();
                            else
                                selectedSE[(int)SelectedSE.HChant].Play();
                            break;
                        case UnitType.Farm:
                            if (Game1.map.players[u2.owner].isOrc)
                                selectedSE[(int)SelectedSE.OFarm].Play();
                            else
                                selectedSE[(int)SelectedSE.HFarm].Play();
                            break;
                        case UnitType.GryffinTower:
                            if (Game1.map.players[u2.owner].isOrc)
                                selectedSE[(int)SelectedSE.ODragon].Play();
                            else
                                selectedSE[(int)SelectedSE.HAviary].Play();
                            break;
                        case UnitType.MageTower:
                            if (Game1.map.players[u2.owner].isOrc)
                                selectedSE[(int)SelectedSE.ODthTower].Play();
                            else
                                selectedSE[(int)SelectedSE.HWZTower].Play();
                            break;
                        case UnitType.GoblinWorkBench:
                            if (Game1.map.players[u2.owner].isOrc)
                                selectedSE[(int)SelectedSE.OAlchemis].Play();
                            else
                                selectedSE[(int)SelectedSE.HInventor].Play();
                            break;
                        case UnitType.Stable:
                            if (Game1.map.players[u2.owner].isOrc)
                                selectedSE[(int)SelectedSE.OOgreCamp].Play();
                            else
                                selectedSE[(int)SelectedSE.HStables].Play();
                            break;
                        default:
                            miscallieanousSE[(int)MiscSoundEffect.TICK].Play();
                            break;

                    }
                }
                else
                    miscallieanousSE[(int)MiscSoundEffect.CONSTRCT].Play();

            }
            else
            {
                Random r = new Random();
                if (Game1.map.players[u2.owner].isOrc)
                {
                    switch (u2.type)
                    {
                        case (UnitType.Fantassin):
                            int rand = r.Next(6);
                            orcSelectedSE[(int)UnitSelectedSE.FootmanSelected1 + rand].Play();
                            break;
                        case (UnitType.Archer):
                            rand = r.Next(4);
                            orcSelectedSE[(int)UnitSelectedSE.Archer1 + rand].Play();
                            break;
                        case (UnitType.Chevalier):
                            rand = r.Next(4);
                            orcSelectedSE[(int)UnitSelectedSE.Knight1 + rand].Play();
                            break;
                        case (UnitType.Mage):
                            rand = r.Next(3);
                            orcSelectedSE[(int)UnitSelectedSE.Mage1 + rand].Play();
                            break;
                        case (UnitType.Nains):
                            rand = r.Next(2);
                            orcSelectedSE[(int)UnitSelectedSE.Dwarves1 + rand].Play();
                            break;
                        case (UnitType.Paysan):
                            rand = r.Next(4);
                            orcSelectedSE[(int)UnitSelectedSE.Peasant1 + rand].Play();
                            break;
                    }
                }
                else
                {
                    switch (u2.type)
                    {
                        case (UnitType.Fantassin):
                            int rand = r.Next(6);
                            humanSelectedSE[(int)UnitSelectedSE.FootmanSelected1 + rand].Play();
                            break;
                        case (UnitType.Archer):
                            rand = r.Next(4);
                            humanSelectedSE[(int)UnitSelectedSE.Archer1 + rand].Play();
                            break;
                        case (UnitType.Chevalier):
                            rand = r.Next(4);
                            humanSelectedSE[(int)UnitSelectedSE.Knight1 + rand].Play();
                            break;
                        case (UnitType.Mage):
                            rand = r.Next(3);
                            humanSelectedSE[(int)UnitSelectedSE.Mage1 + rand].Play();
                            break;
                        case (UnitType.Nains):
                            rand = r.Next(2);
                            humanSelectedSE[(int)UnitSelectedSE.Dwarves1 + rand].Play();
                            break;
                        case (UnitType.Paysan):
                            rand = r.Next(4);
                            humanSelectedSE[(int)UnitSelectedSE.Peasant1 + rand].Play();
                            break;
                    }

                    if (u2.sailing)
                    {
                        int rand = r.Next(3);
                        humanSelectedSE[(int)UnitSelectedSE.Ship1 + rand].Play();

                    }
                }
            }


        }

        internal void PlayUnitSelectedSound(Unit unit)
        {
          

        }

        internal void PlayAnnoucementSound(SoundEffect soundEffect)
        {
            soundEffect.Play();
        }

        internal void setVolume(int sound)
        {
            MediaPlayer.Volume = (float)sound / 100f;
        }

        internal void setSEVolume(int soundeffect)
        {
            SoundEffect.MasterVolume = (float)soundeffect / 100f;
        }

        internal void ContinuePlaying()
        {
            stopplaying = false;
        }

        internal void StopPlaying()
        {
            stopplaying = true;
            MediaPlayer.Stop();
        }

        internal void playNewUnitSound(UnitType unitType)
        {
            switch (unitType)
            {
                case UnitType.Paysan:
                    if (!Game1.map.players[Game1.currentPLayer].isOrc)
                        newUnit[(int) NewGameUnitSE.Peon].Play();
                    else
                        newUnit[(int)NewGameUnitSE.Peon + 7].Play();                   
                    break;
                case UnitType.Catapulte:
                    if (!Game1.map.players[Game1.currentPLayer].isOrc)
                        newUnit[(int)NewGameUnitSE.Catapult].Play();
                    else
                        newUnit[(int)NewGameUnitSE.Catapult + 7].Play();
                    break;
                case UnitType.Fantassin:
                    if (!Game1.map.players[Game1.currentPLayer].isOrc)
                        newUnit[(int)NewGameUnitSE.Grunt].Play();
                    else
                        newUnit[(int)NewGameUnitSE.Grunt + 7].Play();
                    break;
                case UnitType.Chevalier:
                    if (!Game1.map.players[Game1.currentPLayer].isOrc)
                        newUnit[(int)NewGameUnitSE.Ogre].Play();
                    else
                        newUnit[(int)NewGameUnitSE.Ogre + 7].Play();
                    break;
                case UnitType.Griffon:
                    if (!Game1.map.players[Game1.currentPLayer].isOrc)
                        newUnit[(int)NewGameUnitSE.Drake].Play();
                    else
                        newUnit[(int)NewGameUnitSE.Drake + 7].Play();
                    break;
                case UnitType.Archer:
                    if (!Game1.map.players[Game1.currentPLayer].isOrc)
                        newUnit[(int)NewGameUnitSE.Archer].Play();
                    else
                        newUnit[(int)NewGameUnitSE.Archer + 7].Play();
                    break;
                case UnitType.Nains:
                    if (!Game1.map.players[Game1.currentPLayer].isOrc)
                        newUnit[(int)NewGameUnitSE.Goblin].Play();
                    else
                        newUnit[(int)NewGameUnitSE.Goblin + 7].Play();
                    break;
            }
        }
    }
}
