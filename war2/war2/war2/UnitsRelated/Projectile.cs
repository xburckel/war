﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.UnitsRelated;

namespace war2
{
    public enum ProjectileType
    {
        RockType, Ballista, Arrow, Axe, Thunder, Spin, BattleShipRock, CannonTowerRock, SubmarineMissile, GriffonHammer, DragonBall

    };




    [Serializable]
    public class Projectile
    {
        public static int rockSpeed = 7;
        public static int arrowSpeed = 6;
        public static int thunderSpeed = 3;
        public static int CannonRockSpeed = 10;
        public static int gryffonProjectileSpeed = 3;
        public static int submarineSpeed = 10;
        private bool flip;
        private Orientation orientation;
        public int timer;
       // public float rotation; // for later
        private Vector2[] positionInTexture;
        public Vector2 positionOnMap;
        public Vector2 targetPosition; // position bsolue : ex : case 5, 32
        private Vector2 trueposition; // position au milieu des cases
        private Vector2 size;
        // PC Computer - Warcraft II - Magic and Missiles.png
        public Vector2 position;
        Vector2 move;
        ProjectileType type;
        AnimationType hitAnimation;
        int interval; // set to 0 by default for no multiple sprites (static lik arrow)
        int currenttexture;
        int ellapsedTime;
        float startDistance; // garde fou en cas de bug, que le projectile soit qd même detrui au bout d'un moment...

        public Projectile(ProjectileType type, Vector2 targetPosition, Vector2 positionOnMap,  Vector2 targetSpriteSize, Vector2 startSpriteSize)
        {
            positionInTexture = new Vector2[1];
            interval = 0;
            currenttexture = 0;
            ellapsedTime = 0;
            hitAnimation = AnimationType.None;
            flip = false;
            this.type = type;
            int speed = 1;
            timer = 0;
            this.targetPosition = new Vector2((int)Math.Round((double)targetPosition.X + targetSpriteSize.X / 32 / 2), (int)Math.Round((double)targetPosition.Y + targetSpriteSize.Y / 32 / 2));
            this.positionOnMap = new Vector2((int)Math.Round((double)positionOnMap.X),
                (int)Math.Round((double)positionOnMap.Y)) + startSpriteSize / 2;
            trueposition = new Vector2(positionOnMap.X * 32 + startSpriteSize.X / 2, positionOnMap.Y * 32 + startSpriteSize.Y / 2);
           

            Tuple<Orientation, bool> orientationResult = Utils.Utils.getOrientation(positionOnMap, targetPosition);

            orientation = orientationResult.Item1;
            flip = orientationResult.Item2;
            startDistance = Vector2.Distance(this.targetPosition, this.trueposition);




            switch (type)
            {
                case (ProjectileType.Arrow):
                    speed = arrowSpeed;
                    positionInTexture[0] = new Vector2(819, 115);
                    Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.BOWFIRE], positionOnMap);
                   
                    switch (orientation)
                    {
                        case Orientation.Top:
                            positionInTexture[0] = new Vector2(821, 116);
                            size = new Vector2(3, 30);

                            break;
                        case Orientation.TopRight:
                            positionInTexture[0] = new Vector2(852, 120);
                            size = new Vector2(21, 33);
                            break;
                        case Orientation.Right:
                            positionInTexture[0] = new Vector2(888, 131);
                            size = new Vector2(30, 3);

                            break;
                        case Orientation.BottomRight:
                            positionInTexture[0] = new Vector2(931, 121);
                            size = new Vector2(23, 21);

                            break;
                        case Orientation.Bottom:
                            positionInTexture[0] = new Vector2(981, 116);
                            size = new Vector2(3, 30);
                            break;
                    }

                    break;
                case (ProjectileType.Axe): 
                   // rotation = 0;
                    speed = arrowSpeed;
                    interval = 100;
                    size = new Vector2(20, 20);
                    positionInTexture = new Vector2[3];
                    positionInTexture[0] = new Vector2(958, 17);
                    positionInTexture[1] = new Vector2(989, 16);
                    positionInTexture[2] = new Vector2(1020, 15);
                    Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.AXE], positionOnMap);
                    break;
                case (ProjectileType.Ballista):
                    speed = rockSpeed;
                    positionInTexture[0] = new Vector2(319, 2);
                    Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.CATAPULT], positionOnMap);

                    switch (orientation)
                    {
                        case Orientation.Top:
                            positionInTexture[0] = new Vector2(322, 5);
                            size = new Vector2(13, 46);

                            break;
                        case Orientation.TopRight:
                            positionInTexture[0] = new Vector2(347, 9);
                            size = new Vector2(39, 38);
                            break;
                        case Orientation.Right:
                            positionInTexture[0] = new Vector2(398, 19);
                            size = new Vector2(50, 15);

                            break;
                        case Orientation.BottomRight:
                            positionInTexture[0] = new Vector2(457, 9);
                            size = new Vector2(38, 39);

                            break;
                        case Orientation.Bottom:
                            positionInTexture[0] = new Vector2(508, 6);
                            size = new Vector2(13, 46);
                            break;
                    }
                    hitAnimation = AnimationType.CatapultHit;
                    break;
                case (ProjectileType.RockType):
                    speed = rockSpeed;
                    size = new Vector2(15, 33);
                    positionInTexture[0] = new Vector2(167, 65);
                    Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.CATAPULT], positionOnMap);
                    switch (orientation)
                    {
                        case Orientation.Top:
                            positionInTexture[0] = new Vector2(168, 65);
                            size = new Vector2(12, 32);

                            break;
                        case Orientation.TopRight:
                            positionInTexture[0] = new Vector2(194, 66);
                            size = new Vector2(27, 28);
                            break;
                        case Orientation.Right:
                            positionInTexture[0] = new Vector2(222, 75);
                            size = new Vector2(32, 12);

                            break;
                        case Orientation.BottomRight:
                            positionInTexture[0] = new Vector2(258, 68);
                            size = new Vector2(27, 28);

                            break;
                        case Orientation.Bottom:
                            positionInTexture[0] = new Vector2(296, 65);
                            size = new Vector2(12, 32);
                            break;
                    }
                    hitAnimation = AnimationType.CatapultHit;

                    break;
                case (ProjectileType.Spin):
                    size = new Vector2(30, 26);
                    positionInTexture = new Vector2[5];
                    positionInTexture[0] = new Vector2(685, 256);
                    positionInTexture[1] = new Vector2(721, 254);
                    positionInTexture[2] = new Vector2(756, 258);
                    positionInTexture[3] = new Vector2(795, 257);
                    positionInTexture[4] = new Vector2(833, 258);
                    interval = 50;
                    speed = thunderSpeed;
                    Game1.soundEngine.PlaySound(Game1.soundEngine.orcSE[(int)OrcSE.MageBasicAttack], positionOnMap);
                    break;
                case (ProjectileType.Thunder):
                    speed = thunderSpeed;
                    size = new Vector2(21, 18);
                    positionInTexture[0] = new Vector2(23, 259);   
                    Game1.soundEngine.PlaySound(Game1.soundEngine.humanSE[(int)HumanSE.MageBasicAttack], positionOnMap);

                    switch (orientation)
                    {
                        case Orientation.Top:
                            positionInTexture[0] = new Vector2(939, 257);
                            size = new Vector2(11, 24);

                            break;
                        case Orientation.TopRight:
                            positionInTexture[0] = new Vector2(965, 258);
                            size = new Vector2(23, 23);
                            break;
                        case Orientation.Right:
                            positionInTexture[0] = new Vector2(996, 266);
                            size = new Vector2(24, 11);

                            break;
                        case Orientation.BottomRight:
                            positionInTexture[0] = new Vector2(1028, 259);
                            size = new Vector2(23, 23);

                            break;
                        case Orientation.Bottom:
                            positionInTexture[0] = new Vector2(1066, 257);
                            size = new Vector2(11, 24);
                            break;
                    }

                    break;
                case (ProjectileType.CannonTowerRock):
                    speed = CannonRockSpeed;
                    size = new Vector2(23, 25);
                    positionInTexture[0] = new Vector2(1, 1);
                    Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.FIREBALL], positionOnMap);
                    hitAnimation = AnimationType.CannonHit;

                    break;
                case (ProjectileType.SubmarineMissile):
                    speed = submarineSpeed;
                    Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.FIREBALL], positionOnMap);

                    switch (orientation)
                    {
                        case Orientation.Top:
                            positionInTexture[0] = new Vector2(336, 68);
                            size = new Vector2(17, 32);

                            break;
                        case Orientation.TopRight:
                            positionInTexture[0] = new Vector2(368, 70);
                            size = new Vector2(30, 32);
                            break;
                        case Orientation.Right:
                            positionInTexture[0] = new Vector2(408, 78);
                            size = new Vector2(32, 19);

                            break;
                        case Orientation.BottomRight:
                            positionInTexture[0] = new Vector2(448, 69);
                            size = new Vector2(32, 28);

                            break;
                        case Orientation.Bottom:
                            positionInTexture[0] = new Vector2(496, 67);
                            size = new Vector2(16, 32);
                            break;
                    }
                    break;
                case (ProjectileType.BattleShipRock):
                    speed = CannonRockSpeed;
                    size = new Vector2(16, 16);
                    positionInTexture = new Vector2[4];
                    positionInTexture[0] = new Vector2(71, 14);
                    positionInTexture[1] = new Vector2(96, 14);
                    positionInTexture[2] = new Vector2(119, 14);
                    positionInTexture[3] = new Vector2(138, 14);
                    Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.FIREBALL], positionOnMap);
                    hitAnimation = AnimationType.CannonHit;

                    break;
                case (ProjectileType.GriffonHammer):
                    speed = gryffonProjectileSpeed;
                    interval = 100;

                    size = new Vector2(32, 31);
                    positionInTexture = new Vector2[3];
                    positionInTexture[0] = new Vector2(820, 10);
                    positionInTexture[1] = new Vector2(857, 11);
                    positionInTexture[2] = new Vector2(904, 9);
                    hitAnimation = AnimationType.FireBallHit;

                    break;
                case (ProjectileType.DragonBall):
                    speed = gryffonProjectileSpeed;
                    size = new Vector2(33, 46);
                    positionInTexture[0] = new Vector2(809, 56);
                    hitAnimation = AnimationType.FireBallHit;

                    break;

            }

            move = Helper_Direction.MoveTowards(trueposition, new Vector2(targetPosition.X * 32 + targetSpriteSize.X / 2, targetPosition.Y * 32 + targetSpriteSize.Y / 2), speed);


        }

        public void Draw(SpriteBatch spbatch)
        {
            if (flip)
            {
                spbatch.Draw(Game1.projectileTexture, new Vector2(this.trueposition.X, this.trueposition.Y), new Rectangle((int)positionInTexture[currenttexture].X, (int)positionInTexture[currenttexture].Y, (int)size.X, (int)size.Y),
                    Color.Wheat, 0, new Vector2(size.X, size.Y), 1, SpriteEffects.FlipHorizontally, 0);
            }
            else
            {
                spbatch.Draw(Game1.projectileTexture, new Vector2(this.trueposition.X, this.trueposition.Y), new Rectangle((int)positionInTexture[currenttexture].X, (int)positionInTexture[currenttexture].Y, (int)size.X, (int)size.Y),
                    Color.Wheat);
            }
        }

        public bool Update(GameTime gameTime) // returns true if it has reached destination + plays the sound
        {
            //if (!Map.Inbounds((int) trueposition.X / 32, (int) trueposition.Y / 32))
              //  return true;
            timer += gameTime.ElapsedGameTime.Milliseconds;



            if (interval > 0)
            { 
                if (ellapsedTime < interval)
                    ellapsedTime += gameTime.ElapsedGameTime.Milliseconds;
                else
                {
                    ellapsedTime = 0;
                    currenttexture = (currenttexture + 1) % positionInTexture.Count();
                }
            }
            targetPosition.X *= 32;
            targetPosition.Y *= 32;

            if (trueposition.X <= targetPosition.X && trueposition.X + move.X >= targetPosition.X)
            {
                trueposition.X = targetPosition.X;
                move.X = 0;
            }
            else if (trueposition.X >= targetPosition.X && trueposition.X + move.X <= targetPosition.X)
            {
                trueposition.X = targetPosition.X;
                move.X = 0;
            }

            if (trueposition.Y >= targetPosition.Y && trueposition.Y + move.Y <= targetPosition.Y)
            {
                trueposition.Y = targetPosition.Y;
                move.Y = 0;
            }
            else if (trueposition.Y <= targetPosition.Y && trueposition.Y + move.Y >= targetPosition.Y)
            {
                trueposition.Y = targetPosition.Y;
                move.Y = 0;
            }

            if (trueposition.X != targetPosition.X)
                trueposition.X += move.X;
            if (trueposition.Y != targetPosition.Y)
                trueposition.Y += move.Y;
            targetPosition /= 32;
            positionOnMap = trueposition / 32;


            // these are ugly workarounds
                if ((timer > 200 && (move.X ==0 || move.Y == 0)) || timer > 5000 || Vector2.Distance(positionOnMap, targetPosition) <= Math.Abs(move.X / 32) + Math.Abs(move.Y / 32))
                {
                    switch (type)
                    {
                        case (ProjectileType.Arrow):
                            Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.BOWHIT], positionOnMap);
                            break;
                        case (ProjectileType.Axe):
                            Game1.soundEngine.PlaySound(Game1.soundEngine.triviaSounds[(int)TriviaSE.AxeMissile1], positionOnMap);
                            break;
                        case (ProjectileType.Ballista):
                            Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.EXPLODE], positionOnMap);
                            hitUnits(70);
                            break;
                        case (ProjectileType.RockType):
                            Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.EXPLODE], positionOnMap);
                            hitUnits(70);
                            break;
                        case (ProjectileType.Spin):
                            break;
                        case (ProjectileType.Thunder):
                            break;
                        case (ProjectileType.CannonTowerRock):
                            Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.FIREHIT], positionOnMap);
                            hitUnits(40);
                            break;
                        case (ProjectileType.SubmarineMissile):
                            break;
                        case (ProjectileType.BattleShipRock):
                            Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.EXPLODE], positionOnMap);
                            hitUnits(60);

                            break;
                        case (ProjectileType.GriffonHammer):
                            hitUnits(20);

                            break;
                        case (ProjectileType.DragonBall):
                            hitUnits(20);

                            break;

                    }

                    return true;

            }

            return false;

        }


        public void hitUnits(int damage)
        {
            Utils.Utils.dealSplashDamage(targetPosition, 24, damage);

            if (hitAnimation != AnimationType.None)
                Animation.AddAnimation(hitAnimation, trueposition, true);

        }
    }





    public static class Helper_Direction
    {
        public static double FaceObject(Vector2 position, Vector2 target)
        {
            return (Math.Atan2(position.Y - target.Y, position.X - target.X) * (180 / Math.PI));
        }

        public static Vector2 MoveTowards(Vector2 position, Vector2 target, float speed)
        {
            double direction = (float)(Math.Atan2(target.Y - position.Y, target.X - position.X) * 180 / Math.PI);

            Vector2 move = new Vector2(0, 0);

            move.X = (float)Math.Cos(direction * Math.PI / 180) * speed;
            move.Y = (float)Math.Sin(direction * Math.PI / 180) * speed;

            return move;
        }
    }
}

