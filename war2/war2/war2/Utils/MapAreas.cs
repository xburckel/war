﻿using EpPathFinding;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.Utils
{
    /*
     * Represents regions of same number that are related one to another by walking, flying or sailing
     * Useful to gain a lot of computing power on pathfinding, first check if reachable
     */

    public class MapAreas
    {
        private Stack<int[]> queue;
        public int[,] stGridsWater;
        public int[,] stGridsGround;
        public int[,] stGridsAir;
        public int[,] array;
        private bool[,] visited;
        private bool[,] traversable;
        int increment;

        public MapAreas(bool[,] groundMap, bool[,] WaterMap, bool[,] AirMap)
        {
            increment = 2;
            stGridsAir = stGridsGround = stGridsWater = new int[Map.mapsize,Map.mapsize];
            for (int i = 0; i < Map.mapsize ; i++)
                for (int j = 0; j < Map.mapsize ; j++)
                {
                    stGridsWater[i,j] = WaterMap[i,j] ? 1 : 0;
                    stGridsGround[i,j] = groundMap[i,j] ? 1 : 0;
                    stGridsAir[i,j] = AirMap[i,j] ? 1 : 0;                            
                }

            stGridsWater = generateGrid(Game1.map.waterTraversableMapTiles, null);
            stGridsGround = generateGrid(Game1.map.groundTraversableMapTiles, null);
            stGridsAir = generateGrid(Game1.map.flyingTraversableMapTiles, null);

        }

        private int[,] genArea(int i, int j)
        {
            queue = new Stack<int[]>();
             int[] tmp = new int[2];
             tmp[0] = i;
             tmp[1] = j;
             queue.Push(tmp);

             while (queue.Count > 0)
             {              
                 int[] current = queue.Pop();
                 if (Map.Inbounds(current[0], current[1]) && !visited[current[0],current[1]])
                 {
                     visited[current[0],current[1]] = true;
                     if (traversable[current[0],current[1]])
                     {
                         array[current[0],current[1]] = increment;
                     for (int temp = -1; temp <= 1; temp++)
                         for (int temp2 = -1; temp2 <= 1; temp2++)
                         {
                             if ((temp != 0 || temp2 != 0) && Map.Inbounds(current[0] + temp, current[1] + temp2))
                             {
                                 int[] newArr = new int[2];

                                 newArr[0] = current[0] + temp;
                                 newArr[1] = current[1] + temp2;
                                 queue.Push(newArr);
                             }
                         }
                     }

                 }
             }

                 /*
            if (!Map.Inbounds(i, j))
                return array;
            if (visited[i, j])
                return array;
            visited[i, j] = true;
            array[i, j] = increment;
            if (traversable[i,j])
            {
               array = genArea(i + 1, j);//ko
              array = genArea(i - 1, j); // ok
               array = genArea(i + 1, j +1); // ko
              array = genArea(i - 1, j + 1);// ok 
                array = genArea(i, j + 1);//ko
                array = genArea(i + 1, j - 1); // ok
               array = genArea(i - 1, j - 1); // ok
                array = genArea(i, j - 1); // ok
                
            }
        */
            return array;
        }
        
        public void SetPosReachableGround(int x, int y)
        {
            if (!Map.Inbounds(x, y))
                return;

            HashSet<int> indexes = new HashSet<int>();
            for (int i = -1; i <= 1 ; i++)
                for (int j = -1 ; j <= 1; j++)
                {
                    if (Map.Inbounds(x+i, y+j))
                    {
                        if (!(i == 0 && j == 0) && stGridsGround[x + i, y + j] != 0) // if not unreachable, fill all regions conniventes with same number
                           indexes.Add(stGridsGround[x+i, y+j]);
                    }

                }
            if ((indexes.Count > 0))
                stGridsGround[x, y] = indexes.First();

            indexes.RemoveWhere(v => v == 0);
            // merge all numbers now related together
            if (indexes.Count > 1)
            {
                int toreplace = indexes.First();
                for (int i = 0; i < Map.mapsize; i++)
                    for (int j = 0; j < Map.mapsize; j++)
                    {
                        if (indexes.Contains(stGridsGround[i, j]))
                            stGridsGround[i, j] = toreplace;

                    }


            } // else create new alone index (should never happen ?)
            else if (indexes.Count == 0)
            {
                increment++;
                stGridsGround[x, y] = increment;
            }


        }
        /**
         * Checks if the arrival is in the same area by using counters / numbers
         * and a map initialized and updated otherwise
         * 
         * */
        public bool checkPosReachable(Vector2 startpos, Vector2 endPos, bool ground = true, bool air = false, bool water = false)
         {
             int startx = (int) Math.Round(startpos.X);
             int endx = (int) Math.Round(endPos.X);
             int starty = (int) Math.Round(startpos.Y);
             int endy = (int) Math.Round(endPos.Y);
             if (!Map.Inbounds(startx, endx) || !Map.Inbounds(endx, endy))
                 return false;
             increment++;

            int[,] grid;


            if (ground)
            {
                grid = stGridsGround;
            }
            else if (water)
            {
                grid = stGridsWater;
            }
            else
            {
                grid = stGridsAir;
            }
            
            if (grid[endx, endy] == 1)
                return false; // should be set to index generated by startpos


            if (grid[endx, endy] == 0) // if node is impossible we can let pathfinding find out (think harvesters going to wood etc)
                return true;

            if (grid[startx, starty] == grid[endx, endy])
                return true;

            return false;

         }

       private int[,] generateGrid(bool[,] map, int[,] aarray, int valx = 0, int valy = 0)
        {
            visited = new bool[Map.mapsize,Map.mapsize];

            array = aarray;
            bool create = false;
            if (array == null)
            {
                create = true;
                array = new int[Map.mapsize, Map.mapsize];
            }

            for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                {
                    visited[i, j] = false;
                    if (create)
                        array[i, j] = map[i, j] ? 1 : 0;
                }

            traversable = map;

            bool isFullyParsed = false;

           while (!isFullyParsed)
           {
             isFullyParsed = true;
             for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                {
                  if (array[i, j] == 1)
                  {
                      isFullyParsed = false;
                      array = genArea(i, j);
                      increment++;
                  }
                }

             
           }
        // 1 1 1 1 1 1 1 1 1 1 1 0 2 2 2 2 2 2 2 
        // 1 1 1 1 1 0 1 1 1 1 1 0 2 2 2 2 2 2 2
        // 1 1 1 1 1 0 1 1 1 1 1 0 0 0 0 0 0 0 0
                return array;
        }

       public void Update()
       {
           /*
           queue = new Stack<int[]>();
           array = null;
           increment = 2;
           stGridsWater = generateGrid(Game1.map.waterTraversableMapTiles, null);
           stGridsGround = generateGrid(Game1.map.groundTraversableMapTiles, null);
           stGridsAir = generateGrid(Game1.map.flyingTraversableMapTiles, null);
            * */

       }


    }
}
