﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.UnitsRelated
{
     [Serializable]
    public class Ameliorations
    {
        public enum buffs { Damage, Armor, PaladinResearch }
        private Dictionary<UnitType, int> damageBuffs;
        private Dictionary<UnitType, int> armorBuffs;
        public bool hasPaladin;

        public Ameliorations()
        {
            damageBuffs = new Dictionary<UnitType, int>();
            armorBuffs = new Dictionary<UnitType, int>();
            hasPaladin = false;
        }


        public int getArmorAmelioration(UnitType t)
        {
            if (armorBuffs.ContainsKey(t))
                return armorBuffs[t];
            else
                return 0;
        }

        public int getDamageAmelioration(UnitType t)
        {
            if (damageBuffs.ContainsKey(t))
                return damageBuffs[t];
            else
                return 0;
        }

        public void addAmelioration(buffs type, UnitType t, int increase)
        {
            Dictionary<UnitType, int> tmp = new Dictionary<UnitType, int>();

            if (type == buffs.Armor)
            {
                tmp = armorBuffs;
            }
            else if (type == buffs.Damage)
            {
                tmp = damageBuffs;
            }
            if (type == buffs.PaladinResearch)
                hasPaladin = true;
            else
            {
                if (tmp.ContainsKey(t))
                    tmp[t] += increase;
                else
                    tmp.Add(t, increase);
            }
        }
    }
}
