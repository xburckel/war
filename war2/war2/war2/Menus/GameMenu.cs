﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace war2
{

    [Serializable]
    public class GameMenu
    {
        Texture2D menuTexture;
        SpriteBatch spriteBatch;
        Vector2 savebuttonpos; // 106x 29 button dimension
        Vector2 loadbuttonpos;
        public bool inSaveMenu;
        public bool inLoadMenu;
        Texture2D SaveMenuTexture;
        MouseState prevMouseState;
        public int selectedFileIndex = 0;
        KeyboardState prevKstate;
        public int fileCount = 0;
        Texture2D menuArrows;
        Texture2D validate;
        Texture2D cancel;
        Texture2D Soundtexture;
        public bool insoundMenu;
        int sound;
        int tempsound;
        int soundeffect;
        int tempsoundeffect;
        int tempZoom;
        int zoom;

        public GameMenu(ContentManager content, SpriteBatch spriteBatch)
        {
            insoundMenu = false;
            Soundtexture = content.Load<Texture2D>("soundMenu");
            menuTexture = content.Load<Texture2D>("Menu-Game");

            this.spriteBatch = spriteBatch;
            savebuttonpos = new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 130 + 16, 250 + 40);
            loadbuttonpos = new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 130 + 134, 250 + 40);
            inSaveMenu = false;
            inLoadMenu = false;
            prevMouseState = Mouse.GetState();
            SaveMenuTexture = content.Load<Texture2D>("loadMenu");

            prevKstate = Keyboard.GetState();
            menuArrows = content.Load<Texture2D>("menuarrow");
            validate = content.Load<Texture2D>("button_ok");
            cancel = content.Load<Texture2D>("button_cancel");
            sound = 100;
            tempsound = sound;
            tempsoundeffect = 100;
            soundeffect = 100;
            tempZoom = 100;
            zoom = 100;
        }



        public void Draw()
        {
            spriteBatch.Begin();

            spriteBatch.Draw(menuTexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 130, 250), new Rectangle(0, 0, 256, 288), Color.White);

            if (inSaveMenu)
                DrawSaveBox();
            else if (inLoadMenu)
                DrawLoadBox();
            else if (insoundMenu)
                DrawSoundMenu();

            spriteBatch.End();
        }

        private void DrawSoundMenu()
        {
            spriteBatch.Draw(Soundtexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130, Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f), new Rectangle(0, 0, 203, 230), Color.White);
            spriteBatch.Draw(Soundtexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 171 + tempsound + 15, Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 33), new Rectangle(214, 9, 13, 13), Color.White);
            spriteBatch.Draw(Soundtexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 171 + tempsoundeffect + 15, Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 72), new Rectangle(214, 9, 13, 13), Color.White);
            spriteBatch.Draw(Soundtexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 171 + tempZoom / 2 + 15, Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 96), new Rectangle(214, 9, 13, 13), Color.White);
            // adds a button with onclick set graphicsDevice full screen to on or off
            Game1.statics.menuItem.DrawMenu(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 142, Game1.graphics.GraphicsDevice.Viewport.Height / 4 + 170, "Full screen: " + (Game1.graphics.IsFullScreen ? "On" : "Off"),
                new Action(() => { Game1.graphics.IsFullScreen = !Game1.graphics.IsFullScreen; Game1.graphics.ApplyChanges(); }),
                spriteBatch, false);
            Game1.statics.menuItem.DrawMenu(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 142, Game1.graphics.GraphicsDevice.Viewport.Height / 4 + 140, "Fog of war: " + (Game1.statics.fogOfWarEnabled ? "On" : "Off"),
            new Action(() => { Game1.statics.fogOfWarEnabled = !Game1.statics.fogOfWarEnabled; }),
            spriteBatch, false);

        }


        public void DrawLoadBox(string directoryPath = "Saves", string extension = "*.wsav")
        {
            spriteBatch.Draw(SaveMenuTexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 200, 250), new Rectangle(0, 0, 256, 256), Color.White);
            SpriteFont font = TopPanel.font;
            int i = 0;
            try
            {
                fileCount = Directory.GetFiles(directoryPath, extension).Count();
                foreach (string name in Directory.GetFiles(directoryPath, extension))
                {
                    String namee = name.Replace(extension, "");
                    namee = namee.Replace(directoryPath + "\\", "");
                    if (fileCount <= 15)
                    {
                        if (selectedFileIndex == i)
                            spriteBatch.DrawString(font, namee, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 250 + i * 15), Color.Gold);
                        else
                            spriteBatch.DrawString(font, namee, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 250 + i * 15), Color.White);

                        i++;

                        if (i >= 15)
                            break;
                    }
                    else
                    {
                        if (i >= selectedFileIndex && (i < selectedFileIndex + 15))
                        {

                            if (selectedFileIndex == i)
                                spriteBatch.DrawString(font, namee, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 250), Color.Gold);
                            else
                                spriteBatch.DrawString(font, namee, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 250 + (i - selectedFileIndex) * 15), Color.White);

                        }



                        i++;
                    }
                }

                spriteBatch.Draw(menuArrows, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 420, 250), new Rectangle(0, 0, 40, 37), Color.White);
                spriteBatch.Draw(menuArrows, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 420, 465), new Rectangle(0, 40, 40, 40), Color.White);
                spriteBatch.Draw(validate, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 220, 485), new Rectangle(0, 0, 60, 60), Color.White);
                spriteBatch.Draw(cancel, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 285, 485), new Rectangle(0, 0, 60, 60), Color.White);

                if (directoryPath.Equals("Saves"))
                    spriteBatch.DrawString(TopPanel.font, "Choose game to LOAD", new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 235), Color.Gold);
                else
                    spriteBatch.DrawString(TopPanel.font, "Choose game to LOAD", new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 235), Color.Gold);
            }
            catch (DirectoryNotFoundException)
            {
                System.IO.Directory.CreateDirectory("Saves");
            }
        }

        private void DrawSaveBox()
        {

            spriteBatch.Draw(SaveMenuTexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 200, 250), new Rectangle(0, 0, 256, 256), Color.White);
            SpriteFont font = TopPanel.font;
            int i = 0;
            try
            {
                fileCount = Directory.GetFiles("Saves", "*.wsav").Count();
                foreach (string name in Directory.GetFiles("Saves", "*.wsav"))
                {
                    String namee = name.Replace(".wsav", "");
                    namee = namee.Replace("Saves\\", "");
                    if (fileCount <= 15)
                    {
                        if (selectedFileIndex == i)
                            spriteBatch.DrawString(font, namee, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 250 + i * 15), Color.Gold);
                        else
                            spriteBatch.DrawString(font, namee, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 250 + i * 15), Color.White);

                        i++;

                        if (i >= 15)
                            break;
                    }
                    else
                    {
                        if (i >= selectedFileIndex && (i < selectedFileIndex + 15))
                        {

                            if (selectedFileIndex == i)
                                spriteBatch.DrawString(font, namee, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 250), Color.Gold);
                            else
                                spriteBatch.DrawString(font, namee, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 250 + (i - selectedFileIndex) * 15), Color.White);

                        }



                        i++;
                    }
                }

                spriteBatch.Draw(menuArrows, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 420, 250), new Rectangle(0, 0, 40, 37), Color.White);
                spriteBatch.Draw(menuArrows, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 420, 465), new Rectangle(0, 40, 40, 40), Color.White);
                spriteBatch.Draw(validate, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 220, 485), new Rectangle(0, 0, 60, 60), Color.White);
                spriteBatch.Draw(cancel, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 285, 485), new Rectangle(0, 0, 60, 60), Color.White);

                spriteBatch.DrawString(TopPanel.font, "Choose game to SAVE", new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 235), Color.Gold);
            }
            catch (DirectoryNotFoundException)
            {
                System.IO.Directory.CreateDirectory("Saves");
                //spriteBatch.DrawString(TopPanel.font, "Please create the 'Saves' Directory", new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 235), Color.Gold);
            }
        }

        public void MenuPressed()
        {
            Game1.isInMenu = !Game1.isInMenu;
            this.inLoadMenu = false;
            this.inSaveMenu = false;
            this.insoundMenu = false;
        }

        public void Update()
        {
            MouseState ms = Mouse.GetState();
            KeyboardState kstate = Keyboard.GetState();

            bool prevloadmenu = inLoadMenu;
            bool prevsavemenu = inSaveMenu;
            bool prefsoundmenu = insoundMenu;
            bool previngamemenu = Game1.isInMainMenu;


            if (inLoadMenu)
            {
                #region LoadMenu

                if (kstate.IsKeyDown(Keys.Down) && prevKstate.IsKeyUp(Keys.Down))
                    selectedFileIndex += 1;
                if (kstate.IsKeyDown(Keys.Up) && prevKstate.IsKeyUp(Keys.Up))
                    selectedFileIndex -= 1;

                if (ms.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                {
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 300 && ms.Y > 250 && ms.Y < 250 + 15 * 15)
                    {
                        selectedFileIndex = selectedFileIndex + (int)Math.Round((ms.Y - 250) / (double)15);

                    }
                    else if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 420 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 420 + 40 && ms.Y > 250 && ms.Y < 250 + 37) // up arrow
                        selectedFileIndex = 0;
                    else if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 420 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 420 + 40 && ms.Y > 465 && ms.Y < 505) // down arrow
                        selectedFileIndex = fileCount - 1;
                }

                if (selectedFileIndex < 0)
                    selectedFileIndex = 0;
                if (selectedFileIndex > fileCount - 1)
                    selectedFileIndex = fileCount - 1;
                if (ms.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                {
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 220 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 220 + 60 && ms.Y > 485 && ms.Y < 485 + 60) // load
                    {
                        try
                        {
                            Game1.loadGame(Directory.GetFiles("Saves", "*.wsav")[selectedFileIndex]);
                            selectedFileIndex = 0;
                        }
                        catch (Exception)
                        {
                            spriteBatch.Begin();
                            spriteBatch.DrawString(Game1.statics.font, "No saved games yet", new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210, 250), Color.Gold);
                            spriteBatch.End();
                        }

                    }
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 285 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 285 + 60 && ms.Y > 485 && ms.Y < 485 + 60) // cancel
                    {
                        selectedFileIndex = 0;
                        inLoadMenu = false;
                    }
                }
                if (kstate.IsKeyDown(Keys.Escape) && prevKstate.IsKeyUp(Keys.Escape))
                {
                    selectedFileIndex = 0;
                    inLoadMenu = false;
                }
                #endregion
            }
            else if (inSaveMenu)
            {

                #region SaveMenu
                if (kstate.IsKeyDown(Keys.Down) && prevKstate.IsKeyUp(Keys.Down))
                    selectedFileIndex += 1;
                if (kstate.IsKeyDown(Keys.Up) && prevKstate.IsKeyUp(Keys.Up))
                    selectedFileIndex -= 1;

                if (ms.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                {
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 210 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 300 && ms.Y > 250 && ms.Y < 250 + 15 * 15)
                    {
                        selectedFileIndex = selectedFileIndex + (int)Math.Round((ms.Y - 250) / (double)15);

                    }
                    else if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 420 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 420 + 40 && ms.Y > 250 && ms.Y < 250 + 37) // up arrow
                        selectedFileIndex = 0;
                    else if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 420 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 420 + 40 && ms.Y > 465 && ms.Y < 505) // down arrow
                        selectedFileIndex = fileCount - 1;
                }

                if (selectedFileIndex < 0)
                    selectedFileIndex = 0;
                if (selectedFileIndex > fileCount - 1)
                    selectedFileIndex = fileCount - 1;
                if (ms.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                {
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 220 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 220 + 60 && ms.Y > 485 && ms.Y < 485 + 60) // save
                    {
                        try
                        {
                            Game1.saveGame(Directory.GetFiles("Saves", "*.wsav")[selectedFileIndex]);
                            selectedFileIndex = 0;
                            inSaveMenu = false;
                        }
                        catch (IndexOutOfRangeException)
                        {
                            System.IO.File.Create("Saves\\Save1.wsav");
                            System.IO.File.Create("Saves\\Save2.wsav");
                            System.IO.File.Create("Saves\\Save3.wsav");
                            System.IO.File.Create("Saves\\Save4.wsav");
                            System.IO.File.Create("Saves\\Save5.wsav");
                            System.IO.File.Create("Saves\\Save6.wsav");
                            System.IO.File.Create("Saves\\Save7.wsav");
                            System.IO.File.Create("Saves\\Save8.wsav");
                            System.IO.File.Create("Saves\\Save9.wsav");
                            selectedFileIndex = 0;
                            inSaveMenu = true;
                        }

                    }
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 285 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 285 + 60 && ms.Y > 485 && ms.Y < 485 + 60) // cancel
                    {
                        selectedFileIndex = 0;
                        inSaveMenu = false;
                    }
                }
                if (kstate.IsKeyDown(Keys.Escape) && prevKstate.IsKeyUp(Keys.Escape))
                {
                    selectedFileIndex = 0;
                    inSaveMenu = false;
                }
                #endregion
            }

            else if (insoundMenu)
            {
                #region SoundMenu
                if (kstate.IsKeyDown(Keys.Escape) && prevKstate.IsKeyUp(Keys.Escape))
                {
                    insoundMenu = false;
                }

                if (ms.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                {
                    // click on OK
                    if (kstate.IsKeyDown(Keys.O) || (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 111 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 214 && ms.Y > Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 205
                       && ms.Y < Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 227)) // ok
                    {
                        sound = tempsound;
                        soundeffect = tempsoundeffect;

                        Game1.soundEngine.setVolume(sound);
                        Game1.soundEngine.setSEVolume(soundeffect);
                        zoom = tempZoom;
                        Game1.cam.Zoom = (Game1.cam.INIT_ZOOM_VALUE * zoom) / 100;

                        insoundMenu = false;
                    }
                    if (kstate.IsKeyDown(Keys.C) || (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 10 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 93 && ms.Y > Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 205
                       && ms.Y < Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 227)) // cancel
                    {
                        tempsound = sound;
                        insoundMenu = false;
                        tempsoundeffect = soundeffect;


                    }
                    // left arrow for music
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 26 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 39
                        && ms.Y > Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 32 && ms.Y < Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 46)
                    {
                        tempsound -= 5;
                        if (tempsound < 0)
                            tempsound = 0;
                        if (tempsound > 100)
                            tempsound = 100;
                    }

                    // right arrow for music
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 180 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 195
                        && ms.Y > Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 32 && ms.Y < Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 46)
                    {
                        tempsound += 5;
                        if (tempsound < 0)
                            tempsound = 0;
                        if (tempsound > 100)
                            tempsound = 100;
                    }

                    // in the middle for music
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 42 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 178
    && ms.Y > Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 32 && ms.Y < Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 46)
                    {
                        tempsound = ms.X - (Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 42);
                        if (tempsound < 0)
                            tempsound = 0;
                        if (tempsound > 100)
                            tempsound = 100;
                    }

                    // left arrow for sound

                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 26 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 39
    && ms.Y > Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 72 && ms.Y < Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 86)
                    {
                        tempsoundeffect -= 5;
                        if (tempsoundeffect < 0)
                            tempsoundeffect = 0;
                        if (tempsoundeffect > 100)
                            tempsoundeffect = 100;
                    }

                    // right arrow for sound
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 180 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 195
                        && ms.Y > Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 72 && ms.Y < Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 86)
                    {
                        tempsoundeffect += 5;
                        if (tempsoundeffect < 0)
                            tempsoundeffect = 0;
                        if (tempsoundeffect > 100)
                            tempsoundeffect = 100;
                    }

                    // in the middle for sound
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 42 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 178
    && ms.Y > Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 72 && ms.Y < Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 86)
                    {
                        tempsoundeffect = ms.X - (Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 42);
                        if (tempsoundeffect < 0)
                            tempsoundeffect = 0;
                        if (tempsoundeffect > 100)
                            tempsoundeffect = 100;
                    }

                    // left for zoom
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 26 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 39
&& ms.Y > Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 96 && ms.Y < Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 110)
                    {
                        tempZoom -= 10;
                        if (tempZoom <= 5)
                            tempZoom = 5;
                        if (tempZoom > 200)
                            tempZoom = 200;
                    }

                    // right arrow for zoom
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 180 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 195
                        && ms.Y > Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 96 && ms.Y < Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 110)
                    {
                        tempZoom += 10;
                        if (tempZoom <= 5)
                            tempZoom = 5;
                        if (tempZoom > 200)
                            tempZoom = 200;
                    }

                    // in the middle for zoom
                    if (ms.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 42 && ms.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 178
    && ms.Y > Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 96 && ms.Y < Game1.graphics.GraphicsDevice.Viewport.Height * 0.25f + 110)
                    {
                        tempZoom = (ms.X - (Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 130 + 42)) * 2;
                        if (tempZoom <= 5)
                            tempZoom = 5;
                        if (tempZoom > 200)
                            tempZoom = 200;
                    }



                }

                #endregion
            }

                // chosing menu
            #region menu selection
            else if (!inLoadMenu && !inSaveMenu && !insoundMenu)
            {

                if (ms.X > savebuttonpos.X && ms.X < loadbuttonpos.X + 106 && ms.Y > savebuttonpos.Y + 200 && ms.Y < savebuttonpos.Y + 229) // back to game pressed
                {
                    if (ms.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                    {
                        Game1.isInMenu = false;
                    }

                }
                else if (ms.X > savebuttonpos.X && ms.X < loadbuttonpos.X + 106 && ms.Y > savebuttonpos.Y + 200 - (248 - 177) && ms.Y < savebuttonpos.Y + 229 - (248 - 177) + 5) // back to menu
                {
                    if (ms.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                    {
                        Game1.isInMenu = false;
                        Game1.soundEngine.StopPlaying();
                        Game1.isInMainMenu = true;
                    }

                }
                else if (ms.X > savebuttonpos.X && ms.X < savebuttonpos.X + 106 && ms.Y > savebuttonpos.Y && ms.Y < savebuttonpos.Y + 29)
                {
                    if (ms.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                    {
                        inSaveMenu = true;
                    }

                }
                else if (ms.X > loadbuttonpos.X && ms.X < loadbuttonpos.X + 106 && ms.Y > loadbuttonpos.Y && ms.Y < loadbuttonpos.Y + 29)
                {
                    if (ms.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                    {
                        inLoadMenu = true;
                    }
                }
                else if (kstate.IsKeyDown(Keys.F5) && prevKstate.IsKeyUp(Keys.F5))
                {
                    insoundMenu = true;
                }
                else if (ms.X > savebuttonpos.X && ms.X < savebuttonpos.X + 226
                    && ms.Y > savebuttonpos.Y + 3 && ms.Y < savebuttonpos.Y + 60)
                {
                    if (ms.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                    {
                        insoundMenu = true;
                    }
                }
                else if (kstate.IsKeyDown(Keys.Escape) && prevKstate.IsKeyUp(Keys.Escape))
                {
                    Game1.isInMenu = false;
                }

            }
            #endregion

            prevMouseState = ms;
            prevKstate = kstate;

            if (prevloadmenu != inLoadMenu ||
                prevsavemenu != inSaveMenu ||
                prefsoundmenu != insoundMenu ||
                previngamemenu != Game1.isInMainMenu)
            {
                Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.BigButtonClick]);
            }
        }

    }
}
