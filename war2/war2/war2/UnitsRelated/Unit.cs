﻿using EpPathFinding;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.Astar;
using war2.PathFindingRelated;
using war2.SpriteSheetCode;
using war2.UnitsRelated;

namespace war2
{

    public enum UnitType : int
    {
        None = -1, Paysan = 0, Fantassin = 1, Archer = 2, Catapulte = 3, Chevalier = 4, Paladin = 5, Nains = 6,
        Mage = 7, Petrolier = 8, Destroyer = 9, Battleship = 10, Griffon = 11, Sousmarin = 12, Transport = 13, MachineVolante = 14,
        Farm = 15, Tower, BallistaTower, CannonTower, MilitaryBase, LumberMill, Stable, Forge, NavalChantier, NavalPlatform, Foundry, Raffinery, Church, MageTower, GryffinTower, GoblinWorkBench, TownHall, StrongHold, Fortress
        // unittype >= 15 = building

    };




    [Serializable]
    public class Unit
    {

        //int tries; // after a number of tries, try to unstuck unit
        public bool isAIA;
        public int disabled;
        public int hp;
        public int mana;
        public float speed;
        public int damage;
        public bool flying;
        public bool sailing;
        public int goldCost;
        public int woodCost;
        public int oilCost;
        public int range;
        public int sight;
        public int armor;
        public Vector2 position;
        public UnitType type;
        public int buildtime;
        public int owner;
        public Vector2 prevPos;
        public Vector2 direction;
        public Vector2 destination;
        public List<GridPos> path;
        public int penetratingDamage;
        public readonly TimeSpan intervalBetweenAttack = TimeSpan.FromMilliseconds(2000);
        public int attackCooldown;
        public TimeSpan lastTimeAttack;
        public Vector2 spriteSize;
        public static int littlebuildingindex = (int)UnitType.Farm;
        public static int mediumbuildingindex = (int)UnitType.MilitaryBase;
        public static int largebuildingindex = (int)UnitType.TownHall;
        public Vector2 spritePos;
        public bool isABuilding;
        public bool CanAttackGround;
        public bool CanAttackAir;
        public bool CanAttackWater;
        public int stopMove;
        public int timer; // to reset agro
        public Int64 index;
        public bool discovered;
        [NonSerialized]
        public UnitAnimation currentAnimation;
        public Unit target;


        public Unit()
        {
        }

        public Unit(int hp, int mana, float speed,
         int damage,
         bool flying,
         bool sailing,
         int goldCost,
         int woodCost,
         int oilCost,
         int range,
         int sight,
         Vector2 position,
        int buildtime,
            UnitType type,
        int armor, int owner)
        {
            isABuilding = false;
            //tries = 0;
            this.damage = damage;
            this.flying = flying;
            this.sailing = sailing;
            this.goldCost = goldCost;
            this.woodCost = woodCost;
            this.hp = hp;
            this.mana = mana;
            this.speed = speed / 2;
            this.oilCost = oilCost;
            this.range = range;
            this.sight = sight;
            this.armor = armor;
            this.position = position;
            this.buildtime = buildtime;
            this.owner = owner;
            this.type = type;
            this.isAIA = false;
            direction = new Vector2();
            direction.X = 0;
            direction.Y = 0;
            destination.X = (float)position.X;
            destination.Y = (float)position.Y;
            path = new List<GridPos>();
            target = null;
            lastTimeAttack = new TimeSpan();
            prevPos = new Vector2();
            discovered = false;
            if ((int) type < littlebuildingindex)
            { 
               
                spriteSize.X = UnitProperties.getUnitProperties(type, Game1.map.players[owner].isOrc)[(int) Properties.SPRITESIZEX];
                spriteSize.Y = UnitProperties.getUnitProperties(type, Game1.map.players[owner].isOrc)[(int)Properties.SPRITESIZEY];
            }
            else
            {
                spriteSize.X = 32;
                spriteSize.Y = 32;
            }
            this.penetratingDamage = 3;
            if (type == UnitType.BallistaTower)
                this.penetratingDamage = 8;

            disabled = 0;
            spritePos = new Vector2(0, 0);
            direction = new Vector2(0, 0);
            attackCooldown = 2000;
            if (type == UnitType.Catapulte)
                attackCooldown = 4000;


            CanAttackGround = true;
            CanAttackAir = true;
            CanAttackWater = true;

            if (type == UnitType.Battleship || type == UnitType.CannonTower || type == UnitType.Catapulte || type == UnitType.Chevalier || type == UnitType.Fantassin || type == UnitType.Nains ||
                type == UnitType.Paladin || type == UnitType.Paysan || type == UnitType.Sousmarin)
                CanAttackAir = false;

            if (type == UnitType.MachineVolante || type == UnitType.Transport || type == UnitType.Petrolier)
            {
                CanAttackGround = false;
                CanAttackAir = false;
                CanAttackWater = false;
            }

            if (type == UnitType.Sousmarin)
                CanAttackGround = false;


            stopMove = 0;
            timer = 0;
            index = Game1.unitindex;
            intervalBetweenAttack = TimeSpan.FromMilliseconds(attackCooldown);
            Game1.unitindex++;
                        if ((int)type < littlebuildingindex) // buildings dont have unit animations
                currentAnimation = new UnitAnimation(position, this);
        }

        // change the path
        public virtual void Move(Vector2 destination, Map map, bool AIPlaying = false, bool goNearest = false)
        {
            isAIA = AIPlaying;
            List<Vector2> buildpositions = new List<Vector2>();

            if (owner == Game1.currentPLayer || AIPlaying) // move only ones units
            {
                if (target != null && target.isABuilding) // so we can attack buildings and arent stuck because the position is on topleft
                {
                    buildpositions = Utils.Utils.getAllPositions((int)target.spriteSize.X / 32, (int)target.spriteSize.Y / 32, target.position); // let us walk on the building itself
                }
                if ((int)this.type < littlebuildingindex) // not building
                {
                    if (Map.Inbounds((int)Math.Round(destination.X), (int)Math.Round(destination.Y)))
                    {
                        this.destination = destination;
                        // go to nearest tile if it is a pack move
                        // TODO : implementation is broken currently so I comment this
                        /*if (AIPlaying)
                            destination = map.packMoves.GetAdjacentTile(destination);*/

                        if (Vector2.Distance(destination, position) >= 0.5 && this.owner == Game1.currentPLayer || goNearest) // handle click-move (human only)
                            path = CalculateNearPath(new Vector2(position.X, position.Y), destination, map);

                        else if (Vector2.Distance(destination, position) >= 0.5)
                        {
                            // for AI, run it in a separate thread for performance
                            if (isAIA)
                            {
                                path = Game1.statics.threading.SafePathFindingRequest(this, destination);

                            }
                            else
                                path = CalculatePath(new Vector2(position.X, position.Y), destination, map, buildpositions);
                        }
                        
                        if (path.Count == 0)
                        {
                            this.destination = position;
                        }
                    }
                }
            }

        }

        /**
         * Lil helper function
         */
        public bool isOrc()
        {
            if (Game1.map.players != null && Game1.map.players.Count > this.owner)
                return Game1.map.players[this.owner].isOrc;
            return false;
        }

        // use for player only
        public List<GridPos> CalculateNearPath(Vector2 position, Vector2 destination, Map map)
        {
            List<GridPos>  testPos = CalculatePath(position, destination, map);
            position = new Vector2((int)Math.Round(position.X), (int)Math.Round(position.Y));
            if (testPos.Count != 0)
                return testPos;
             Vector2 newPos = new Vector2(0, 0);

             int[,] grid;

             if (!this.flying && !this.sailing)
             {
                 grid = Game1.map.mapAreas.stGridsGround;
             }
             else if (this.sailing)
             {
                 grid = Game1.map.mapAreas.stGridsWater;
             }
             else
             {
                 grid = Game1.map.mapAreas.stGridsAir;
             }

            // possibilité d'optimiser ici mais bon... c'est ponctuel et raisonnable en calcul ; l'"algo" peut être carrement amélioré^^
                 for (int i = 0; i < Map.mapsize; i++)
                     for (int j = 0; j < Map.mapsize; j++)
                     {
                         if ((Vector2.Distance(newPos, destination) > Vector2.Distance(new Vector2(i, j), destination))
                             && grid[i,j] == grid[(int) position.X, (int) position.Y]) // if c'est plus proche && accessible
                         {


                                 newPos = new Vector2(i, j);
                         }

                     }
            this.destination = newPos;

            return CalculatePath(position, newPos, map);
        }

        /**
         * Pathfinding algorithm (yes, this part was taken from the net ; works ok though)
         * */
        public List<GridPos> CalculatePath(Vector2 position, Vector2 destination, Map map = null, List<Vector2> forcewalkable = null, List<Vector2> forceunwalkable = null)
        {
            if (map == null)
                map = Game1.map;
            position.Y = (int)Math.Round(position.Y);
            position.X = (int)Math.Round(position.X);
            destination.Y = (int)Math.Round(destination.Y);
            destination.X = (int)Math.Round(destination.X);

            if (sailing || flying)
            {
                if (forcewalkable == null)
                    forcewalkable = new List<Vector2>();
                forcewalkable.Add(new Vector2(position.X + 1, position.Y + 1));
                forcewalkable.Add(new Vector2(position.X, position.Y + 1));
                forcewalkable.Add(new Vector2(position.X + 1, position.Y));
            }

            BaseGrid searchGrid = new StaticGrid(Map.mapsize, Map.mapsize);

            if (!Game1.map.mapAreas.checkPosReachable(position, destination, (!this.flying && !this.sailing), this.flying, this.sailing)) // if this is not reachable
               return new List < GridPos >();

            for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                {
                    if (this.flying)
                    {
                        searchGrid.SetWalkableAt(i, j, map.flyingTraversableMapTiles[i, j]);
                    }
                    else if (this.sailing)
                    {
                        searchGrid.SetWalkableAt(i, j, map.waterTraversableMapTiles[i, j]);
                    }
                    else
                    {
                        searchGrid.SetWalkableAt(i, j, map.groundTraversableMapTiles[i, j]);
                    }
                }
            if (forcewalkable != null)
            {
                foreach (Vector2 vec in forcewalkable)
                    {
                        if (Map.Inbounds((int)vec.X, (int) vec.Y))
                          searchGrid.SetWalkableAt((int) vec.X, (int) vec.Y, true);
                    }

            }
            if (forceunwalkable != null)
            {
                foreach (Vector2 vec in forceunwalkable)
                {
                    if (Map.Inbounds((int)vec.X, (int)vec.Y))
                        searchGrid.SetWalkableAt((int)vec.X, (int)vec.Y, false);
                }

            }
            if (owner != Game1.currentPLayer && Game1.statics.basicBehaviourAttackIngUnits.Contains(this.type)) // if it is an attacking AI, ignore collision with player buildings and units as they will be set to be attacked
            {
                lock (Game1.map.playerZones) // thread safety need here
                {
                    try
                    {
                        foreach (Vector2 vec in Game1.map.playerZones.getPositions().ToList()) // toList necessary here, see https://stackoverflow.com/questions/604831/collection-was-modified-enumeration-operation-may-not-execute
                        {
                            if (Map.Inbounds((int)vec.X, (int)vec.Y))
                                searchGrid.SetWalkableAt((int)vec.X, (int)vec.Y, false);
                        }
                    }
                    catch (ArgumentException) // still happens sometimes.. to be looked into in more details
                    {
                        Game1.statics.annouceMessage.UpdateMessage("Lock triggered issue ");
                    }
                }
            }
       

            GridPos startPos = new GridPos((int)(position.X), (int)(position.Y));
            GridPos endPos = new GridPos((int)(destination.X), (int)(destination.Y));
            JumpPointParam jpParam = new JumpPointParam(searchGrid, startPos, endPos, true, true, true);
            List<GridPos> resultPathList = JumpPointFinder.FindPath(jpParam);

            return resultPathList;
        }

        public static void RemoveDeadUnits(Map map)
        {
            List<Unit> toremove = new List<Unit>();
            foreach (Unit u in map.units)
            {
                if (u.hp <= 0)
                {
                    if ((int)u.type >= Unit.littlebuildingindex && ((Building)u).builder != null) // kill the associated builder
                    {
                        ((Building)u).builder.position = u.position;
                        ((Building)u).builder.inbuilding = null;
                        ((Building)u).builder.disabled = 0;
                        ((Building)u).builder.timeharvesting = 0;
                        if (!((Building)u).builder.cancelledBuild)
                            ((Building)u).builder.hp = -1;
                        else
                        {
                            ((Building)u).builder.cancelledBuild = false;
                        }
                    }

                    if (u.type == UnitType.Paysan && ((Harvester)u).ShouldntGetHarmed())
                        u.hp = ((Harvester)u).lastTurnHp;
                    // network
                    DeadUnits du = new DeadUnits();
                    du.index = u.index;
                    if (u.type != UnitType.Nains) // else it could ptentially cancel dwarvesdemolish requests
                        Game1.map.players[u.owner].playerMoves.deadunits.Add(du);


                    // if building dstroyed, reset walkability
                    if ((int)(u.type) >= (Unit.largebuildingindex))
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            for (int j = 0; j < 4; j++)
                            {
                                map.groundTraversableMapTiles[(int)Math.Round(u.position.X) + i, (int)Math.Round(u.position.Y) + j] = true;
                            }
                        }
                    }
                    else if ((int)(u.type) >= Unit.mediumbuildingindex)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            for (int j = 0; j < 3; j++)
                            {
                                map.groundTraversableMapTiles[(int)Math.Round(Math.Round(u.position.X) + i), (int)Math.Round(Math.Round(u.position.Y) + j)] = true;
                            }
                        }
                    }
                    else if ((int)u.type >= Unit.littlebuildingindex)
                    {
                        for (int i = 0; i < 2; i++)
                        {
                            for (int j = 0; j < 2; j++)
                            {
                                map.groundTraversableMapTiles[(int)Math.Round(u.position.X) + i, (int)Math.Round(u.position.Y) + j] = true;
                            }
                        }

                    }
                    else
                    {
                        if (u.flying)
                            map.flyingTraversableMapTiles[(int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)] = true;
                        else if (u.sailing)
                            map.waterTraversableMapTiles[(int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)] = true;
                        else
                            map.groundTraversableMapTiles[(int)Math.Round(u.position.X), (int)Math.Round(u.position.Y)] = true;

                    }

                    foreach (Unit u2 in map.units)
                    {
                        if (u2.hp >= 0 && u2.target != null && u2.target.Equals(u)) // remove targets from unit targetting it
                        {
                            u2.target = null;
                            u2.destination = u2.position;
                        }

                        if (u2.type == UnitType.Paysan || u2.type == UnitType.Petrolier) // remove store if its a store
                        {
                            if (((Harvester)u2).store != null && ((Harvester)u2).store.index == u.index)
                                ((Harvester)u2).store = null;
                        }
                    }
                    if (u.type == UnitType.NavalPlatform)
                    {
                        foreach (Resource r in map.OilResources)
                        {
                            if (r.platForm != null && r.platForm == u)
                                r.platForm = null;
                        }

                    }
                    map.selected.Remove(u);
                    toremove.Add(u);


                    if (u.type == UnitType.Farm)
                    {
                        map.players[u.owner].food -= 4;

                    }
                    if (u.type == UnitType.Fortress || u.type == UnitType.StrongHold || u.type == UnitType.TownHall)
                    {
                        map.players[u.owner].food -= 1;
                    }
                    Game1.statics.threading.cleanRequests(u);



                }
            }
            foreach (Unit u in toremove)
            {
                DeadUnits du = new DeadUnits();
                du.index = u.index;
                Game1.map.players[Game1.currentPLayer].playerMoves.deadunits.Add(du);
                Cadavre c = new Cadavre(u);
                map.cadavres.Add(c);
                map.players[u.owner].playerAI.numberOfLostUnits++;
                map.units.Remove(u);
            }

            if (toremove.Count > 0) // update tech tree over death
            {
                foreach (Unit u in toremove)
                    map.players[u.owner].playerTechs.Update(u.owner, map.units);


            }
        }

        // Does the actual unit move/position update in the right direction
        protected void DoMovementAction(GameTime gameTime)
        {
            if (onRangeForAttack())
            {
                // orientation
                direction.X = -(this.position.X - target.position.X);
                direction.Y = -(this.position.Y - target.position.Y);
                return;
            }

            float unitsMove = this.speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            // If we are aligned on X or y Axis with next gridpos, stop moving this direction
            if (Math.Abs(position.X - path.First().x) <= unitsMove)
                this.position.X = path.First().x;
            if (Math.Abs(position.Y - path.First().y) <= unitsMove)
                this.position.Y = path.First().y;

            if (Vector2.Distance(this.position, new Vector2(this.path.First().x, this.path.First().y)) <= unitsMove)
                // if we are on next grid path then set it to be exactly that position
            {
                this.position = new Vector2(this.path.First().x, this.path.First().y);
                if (this.path.Count > 0)
                    this.path.Remove(this.path.First());
            }
            else // else move in the appropriate direction
                this.position += (direction * unitsMove);

            if (this.path.Count == 0)
                this.destination = this.position;
        }


        public virtual void Update(Map map, GameTime gameTime)
        {
            prevPos = position;
            // Try to unstuck unit / recompute path if needed
            Game1.statics.threading.CheckoutUnit(this);

            // Clear path if we reached destination
            if (position.Equals(destination) && path.Count >= 1)
                path.Clear();

            // if we didnt and are attacking and no more path, stop
            if (!position.Equals(destination) && path.Count == 0 && target == null && Game1.statics.basicBehaviourAttackIngUnits.Contains(this.type))
                destination = position;

            List<Unit> units = map.units;
            List<Unit> newUnits = new List<Unit>();
            timer += gameTime.ElapsedGameTime.Milliseconds;


            if (timer / 1000 > 0) // rset agro every now and then
            {
                Vector2 prevTPos = Game1.statics.ImpossiblePos;
                if (target != null)
                    prevTPos = target.position;

                if (target == null && owner == Game1.currentPLayer && path.Count == 0) // check target only if not moving for players
                    checkNearbyUnitsForTarget();

                if (target == null && owner != Game1.currentPLayer && (int)type < littlebuildingindex)
                {
                    checkNearbyUnitsForTarget();

                }
                else if (target != null && !onRangeForAttack() && owner != Game1.currentPLayer && (int)type < littlebuildingindex
                    && !Game1.map.players[owner].network) // reset aggro only for units with defensive behaviour, others are updated every 3s
                {
                    target = null;
                    checkNearbyUnitsForTarget();
                }

                if (type == UnitType.BallistaTower || type == UnitType.CannonTower)
                {
                    target = null;
                    checkNearbyUnitsForTarget();
                }

                if (target != null && (path.Count == 0 || !prevTPos.Equals(target.position)) && type != UnitType.BallistaTower && type != UnitType.CannonTower && !onRangeForAttack())
                    Move(target.position, Game1.map, true);
                timer = -Game1.statics.ra.Next(0, 500);
            }

            if ((int)type >= littlebuildingindex && type != UnitType.BallistaTower && type != UnitType.CannonTower)
                target = null;

            if (target != null && (type == UnitType.BallistaTower || type == UnitType.CannonTower) && onRangeForAttack())
            {
                Attack(gameTime);
            }




            if ((int)(this.type) >= (Unit.littlebuildingindex))
            // handle building
            {
                Building b = (Building)this;
                if (b.Built(gameTime) != UnitType.None) // a new unit is built
                {
                    map.newUnits.Add(map.CreateUnit(b.unitbuilding, new Vector2(b.position.X, b.position.Y), b.owner));
                    if (owner == Game1.currentPLayer)
                        Game1.soundEngine.playNewUnitSound(b.unitbuilding);
                    ((Building)this).unitbuilding = UnitType.None;

                }


            }
            else
            { // normal units

                if (this.path.Count > 0)
                {
                    DoMovementAction(gameTime);
                }
                bool isAttacking_tmp = false;
                if (onRangeForAttack())
                {
                    this.path.Clear();
                    this.destination = this.position;

                    Attack(gameTime);
                    isAttacking_tmp = true;
                    currentAnimation.getCurrentAnime().changeAction(DisplayAction.Attack);
                }


                if (type != UnitType.Paysan)
                {
                    if (path.Count != 0 && (currentAnimation.getCurrentAnime().action == DisplayAction.Stand || (currentAnimation.getCurrentAnime().action == DisplayAction.Attack && !isAttacking_tmp)))
                        currentAnimation.getCurrentAnime().changeAction(DisplayAction.Move);
                    currentAnimation.Update(gameTime, this);
                    if (this.target == null && (Vector2.Distance(this.destination, this.position) <= 0.5) && !this.flying && currentAnimation.getCurrentAnime().action != DisplayAction.Repair) // flying units still do the move animations
                        currentAnimation.getCurrentAnime().changeAction(DisplayAction.Stand);

                }
            }
        }

        /**
         * Called for instance after a new building is finished (collision for under construction handled separately)
         * */
        public void ReCalculatePath()
        {
            if (owner != Game1.currentPLayer)
                path = Game1.statics.threading.SafePathFindingRequest(this, destination);
            else // human player so real time pathfinding
                path = CalculatePath(position, destination);
        }

        internal static void loadUnitTextureMapping(Microsoft.Xna.Framework.Graphics.Texture2D[,] unitTextureAssociated, UnitTexture[] unitTextures, Player owner)
        {
            #region unit-Texture mapping
            for (int i = 0; i < Enum.GetNames(typeof(UnitType)).Length; i++)
            {
                UnitType ut = (UnitType)i;
                switch (ut)
                {
                    /* load mapping*/

                    case UnitType.Fantassin:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].FootmanTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].FootmanTexture;
                        break;
                    case UnitType.Archer:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].ArcherTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].ArcherTexture;
                        break;
                    case UnitType.Catapulte:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].BallistaTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].BallistaTexture;
                        break;
                    case UnitType.Chevalier:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].KnightTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].KnightTexture;
                        break;
                    case UnitType.Destroyer:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].DestroyerTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].DestroyerTexture;
                        break;
                    case UnitType.Griffon:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].DragonTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].DragonTexture;
                        break;
                    case UnitType.MachineVolante:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].FlyMachineTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].FlyMachineTexture;
                        break;
                    case UnitType.Mage:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].MageTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].MageTexture;
                        break;
                    case UnitType.Nains:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].DwarvesTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].DwarvesTexture;
                        break;
                    case UnitType.Paladin:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].KnightTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].KnightTexture;
                        break;
                    case UnitType.Petrolier:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].OilTankersTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].OilTankersTexture;
                        break;
                    case UnitType.Sousmarin:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].SubmarineTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].SubmarineTexture;
                        break;
                    case UnitType.Transport:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].TransportTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].TransportTexture;
                        break;
                    case UnitType.Battleship:
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].BattleshitTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].BattleshitTexture;
                        break;
                    case UnitType.Paysan: // peon
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].PeasantTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].PeasantTexture;
                        break;

                    default:// building
                        if (!owner.isOrc)
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2].SummerBuildingTexture;
                        else
                            unitTextureAssociated[i, owner.number] = unitTextures[owner.number * 2 + 1].SummerBuildingTexture;
                        break;
                }


            }

            #endregion

        }

        public virtual void Draw(SpriteBatch spriteBatch, Texture2D texture, Vector2 pos)
        {

            currentAnimation.Draw(spriteBatch);
            if (Game1.map.selected.Contains(this) && Game1.map.selected.Count == 1)
            {
                DrawLeftPanelIcons(spriteBatch);
            }

        }

        public bool onRangeToAttackTarget(Unit mytarget)
        {
            if (Vector2.Distance(new Vector2(position.X + spriteSize.X / 64, position.Y + spriteSize.Y / 64), 
                new Vector2(mytarget.position.X + mytarget.spriteSize.X / 64, mytarget.position.Y + mytarget.spriteSize.X / 64)) < range)
                return true;


            return false;
        }

        // check if we can attack our target
        public bool onRangeForAttack()
        {
            if (this.target != null && ((this.target.sailing && CanAttackWater) || (this.target.flying && CanAttackAir) || (!this.target.flying && !this.target.sailing && CanAttackGround))
                && (this.target.type != UnitType.Sousmarin || (this.target.type == UnitType.Sousmarin && ((Submarine)this.target).canBeSeenBy(this.owner)))
                )// check attack
            {
                float decallageX = target.spriteSize.X / 32 / 2;
                float decallageY = target.spriteSize.Y / 32 / 2;

                float distance = Vector2.Distance(this.position + spriteSize / 32 / 2, this.target.position + target.spriteSize / 2 / 32);
                float margin = (float) target.spriteSize.X / 2f / 32f + (float) spriteSize.X / 32f / 2f;
                /*for (int i = 0; i < decallageX; i++)
                    for (int j = 0; j < decallageY; j++)
                    {
                        if (Vector2.Distance(this.position, new Vector2(this.target.position.X + i, this.target.position.Y + j)) < distance)
                            distance = Vector2.Distance(this.position, new Vector2(this.target.position.X + i, this.target.position.Y + j));
                    }*/


                if (this.type == UnitType.Catapulte && distance < 3 || this.type == UnitType.CannonTower && distance < 2)
                            return false;

                if (distance <= this.range + margin) // add a little margin
                {
                    return true;
                }

            }
            return false;
        }
        // Game1.statics.ImpossiblePos to use this position
        internal Vector2 findClearSortieSpot(Vector2 position)
        {
            if (position.Equals(Game1.statics.ImpossiblePos))
                position = this.position;
            #region getout of building
            Vector2 pos = Game1.statics.ImpossiblePos;
            bool[,] traversable;

            if (this.sailing)
                traversable = Game1.map.waterTraversableMapTiles;
            else if (this.flying)
                traversable = Game1.map.flyingTraversableMapTiles;
            else
                traversable = Game1.map.groundTraversableMapTiles;


            for (int a = 0; a < Map.mapsize; a++)
            {
                for (int i = -1 - a; i < 2 + a; i++) // check 8 adjacent cases first
                {
                    for (int j = -1 - a; j < 2 + a; j++)
                    {
                        int posx = (int)position.X + i;
                        int posy = (int)position.Y + j;
                        if (Map.Inbounds(posx, posy))
                            if (traversable[posx, posy] && !(posx == (int)position.X && posy == (int)position.Y) && !HandleCollisions.SomethingBlockingAtDest(posx, posy, (!this.flying && !this.sailing), this.sailing, this.flying))
                            {
                                return new Vector2(posx, posy);
                            }
                    }

                }
            }


            if (pos == Game1.statics.ImpossiblePos)
                return new Vector2(0, 0);
            return pos;

        }
            #endregion


        public virtual void checkNearbyUnitsForTarget()
        {
            IA.findNearbyTarget(this);
        }

        public virtual void DrawLeftPanelIcons(SpriteBatch spritebacch)
        {


        }

        public void Attack(GameTime gameTime)
        {
            if (target != null && target.type == UnitType.Paysan)
            {
                if (((Harvester)target).ShouldntGetHarmed()) // dont target a builder that is constructing
                {
                    target = null;
                    IA.findNearbyTarget(this);
                }
            }
            if (target != null)
            {
                if (!this.isABuilding)
                {
                    Tuple<Orientation, bool> orientationResult = Utils.Utils.getOrientation(position, target.position);
                    currentAnimation.getCurrentAnime().changeOrientation(orientationResult.Item1, orientationResult.Item2);
                }


                if (target.target == null && !(target.owner == Game1.currentPLayer && target.path.Count > 0))
                {
                    target.target = this;
                }
                if (target.owner == Game1.currentPLayer)
                {
                    if ((int)target.type >= littlebuildingindex)
                    {
                        Game1.map.players[Game1.currentPLayer].TownAttackedAnnoucement = true;
                    }
                    else
                    {
                        Game1.map.players[Game1.currentPLayer].attackedAnnoucement = true;
                    }
                }
                else
                {
                    Game1.map.players[target.owner].playerAI.isUnderAttack = true;
                    Game1.map.players[target.owner].playerAI.attackPositions.Add(this.position);

                    if ((target.target != null && target.target.isABuilding) || target.target == null)
                        target.target = this;
                }


                if (this.lastTimeAttack + intervalBetweenAttack < gameTime.TotalGameTime)
                {
                    if (this.range <= 1) // melee
                    {
                        if (type == UnitType.Fantassin || type == UnitType.Chevalier || type == UnitType.Nains)
                        {
                            Random r = new Random();
                            int rand = r.Next(3);
                            Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.SWORD1 + rand], position);
                        }
                        else if (type == UnitType.Paysan)
                        {
                            Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.PEONATAK], position);
                        }


                        if (this.damage + Game1.map.players[owner].playerAmeliorations.getDamageAmelioration(type) - this.target.armor < 0)
                            this.target.hp -= this.penetratingDamage * (Game1.statics.invulnerableCheatcodeOn ? 0 : 1);
                        else
                            this.target.hp -= ((this.damage + Game1.map.players[owner].playerAmeliorations.getDamageAmelioration(type) - this.target.armor + this.penetratingDamage)) * (Game1.statics.invulnerableCheatcodeOn ? 0 : 1);
                    }

                    else // projectile
                    {
                        Map map = Game1.map;
                        if (!(type == UnitType.Catapulte) && !(type == UnitType.CannonTower) && !(type == UnitType.Battleship) && !(type == UnitType.Destroyer))
                        {
                            if (this.damage - this.target.armor < 0)
                                this.target.hp -= this.penetratingDamage * (Game1.statics.invulnerableCheatcodeOn ? 0 : 1);
                            else
                                this.target.hp -= (this.damage - this.target.armor + this.penetratingDamage) * (Game1.statics.invulnerableCheatcodeOn ? 0 : 1);
                        }
                        Projectile p;
                        if ((this.type == UnitType.Archer && !map.players[this.owner].isOrc) || this.type == UnitType.BallistaTower)
                            p = new Projectile(ProjectileType.Arrow, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        else if (this.type == UnitType.Catapulte && map.players[this.owner].isOrc)
                            p = new Projectile(ProjectileType.RockType, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        else if (this.type == UnitType.Archer && map.players[this.owner].isOrc)
                            p = new Projectile(ProjectileType.Axe, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        else if (this.type == UnitType.Catapulte && !map.players[this.owner].isOrc)
                            p = new Projectile(ProjectileType.Ballista, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        else if (this.type == UnitType.Mage && map.players[this.owner].isOrc)
                            p = new Projectile(ProjectileType.Spin, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        else if (this.type == UnitType.Mage && !map.players[this.owner].isOrc)
                            p = new Projectile(ProjectileType.Thunder, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        else if (this.type == UnitType.Sousmarin)
                            p = new Projectile(ProjectileType.SubmarineMissile, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        else if (this.type == UnitType.CannonTower || this.type == UnitType.Destroyer)
                            p = new Projectile(ProjectileType.CannonTowerRock, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        else if (this.type == UnitType.Battleship)
                            p = new Projectile(ProjectileType.BattleShipRock, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        else if (this.type == UnitType.Griffon && !map.players[this.owner].isOrc)
                            p = new Projectile(ProjectileType.GriffonHammer, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        else if (this.type == UnitType.Griffon)
                            p = new Projectile(ProjectileType.DragonBall, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        else if (this.type == UnitType.Destroyer)
                            p = new Projectile(ProjectileType.CannonTowerRock, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        else
                            p = new Projectile(ProjectileType.RockType, this.target.position, this.position, this.target.spriteSize, this.spriteSize);
                        Game1.map.projectiles.Add(p);
                    }


                    this.lastTimeAttack = gameTime.TotalGameTime;
                }
            }
        }

    }







}



