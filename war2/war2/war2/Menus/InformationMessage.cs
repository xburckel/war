﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.Menus
{
    public class InformationMessage : Message
    {
        public InformationMessage(string text)
            : base(text)
        {
        }

        private void checkDisplayShortCuts()
        {
            Vector2 position = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
           // if (Game1.leftPanel.)
        }

        public override void Draw(SpriteBatch spbatch)
        {
            if (!Game1.isInMainMenu && !Game1.isInMenu)
            {
                if (duration > 0)
                    spbatch.DrawString(TopPanel.font, text, new Vector2(LeftPanel.LEFTPANEL_WIDTH + 100, Game1.graphics.PreferredBackBufferHeight - 20), Color.White);
            }
        }

        public override void Update(GameTime gametime)
        {
            checkDisplayShortCuts();
            if (duration > 0)
                duration -= gametime.ElapsedGameTime.Milliseconds;
        }

        public override void UpdateMessage(string text)
        {
            base.UpdateMessage(text);
            duration = 100;
        }

    }
}
