using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;
using System.Diagnostics;
using war2.UnitsRelated;
using Microsoft.Xna.Framework.Net;
using war2.Utils;
using war2.SpriteSheetCode;
using war2.AI;
using war2.Astar;

namespace war2
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public static List<Animation> animations;
        public static List<Animation> animationstoAdd; // to add in order not to modify current collection
        public static Texture2D rectangle;
        public static string defaultMap = "Maps/map.pud";
        public static Texture2D maptiles;
        public static Texture2D wintercurrentTile;
        public static Texture2D wastelandcurrentTile;
        public static Texture2D currentTile;
        private int deathtimer = 0;
        public static GraphicsDeviceManager graphics;
        public static SpriteBatch spriteBatch;
        public static Map map;
        public static UnitTexture[] unitTextures;
        public static Camera2D cam;
        public static MyMouse mouse;
        public static RenderTarget2D renderTarget;
        public static LeftPanel leftPanel;
        public static Texture2D[,] unitTextureAssociated; //  unitexture[unitType, OwningPlayer.number] -> spritesheet associ�e
        public static TopPanel topPanel;
        public static Texture2D ResourceTexture;
        public static bool isInMenu;
        public static bool isInMainMenu;
        public static Texture2D ColoredRectangle;
        public static GameMenu gmenu;
        public static KeyboardState prevKstate;
        public static int currentPLayer = 0;
        public static MainMenu mainMenu;
        public static Texture2D projectileTexture;
        public static Texture2D corpsesTexture;
        public static SoundEngine soundEngine = null;
        public static bool spellCursorActive; // check if spell selected
        public static bool isHost;
        public static Server server;
        public static Client client;
        public static List<PlayerMoves> toExecute;
        public static Int64 unitindex;//for network
        public static GameWindow window;
        public static ContentManager content;
        public static MapTextureType currentMapTexture = MapTextureType.Summer;
        public static Statics statics;
        public static RightClick rclick;
        public static MinimapCamera mmcam;
        public static List<AlreadyComputedPaths>[,] recentPaths;
       // public static Unit[,] units;

        public Game1()
        {

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            base.Initialize();
            base.LoadContent();

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            isInMainMenu = true;
            content = Content;
            statics = new Statics();
            rclick = new RightClick();
            window = Window;
            unitindex = 0;
            toExecute = null;
            server = null;
            client = null;
            isHost = false;
            spellCursorActive = false;
            rectangle = new Texture2D(graphics.GraphicsDevice, 32, 32); ;
            maptiles = Content.Load<Texture2D>("Maptiles");
            wintercurrentTile = Content.Load<Texture2D>("wintertiles");
            wastelandcurrentTile = Content.Load<Texture2D>("wastelandtiles");
            currentTile = wastelandcurrentTile;
            map = new Map();
            generatePlayers();
            animations = new List<Animation>();
            animationstoAdd = new List<Animation>();
            map.minimapHidden = false;

            map.Load(defaultMap);


            prevKstate = Keyboard.GetState();
            isInMenu = false;
            Color[] data = new Color[32 * 32];
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Red;
            ColoredRectangle = new Texture2D(GraphicsDevice, 32, 32);
            ColoredRectangle.SetData(data);


            soundEngine = new SoundEngine(Content);
            corpsesTexture = Content.Load<Texture2D>("Units\\Warcraft 2 - corpses");

            Texture2D menuPanel = Content.Load<Texture2D>("war2menu");
            Texture2D HealthBar = Content.Load<Texture2D>("HealthBar2");
            Texture2D icons = Content.Load<Texture2D>("icons");
            Texture2D dottedline = Content.Load<Texture2D>("select");
            Texture2D progressBar = Content.Load<Texture2D>("progressbar");
            projectileTexture = Content.Load<Texture2D>("Units\\PC Computer - Warcraft II - Magic and Missiles");

            topPanel = new TopPanel(Content);

            graphics.PreferredBackBufferWidth = 1500;
            graphics.PreferredBackBufferHeight = 1000;

            leftPanel = new LeftPanel(menuPanel, HealthBar, icons, progressBar, Content);


            graphics.IsFullScreen = false;

            graphics.ApplyChanges();

            gmenu = new GameMenu(Content, spriteBatch);





            renderTarget = new RenderTarget2D(
                        GraphicsDevice,
                        4096,
                        4096);




            cam = new Camera2D(GraphicsDevice.Viewport);
            mmcam = new MinimapCamera();

            // this.IsMouseVisible = true;
            Texture2D cursor = Content.Load<Texture2D>("cursor");
            Texture2D spellCursor = Content.Load<Texture2D>("spellcursor");

            mouse = new MyMouse(dottedline, cursor, spellCursor);

            mainMenu = new MainMenu(Content, spriteBatch, this);
            // isInMainMenu = false;
            // TODO: Add your initialization logic here
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            ResourceTexture = Content.Load<Texture2D>("resources");
            


            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }


        protected override void Update(GameTime gameTime)
        {

            foreach (Unit u in Game1.map.units)
            {
                if (u.type == UnitType.Chevalier && Game1.map.players[u.owner].playerAmeliorations.hasPaladin)
                {
                    u.type = UnitType.Paladin;
                    u.damage = UnitProperties.PALADIN_DAMAGE;
                }
            }

              if (statics.govermenu != null)
                  statics.govermenu.Update(gameTime);

              else if (!isInMainMenu)
              {
                  #region Game


                  map.players[currentPLayer].CalculateFood();

                  KeyboardState ks = Keyboard.GetState();
                  if (ks.IsKeyDown(Keys.F10) && prevKstate.IsKeyUp(Keys.F10) || ks.IsKeyDown(Keys.Escape) && prevKstate.IsKeyUp(Keys.Escape))
                      gmenu.MenuPressed();

                  prevKstate = ks;

                  // NOT MENU
                  if (!isInMenu)
                  {
                      // Allows the game to exit
                      if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                          this.Exit();
                      // This updates the zone / regions array
                      map.updateCollisions(gameTime);
                      //This updates the array of unit / positions / and each units direction direction
                      statics.collisionUpdates.Update(gameTime);
                      mouse.Update(map, cam, leftPanel);
                      statics.constructionToolTips.Update();
                      statics.pathFindingResults.Update(gameTime);
                      Game1.statics.debugWatch.Restart();

                      foreach (Unit u in map.units)
                      {
                          u.Update(map, gameTime);
                          //map.packMoves.Update(u);
                      }

                      if (Game1.statics.debugWatch.ElapsedMilliseconds > 100)
                          war2.Utils.Debug.debugString = "all units updated in " + Game1.statics.debugWatch.ElapsedMilliseconds + "miliseconds";
                      Game1.statics.debugWatch.Stop();
                      foreach (Resource r in map.resources)
                      {
                          if (r != null)
                              r.Update(gameTime);
                          /* u.posx += (u.direction.X * u.speed * gameTime.ElapsedGameTime.TotalSeconds);
                           u.posy += (u.direction.Y * u.speed * gameTime.ElapsedGameTime.TotalSeconds);
                           */
                      }

                      foreach (Unit u in map.newUnits)
                      {
                          if ((int)u.type < Unit.littlebuildingindex)
                          {
                              u.position = u.findClearSortieSpot(Game1.statics.ImpossiblePos);
                              u.destination = u.position;
                              u.currentAnimation.Update(gameTime, u); // in order not to draw on building position
                          }
                      }
                      map.units.AddRange(map.newUnits);
                      if (map.newUnits.Count > 0) // update tech tree
                      {
                          foreach (Unit u in map.newUnits)
                              map.players[u.owner].playerTechs.Update(u.owner, map.units);
                      }
                      map.newUnits.Clear();

                      soundEngine.Update(gameTime);

                      Unit.RemoveDeadUnits(map);

                      foreach (Cadavre c in map.cadavres)
                      {
                          c.Update(gameTime);
                      }
                      map.cadavres.RemoveAll(x => x.timeLeftUntilDisappears <= 0);

                      foreach (Spell s in map.mapSpells)
                      {
                          s.Update(gameTime);
                      }
                      map.mapSpells.RemoveAll(x => x.duration <= 0);


                      cam.Update(graphics, map);
                      topPanel.Update(map.players[currentPLayer].gold, map.players[currentPLayer].wood, map.players[currentPLayer].oil, map.players[currentPLayer].food, map.players[currentPLayer].usedFood);

                      foreach (Resource r in map.toRemove)
                      {
                          map.resources[(int)r.Position.X, (int)r.Position.Y] = null;
                      }
                      map.toRemove.Clear();


                      foreach (Projectile p in map.projectiles)
                      {
                          if (p.Update(gameTime)) // true if reached destination
                              map.projectilesToRemove.Add(p);
                      }
                      foreach (Projectile p in map.projectilesToRemove)
                      {
                          map.projectiles.Remove(p);
                      }
                      map.projectilesToRemove.Clear();
                      foreach (Unit u in map.unitsToRemove)
                      {
                          map.units.Remove(u);
                      }
                      map.unitsToRemove.Clear();

                      map.players[currentPLayer].calculateVision();

                      foreach (SingleAmelioration sa in statics.ameliorations)
                          sa.Update(gameTime);

                      foreach (Player p in map.players)
                          p.Update(gameTime);

                      foreach (Animation an in animations)
                          an.Update(gameTime);
                      animations.RemoveAll(x => x.animationEnded());
                      animations.AddRange(animationstoAdd);
                      animationstoAdd.Clear();
                      rclick.Update(gameTime); 
                      statics.printedMessage.Update(gameTime); // player messages
                      statics.infoMessage.Update(gameTime); // global messages
                      statics.annouceMessage.Update(gameTime); // info about gold gold, shortcuts...
                      statics.constructionToolTips.Update();
                  }
                  else
                      gmenu.Update();
                  #endregion
              }
              else
                  mainMenu.Update(gameTime);

            // updates all the custom menu items (checks for clicks and cleans the list of functions)
            Game1.statics.menuItem.Update();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            //4.DRAW TO THE RENDER TARGET (NOT ON THE SCREEN OR TO THE DEFAULT BACKBUFFER) 
            if (!isInMainMenu && statics.govermenu == null)
            {
                GraphicsDevice.SetRenderTarget(renderTarget);
                GraphicsDevice.Clear(Color.Black);

                #region Game
                DrawAll(gameTime, false);

                //5.RESET TO THE DEFAULT BACKBUFFER 
                GraphicsDevice.SetRenderTarget(null);
                GraphicsDevice.Clear(Color.Black);
                DrawAll(gameTime, true);



                leftPanel.Draw(spriteBatch, graphics.GraphicsDevice, map.selected, map.players);
                statics.constructionToolTips.Draw(spriteBatch);

                if (map.selected.Count <= 1)
                {
                    if (map.selected.Count == 0 && map.selectedResource != null)
                    {
                        leftPanel.DrawInfoPanel(spriteBatch, null, map.selectedResource);

                    }
                    else if (map.selected.Count == 1)
                    {
                        leftPanel.DrawInfoPanel(spriteBatch, map.selected.First());
                    }
                }


                if (map.selected.Count == 1 && (int)map.selected[0].type >= Unit.littlebuildingindex)
                {
                    if (((Building)map.selected[0]).unitbuilding != UnitType.None || !((Building)map.selected[0]).isBuilt())
                    {
                        leftPanel.DrawBuildProgressBar(spriteBatch, (Building)map.selected[0], graphics.GraphicsDevice, map.players);

                    }
                    else if (((Building)map.selected[0]).upgradingto != UnitType.None)
                        leftPanel.DrawBuildProgressBar(spriteBatch, (Building)map.selected[0], graphics.GraphicsDevice, map.players, true);

                    if (((Building)map.selected[0]).sa != null)
                    {
                        leftPanel.DrawAmeliorationProgressBar(spriteBatch, (Building)map.selected[0], graphics.GraphicsDevice, map.players);

                    }

                }
                topPanel.Draw(spriteBatch, graphics.GraphicsDevice);
                spriteBatch.Begin();
                statics.printedMessage.Draw(spriteBatch); // draw messages
                statics.infoMessage.Draw(spriteBatch);
                statics.annouceMessage.Draw(spriteBatch);

                mouse.Draw(spriteBatch); // draw selected
                spriteBatch.End();
                if (!map.minimapHidden)
                {
                    spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, // draw minimap
                   SamplerState.LinearClamp, DepthStencilState.Default,
                   RasterizerState.CullNone);

                    spriteBatch.Draw(renderTarget, new Rectangle(0, 0, 250, 250), Color.White);//WINDOW SIZE 
                    spriteBatch.End();
                }
                rclick.Draw(spriteBatch);

                if (isInMenu)
                    gmenu.Draw();
                deathtimer += gameTime.ElapsedGameTime.Milliseconds;

                if (deathtimer / 5000 > 0)
                {
                    deathtimer = 0;
                    bool currentPlayerlost = true;
                    bool currentPlayerwin = true;

                    foreach (Unit u in map.units)
                    {
                        if (u.owner == currentPLayer)
                        {
                            currentPlayerlost = false;
                            break;
                        }
                    }
                    foreach (Unit u in map.units)
                    {
                        if (u.owner != currentPLayer)
                        {
                            currentPlayerwin = false;
                            break;
                        }
                    }
                    if (currentPlayerlost == true)
                        statics.govermenu = new Menus.GameOverMenu(map.players[currentPLayer].isOrc, false);
                    else if (currentPlayerwin == true)
                        statics.govermenu = new Menus.GameOverMenu(map.players[currentPLayer].isOrc, true);
                }
                #endregion

            }
            else if (statics.govermenu == null)
                mainMenu.Draw(this, gameTime);
            else
                statics.govermenu.Draw(spriteBatch);


            mouse.DrawCursor(spriteBatch);

            base.Draw(gameTime);

        }

        private void DrawAll(GameTime gameTime, bool scrolling)
        {
            Texture2D select = Content.Load<Texture2D>("select");
            #region game rendering
            if (scrolling) // rendering to game
            {

                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp,
                null, null, null, cam.Transform);
                DrawMap();

                spriteBatch.End();

                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp,
                null, null, null, cam.Transform);

                foreach (Cadavre c in map.cadavres)
                {
                    c.Draw(spriteBatch);
                }
                foreach (Unit unit in map.selected)
                {
                    if (!(unit.type == UnitType.Sousmarin && (!((Submarine)unit).canBeSeenBy(Game1.currentPLayer)))) // if it is not an invisible submarine 
                    {
                        Color c = new Color(102, 255, 51);

                        if (unit.owner != Game1.currentPLayer)
                            c = Color.Red;
                        int spritex = (int)unit.spriteSize.X;
                        int spritey = (int)unit.spriteSize.Y;


                        Vector2 pos = new Vector2((float)unit.position.X * 32, (float)unit.position.Y * 32);
                        spriteBatch.Draw(select, pos, new Rectangle(1, 1, 1, spritey), c);
                        spriteBatch.Draw(select, pos, new Rectangle(1, 1, spritex, 1), c);
                        pos = new Vector2(spritex + unit.position.X * 32, unit.position.Y * 32);
                        spriteBatch.Draw(select, pos, new Rectangle(1, 1, 1, spritey), c);
                        pos = new Vector2((float)unit.position.X * 32, (float)unit.position.Y * 32 + spritey);

                        spriteBatch.Draw(select, pos, new Rectangle(1, 1, spritex, 1), c);
                    }

                }


                if (map.selected.Count == 0 && map.selectedResource != null)
                {
                    Vector2 pos = new Vector2((float)map.selectedResource.Position.X * 32, (float)map.selectedResource.Position.Y * 32);
                    spriteBatch.Draw(select, pos, new Rectangle(0, 0, 2, 96), Color.Yellow);
                    spriteBatch.Draw(select, pos, new Rectangle(0, 0, 96, 2), Color.Yellow);
                    pos = new Vector2(96 + map.selectedResource.Position.X * 32, map.selectedResource.Position.Y * 32);
                    spriteBatch.Draw(select, pos, new Rectangle(0, 0, 2, 96), Color.Yellow);
                    pos = new Vector2((float)map.selectedResource.Position.X * 32, (float)map.selectedResource.Position.Y * 32 + 96);

                    spriteBatch.Draw(select, pos, new Rectangle(0, 0, 96, 2), Color.Yellow);
                }


                foreach (Unit unit in map.units)
                {
                    if (!statics.fogOfWarEnabled || map.players[currentPLayer].hasVision[(int)Math.Round(unit.position.X), (int)Math.Round(unit.position.Y)])
                        DrawUnit(unit);
                    else
                    {
                        bool loop = true;
                        for (int i = (int)Math.Round(unit.position.X); i < (int)Math.Round(unit.position.X) + (unit.spriteSize.X / 32) && loop; i++)
                            for (int j = (int)Math.Round(unit.position.Y); j < (int)Math.Round(unit.position.Y) + (unit.spriteSize.Y / 32) && loop; j++)
                            {
                                if (map.players[currentPLayer].hasVision[i, j])
                                {
                                    loop = false;
                                    DrawUnit(unit);
                                }
                            }
                    }
                }

                foreach (Resource r in map.resources)
                {
                    if (r != null)
                    {
                        r.Draw(spriteBatch);
                    }
                }


                foreach (Spell s in map.mapSpells)
                    s.Draw(spriteBatch);


                foreach (Projectile p in map.projectiles)
                    p.Draw(spriteBatch);
                if (statics.fogOfWarEnabled)
                     map.players[currentPLayer].DrawFogOfWar(graphics, spriteBatch);

                foreach (Animation an in Game1.animations)
                    an.Draw(spriteBatch);

                if (statics.debug)
                {
                    war2.Utils.Debug.DrawFunctionTimer(spriteBatch);
                    //war2.Utils.Debug.DrawDebug(spriteBatch);
                    //war2.Utils.Debug.DrawGroundTraversableDebug(spriteBatch);
                }
                spriteBatch.End();

            }
            #endregion
            else  if (!map.minimapHidden)// rendering to minimap
            {
                #region minimap

              spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp,
                  null, null, null, mmcam.Transform());
                    DrawMap();
                    if (statics.fogOfWarEnabled)
                        map.players[currentPLayer].DrawFogOfWar(graphics, spriteBatch);
                    // todo : verbessern

                    map.DrawGoldMinesAsYellowSquares(spriteBatch);
                    map.DrawOilPatchesAsBlackSquares(spriteBatch);

                    // To draw player-colored rectangles on the minimap
                    Color[] data = new Color[32 * 32];
                    for (int i = 0; i < data.Length; ++i) data[i] = Color.LightGreen;
                    ColoredRectangle = new Texture2D(Game1.graphics.GraphicsDevice, 32, 32);
                    ColoredRectangle.SetData(data);

                    foreach (Unit unit in map.units) // dont draw unit but draw their image as squares of players color
                    {
                        if (!(unit.type == UnitType.Sousmarin && (!((Submarine)unit).canBeSeenBy(Game1.currentPLayer))) && // don't draw invisible submarines
                            !statics.fogOfWarEnabled || (unit.discovered && (map.players[currentPLayer].explored[(int)unit.position.X, (int)unit.position.Y] && unit.isABuilding) || map.players[currentPLayer].hasVision[(int)unit.position.X, (int)unit.position.Y]))
                        {

                            if (unit.owner == Game1.currentPLayer)
                            {


                                for (int i = (int)unit.position.X * 32; i < (int)unit.position.X * 32 + 64; i += 32)
                                    for (int j = (int)unit.position.Y * 32; j < (int)unit.position.Y * 32 + 64; j += 32)
                                        spriteBatch.Draw(ColoredRectangle, new Vector2(i, j), new Rectangle(12, 105, (int)unit.spriteSize.X / 2, (int)unit.spriteSize.Y / 2), Color.LightGreen);

                            }
                            else
                                for (int i = (int)unit.position.X * 32; i < (int)unit.position.X * 32 + 64; i += 32)
                                    for (int j = (int)unit.position.Y * 32; j < (int)unit.position.Y * 32 + 64; j += 32)
                                        spriteBatch.Draw(Game1.statics.playerColorRectangle[unit.owner], new Vector2(i, j),
                                            new Rectangle(12, 105, (int)unit.spriteSize.X / 2, ((int)unit.spriteSize.Y / 32) * 32), Color.White); // dont draw them full size
                        }
                    }
 





                // draw minimap current position square
                    spriteBatch.Draw(ColoredRectangle, new Vector2(-cam.Pos.X + LeftPanel.LEFTPANEL_WIDTH, -cam.Pos.Y) / cam.Zoom, new Rectangle(32, 0, 32, (int) (graphics.PreferredBackBufferHeight / cam.Zoom)), Color.Gold);
                    spriteBatch.Draw(ColoredRectangle, new Vector2(-cam.Pos.X + LeftPanel.LEFTPANEL_WIDTH, -cam.Pos.Y) / cam.Zoom, new Rectangle(32, 0, (int) (graphics.PreferredBackBufferWidth / cam.Zoom), 32), Color.Gold);
                    spriteBatch.Draw(ColoredRectangle, new Vector2(-cam.Pos.X + (LeftPanel.LEFTPANEL_WIDTH + graphics.PreferredBackBufferWidth), -cam.Pos.Y) / cam.Zoom, new Rectangle(0, 0, 32, (int) (graphics.PreferredBackBufferHeight / cam.Zoom)), Color.Gold);
                    spriteBatch.Draw(ColoredRectangle, new Vector2(-cam.Pos.X + LeftPanel.LEFTPANEL_WIDTH, -cam.Pos.Y + graphics.PreferredBackBufferHeight) / cam.Zoom, new Rectangle(0, 0, (int) (graphics.PreferredBackBufferWidth / cam.Zoom), 32), Color.Gold);

                spriteBatch.End();
                #endregion

            }




        }


        public void DrawMap(bool forceNoFogOfWar = false)
        {


            for (int i = 0; i < Map.mapsize; i++)
            {
                for (int j = 0; j < Map.mapsize; j++)
                {
                    if (forceNoFogOfWar || (!Game1.statics.fogOfWarEnabled || (Game1.statics.fogOfWarEnabled && map.players[currentPLayer].hasVision[i, j])))
                        DrawElement(map.getMap()[i, j], i, j);
                }

            }


        }



        private void DrawElement(MapElement melement, int posi, int posj)
        {
            int[] positions = map.GetMaptilesPosition(posi, posj);
            //  spriteBatch.Begin();
            Vector2 pos = new Vector2(posi * 32, posj * 32);
            spriteBatch.Draw(Game1.currentTile, pos, new Rectangle(positions[0], positions[1], 32, 32), Color.White);
            //   spriteBatch.End();
        }


        private void DrawUnit(Unit unit)
        {

            Texture2D texture;

            texture = unitTextureAssociated[(int)unit.type, unit.owner]; // retreive associated texture

            Vector2 pos = new Vector2((float)unit.position.X * 32, (float)unit.position.Y * 32);


            if (!(unit.type == UnitType.Paysan && ((Harvester)unit).inbuilding != null))
            {
                unit.Draw(spriteBatch, texture, pos);

            }

        }




        internal static void loadGame(string path)
        {
            // TODO
            SaveGameData svg = Save.Load(path);
            map.array = svg.array;
            map.flyingTraversableMapTiles = svg.flyingTraversableMapTiles;
            map.groundTraversableMapTiles = svg.groundTraversableMapTiles;
            Map.mapsize = svg.mapsize;
            map.mapTextures = svg.mapTextures;
            map.players = svg.players;
            map.projectiles = svg.projectiles;
            map.resources = svg.resources;
            map.selected = svg.selected;
            map.selectedResource = svg.selectedResource;
            map.units = svg.units;
            map.waterTraversableMapTiles = svg.waterTraversableMapTiles;
            isInMainMenu = false;
            isInMenu = false;
            map.mapSpells = svg.spells;
            Game1.map.players = svg.players;

            foreach (Unit u in map.units)
            {
                if ((int)u.type < Unit.littlebuildingindex) // buildings dont have unit animations
                    u.currentAnimation = new UnitAnimation(u.position, u);
            }

        }

        internal static void saveGame(string path)
        {
            Save save = new Save(map.groundTraversableMapTiles, map.waterTraversableMapTiles, map.flyingTraversableMapTiles,
                                                              map.array, Map.mapsize, map.units, map.selected, map.projectiles,
                                                              map.resources, map.selectedResource, map.players, map.mapTextures, map.mapSpells);

            save.doSave(path);
            statics.annouceMessage.UpdateMessage("Save completed: " + path);
            isInMenu = false;

        }

        public void Quit()
        {
            this.Exit();
        }

        //  --------------------------------------------------- Network --------------------------------------------------

        public void Host()
        {
            server = new Server();
        }





        internal static void generatePlayers()
        {
            map.players = new List<Player>();
            for (int i = 0; i < 8; i++)
            {

                bool isOrc = true;
                if (i % 2 == 0)
                    isOrc = false;
                map.players.Add(new Player(i, isOrc));
            }      
        }
    }
}
