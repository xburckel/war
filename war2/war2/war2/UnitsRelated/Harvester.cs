﻿using EpPathFinding;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.PathFindingRelated;
using war2.UnitsRelated;

namespace war2
{

    [Serializable]
    public class Harvester : Unit
    {
        public int lastTurnHp;
        public Resource currentResource; // reource to which we are affected
        public ResourceType carrying; // resource we are carrying;
        public int timeharvesting;
        public Building inbuilding;
        public Building selectedBuilding;
        public Building constructingBuilding;
        public Vector2 buildingConstructPosition; // where to display the building if selected but not constructed
        public Building repairTarget;
        public Building store;
        public int timeRepairing = 0;
        public int stuckTime = 0;
        public bool cancelledBuild;
        public int timeWoodharvesting;
        public Harvester(int hp, int mana, float speed,
        int damage,
        bool flying,
        bool sailing,
        int goldCost,
        int woodCost,
        int oilCost,
        int range,
        int sight,
        Vector2 position,
       int buildtime,
           UnitType type,
       int armor, int owner)
            : base(hp, mana, speed,
                damage,
                flying,
                sailing,
                goldCost,
                woodCost,
                oilCost,
                range,
                sight,
                position,
                buildtime,
                type,
                armor, owner)
        {
            currentResource = null;
            carrying = ResourceType.None;
            timeharvesting = 0;
            selectedBuilding = null;
            constructingBuilding = null;
            buildingConstructPosition = Game1.statics.ImpossiblePos;
            disabled = 0;
            repairTarget = null;
            cancelledBuild = false;
            timeWoodharvesting = 0;
            store = null;
        }

        /**
         * Handle resource harvesting
         * */
        private void CheckResource()
        {
            if (currentResource != null && currentResource.type == ResourceType.Wood)
            {
                currentResource.harvesters.Remove(this);
                timeWoodharvesting = 0;
                timeharvesting = 0;
            }
            // reset as it is a mover request
            currentResource = null;
            // if we move and were collecting wood, stop collecting

            // if we are already collecting or busy otherwise (building, in store...)

            Resource r = Resource.ResourceAtDest(destination);

            if (r == null || r.type == ResourceType.None)
                return;



            // if there is wood at destination...
            if (r.type == ResourceType.Wood && this.type == UnitType.Paysan)
            {
                this.currentResource = r;
            } // ... or other types of resource that we can collect then set it as the current target resource
            else if (r.type == ResourceType.Gold && this.type == UnitType.Paysan)
                this.currentResource = r;
            else if (r.type == ResourceType.Oil && this.type == UnitType.Petrolier && r.platForm != null && r.platForm.owner == owner && r.platForm.isBuilt())
                this.currentResource = r;
            
            // Tell our pathFinding that we can walk on these tiles... that time
            List<Vector2> positionsWalkable = new List<Vector2>();
            if (r.type == ResourceType.Wood && carrying == ResourceType.None)
            {
               positionsWalkable.Add(new Vector2((int)Math.Round(r.Position.X), (int)Math.Round(r.Position.Y)));
            }
            else if ((r.type == ResourceType.Gold && carrying == ResourceType.None) || (r.type == ResourceType.Oil && carrying == ResourceType.None))
            {
                for (int i = (int)r.Position.X; i < (int)r.Position.X + 3; i++)
                    for (int j = (int)r.Position.Y; j < (int)r.Position.Y + 3; j++)
                        if (Map.Inbounds(i, j))
                        {
                           positionsWalkable.Add(new Vector2(i, j));
                        }
            }

            if (owner == Game1.currentPLayer) // no threading needed for human player
            {
                path = CalculatePath(this.position, destination, Game1.map, positionsWalkable);
                if (path != null && path.Count == 0) // if clicked on forest for instance, go to the nearest available spot
                {
                    path = CalculateNearPath(new Vector2((int)Math.Round(position.X), (int)Math.Round(position.Y)), destination, Game1.map);
                    this.currentResource = null;
                }
            }
            else
                path = Game1.statics.threading.SafePathFindingRequest(this, destination, Game1.map, positionsWalkable);

        }

        /**
         * Handle repair requests or store
         * */
        private void CheckBuilding()
        {
            // Gets the building at dest if it is a store
            Building b = BuildingAtDest(true);
            store = null;
            repairTarget = null;

            // if carrying resource and there is a building at destination, register that building
            if (carrying != ResourceType.None && b != null)
            {
                store = b;
                destination = b.position;
                currentResource = Resource.findAnotherResource(carrying, this);
            }
            else // repair
            {
                // Gets all buildings at dest
                b = BuildingAtDest(false);
                if (b != null && b.type != UnitType.NavalPlatform)
                {
                    if (b.hp < UnitProperties.getBuildingProperties(b.type)[(int)BuildingProperties.HP])
                    {
                        repairTarget = b;
                    }
                    destination = b.position;
                }
            }

            // If there is no building at dest to be repaired then dont do anything
            if (repairTarget == null && store == null)
                return;

            if (!destination.Equals(this.position))
            {
                Building tmp = (repairTarget == null ? store : repairTarget);


                if (owner == Game1.currentPLayer) // no threading needed for human player
                    path = CalculatePath(this.position, destination, Game1.map, Utils.Utils.getAllPositions((int)tmp.spriteSize.X / 32, (int)tmp.spriteSize.Y / 32, tmp.position));
                else
                    path = Game1.statics.threading.SafePathFindingRequest(this, destination, Game1.map, Utils.Utils.getAllPositions((int)tmp.spriteSize.X / 32, (int)tmp.spriteSize.Y / 32, tmp.position));
            }
        }



        public override void Move(Vector2 destination, Map map = null, bool AIPlaying = false, bool checkNearest = false)
        {
            if (map == null)
                map = Game1.map;
            // Peasant specific updating logic
            if (Map.Inbounds((int)destination.X, (int)destination.Y) && !destination.Equals(Game1.statics.ImpossiblePos))
            {
                this.path.Clear();
                this.destination = destination;
                this.CheckResource();
                this.CheckBuilding();
            }

            // If the peasant specific logic didn't handle that case, then normal move
            if (path.Count == 0)
                base.Move(destination, map, AIPlaying, checkNearest);

            /* Right click so reset selected building and building construction order */
            selectedBuilding = null;
            constructingBuilding = null;
        }


        /** check if there is a building where we click, to repair or put things in
        * The building must belong to the units owner and be built
         * */
        private Building BuildingAtDest(bool needStore = true)
        {
            int destx = (int)destination.X;
            int desty = (int)destination.Y;

            List<Building> matchingBuildings = new List<Building>();

            Unit b = HandleCollisions.BiggestUnitAtPos(destx, desty);
            if (b != null && b.isABuilding && ((Building) b).isBuilt() && b.owner == owner)
            {
                if (needStore && (
                    (b.type == UnitType.LumberMill && carrying == ResourceType.Wood) ||
                    (carrying != ResourceType.Oil && (b.type == UnitType.TownHall || b.type == UnitType.Fortress || b.type == UnitType.StrongHold)) ||
                    (carrying == ResourceType.Oil && (b.type == UnitType.Raffinery || b.type == UnitType.NavalChantier))))
                    return (Building) b;
                if (!needStore)
                    return (Building) b;
            }

            return null;
        }


        private bool Ashore(int posx, int posy)
        {
            for (int i = -1; i < 1; i++)
                for (int j = -1; j < 1; j++)
                {
                    if (i + posx < Map.mapsize && i + posy < Map.mapsize && i + posx > 0 && i + posy > 0)
                    {
                        if (Game1.map.array[posx + i, posy + j] == MapElement.Grass || Game1.map.array[posx + i, posy + j] == MapElement.Mud)
                            return true;
                    }
                }


            return false;
        }

        public override void Update(Map map, GameTime gameTime)
        {

            if ((disabled <= 0)) // if not disabled as being constructing a new building
            {
                if (IsDoingNothing())
                    AssignTaskToHarvester(gameTime);


                HandleMisc(map, gameTime); // Base update etc
                if (inbuilding != null) // if in a store
                    HandleHarvesterInStore(gameTime);
                else
                {
                    HandleHarvesterNewBuilding(gameTime); // New building at pos : build it
                    HandleHarvesterRepairing(gameTime); // Repairs
                    HandleHarvesterHarvesting(gameTime); // Harvests
                    HandleHarvesterDepositingResources(gameTime); // Deposits
                    SetAnimations(gameTime); // animations
                }
            }
            else // Building
            {
                disabled -= gameTime.ElapsedGameTime.Milliseconds;
            }
        }

        private void AssignTaskToHarvester(GameTime gameTime)
        {
            if (owner == Game1.currentPLayer && currentResource != null && carrying == ResourceType.None)
                SetHarvesterToCollect();
        }

        /**
         * Checks if necessary conditions for deposit are met (store + resource)
         * if so, deposits the resource
         * */
        private void HandleHarvesterDepositingResources(GameTime gameTime)
        {
            if (this.carrying != ResourceType.None && store != null && canDeposit(store, carrying)) // holding resources
            {
                int destx = (int)store.position.X;
                int desty = (int)store.position.Y;
                for (int i = -1; i < (((int)store.spriteSize.X / 32) + 1); i++) // check near building
                {
                    for (int j = -1; j < (((int)store.spriteSize.Y / 32) + 1); j++)
                    {
                        if (Map.Inbounds(destx + i, desty + j))
                        {
                            if ((int)Math.Round(position.X) == destx + i && (int)Math.Round(position.Y) == desty + j)
                            {
                                DropResource(store);
                                return;
                            }
                        }

                    }
                }
            }
        }

        /**
         * Sets the correct animation depending on the action
         */
        private void SetAnimations(GameTime gameTime)
        {
            if (path.Count != 0)
            {
                if (carrying == ResourceType.Gold)
                    currentAnimation.getCurrentAnime().changeAction(DisplayAction.CarryGold);
                else if (carrying == ResourceType.Wood)
                    currentAnimation.getCurrentAnime().changeAction(DisplayAction.CarryWood);
                else if (carrying == ResourceType.Oil)
                    currentAnimation.getCurrentAnime().changeAction(DisplayAction.CarryOil);
                else
                    currentAnimation.getCurrentAnime().changeAction(DisplayAction.Move);

            }
            else if (timeharvesting == 0 && this.target == null && currentAnimation.getCurrentAnime().action != DisplayAction.Repair) // flying units still do the move animations
            {
                currentAnimation.SetNoAnimation();
            }

            if (timeharvesting > 0 && currentResource != null && currentResource.type == ResourceType.Wood && currentResource.harvesters.Contains(this))
                currentAnimation.getCurrentAnime().changeAction(DisplayAction.Collect);
            else if (repairTarget != null && NextToBuilding(repairTarget)) // We can't have a store if we are not repairing or bringing back resources
                currentAnimation.getCurrentAnime().changeAction(DisplayAction.Repair);

            currentAnimation.Update(gameTime, this);
        }

        private void HandleMisc(Map map, GameTime gameTime)
        {
            if (timeharvesting == 0)
                base.Update(map, gameTime);

            if (!map.selected.Contains(this) || map.selected.Count != 1)
                this.selectedBuilding = null;

            // Sets worker to invulnerable if in building or gold mine
            if (ShouldntGetHarmed())
                hp = lastTurnHp;
            else
                lastTurnHp = hp;
        }

        /**
         * Checks if harvesters is able to build a new building; if so, builds it
         */
        private void HandleHarvesterNewBuilding(GameTime gameTime)
        {
            if (constructingBuilding != null && (
            ((constructingBuilding.type != UnitType.NavalChantier && constructingBuilding.type != UnitType.Foundry && constructingBuilding.type != UnitType.Raffinery) &&
            (Vector2.Distance(buildingConstructPosition, this.position) < (constructingBuilding.spriteSize.X / 32)))
            || ((constructingBuilding.type == UnitType.NavalChantier || constructingBuilding.type == UnitType.Foundry || constructingBuilding.type == UnitType.Raffinery) &&
            (Vector2.Distance(new Vector2(buildingConstructPosition.X, buildingConstructPosition.Y), this.position) <= 2)))) // TODO : fix jump bug eventually
            {
                BuildBuilding();
            }
        }

        /** 
         * Handles the repair logic
         * */
        private void HandleHarvesterRepairing(GameTime gameTime)
        {
            if (repairTarget != null)
            {
                if (NextToBuilding(repairTarget))
                {
                    this.path.Clear();
                    destination = position;
                    Repair(gameTime);
                }
            }
        }

        private void HandleHarvesterHarvesting(GameTime gameTime)
        {
            if (carrying != ResourceType.None)
                return;
            /* Check near resource if resource is assigned
             * if near resource, harvests it
             * */
            if (this.currentResource != null) 
            {
                float distance = Vector2.Distance(this.position, this.currentResource.Position);
                if (this.currentResource.type == ResourceType.Wood)
                {
                    if (distance <= 1.42 && !this.currentResource.harvesters.Contains(this)) // harvest from side
                    {
                        Harvest();
                    }
                }
                else if (this.currentResource.type == ResourceType.Gold)
                {
                    distance = Vector2.Distance(this.position, new Vector2(this.currentResource.Position.X + 1, this.currentResource.Position.Y + 1));
                    if (distance <= 3 && !this.currentResource.harvesters.Contains(this))
                    {
                        Harvest();
                    }
                }
                else // oil
                {
                    distance = Vector2.Distance(this.position, new Vector2(this.currentResource.Position.X + 1, this.currentResource.Position.Y + 1));
                    if (distance <= 3 && !this.currentResource.harvesters.Contains(this))
                    {
                        Harvest();
                    }
                }

            }
        }
        /**
         * The harvester is in a building:
         * - Constructing
         * - Or after harvest
         * He is set to invincible while inside then the different cases are handled
         */
        private void HandleHarvesterInStore(GameTime gameTime)
        {
            // time sent in building ++
            timeharvesting += gameTime.ElapsedGameTime.Milliseconds;

            if (timeharvesting / 2000 > 0) // spent enough time in store
            {
                this.position = this.findClearSortieSpot(inbuilding.position);
                inbuilding = null;
                timeharvesting = 0;
                this.currentAnimation.Update(gameTime, this); // to avoid drawing on building
                SetHarvesterToCollect();
            }
        }
    
        /**
         * Launches the harvester towards its current resource ; does nothing if there is no current resource assigned
         */
        private void SetHarvesterToCollect()
        {
            if (currentResource == null || currentResource.type == ResourceType.None)
                return;

            List<Vector2> goldTiles = new List<Vector2>();
            if (currentResource.type == ResourceType.Gold)
            {
                for (int i = 0; i < 3; i++)
                    for (int j = 0; j < 3; j++)
                        goldTiles.Add(new Vector2(currentResource.Position.X + i, currentResource.Position.Y + j));
            }
            this.path = Game1.statics.threading.SafePathFindingRequest(this, this.currentResource.Position, Game1.map, goldTiles);
        }
        private bool canDeposit(Building b, ResourceType carrying)
        {
            if ((carrying == ResourceType.Wood || carrying == ResourceType.Gold) && (b.type == UnitType.StrongHold || b.type == UnitType.TownHall || b.type == UnitType.Fortress))
                return true;
            if (b.type == UnitType.LumberMill && carrying == ResourceType.Wood)
                return true;
            if ((b.type == UnitType.Raffinery || b.type == UnitType.NavalChantier) && carrying == ResourceType.Oil)
                return true;

            return false;
        }

        /**
         * Repairs the building
         * */
        private void Repair(GameTime gameTime)
        {
            if (repairTarget == null) // nothing to repair, return
                return;

            if (repairTarget.hp < UnitProperties.getBuildingProperties(repairTarget.type)[(int)BuildingProperties.HP])
            {
                timeRepairing += gameTime.ElapsedGameTime.Milliseconds;
                if (timeRepairing > 500)
                {
                    timeRepairing = 0;
                    if (Game1.map.players[Game1.currentPLayer].gold > 0 && Game1.map.players[Game1.currentPLayer].wood > 0 && Game1.currentPLayer == owner)
                    {
                        repairTarget.hp += (int)(0.5 / (double)((UnitProperties.getBuildingProperties(repairTarget.type)[(int)BuildingProperties.BUILDTIME])) * (double)UnitProperties.getBuildingProperties(repairTarget.type)[(int)BuildingProperties.HP]);
                        repairTarget.currentTimeSpentBuilding += 500;

                        Game1.map.players[Game1.currentPLayer].gold--;
                        Game1.map.players[Game1.currentPLayer].wood--;
                        if (currentAnimation.getCurrentAnime().action != DisplayAction.Repair)
                        {
                            currentAnimation.getCurrentAnime().changeAction(DisplayAction.Repair);
                        }
                    }
                    else if (Game1.currentPLayer != owner) // another player repairs, dont check gold cost
                    {
                        repairTarget.hp += (int)(0.5 / (double)((UnitProperties.getBuildingProperties(repairTarget.type)[(int)BuildingProperties.BUILDTIME])) * (double)UnitProperties.getBuildingProperties(repairTarget.type)[(int)BuildingProperties.HP]);
                        repairTarget.currentTimeSpentBuilding += 500;

                        if (currentAnimation.getCurrentAnime().action != DisplayAction.Repair)
                        {
                            currentAnimation.getCurrentAnime().changeAction(DisplayAction.Repair);
                        }
                    }
                    else // no gold
                    {
                        currentAnimation.getCurrentAnime().changeAction(DisplayAction.Stand);
                        repairTarget = null;
                        timeRepairing = 0;
                    }
                }
            }
            else // finished repair
            {
                if (carrying == ResourceType.None)
                    currentAnimation.getCurrentAnime().changeAction(DisplayAction.Stand);
                else if (carrying == ResourceType.Gold)
                    currentAnimation.getCurrentAnime().changeAction(DisplayAction.StandWithGold);
                else if (carrying == ResourceType.Wood)
                    currentAnimation.getCurrentAnime().changeAction(DisplayAction.StandWithWood);
                timeRepairing = 0;
                repairTarget = null;
            }

        }

        public bool NextToBuilding(Building store)
        {
            int posx = (int)store.position.X;
            int posy = (int)store.position.Y;

            for (int i = posx - 1; i < (posx + (int)store.spriteSize.X / 32 + 1); i++)
                for (int j = posy - 1; j < (posy + (int)store.spriteSize.Y / 32 + 1); j++)
                {
                    if (i == (int)Math.Round(this.position.X) && j == Math.Round((this).position.Y))
                        return true;
                }


            return false;
        }

        public void Harvest()
        {
            if (Game1.map.resources[(int)this.currentResource.Position.X, (int)this.currentResource.Position.Y] == null
                || (Game1.map.resources[(int)this.currentResource.Position.X, (int)this.currentResource.Position.Y].harvesters.Count > 0
                && this.currentResource.type == ResourceType.Wood)) // the resource was cut during the way to get it
            {
                this.currentResource = Resource.findAnotherResource(ResourceType.Wood, this);
                this.timeharvesting = 0;
                this.timeWoodharvesting = 0;
                if (currentResource != null)
                {
                    this.path = CalculatePath(this.position, this.currentResource.Position, Game1.map);
                    this.destination = currentResource.Position;
                }
            }
            else
            {
                Game1.map.resources[(int)this.currentResource.Position.X, (int)this.currentResource.Position.Y].harvesters.Add(this);
                if (Game1.map.resources[(int)this.currentResource.Position.X, (int)this.currentResource.Position.Y].type != ResourceType.Wood)
                {
                    this.position.X = (int)this.currentResource.Position.X + 1;
                    this.position.Y = (int)this.currentResource.Position.Y + 1;
                    Game1.map.selected.Remove(this);
                }
                timeharvesting = 1;
            }

        }


        public void BuildBuilding()
        {
            // check case still available :
            bool placeOk = false;
            constructingBuilding.position = new Vector2((int)Math.Round(constructingBuilding.position.X), (int)Math.Round(constructingBuilding.position.Y));
            // TODO : probably bugs with math.round not always used
            if ((Game1.statics.basicSailingBuildings.Contains(constructingBuilding.type)))
            {
                placeOk = true;
                foreach (Unit u in Game1.map.units)
                {
                    // TODOOOOOOOOOOOOOOOOOOO
                    // TODOOOOOO
                    if ((Game1.statics.basicSailingBuildings.Contains(u.type)))
                    {
                        if (Vector2.Distance(new Vector2((int)Math.Round(u.position.X) + 1, (int)Math.Round(u.position.Y) + 1), new Vector2((int)Math.Round(constructingBuilding.position.X )+ 1, (int)Math.Round(constructingBuilding.position.Y) + 1)) <= 3)
                            placeOk = false;
                    }
                }
            }
            else
            {
                // if something is blocking our current building place
                if (HandleCollisions.UnitAtPos((int) constructingBuilding.position.X, (int) constructingBuilding.position.Y, this) != null)
                {
                    placeOk = false;
                }
                //Game1.map.groundTraversableMapTiles[(int)Math.Round(constructingBuilding.position.X), (int)Math.Round(constructingBuilding.position.Y)] = true;
                //Game1.map.groundTraversableMapTiles[(int)Math.Round(this.position.X), (int)Math.Round(this.position.Y)] = true;
            }

            if (!Game1.statics.basicSailingBuildings.Contains(constructingBuilding.type) && Harvester.IsCorrectBuildPosition(constructingBuilding.position, constructingBuilding, this))
                placeOk = true;

            //Game1.map.groundTraversableMapTiles[(int)this.position.X, (int)position.Y] = false;


            if (placeOk)
            {
                Game1.map.newUnits.Add(Game1.map.CreateUnit(constructingBuilding.type, constructingBuilding.position, owner));
                ((Building)Game1.map.newUnits.Last()).builder = this;
                this.disabled = ((Building)Game1.map.newUnits.Last()).buildtime * 1000;
                this.position = constructingBuilding.position;
                ((Building)Game1.map.newUnits.Last()).percentBuilt = 0;
                ((Building)Game1.map.newUnits.Last()).hp = UnitProperties.getBuildingProperties(constructingBuilding.type)[(int)(BuildingProperties.HP)] / UnitProperties.getBuildingProperties(constructingBuilding.type)[(int)(BuildingProperties.BUILDTIME)];

                if (Game1.map.newUnits.Last().type == UnitType.NavalPlatform)
                {
                    Game1.map.resources[(int)Game1.map.newUnits.Last().position.X, (int)Game1.map.newUnits.Last().position.Y].platForm = ((Building)Game1.map.newUnits.Last());
                }

                // TODO: check still enough gold
                Game1.map.players[owner].gold -= constructingBuilding.goldCost;
                Game1.map.players[owner].wood -= constructingBuilding.woodCost;
                Game1.map.players[owner].oil -= constructingBuilding.oilCost;

                constructingBuilding = null;
            }
            else
            {
                if (owner == Game1.currentPLayer)
                    Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.PeasantCannotBuildThere1]);
                constructingBuilding = null;
            }


        }

        private void DropResource(Building u)
        {
            if (this.carrying == ResourceType.Gold && Game1.map.players[owner].PlayerHas(UnitType.Fortress) > 0)
                Game1.map.players[owner].gold += 120;
            else if (this.carrying == ResourceType.Gold && Game1.map.players[owner].PlayerHas(UnitType.StrongHold) > 0)
                Game1.map.players[owner].gold += 110;
            else if (this.carrying == ResourceType.Gold)
                Game1.map.players[owner].gold += 100;
            else if (this.carrying == ResourceType.Wood && Game1.map.players[owner].PlayerHas(UnitType.LumberMill) > 0)
                Game1.map.players[owner].wood += 125;
            else if (this.carrying == ResourceType.Wood)
                Game1.map.players[owner].wood += 100;
            else if (this.carrying == ResourceType.Oil && Game1.map.players[owner].PlayerHas(UnitType.Raffinery) > 0)
                Game1.map.players[owner].oil += 125;
            else if (this.carrying == ResourceType.Oil)
                Game1.map.players[owner].oil += 100;
            this.carrying = ResourceType.None;

            this.inbuilding = u;
            this.position = inbuilding.position;

            HarvestersInBuilding h = new HarvestersInBuilding();
            h.buildingindex = u.index;
            h.hindex = index;
            Game1.map.players[Game1.currentPLayer].playerMoves.harvestersinbuilding.Add(h);

            if (Game1.map.selected.Contains(this))
                Game1.map.selected.Remove(this);


        }

        public void drawSelectedBuilding(SpriteBatch spriteBatch)
        {
            if (Game1.map.selected.Count == 0) // if we do not have any builder selected, do nothing
                return;
            if (selectedBuilding != null && owner == Game1.currentPLayer)
            {
                MouseState ms = Mouse.GetState();
                Vector2 truePos = (new Vector2(ms.X, ms.Y) - Game1.cam.Pos) / Game1.cam.Zoom - new Vector2(selectedBuilding.spriteSize.X / 2, selectedBuilding.spriteSize.Y / 2); // center it also

                if (Harvester.IsCorrectBuildPosition(truePos / 32, selectedBuilding, Game1.map.selected.First(), true))
                    spriteBatch.Draw(Game1.unitTextureAssociated[(int)selectedBuilding.type, selectedBuilding.owner], truePos, new Rectangle((int)Math.Round(selectedBuilding.spriteSheetPosition.X), (int)Math.Round(selectedBuilding.spriteSheetPosition.Y), (int)Math.Round(selectedBuilding.spriteSize.X), (int)Math.Round(selectedBuilding.spriteSize.Y)), Color.White);
                else
                {
                    for (int i = 0; i < (selectedBuilding.spriteSize.X / 32); i++)
                    {
                        for (int j = 0; j < (selectedBuilding.spriteSize.Y / 32); j++)
                        {
                            spriteBatch.Draw(Game1.ColoredRectangle, new Vector2(truePos.X + 32 * i, truePos.Y + 32 * j), new Rectangle(0, 0, 32, 32), Color.Red);
                        }
                    }
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch, Texture2D texture, Vector2 pos)
        {

 
            if (disabled <= 0 && (!(timeharvesting > 0 && currentResource != null && currentResource.type == ResourceType.Gold)))
            {
                // draw the unit itself
                if (Game1.map.players[owner].isOrc)
                {
                        currentAnimation.Draw(spriteBatch);
                }

                else
                {
                        currentAnimation.Draw(spriteBatch);
                }
            }
            // because this only gets called in drawfogofwar else
            if (!Game1.statics.fogOfWarEnabled)
                drawSelectedBuilding(spriteBatch);
        }

        /** 
         * Checks if builder can currently build toBuild on position position
         * */
        public static bool IsCorrectBuildPosition(Vector2 position, Unit toBuild, Unit builder, bool draw = false)
        {
            int posX = (int) Math.Round(position.X);
            int posY = (int) Math.Round(position.Y);

            if (Game1.statics.basicSailingBuildings.Contains(toBuild.type))
            {
                // TODO
                return false;
            }
            /* Ground building */
            else
            {
                for (int i = posX; i < posX + toBuild.spriteSize.X / 32; i++)
                {
                    for (int j = posY; j < posY + toBuild.spriteSize.Y / 32; j++)
                    {
                        if (!Map.Inbounds(i, j))
                            return false;
                        if (Game1.map.array[i, j] == MapElement.Mud)
                            return false;
                        // we are drawing the tiles in red or sprite for the human player : take fog of war into account
                        if (draw && Game1.statics.fogOfWarEnabled && !Game1.map.players[Game1.currentPLayer].explored[posX, posY])
                            return false;
                        if (HandleCollisions.UnitAtPos(i, j, builder) != null)
                            return false;
                        if (Resource.ResourceAtDest(new Vector2(i, j)) != null)
                        {
                            return false;
                        }

                    }    
                }

                if (HandleCollisions.UnitAtPos(posX, posY, builder) != null)
                {
                    return false;
                }

                if (toBuild.type == UnitType.TownHall)
                {
                    foreach (Resource r in Game1.map.GoldResources)
                    {
                        if (r != null && Vector2.Distance(position, r.Position) <= 7)
                        {
                            return false;
                        }
                    }
                }


            }

            return true;  
            }


        /*public static bool isCorrectBuildTile(int iindex, int jindex, Unit selectedBuilding, int owner = 999999) // overload owner to currentplayer for display 
        {

            bool correctSpot = true;
            if (iindex >= 0 && jindex >= 0 && iindex < Map.mapsize && jindex < Map.mapsize)
            {
                if (Game1.statics.fogOfWarEnabled && owner == Game1.currentPLayer && !Game1.map.players[Game1.currentPLayer].explored[iindex, jindex])
                    return false;

                if (selectedBuilding.type == UnitType.NavalPlatform) // check not near oil patch
                {
                    if (Game1.map.resources[iindex, jindex] != null && Game1.map.resources[iindex, jindex].type == ResourceType.Oil && Game1.map.resources[iindex, jindex].platForm == null)
                    {
                        return true;

                    }
                    else
                        return false;
                }



                if ((Game1.statics.basicSailingBuildings.Contains(selectedBuilding.type)))

                {                          
                    // we need 3 water spots side by side at least and to check no other building near

                    foreach (Unit u in Game1.map.units)
                    {
                        if (Game1.statics.basicSailingBuildings.Contains(u.type) && Vector2.Distance(new Vector2(u.position.X, u.position.Y), new Vector2(iindex, jindex)) <= 3)
                            return false;
                    }

                    bool lateral1 = true;
                    bool horizontal1 = true;
                    bool lateral2 = true;
                    bool horizontal2 = true;

                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                        {
                            if (!Map.Inbounds(iindex + i, jindex + j))
                            {
                                return false;
                            }
                            else
                            {
                                if (j == 0 && (i == 0 || i == 1 || i == 2) && !Game1.map.waterTraversableMapTiles[iindex + i, jindex + j])
                                    lateral1 = false;
                                if (j == 2 && (i == 0 || i == 1 || i == 2) && !Game1.map.waterTraversableMapTiles[iindex + i, jindex + j])
                                    lateral2 = false;
                                if (i == 2 && (j == 0 || j == 1 || j == 2) && !Game1.map.waterTraversableMapTiles[iindex + i, jindex + j])
                                    horizontal1 = false;
                                if (i == 0 && (j == 0 || j == 1 || j == 2) && !Game1.map.waterTraversableMapTiles[iindex + i, jindex + j])
                                    horizontal2 = false;
                            }
                        }

                    if (!horizontal1 && !horizontal2 && !lateral1 && !lateral2)
                        return false;

                    if (!Game1.map.waterTraversableMapTiles[iindex + 1, jindex + 1]) // center must be water
                    {
                        return false;
                    }

                    // check we are building somewhere we can reach an oil patch
                    bool goodArea = false;
                    if (owner != 999999) // the player can build even if there is no oil patch in the area, while the IA should not
                        goodArea = true;
                    foreach (Resource o in Game1.map.OilResources)
                    {
                        if (Game1.map.mapAreas.stGridsWater[(int)o.Position.X, (int)o.Position.Y] == Game1.map.mapAreas.stGridsWater[iindex + 1, jindex + 1])
                            goodArea = true;
                    }
                    if (!goodArea)
                        return false;

                    bool landnear = false;
                    for (int v = iindex + 1 - 2; v < iindex + 1 + 2; v++) // + 1 to go from center
                    {
                        for (int k = jindex - 2 + 1; k < jindex + 2 + 1; k++)
                        { // array[v, k] as rock means also coast; because, thats why
                            if (Map.Inbounds(v, k) && (Game1.map.array[v, k] == MapElement.Grass || Game1.map.array[v, k] == MapElement.Mud || Game1.map.array[v, k] == MapElement.Rock)) // TODO : to be improved
                                landnear = true;
                        }
                    }
                    if (!landnear)
                        return false;



                    if (selectedBuilding.type == UnitType.NavalChantier || (selectedBuilding.type == UnitType.Raffinery))
                    {
                        foreach (Resource r in Game1.map.resources)
                        {
                            if (r != null && r.type == ResourceType.Oil && Vector2.Distance(new Vector2(iindex, jindex), r.Position) <= 8)
                            {
                                correctSpot = false;
                            }
                        }

                    }
                }
                else if (selectedBuilding.type == UnitType.TownHall) // check not near gold mine
                {
                    if (!Game1.map.groundTraversableMapTiles[iindex, jindex] || Game1.map.array[iindex, jindex] == MapElement.Mud)
                    {
                        correctSpot = false;
                    }
                    else
                    {
                        foreach (Resource r in Game1.map.GoldResources)
                        {
                            if (r != null && Vector2.Distance(new Vector2(iindex, jindex), r.Position) <= 7)
                            {
                                correctSpot = false;
                            }
                        }

                    }

                }


                else if (!Game1.map.groundTraversableMapTiles[iindex, jindex] || Game1.map.array[iindex, jindex] == MapElement.Mud)
                {
                    correctSpot = false;
                }





            }
            return correctSpot;

        }*/

        public override void DrawLeftPanelIcons(SpriteBatch spriteBatch)
        {
            Vector2 pos = new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 45);
            Game1.leftPanel.DrawBuildIcons(spriteBatch, type, pos, Game1.map.players[owner].isOrc);
            pos = new Vector2(10, Game1.graphics.GraphicsDevice.Viewport.Height - 90);

            if (type == UnitType.Petrolier)
            {
                Game1.leftPanel.DrawTankerBuildPanel(Game1.map.players[owner].isOrc, pos, spriteBatch, Game1.map.players[owner]);
            }
            else if (Game1.map.players[Game1.currentPLayer].buildPanelToDraw)
                Game1.leftPanel.DrawBuildPanel(Game1.map.players[owner].isOrc, pos, spriteBatch, Game1.map.players[owner]);
            else if (Game1.map.players[Game1.currentPLayer].advancedbuildPanelToDraw)
                Game1.leftPanel.DrawAdvancedBuildPanel(Game1.map.players[owner].isOrc, pos, spriteBatch, Game1.map.players[owner]);


        }

        internal void GiveBuildingConstructOrder(Vector2 buildPos)
        {
            constructingBuilding = (Building)Game1.map.CreateUnit(selectedBuilding.type, buildPos, owner, false);
            /* unsure why i put this in the first place
              if (((Harvester)map.selected.First()).type == UnitType.NavalPlatform)
             {
                 if (Game1.map.resources[(int)buildPos.X, (int)buildPos.Y] != null)
                     Game1.map.resources[(int)buildPos.X, (int)buildPos.Y].platForm = ((Harvester)map.selected.First()).constructingBuilding;
             }
             */
            if (owner == Game1.currentPLayer)
                Game1.statics.istryingtobuild = false; // to stop displaying theimage of the building to be constructed
            selectedBuilding = null;
            buildingConstructPosition = buildPos;
            if (owner == Game1.currentPLayer)
                Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.THUNK], buildPos);

            List<Vector2> veclist = new List<Vector2>();

            for (int i = -1; i < (int) (constructingBuilding.spriteSize.X / 32) + 1; i++)
                for (int j = -1; j < (int) (constructingBuilding.spriteSize.Y / 32) + 1; j++)
            {
                if (Map.Inbounds((int) buildPos.X + i, (int) buildPos.Y + j))
                    veclist.Add(new Vector2(buildPos.X + i, buildPos.Y + j));
            }

            path = CalculatePath(position, buildPos, Game1.map, veclist);

            destination = buildPos;
        }

        internal bool ShouldntGetHarmed()
        {
            if ((disabled > 0) || (inbuilding != null && inbuilding.hp > 0) || (timeharvesting > 0 && currentResource != null && (currentResource.type == ResourceType.Gold || currentResource.type == ResourceType.Oil))) // dont target a builder that is constructing
                return true;
            return false;
        }

        public bool IsDoingNothing()
        {
            return (this.inbuilding == null && this.timeharvesting == 0 && this.target == null
                && this.repairTarget == null && this.prevPos == this.position && !Game1.statics.threading.IsLookingForPath(this));
        }
    }

}


