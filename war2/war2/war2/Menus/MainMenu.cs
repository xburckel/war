﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using war2.Utils;

namespace war2
{
    public enum DropDownType { MapType, PlayerCount, RaceSelect, FogOfWar, StartResources }

    public class DropDown
    {
        Vector2 position;
        List<string> toDisplay;
        public static Texture2D texture = Game1.content.Load<Texture2D>("MainMenus/dropdown");
        private int sizeY = 18;
        private int sizeX = 236;
        public DropDownType ddt;
        public bool IsActive = false;

        public DropDown(Vector2 position, List<String> Todisplay, DropDownType ddt)
        {
            IsActive = false;
            this.ddt = ddt;
            if (Todisplay == null)
                toDisplay = new List<string>();
            toDisplay = Todisplay;
            this.position = position;
        }


        public void Draw(SpriteBatch Spbatch)
        {
            if (IsActive)
                for (int i = 0; i < toDisplay.Count; i++)
                {
                    Spbatch.Draw(texture, new Vector2(position.X, position.Y + i * 18), new Rectangle(0, 0, 236, 18), Color.White);
                    Spbatch.DrawString(Game1.statics.font, toDisplay[i], new Vector2(position.X, position.Y + i * 18), Color.Gold);
                }
            else
            {
                Spbatch.Draw(texture, position, new Rectangle(0, 0, 236, 18), Color.White);
                switch (ddt)
                {
                    case DropDownType.MapType:
                        string txt = "Winter";
                        if (Game1.currentMapTexture == MapTextureType.Desert)
                            txt = "Desert";
                        if (Game1.currentMapTexture == MapTextureType.Summer)
                            txt = "Summer";
                        Spbatch.DrawString(Game1.statics.font, txt, position, Color.Gold);
                        break;
                    case DropDownType.RaceSelect:

                        if (MainMenu.orcChosen)
                            Spbatch.DrawString(Game1.statics.font, "Orc", position, Color.Gold);
                        else
                            Spbatch.DrawString(Game1.statics.font, "Human", position, Color.Gold);
                        break;
                    case DropDownType.FogOfWar:
                        string fogofwar = "on";
                        if (!MainMenu.fogofwaractive)
                            fogofwar = "off";

                        Spbatch.DrawString(Game1.statics.font, "Fog of War : " + fogofwar, position, Color.Gold);
                        break;
                    case DropDownType.PlayerCount:
                        Spbatch.DrawString(Game1.statics.font, "Ennemies : " + MainMenu.PlayerCount + " computers", position, Color.Gold);

                        break;
                    case DropDownType.StartResources:
                        if (MainMenu.startResources == 1)
                            Spbatch.DrawString(Game1.statics.font, "Medium resources", position, Color.Gold);
                        else if (MainMenu.startResources == 2)
                            Spbatch.DrawString(Game1.statics.font, "Lots of resources", position, Color.Gold);
                        else
                            Spbatch.DrawString(Game1.statics.font, "Low resources", position, Color.Gold);

                        break;
                }

            }
        }

        public void Update(MouseState m, MouseState prevm)
        {
            if (IsActive && !(m.X > position.X && m.X < position.X + sizeX && m.Y > position.Y && m.Y < position.Y + sizeY * toDisplay.Count)) // out of selection box
                IsActive = false;
            else if (IsActive)
            {
                if (ddt == DropDownType.MapType)
                {
                    int choice = (int)((m.Y - position.Y) / sizeY);
                    if (choice == 0)
                        Game1.currentMapTexture = MapTextureType.Snow;
                    if (choice == 1)
                        Game1.currentMapTexture = MapTextureType.Summer;
                    if (choice == 2)
                        Game1.currentMapTexture = MapTextureType.Desert;
                    IsActive = false;
                    return;
                }
                else if (ddt == DropDownType.RaceSelect)
                {
                    int choice = (int)((m.Y - position.Y) / sizeY);
                    if (choice == 0)
                        MainMenu.orcChosen = false;
                    else
                        MainMenu.orcChosen = true;

                    IsActive = false;
                    return;
                }
                else if (ddt == DropDownType.PlayerCount)
                {
                    int choice = (int)((m.Y - position.Y) / sizeY) + 1;

                    MainMenu.PlayerCount = choice;
                    if (choice > 7) // shouldn't happen
                        MainMenu.PlayerCount = 7;

                    IsActive = false;
                    return;
                }
                else if (ddt == DropDownType.FogOfWar)
                {
                    int choice = (int)((m.Y - position.Y) / sizeY);

                    if (choice == 0)
                        MainMenu.fogofwaractive = true;
                    else
                        MainMenu.fogofwaractive = false;

                    IsActive = false;
                    return;
                }
                else if (ddt == DropDownType.StartResources)
                {
                    int choice = (int)((m.Y - position.Y) / sizeY);

                    if (choice <= 2 && choice >= 0)
                        MainMenu.startResources = choice;
                    else // shouldn't happen
                        MainMenu.startResources = 0;

                    IsActive = false;
                    return;
                }
            }


            if (m.LeftButton == ButtonState.Pressed && prevm.LeftButton == ButtonState.Released)
            {
                if (m.X > position.X && m.X < position.X + sizeX && m.Y > position.Y && m.Y < position.Y + sizeY && !IsActive) // choiceseMenu
                {
                    IsActive = true;
                }
            }

        }
    }


    [Serializable]
    public class MainMenu
    {
        public static bool fogofwaractive = true;

        public static int PlayerCount = 7;
        public static bool orcChosen;
        bool chooseMapMenu;
        string currentMapName = Game1.defaultMap;
        SpriteBatch Spbatch;
        ContentManager content;
        Texture2D mainMenuTexture;
        Texture2D newGameMenuTexture;
        Texture2D choicesTexture;
        Texture2D previousSubMenuTexture;
        Texture2D startSubMenuTexture;
        Texture2D selectMapSubMenuTexture;
        Texture2D exitSubMenuTexture;
        Texture2D singlePlayerGameSubMenuTexture;
        Texture2D multiplayerGameSubMenuTexture;


        bool newGameMenu;
        bool choicesMenu;
        bool mainMenu;
        bool loadBox;
        bool multiplayerMenu;
        public bool lobbymenu;
        public MouseState prevm;
        public KeyboardState prevkstate;
        Game1 game1;
        Texture2D SaveMenuTexture;
        MouseState prevMouseState;
        Texture2D menuArrows;
        Texture2D validate;
        Texture2D cancel;
        Texture2D loadMenu;
        Texture2D joinorhost;
        Texture2D lobby;
        List<DropDown> dropDowns;
        private Texture2D previousbigsubmenutexture;
        private Texture2D loadsubmenutexture;
        private Texture2D newgamesubmenutexture;
        public static int startResources; // 0 = low, 1 = medium, 2 = lots of

        public MainMenu(ContentManager Content, SpriteBatch Spbatch, Game1 game1)
        {
            startResources = 0;
            orcChosen = false;
            dropDowns = new List<DropDown>();
            this.Spbatch = Spbatch;
            this.content = Content;
            mainMenuTexture = Content.Load<Texture2D>("MainMenus\\mainMenu");
            newGameMenuTexture = Content.Load<Texture2D>("MainMenus\\newGameMenu");
            choicesTexture = Content.Load<Texture2D>("MainMenus\\war2menuchoix");
            previousSubMenuTexture = Content.Load<Texture2D>("MainMenus\\previous");
            selectMapSubMenuTexture = Content.Load<Texture2D>("MainMenus\\selectmap");
            startSubMenuTexture = Content.Load<Texture2D>("MainMenus\\startgame");
            exitSubMenuTexture = Content.Load<Texture2D>("MainMenus\\exit");
            singlePlayerGameSubMenuTexture = Content.Load<Texture2D>("MainMenus\\singleplayerGame");
            multiplayerGameSubMenuTexture = Content.Load<Texture2D>("MainMenus\\multiplayergame");
            previousbigsubmenutexture = Content.Load<Texture2D>("MainMenus\\previousbig");
            loadsubmenutexture = Content.Load<Texture2D>("MainMenus\\load");
            newgamesubmenutexture = Content.Load<Texture2D>("MainMenus\\scenarperso");
            loadBox = false;
            multiplayerMenu = false;
            newGameMenu = false;
            mainMenu = true;
            choicesMenu = false;
            prevm = Mouse.GetState();
            prevkstate = Keyboard.GetState();
            this.game1 = game1;
            lobbymenu = false;
            SaveMenuTexture = content.Load<Texture2D>("loadMenu");
            menuArrows = content.Load<Texture2D>("menuarrow");
            validate = content.Load<Texture2D>("button_ok");
            cancel = content.Load<Texture2D>("button_cancel");
            loadMenu = content.Load<Texture2D>("MainMenus\\loadScreen");
            joinorhost = content.Load<Texture2D>("multimenu");
            lobby = content.Load<Texture2D>("lobby");
            chooseMapMenu = false;
        }






        public void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            bool prevnewGameMenu = newGameMenu;
            bool prevchoicesMenu = choicesMenu;
            bool prevmainMenu = mainMenu;
            bool prevloadBox = loadBox;
            bool prevmultiplayerMenu = multiplayerMenu;
            bool prevlobbymenu = lobbymenu;
            bool prevchoosemapmenu = chooseMapMenu;


            if (!mainMenu && !newGameMenu && !choicesMenu && !loadBox && !multiplayerMenu && !lobbymenu)
                mainMenu = true;


            MouseState m = Mouse.GetState();
            KeyboardState kstate = Keyboard.GetState();

            if (chooseMapMenu)
            {
                string str = UpdateLoadBox("Maps", "*.pud");
                string tmp = Directory.GetFiles("Maps", "*.pud")[Game1.gmenu.selectedFileIndex];
                string fullPath = tmp;
                tmp = StringsUtils.ReplaceFirstOccurrence(tmp, "Maps", "");
                tmp = StringsUtils.ReplaceLastOccurrence(tmp, ".pud", "");
                if (Game1.gmenu.selectedFileIndex == 0)
                {
                    tmp = "map"; // this is the default map
                    fullPath = "Maps\\map.pud";
                }
                Game1.statics.miniMapIcon.UpdateCurrent(tmp, fullPath);

                if (str != "")
                {
                    chooseMapMenu = false;
                    currentMapName = str;
                }
            }


            if ((m.LeftButton == ButtonState.Pressed && prevm.LeftButton == ButtonState.Released) || kstate.GetPressedKeys().Count() > 0 || (newGameMenu && (dropDowns.Count == 0))) // last condition to update dropdowns even without a first click
            {
                if (mainMenu)
                {
                    if (kstate.IsKeyDown(Keys.S) && prevkstate.IsKeyUp(Keys.S) || ((m.LeftButton == ButtonState.Pressed && prevm.LeftButton == ButtonState.Released) && m.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 300
                        && m.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 310
                        && m.Y > Game1.graphics.GraphicsDevice.Viewport.Height / 2
                        && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 75)) // choiceseMenu
                    {
                        loadBox = false;
                        newGameMenu = false;
                        mainMenu = false;
                        choicesMenu = true;
                        Thread.Sleep(100);

                    }
                    else if (kstate.IsKeyDown(Keys.M) || ((m.LeftButton == ButtonState.Pressed && prevm.LeftButton == ButtonState.Released) && m.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 300
                        && m.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 310
                        && m.Y > Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 100
                        && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 175)) // multiplayer
                    {

                        loadBox = false;
                        newGameMenu = false;
                        mainMenu = false;
                        choicesMenu = false;
                        multiplayerMenu = true;
                    }

                    else if (kstate.IsKeyDown(Keys.X) || ((m.LeftButton == ButtonState.Pressed && prevm.LeftButton == ButtonState.Released) && m.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 300
                        && m.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 310
                        && m.Y > Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 200
                        && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 275)) // exit
                        game1.Quit();


                }
                else if (newGameMenu)
                {
                    if (kstate.IsKeyDown(Keys.P) && prevkstate.IsKeyUp(Keys.P) || ((m.LeftButton == ButtonState.Pressed && prevm.LeftButton == ButtonState.Released) && m.X > 55 && m.X < 55 + 209 && m.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 300 && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 300 + 28)) // newGameMenu
                    {
                        loadBox = false;
                        newGameMenu = false;
                        mainMenu = false;
                        choicesMenu = true;
                    }
                    if ((kstate.IsKeyDown(Keys.S) && prevkstate.IsKeyUp(Keys.S)) || ((m.LeftButton == ButtonState.Pressed && prevm.LeftButton == ButtonState.Released) &&
                        m.X > Game1.graphics.GraphicsDevice.Viewport.Width - 300 && m.X < Game1.graphics.GraphicsDevice.Viewport.Width - 77 &&
                        m.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 300 && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 271)) // new game launched
                    {
                        loadBox = false;
                        newGameMenu = false;
                        mainMenu = false;
                        choicesMenu = false;

                        Game1.generatePlayers();


                        Game1.isInMainMenu = false;
                        Game1.map.players[Game1.currentPLayer].isOrc = orcChosen;
                        Game1.statics.fogOfWarEnabled = fogofwaractive;
                        Game1.map.Load(currentMapName);
                        foreach (Player p in Game1.map.players)
                        {
                            if (startResources == 1) // medium
                            {
                                p.gold = 5000;
                                p.wood = 2500;
                                p.oil = 5000;
                            }
                            else if (startResources == 2) // lots of)
                            {
                                p.gold = 10000;
                                p.wood = 5000;
                                p.oil = 10000;
                            }
                            else // normal - low
                            {
                                p.wood = 1100;
                                p.gold = 2100;
                                p.oil = 1500;
                            }


                        }
                        Game1.map.units.RemoveAll(x => x.owner > PlayerCount);
                    }

                    if (kstate.IsKeyDown(Keys.E) || ((m.LeftButton == ButtonState.Pressed && prevm.LeftButton == ButtonState.Released) &&
                        m.X > Game1.graphics.GraphicsDevice.Viewport.Width - 300 && m.X < Game1.graphics.GraphicsDevice.Viewport.Width - 80 &&
                        m.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 200 && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 178)) // choose map
                    {
                        loadBox = false;
                        mainMenu = false;
                        choicesMenu = false;
                        chooseMapMenu = true;
                    }


                    if (dropDowns.Count == 0)
                    {

                        List<string> todisplay = new List<string>();
                        todisplay.Add("Winter");
                        todisplay.Add("Summer");
                        todisplay.Add("Desert");

                        DropDown dd = new DropDown(new Vector2(175, 538), todisplay, DropDownType.MapType);
                        dropDowns.Add(dd);

                        todisplay = new List<string>();
                        todisplay.Add("Human");
                        todisplay.Add("Orc");

                        DropDown ddi = new DropDown(new Vector2(175 + 218 + 50, 538), todisplay, DropDownType.RaceSelect);
                        dropDowns.Add(ddi);

                        todisplay = new List<string>();
                        todisplay.Add("1 Ennemiy");
                        todisplay.Add("2 Ennemies");
                        todisplay.Add("3 Ennemies");
                        todisplay.Add("4 Ennemies");
                        todisplay.Add("5 Ennemies");
                        todisplay.Add("6 Ennemies");
                        todisplay.Add("7 Ennemies");

                        DropDown ddis = new DropDown(new Vector2(175 + 218 + 50 + 218 + 50, 538), todisplay, DropDownType.PlayerCount);
                        dropDowns.Add(ddis);

                        todisplay = new List<string>();
                        todisplay.Add("On");
                        todisplay.Add("Off");

                        DropDown ddist = new DropDown(new Vector2(175 + 218 + 50 + 218 + 50 + 218 + 50, 538), todisplay, DropDownType.FogOfWar);
                        dropDowns.Add(ddist);
                        todisplay = new List<string>();
                        todisplay.Add("Low resources");
                        todisplay.Add("Medium resources");
                        todisplay.Add("Lots of resources");

                        DropDown ddists = new DropDown(new Vector2(175, 620), todisplay, DropDownType.StartResources);
                        dropDowns.Add(ddists);
                    }

                    foreach (DropDown dd in dropDowns)
                        dd.Update(m, prevm);


                }
                else if (choicesMenu)
                {

                    if (kstate.IsKeyDown(Keys.S) && prevkstate.IsKeyUp(Keys.S) || ((m.LeftButton == ButtonState.Pressed && prevm.LeftButton == ButtonState.Released) && m.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 300
                        && m.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 310
                        && m.Y > Game1.graphics.GraphicsDevice.Viewport.Height / 2
                        && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 75)) // newGameMenu
                    {
                        loadBox = false;
                        newGameMenu = true;
                        mainMenu = false;
                        choicesMenu = false;
                        Thread.Sleep(100);

                    }
                    else if (kstate.IsKeyDown(Keys.P) || ((m.LeftButton == ButtonState.Pressed && prevm.LeftButton == ButtonState.Released) && m.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 300
                        && m.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 310
                        && m.Y > Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 200
                        && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 275)) // previous
                    {
                        loadBox = false;
                        newGameMenu = false;
                        mainMenu = true;
                        choicesMenu = false;
                    }

                    else if (kstate.IsKeyDown(Keys.C) || ((m.LeftButton == ButtonState.Pressed && prevm.LeftButton == ButtonState.Released) && m.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 300
                        && m.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 310
                        && m.Y > Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 100
                        && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 175)) // loadMenu
                    {
                        loadBox = true;
                        newGameMenu = false;
                        mainMenu = false;
                        choicesMenu = false;
                    }

                }
                else if (multiplayerMenu)
                {
                    if (m.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 && m.Y > Game1.graphics.GraphicsDevice.Viewport.Height / 2 && m.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 180 && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 25) // host
                    {
                        lobbymenu = true;
                        multiplayerMenu = false;

                        Game1.server = new Server();
                    }
                    else if (m.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 && m.Y > Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 25 && m.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 180 && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 52) // join
                    {
                        lobbymenu = true;
                        multiplayerMenu = false;
                        Game1.client = new Client();
                    }
                    else if (kstate.IsKeyDown(Keys.P) || ((m.LeftButton == ButtonState.Pressed && prevm.LeftButton == ButtonState.Released) && m.X > Game1.graphics.GraphicsDevice.Viewport.Width / 2 && m.Y > Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 55 &&
                        m.X < Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 180 && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 80)) // previous
                    {
                        multiplayerMenu = false;
                        mainMenu = true;
                    }
                }
                else if (lobbymenu)
                {
                    if (m.X > Game1.graphics.GraphicsDevice.Viewport.Width - 325 && m.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 150 && m.X < Game1.graphics.GraphicsDevice.Viewport.Width && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 100) // start
                    {
                    }
                    if (m.X > Game1.graphics.GraphicsDevice.Viewport.Width - 200 && m.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 55 && m.X < Game1.graphics.GraphicsDevice.Viewport.Width && m.Y < Game1.graphics.GraphicsDevice.Viewport.Height) // previous
                    {
                        lobbymenu = false;
                        mainMenu = true;
                    }
                }

            }
            if (loadBox)
            {
                string str = UpdateLoadBox();
                if (str != "")
                    Game1.loadGame(str);

            }

            if (prevnewGameMenu != newGameMenu || prevchoicesMenu != choicesMenu || prevmainMenu != mainMenu || prevloadBox != loadBox ||
                prevmultiplayerMenu != multiplayerMenu || prevlobbymenu != lobbymenu || prevchoosemapmenu != chooseMapMenu)
                Game1.soundEngine.PlayAnnoucementSound(Game1.soundEngine.triviaSounds[(int)TriviaSE.BigButtonClick]);

            prevm = m;
            prevkstate = kstate;
        }

        public void Draw(Game1 gameObject, Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (choicesMenu)
            {
                Spbatch.Begin();
                Spbatch.Draw(choicesTexture, new Rectangle(0, 0, Game1.graphics.GraphicsDevice.Viewport.Width, Game1.graphics.GraphicsDevice.Viewport.Height), Color.White);
                Spbatch.Draw(newgamesubmenutexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 300, Game1.graphics.GraphicsDevice.Viewport.Height / 2),
    new Rectangle(0, 0, 610, 70), Color.White);
                Spbatch.Draw(loadsubmenutexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 300, Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 100),
    new Rectangle(0, 0, 610, 70), Color.White);
                Spbatch.Draw(previousbigsubmenutexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 300, Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 200),
    new Rectangle(0, 0, 610, 70), Color.White);
                Spbatch.End();
            }
            if (mainMenu)
            {
                Spbatch.Begin();
                Spbatch.Draw(mainMenuTexture, new Rectangle(0, 0, Game1.graphics.GraphicsDevice.Viewport.Width, Game1.graphics.GraphicsDevice.Viewport.Height), Color.White);
                Spbatch.Draw(singlePlayerGameSubMenuTexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 300, Game1.graphics.GraphicsDevice.Viewport.Height / 2),
                    new Rectangle(0, 0, 610, 70), Color.White);
                Spbatch.Draw(multiplayerGameSubMenuTexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 300, Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 100),
    new Rectangle(0, 0, 610, 70), Color.White);
                Spbatch.Draw(exitSubMenuTexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 300, Game1.graphics.GraphicsDevice.Viewport.Height / 2 + 200),
    new Rectangle(0, 0, 610, 70), Color.White);
                Spbatch.End();
            }
            if (newGameMenu)
            {
                Spbatch.Begin();
                Spbatch.Draw(newGameMenuTexture, new Rectangle(0, 0, Game1.graphics.GraphicsDevice.Viewport.Width, Game1.graphics.GraphicsDevice.Viewport.Height), Color.White);
                Spbatch.Draw(previousSubMenuTexture, new Vector2(55, Game1.graphics.GraphicsDevice.Viewport.Height - 300), new Rectangle(0, 0, 209, 28), Color.White);
                Spbatch.Draw(startSubMenuTexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width - 300, Game1.graphics.GraphicsDevice.Viewport.Height - 300), new Rectangle(0, 0, 223, 29), Color.White);
                Spbatch.Draw(selectMapSubMenuTexture, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width - 300, Game1.graphics.GraphicsDevice.Viewport.Height - 200), new Rectangle(0, 0, 222, 28), Color.White);

                foreach (DropDown dd in dropDowns)
                    dd.Draw(Spbatch);

                if (chooseMapMenu)
                {
                    Game1.gmenu.DrawLoadBox("Maps", "*.pud");
                    Game1.statics.miniMapIcon.DrawMinimap(gameObject, Spbatch); // these coordinates should but it right 
                }
                Spbatch.DrawString(Game1.statics.font, "Current map : " + this.currentMapName.Replace("Maps/", "").Replace("Maps\\", ""), new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2 - 100, Game1.graphics.GraphicsDevice.Viewport.Height - 100), Color.Gold);


                Spbatch.End();
            }
            if (loadBox)
            {
                Spbatch.Begin();
                Spbatch.Draw(choicesTexture, new Rectangle(0, 0, Game1.graphics.GraphicsDevice.Viewport.Width, Game1.graphics.GraphicsDevice.Viewport.Height), Color.White);

                Game1.gmenu.DrawLoadBox();
                Spbatch.End();

            }
            if (multiplayerMenu)
            {

                Spbatch.Begin();
                Spbatch.Draw(mainMenuTexture, new Rectangle(0, 0, Game1.graphics.GraphicsDevice.Viewport.Width, Game1.graphics.GraphicsDevice.Viewport.Height), Color.White);

                Spbatch.Draw(joinorhost, new Vector2(Game1.graphics.GraphicsDevice.Viewport.Width / 2, Game1.graphics.GraphicsDevice.Viewport.Height / 2), new Rectangle(0, 0, 180, 81), Color.White);
                Spbatch.End();

            }
            if (lobbymenu)
            {

                Spbatch.Begin();
                Spbatch.Draw(lobby, new Rectangle(0, 0, Game1.graphics.GraphicsDevice.Viewport.Width, Game1.graphics.GraphicsDevice.Viewport.Height), Color.White);
                Spbatch.End();

            }
        }




        public string UpdateLoadBox(string dirName = "Saves", string ext = "*.wsav")
        {

            int posx = Game1.graphics.GraphicsDevice.Viewport.Width / 2 + 200;
            int posy = 250;
            MouseState m = Mouse.GetState();
            KeyboardState kstate = Keyboard.GetState();

            if (kstate.IsKeyDown(Keys.Down) && prevkstate.IsKeyUp(Keys.Down))
                Game1.gmenu.selectedFileIndex += 1;
            if (kstate.IsKeyDown(Keys.Up) && prevkstate.IsKeyUp(Keys.Up))
                Game1.gmenu.selectedFileIndex -= 1;

            if (m.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
            {
                if (m.X > posx + 10 && m.X < posx + 90 && m.Y > posy && m.Y < posy + 15 * 15)
                {
                    Game1.gmenu.selectedFileIndex = Game1.gmenu.selectedFileIndex + (int)Math.Round((m.Y - posy) / (double)15);

                }
                else if (m.X > posx + 10 && m.X < posx + 10 + 40 && m.Y > posy && m.Y < posy + 37) // up arrow
                    Game1.gmenu.selectedFileIndex = 0;
                else if (m.X > posx + 10 && m.X < posx + 10 + 40 && m.Y > posy + 220 && m.Y < posy + 260) // down arrow
                    Game1.gmenu.selectedFileIndex = Game1.gmenu.fileCount - 1;
            }

            if (Game1.gmenu.selectedFileIndex < 0)
                Game1.gmenu.selectedFileIndex = 0;
            if (Game1.gmenu.selectedFileIndex > Game1.gmenu.fileCount - 1)
                Game1.gmenu.selectedFileIndex = Game1.gmenu.fileCount - 1;
            if (m.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
            {
                if (m.X > posx && m.X < posx + 60 && m.Y > posy + 250 && m.Y < posy + 250 + 60) // load
                {
                    if (dirName.Equals("Saves")) // means we are not loading a map from new game menu
                    {
                        Game1.isInMainMenu = false;
                        Game1.gmenu.selectedFileIndex = 0;
                    }
                    loadBox = false;
                    return (Directory.GetFiles(dirName, ext)[Game1.gmenu.selectedFileIndex]);
                }
                if (m.X > posx + 65 && m.X < posx + 65 + 60 && m.Y > posy + 250 && m.Y < posy + 250 + 60) // cancel
                {
                    loadBox = false;
                    if (dirName.Equals("Saves"))
                        choicesMenu = true;
                    else
                        chooseMapMenu = false;
                    Game1.gmenu.selectedFileIndex = 0;
                }
            }
            if (kstate.IsKeyDown(Keys.Escape) && prevkstate.IsKeyUp(Keys.Escape))
            {
                Game1.gmenu.selectedFileIndex = 0;
                loadBox = false;
                if (dirName.Equals("Saves"))
                    choicesMenu = true;
                else
                    chooseMapMenu = false;
            }



            return "";
        }

    }
}
