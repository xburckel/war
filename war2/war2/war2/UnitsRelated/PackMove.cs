﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.UnitsRelated
{
    // currently only for ground units
    public class PackMove
    {
        private List<Vector2> destinations;

        public PackMove()
        {
            destinations = new List<Vector2>();
        }

        public void Update(Unit u)
        {
             if (!(u.isABuilding || u.type == UnitType.Paysan || u.type == UnitType.Petrolier) && !u.sailing && !u.flying)
                 destinations.Add(new Vector2((int)Math.Round(u.destination.X), (int)Math.Round(u.destination.Y)));

        }

        public void addDestination(Vector2 dest)
        {
            destinations.Add(new Vector2((int)Math.Round(dest.X), (int)Math.Round(dest.Y)));
        }

        public Vector2 GetAdjacentTile(Vector2 moveTo)
        {
            moveTo = new Vector2((int)Math.Round(moveTo.X), (int)Math.Round(moveTo.Y));
            if (destinations.Contains(moveTo))
            {
                for (int i = -2; i <= 2 ; i++)
                    for (int j = -2; j <= 2; j++)
                    {
                        if (Map.Inbounds((int) moveTo.X + i, (int) moveTo.Y + j)
                            && !destinations.Contains(new Vector2(moveTo.X + i, moveTo.X + j)) 
                            && Game1.map.mapAreas.checkPosReachable(moveTo, new Vector2(moveTo.X + i, moveTo.X + j), true))
                            return (new Vector2(moveTo.X + i, moveTo.X + j));
                    }

            }
            return moveTo;
        }
    }
}
