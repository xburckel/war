﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2
{

    public enum ResourceType { None = -1, Gold, Oil, Wood };

    [Serializable]
    public class Resource
    {
        public int amount;
        public Vector2 Position;
        public ResourceType type;
        public int harvestTime;
        public List<Harvester> harvesters;
        public Building platForm;

        public Resource(int amount, Vector2 pos, ResourceType type)
        {
            platForm = null;
            this.amount = amount;
            Position = pos;
            this.type = type;
            harvesters = new List<Harvester>();
            switch (this.type)
            {
                case ResourceType.Gold:
                    harvestTime = Game1.statics.harvestTimeGold;
                    break;
                case ResourceType.Oil:
                    harvestTime = Game1.statics.harvestTimeOil;
                    break;
                case ResourceType.Wood:
                    harvestTime = Game1.statics.harvestTimeWood;
                    break;
            }
        }


        public static Resource ResourceAtDest(Vector2 destination)
        {
            int destx = (int)destination.X;
            int desty = (int)destination.Y;

            if (Map.Inbounds(destx, desty) && Game1.map.resources[destx, desty] != null && Game1.map.resources[destx, desty].type == ResourceType.Wood)
            {
                return Game1.map.resources[destx, desty];
            }
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (Map.Inbounds(destx - i, desty - j))
                    {
                        if (Game1.map.resources[destx - i, desty - j] != null && (Game1.map.resources[destx - i, desty - j].type == ResourceType.Gold || Game1.map.resources[destx - i, desty - j].type == ResourceType.Oil))
                        {
                            return Game1.map.resources[destx - i, desty - j];
                        }
                    }

                }
            }


            return null;
        }


        public void Update(GameTime gameTime)
        {
            List<Harvester> finishedharvesting = new List<Harvester>();

            foreach (Harvester h in this.harvesters)
            {
                h.timeharvesting += gameTime.ElapsedGameTime.Milliseconds;
                if (type == ResourceType.Wood)
                {
                    h.timeWoodharvesting += gameTime.ElapsedGameTime.Milliseconds;
                    Random r = new Random();
                    int tempR = r.Next(1500, 2000);
                    if (h.timeWoodharvesting > tempR)
                    {
                        int tmp = r.Next(3);
                        Game1.soundEngine.PlaySound(Game1.soundEngine.miscallieanousSE[(int)MiscSoundEffect.TREE1 + tmp], h.position);
                        h.timeWoodharvesting = 0;
                    }
                }


                if ((h.timeharvesting / 1000) >= this.harvestTime)
                {
                    h.carrying = this.type;
                    h.timeharvesting = 0;
                    h.currentResource = this;


                    if (h.carrying != ResourceType.Wood)
                    {
                        h.position = h.findClearSortieSpot(h.position);
                        h.currentAnimation.Update(gameTime, h);
                    }
                        Building b = Game1.map.players[h.owner].GetNearestStorePosition(this.type, h.position, h.sailing);

                        if (b == null)
                        {
                            h.destination = h.position;
                            h.path.Clear();
                        }
                        else
                        {
                            h.destination = b.position;
                            h.path = h.CalculatePath(h.position, h.destination, Game1.map, Utils.Utils.getAllPositions((int) b.spriteSize.X / 32, (int) b.spriteSize.Y / 32, b.position));

                        }
                        h.store = b;

                    finishedharvesting.Add(h);
                    this.amount -= 100;
                }

            }



            if (this.amount <= 0)
            {
                if (this.type == ResourceType.Wood)
                {
                   Game1.map.groundTraversableMapTiles[(int)this.Position.X, (int)this.Position.Y] = true;
                   Game1.map.mapTextures[(int)this.Position.X, (int)this.Position.Y].X = 12; // texture for cutted woods
                   Game1.map.mapTextures[(int)this.Position.X, (int)this.Position.Y].Y = 6;
                   Game1.map.array[(int)this.Position.X, (int)this.Position.Y] = MapElement.Grass;
                    foreach (Harvester h in this.harvesters)
                    {
                        h.currentResource = findAnotherResource(this.type, h);
                        h.timeharvesting = 0;
                        if (h.carrying == ResourceType.None && h.currentResource != null)
                        {
                            h.path = Game1.statics.threading.SafePathFindingRequest(h, h.currentResource.Position, Game1.map);
                            h.destination = h.currentResource.Position;
                        }
                    }
                   Game1.map.toRemove.Add(this);
                   Game1.map.mapAreas.SetPosReachableGround((int)this.Position.X, (int)this.Position.Y);
                }
                else if (this.type == ResourceType.Gold)
                {
                    foreach (Player p in Game1.map.players)
                    {
                        if (p.playerAI != null)
                        {
                            if (p.playerAI.goldminepos.Equals(this.Position))
                                p.playerAI.goldminepos = Game1.statics.ImpossiblePos;
                        }

                    }


                   Game1.map.GoldResources.Remove(this);
                    foreach (Harvester h in this.harvesters)
                    {
                        h.currentResource = findAnotherResource(this.type, h);
                        h.timeharvesting = 0;
                    }
                   Game1.map.toRemove.Add(this);
                    foreach (Player p in Game1.map.players)
                    {
                        if (p.playerAI != null)
                            if (p.playerAI.goldminepos.Equals(this.Position))
                                p.playerAI.goldminepos = Game1.statics.ImpossiblePos;

                    }
                    Game1.soundEngine.PlaySound(Game1.soundEngine.triviaSounds[(int) TriviaSE.goldmineboom], Position);
                    Unit b = Game1.map.CreateUnit(UnitType.MilitaryBase, Position, 0);
                   Game1.map.cadavres.Add(new Cadavre(b));
                   for (int i = -1; i <= 1; i++)
                       for (int j = -1; j <= 2; j++)
                            Game1.map.mapAreas.SetPosReachableGround((int)this.Position.X + i, (int)this.Position.Y + j);

                }
                else if (this.type == ResourceType.Oil)
                {
                    if (platForm != null)
                        platForm.hp = -1;

                }

            }
            foreach (Harvester h in finishedharvesting)
                this.harvesters.Remove(h);

        }
        // TODO : improve
        public static Resource findAnotherResource(ResourceType type, Harvester h) // position = pos from which to start search
        {
            Dictionary<int, Resource> dic = new Dictionary<int, Resource>();
            Resource toReturn = null;
            int distance = 10000;


            if (type == ResourceType.Gold)
            {
                foreach (Resource r in Game1.map.GoldResources)
                        if (r != null && r.amount > 0) // look for smallest distance
                        {
                                if (Vector2.Distance(h.position, r.Position) < distance)
                                {
                                    distance = (int)Vector2.Distance(h.position, r.Position);
                                    toReturn = r;
                                }

                        }
                    
                
                return toReturn;
            }
            else if (type == ResourceType.Oil)
            {

                 foreach (Resource r in Game1.map.OilResources)
                       if (r.platForm != null && r.platForm.owner == h.owner)
                          return r;


            }


            // Wood

            /* List<Resource> alreadyPicked = new List<Resource>();
            foreach (Unit u in Game1.map.units)
            {
                if (u.owner == h.owner && u.type == UnitType.Paysan)
                {
                    Harvester v = (Harvester) u;
                    if (v.currentResource != null && v.currentResource.type == ResourceType.Wood)
                        alreadyPicked.Add(v.currentResource);
                }
            }
             * */

            for (int v = 0; v < Map.mapsize; v++)
                for (int i = -v + (int) h.position.X; i < v + (int) h.position.X; i++)
                {
                    for (int j = -v + (int) h.position.Y; j < v + (int) h.position.X; j++)
                    {
                        if (Map.Inbounds(i, j))
                        {
                            if (Game1.map.resources[i, j] != null && Game1.map.resources[i, j].type == type && Game1.map.resources[i, j].amount > 0) // look for smallest distance
                            {
                                if ((Game1.map.resources[i, j].harvesters.Count == 0 /* && !alreadyPicked.Contains(Game1.map.resources[i, j])*/) || type == ResourceType.Gold || type == ResourceType.Oil)
                                {
                                    if (Vector2.Distance(h.position, Game1.map.resources[i, j].Position) < distance)
                                    {
                                        // if it is not in the middle of the forest, i.e. there is a tile in the 9 tiles around that is in the same map region as the worker
                                        // so we check for that condition                        

                                        if (NearbyTileInSameRegion(h.position, new Vector2(i, j)))
                                        {
                                            distance = (int)Vector2.Distance(h.position, Game1.map.resources[i, j].Position);
                                            toReturn = Game1.map.resources[i, j];
                                        }

                                    }
                                }
                            }
                        }
                    }
                }

            return toReturn;
            

        }
        // TODO

        /**
         * Returns true if a nearby tile (in the 44 adjacent tiles) is in the same region as the ending position
         * Basically used to check if a tree is reachable by a harvester
         */
        public static bool NearbyTileInSameRegion(Vector2 start, Vector2 end)
        {

            if (Game1.map.mapAreas.checkPosReachable(start, end + new Vector2(1, 0)))
                return true;
            if (Game1.map.mapAreas.checkPosReachable(start, end + new Vector2(0, 1)))
                return true;
            if (Game1.map.mapAreas.checkPosReachable(start, end + new Vector2(-1, 0)))
                return true;
            if (Game1.map.mapAreas.checkPosReachable(start, end + new Vector2(0, -1)))
                return true;
            return false;

        }

        internal void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            if ((type == ResourceType.Gold || type == ResourceType.Oil) && (Game1.map.players[Game1.currentPLayer].hasVision[(int)Position.X, (int)Position.Y] || !Game1.statics.fogOfWarEnabled))
            {
                if (type == ResourceType.Gold && harvesters.Count > 0)
                {

                    spriteBatch.Draw(Game1.ResourceTexture, new Vector2(Position.X * 32, Position.Y * 32), new Rectangle(12, 105, 96, 96), Color.White);

                }
                else if (type == ResourceType.Gold)
                {
                    spriteBatch.Draw(Game1.ResourceTexture, new Vector2(Position.X * 32, Position.Y * 32), new Rectangle(12, 12, 96, 96), Color.White);

                }
                else if (type == ResourceType.Oil/* && this.platForm == null*/)
                {

                    spriteBatch.Draw(Game1.ResourceTexture, new Vector2(Position.X * 32, Position.Y * 32), new Rectangle(12, 200, 96, 96), Color.White);
                }
            }
        }
    }
}
