﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.SpriteSheetCode;
using war2.UnitsRelated;

namespace war2
{
    public class Cadavre
    {

        public Unit deadUnit;
        private int timerFirst;
        private int timerSecond;
        private int timeDisplayed = 0;
        public int timeLeftUntilDisappears = 0;

        public Cadavre(Unit u, int timerFirst = 10000, int timerSecond = 5000)
        {
            if (u.isABuilding || u.type == UnitType.Catapulte)
            {
                Animation.AddAnimation(AnimationType.Explosion, u.position * 32 + u.spriteSize / 2, true);
                if (u.type == UnitType.Catapulte)
                    timerFirst = 0;
                timerSecond = 0;
            }
            if (u.type == UnitType.MachineVolante || u.type == UnitType.Mage ||u.type == UnitType.Griffon || u.type == UnitType.Nains)
            {
                timerFirst = 2000;
                timerSecond = 0;
            }
            if (u.type == UnitType.Sousmarin)
            {
                timerFirst = 0;
                timerSecond = 5000;
            }
                

            timeLeftUntilDisappears = timerFirst + timerSecond;
            this.timerFirst = timerFirst;
            this.timerSecond = timerSecond;
            deadUnit = u;
            u.attackCooldown = timerFirst;
            deadUnit.currentAnimation = new UnitAnimation(u.position, u);
        }


        public void Draw(SpriteBatch spbatch)
        {
            if (timeDisplayed > timerFirst)
                deadUnit.currentAnimation.Draw(spbatch, Game1.corpsesTexture);
            else if (deadUnit.isABuilding)
                deadUnit.currentAnimation.Draw(spbatch, Game1.ResourceTexture);
            else
                deadUnit.currentAnimation.Draw(spbatch);

        }

        public void Update(GameTime gameTime)
        {

            timeDisplayed += gameTime.ElapsedGameTime.Milliseconds;
            timeLeftUntilDisappears -= gameTime.ElapsedGameTime.Milliseconds;


            if (timeDisplayed > timerFirst)
            {
                deadUnit.currentAnimation.intervalBetween = timerSecond;
                if (deadUnit.sailing)
                    deadUnit.currentAnimation.getCurrentAnime().changeAction(DisplayAction.Sinking); // update animation 2
                else
                    deadUnit.currentAnimation.getCurrentAnime().changeAction(DisplayAction.Decomposing); // update animation 2
            }
            else
            {
                // draw animation 1
                deadUnit.currentAnimation.getCurrentAnime().changeAction(DisplayAction.Dead); // update animation 1
            }
            deadUnit.currentAnimation.Update(gameTime, deadUnit);

        
        }
       


    }
        #region oldcode
            /*
             * public Texture2D cadavreTextureSheet;
                public int totalTimeUntilDisappears;
                public int timeLeftUntilDisappears;
                public List<Vector2> SpritePositionsOnSheet;
                public Vector2 spritePositionOnSheet; // initial position
                public int spriteSize;
                private int spriteNumber;
                private int spriteSwitchInterval;
                private int innerTimer;
                public UnitType unitType; // used for resurrection
                public int owner;

                public Vector2 spritePositionOnMap;

                public Cadavre(Unit u)
                {
                    unitType = u.type;
                    owner = u.owner;
                    spriteNumber = 0;
                    totalTimeUntilDisappears = 10000; // 10k miliseconds default
                    spritePositionOnMap = u.position;
                    spritePositionOnMap.X = spritePositionOnMap.X * 32 + (u.spriteSize.X - 32) / 2; // center on the middle
                    spritePositionOnMap.Y = spritePositionOnMap.Y * 32 + (u.spriteSize.Y - 32) / 2;
                    spriteSize = 1;
                    SpritePositionsOnSheet = new List<Vector2>();


                    if ((int)u.type >= Unit.littlebuildingindex) // buildings
                    {
                        cadavreTextureSheet = Game1.ResourceTexture;
                        if (u.type != UnitType.NavalChantier && u.type != UnitType.NavalPlatform && u.type != UnitType.Foundry && u.type != UnitType.Raffinery)
                        {
                            if ((int)u.type >= Unit.mediumbuildingindex)
                            {
                                SpritePositionsOnSheet.Add(new Vector2(438, 10));
                                SpritePositionsOnSheet.Add(new Vector2(442, 76));
                                spriteSize = 64;
                      

                            }
                            else if ((int)u.type >= Unit.largebuildingindex)
                            {
                                SpritePositionsOnSheet.Add(new Vector2(509, 10));
                                SpritePositionsOnSheet.Add(new Vector2(509, 76));
                                spriteSize = 64;

                            }
                            else
                            {
                                spriteSize = 35;

                                SpritePositionsOnSheet.Add(new Vector2(515, 218));
                                SpritePositionsOnSheet.Add(new Vector2(515, 250));
                            }

                        }
                        else // naval buildings
                        {
                            spriteSize = 70;

                            SpritePositionsOnSheet.Add(new Vector2(441, 140));
                            SpritePositionsOnSheet.Add(new Vector2(442, 205));

                        }

                    }
                    else // units
                    {

                        if (u.type == UnitType.Catapulte)
                        {
                            spriteSize = 64;

                            cadavreTextureSheet = Game1.projectileTexture; // which has also the explosion texture
                            SpritePositionsOnSheet.Add(new Vector2(8, 170));
                            SpritePositionsOnSheet.Add(new Vector2(70, 170));
                            SpritePositionsOnSheet.Add(new Vector2(130, 170));
                            SpritePositionsOnSheet.Add(new Vector2(195, 170));
                            SpritePositionsOnSheet.Add(new Vector2(256, 170));
                            SpritePositionsOnSheet.Add(new Vector2(320, 170));
                            SpritePositionsOnSheet.Add(new Vector2(390, 170));
                            SpritePositionsOnSheet.Add(new Vector2(450, 170));
                            SpritePositionsOnSheet.Add(new Vector2(520, 170));
                            SpritePositionsOnSheet.Add(new Vector2(585, 170));
                            SpritePositionsOnSheet.Add(new Vector2(650, 170));
                            SpritePositionsOnSheet.Add(new Vector2(720, 170));
                            SpritePositionsOnSheet.Add(new Vector2(785, 170));
                            SpritePositionsOnSheet.Add(new Vector2(851, 170));
                            SpritePositionsOnSheet.Add(new Vector2(913, 170));
                            SpritePositionsOnSheet.Add(new Vector2(975, 170));


                            totalTimeUntilDisappears = 2000;
                        }
                        else if (u.flying)
                        {
                            cadavreTextureSheet = Game1.projectileTexture; // which has also the explosion texture

                            spriteSize = 40;
                            SpritePositionsOnSheet.Add(new Vector2(9, 81));
                            SpritePositionsOnSheet.Add(new Vector2(39, 80));
                            SpritePositionsOnSheet.Add(new Vector2(77, 78));
                            SpritePositionsOnSheet.Add(new Vector2(117, 80));

                            totalTimeUntilDisappears = 1000;
                        }
                        else if (u.type == UnitType.Mage)
                        {
                            spriteSize = 55;
                            cadavreTextureSheet = Game1.projectileTexture; // which has also the explosion texture
                            SpritePositionsOnSheet.Add(new Vector2(328, 300));
                            SpritePositionsOnSheet.Add(new Vector2(371, 300));
                            SpritePositionsOnSheet.Add(new Vector2(414, 300));
                            SpritePositionsOnSheet.Add(new Vector2(468, 300));
                            SpritePositionsOnSheet.Add(new Vector2(530, 300));
                            SpritePositionsOnSheet.Add(new Vector2(590, 300));

                            SpritePositionsOnSheet.Add(new Vector2(9, 307));



                            totalTimeUntilDisappears = 1000;
                        }
                        else if (u.sailing)
                        {
                            cadavreTextureSheet = Game1.corpsesTexture;

                            spriteSize = 66;
                            SpritePositionsOnSheet.Add(new Vector2(38, 250));
                        }
                        else// all
                        {
                            cadavreTextureSheet = Game1.corpsesTexture;
                            spriteSize = 42;
                            SpritePositionsOnSheet.Add(new Vector2(78, 9));
                            SpritePositionsOnSheet.Add(new Vector2(83, 109));
                            SpritePositionsOnSheet.Add(new Vector2(85, 190));
                            // TODO : do all
                        }

                    }
                    innerTimer = 0;
                    timeLeftUntilDisappears = totalTimeUntilDisappears;
                    spriteSwitchInterval = totalTimeUntilDisappears / SpritePositionsOnSheet.Count;
                    spritePositionOnSheet = SpritePositionsOnSheet[0];
                }

                public void Draw(SpriteBatch spbatch)
                {

                    spbatch.Draw(cadavreTextureSheet, new Vector2(spritePositionOnMap.X, spritePositionOnMap.Y), new Rectangle((int)spritePositionOnSheet.X, (int)spritePositionOnSheet.Y, spriteSize, spriteSize), Color.Wheat);

                }

                public void Update(GameTime gameTime)
                {
          

                    innerTimer += gameTime.ElapsedGameTime.Milliseconds; ;
                    timeLeftUntilDisappears -= gameTime.ElapsedGameTime.Milliseconds;
                    if (innerTimer / spriteSwitchInterval >= 1)
                    {
                        innerTimer = 0;
                        spriteNumber++;
                        if (spriteNumber < SpritePositionsOnSheet.Count)
                            spritePositionOnSheet = SpritePositionsOnSheet[spriteNumber];
                    }
            
        
                }*/
        #endregion


    
}
