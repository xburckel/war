﻿using EpPathFinding;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.PathFindingRelated
{

    public class ComputedPath
    {
        public Vector2 startingPos;
        public Vector2 destinationPos;
        public List<GridPos> result;
        public int gameTimeRequestStart;

        public ComputedPath(Vector2 startingPos, Vector2 destinationPos, List<GridPos> result, int gameTimeRequestStart)
        {
            this.startingPos = startingPos;
            this.destinationPos = destinationPos;
            this.result = result;
            this.gameTimeRequestStart = gameTimeRequestStart;
        }

    }

    public class CacheComputedPaths
    {
        public List<ComputedPath> computedPaths;
        public int gameTimeMillisec; // a save of the current game time at the moment of creation of the object

        public CacheComputedPaths()
        {
            computedPaths = new List<ComputedPath>();
        }

        /**
         * Updates the saved game time for later use and removes too old paths
         * */
        public void Update(GameTime gameTime)
        {
            int currentGameTime =  (gameTime.TotalGameTime.Milliseconds + gameTime.TotalGameTime.Seconds * 1000 + gameTime.TotalGameTime.Minutes * 60 * 1000
                + gameTime.TotalGameTime.Hours * 60 * 60 * 1000);
            
            lock (computedPaths)
            {
                computedPaths.RemoveAll(x => x.gameTimeRequestStart + 10000 < currentGameTime
                   ); // after 10 sec removed cached path
            }
            gameTimeMillisec = currentGameTime;
        }

        /**
         * Returns the path from pos to dest if found
         * Returns null if not found
         * */
        public List<GridPos> GetPath(Vector2 position, Vector2 destination)
        {
            try
            {
                lock (computedPaths)
                {
                    List<ComputedPath> positions = (computedPaths.Where(x => x.startingPos == position && x.destinationPos == destination)).ToList();

                    if (positions.Count > 0)
                        return positions.First().result;
                    return null;
                }
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }
        /**
         * Adds a computed path to the list
         */
        public void AddPath(Vector2 startPos, Vector2 dest, List<GridPos> result)
        {
            lock (computedPaths)
            {
                computedPaths.Add(new ComputedPath(startPos, dest, result, gameTimeMillisec));
            }
        }


    }
}
