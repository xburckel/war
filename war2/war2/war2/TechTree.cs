﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2
{
    [Serializable]
    public class TechTree
    {
        private Dictionary<UnitType, bool> isUnitAvailable;

        public bool hasTech(UnitType ut)
        {
            if (ut == UnitType.None)
                return false;
            else
                return isUnitAvailable[ut];
        }

        /**
         * Returns the list of ALL buildings that COULD BE on the base build panel
         */
        public static List<UnitType> GetBasePanelBuildings()
        {
            return new List<UnitType>()
            {
                UnitType.LumberMill, UnitType.TownHall, UnitType.Farm, UnitType.Tower, UnitType.MilitaryBase, UnitType.Forge
            };
        }
        /**
         * Returns the list of ALL buildings that COULD BE on the advances build panel
         */
        public static List<UnitType> GetAdvancedPanelBuildings()
        {
            return new List<UnitType>()
            {
                UnitType.Stable, UnitType.GoblinWorkBench, UnitType.Church, UnitType.MageTower, UnitType.GryffinTower, UnitType.Raffinery, UnitType.Foundry, UnitType.NavalChantier
            };
        }
        /**
         * Returns the list of ALL buildings that COULD BE on the tankers build panel
         */
        public static List<UnitType> GetTankerPanelBuildings()
        {
            return new List<UnitType>()
            {
                UnitType.NavalPlatform
            };
        }

        public TechTree()
        {
            isUnitAvailable = new Dictionary<UnitType,bool>();
            
            // basic stuff allways available

            isUnitAvailable.Add(UnitType.Paysan, true);
            isUnitAvailable.Add(UnitType.Fantassin, true);
            isUnitAvailable.Add(UnitType.Farm, true);
            isUnitAvailable.Add(UnitType.TownHall, true);
            isUnitAvailable.Add(UnitType.Petrolier, true);
            isUnitAvailable.Add(UnitType.Mage, true);
            isUnitAvailable.Add(UnitType.MilitaryBase, true);

            // other stuff
            isUnitAvailable.Add(UnitType.Archer, false);
            isUnitAvailable.Add(UnitType.BallistaTower, false);
            isUnitAvailable.Add(UnitType.Battleship, false);
            isUnitAvailable.Add(UnitType.CannonTower, false);
            isUnitAvailable.Add(UnitType.Catapulte, false);
            isUnitAvailable.Add(UnitType.Chevalier, false);
            isUnitAvailable.Add(UnitType.Church, false);
            isUnitAvailable.Add(UnitType.Destroyer, false);
            isUnitAvailable.Add(UnitType.Forge, false);
            isUnitAvailable.Add(UnitType.Fortress, false);
            isUnitAvailable.Add(UnitType.Foundry, false);
            isUnitAvailable.Add(UnitType.GoblinWorkBench, false);
            isUnitAvailable.Add(UnitType.Griffon, false);
            isUnitAvailable.Add(UnitType.GryffinTower, false);
            isUnitAvailable.Add(UnitType.LumberMill, false);
            isUnitAvailable.Add(UnitType.MachineVolante, false);
            isUnitAvailable.Add(UnitType.MageTower, false);
            isUnitAvailable.Add(UnitType.Nains, false);
            isUnitAvailable.Add(UnitType.NavalChantier, false);
            isUnitAvailable.Add(UnitType.NavalPlatform, false);
            isUnitAvailable.Add(UnitType.None, false);
            isUnitAvailable.Add(UnitType.Paladin, false);
            isUnitAvailable.Add(UnitType.Raffinery, false);
            isUnitAvailable.Add(UnitType.Sousmarin, false);
            isUnitAvailable.Add(UnitType.Stable, false);
            isUnitAvailable.Add(UnitType.StrongHold, false);
            isUnitAvailable.Add(UnitType.Tower, false);
            isUnitAvailable.Add(UnitType.Transport, false);
        }

        public void Update(int playerNumber, List<Unit> allUnits)
        {
            isUnitAvailable[UnitType.Paysan] = true;
            isUnitAvailable[UnitType.Fantassin] = true;
            isUnitAvailable[UnitType.Farm] = true;
            isUnitAvailable[UnitType.MilitaryBase] = true;
            isUnitAvailable[UnitType.TownHall] = true;
            isUnitAvailable[UnitType.Petrolier] = true;

            // other stuff
            isUnitAvailable[UnitType.Archer] = false;
            isUnitAvailable[UnitType.BallistaTower] = false;
            isUnitAvailable[UnitType.Battleship] = false;
            isUnitAvailable[UnitType.CannonTower] = false;
            isUnitAvailable[UnitType.Catapulte] = false;
            isUnitAvailable[UnitType.Chevalier] = false;
            isUnitAvailable[UnitType.Church] = false;
            isUnitAvailable[UnitType.Destroyer] = false;
            isUnitAvailable[UnitType.Forge] = false;
            isUnitAvailable[UnitType.Fortress] = false;
            isUnitAvailable[UnitType.Foundry] = false;
            isUnitAvailable[UnitType.GoblinWorkBench] = false;
            isUnitAvailable[UnitType.Griffon] = false;
            isUnitAvailable[UnitType.GryffinTower] = false;
            isUnitAvailable[UnitType.LumberMill] = false;
            isUnitAvailable[UnitType.MachineVolante] = false;
            isUnitAvailable[UnitType.Mage] = false;
            isUnitAvailable[UnitType.MageTower] = false;
            isUnitAvailable[UnitType.Nains] = false;
            isUnitAvailable[UnitType.NavalChantier] = false;
            isUnitAvailable[UnitType.NavalPlatform] = false;
            isUnitAvailable[UnitType.None] = false;
            isUnitAvailable[UnitType.Paladin] = false;
            isUnitAvailable[UnitType.Raffinery] = false;
            isUnitAvailable[UnitType.Sousmarin] = false;
            isUnitAvailable[UnitType.Stable] = false;
            isUnitAvailable[UnitType.StrongHold] = false;
            isUnitAvailable[UnitType.Tower] = false;
            isUnitAvailable[UnitType.Transport] = false;

            foreach (Unit u in allUnits)
            {
                if (u.owner == playerNumber)
                {
                    if ((int)u.type >= Unit.littlebuildingindex && ((Building)u).isBuilt())// TODO
                    {
                        switch (u.type)
                        {
                            case (UnitType.StrongHold):
                                isUnitAvailable[UnitType.Stable] = true;
                                isUnitAvailable[UnitType.GoblinWorkBench] = true;
                                isUnitAvailable[UnitType.MilitaryBase] = true;
                                isUnitAvailable[UnitType.LumberMill] = true;
                                isUnitAvailable[UnitType.Forge] = true;
                                isUnitAvailable[UnitType.Tower] = true;

                                break;

                            case (UnitType.Church):


                                break;


                            case (UnitType.Forge):
                                isUnitAvailable[UnitType.CannonTower] = true;
                                isUnitAvailable[UnitType.Catapulte] = true;

                                break;


                            case (UnitType.Fortress):
                                isUnitAvailable[UnitType.Stable] = true;
                                isUnitAvailable[UnitType.GoblinWorkBench] = true;
                                isUnitAvailable[UnitType.MilitaryBase] = true;
                                isUnitAvailable[UnitType.LumberMill] = true;
                                isUnitAvailable[UnitType.Forge] = true;
                                isUnitAvailable[UnitType.Tower] = true;
                                isUnitAvailable[UnitType.Church] = true;
                                isUnitAvailable[UnitType.MageTower] = true;
                                isUnitAvailable[UnitType.GryffinTower] = true;
                                break;


                            case (UnitType.Foundry):
                                isUnitAvailable[UnitType.Battleship] = true;
                                isUnitAvailable[UnitType.Transport] = true;

                                break;


                            case (UnitType.GoblinWorkBench):
                                isUnitAvailable[UnitType.Sousmarin] = true;
                                isUnitAvailable[UnitType.Nains] = true;
                                isUnitAvailable[UnitType.MachineVolante] = true;

                                break;


                            case (UnitType.GryffinTower):
                                isUnitAvailable[UnitType.Griffon] = true;


                                break;


                            case (UnitType.LumberMill):
                                isUnitAvailable[UnitType.BallistaTower] = true;
                                isUnitAvailable[UnitType.Archer] = true;
                                isUnitAvailable[UnitType.Destroyer] = true;
                                isUnitAvailable[UnitType.NavalChantier] = true;


                                break;


                            case (UnitType.MageTower):
                                isUnitAvailable[UnitType.Mage] = true;


                                break;

                            case (UnitType.MilitaryBase):
                                isUnitAvailable[UnitType.StrongHold] = true;
                                break;


                            case (UnitType.NavalChantier):
                                isUnitAvailable[UnitType.NavalPlatform] = true;
                                isUnitAvailable[UnitType.Raffinery] = true;
                                isUnitAvailable[UnitType.Foundry] = true;


                                break;


                            case (UnitType.Stable):
                                isUnitAvailable[UnitType.Paladin] = true;
                                isUnitAvailable[UnitType.Chevalier] = true;
                                isUnitAvailable[UnitType.Fortress] = true;


                                break;


                            case (UnitType.TownHall):
                                isUnitAvailable[UnitType.MilitaryBase] = true;
                                isUnitAvailable[UnitType.LumberMill] = true;
                                isUnitAvailable[UnitType.Forge] = true;
                                isUnitAvailable[UnitType.Tower] = true;

                                break;




                        }

                    }




                }


            }



        }



    }
}
