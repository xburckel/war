﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2
{
    [Serializable]
    public class TopPanel
    {
        Texture2D menuTexture;
        Texture2D minimapOpenTexture;
        Texture2D minimapCloseTexture;
        public static SpriteFont font;
        public int gold;
        public int wood;
        public int oil;
        public int food;
        public int usedFood;
        public int minimapHideButtonXPos;
        public int minimapShowButtonXPos;


        public TopPanel(ContentManager content)
        {
            gold = 2000;
            wood = 1000;
            oil = 1000;
            food = 0;

            menuTexture = content.Load < Texture2D > ("topmenupanel");
            minimapCloseTexture = content.Load<Texture2D>("Misc\\minimapclose");
            minimapOpenTexture = content.Load<Texture2D>("Misc\\minimapopen");
            minimapShowButtonXPos = 172;
            minimapHideButtonXPos = 250;

            font = content.Load<SpriteFont>("war2font");

        }

        public void Update(int gold, int wood, int oil, int food, int usedFood)
        {
            this.gold = gold;
            this.wood = wood;
            this.oil = oil;
            this.food = food;
            this.usedFood = usedFood;
        }

        public void Draw(SpriteBatch spbatch, GraphicsDevice graphics)
        {
            spbatch.Begin();
            if (Game1.map.minimapHidden)
            {
                spbatch.Draw(menuTexture, new Vector2(0, 0), new Microsoft.Xna.Framework.Rectangle(0, 0, 100, 14), Color.White);
                spbatch.Draw(menuTexture, new Vector2(100, 0), new Microsoft.Xna.Framework.Rectangle(0, 0, 100, 14), Color.White);
                spbatch.Draw(menuTexture, new Vector2(200, 0), new Microsoft.Xna.Framework.Rectangle(0, 0, 100, 14), Color.White);
                spbatch.Draw(menuTexture, new Vector2(250, 0), new Microsoft.Xna.Framework.Rectangle(0, 0, graphics.Viewport.Width, 14), Color.White);
                spbatch.DrawString(font, gold.ToString(), new Vector2(380, 0), Color.Gold);
                spbatch.DrawString(font, wood.ToString(), new Vector2(650, 0), Color.Gold);
                spbatch.DrawString(font, oil.ToString(), new Vector2(930, 0), Color.Gold);
                spbatch.DrawString(font, usedFood.ToString() + " / " + food.ToString(), new Vector2(1200, 0), Color.Gold);
                spbatch.Draw(minimapOpenTexture, new Vector2(170, 0), new Microsoft.Xna.Framework.Rectangle(0, 0, 25, 25), Color.White);
            }
            else
            {
                spbatch.Draw(menuTexture, new Vector2(250, 0), new Microsoft.Xna.Framework.Rectangle(0, 0, graphics.Viewport.Width, 14), Color.White);
                spbatch.DrawString(font, gold.ToString(), new Vector2(380, 0), Color.Gold);
                spbatch.DrawString(font, wood.ToString(), new Vector2(650, 0), Color.Gold);
                spbatch.DrawString(font, oil.ToString(), new Vector2(930, 0), Color.Gold);
                spbatch.DrawString(font, usedFood.ToString() + " / " + food.ToString(), new Vector2(1200, 0), Color.Gold);
                spbatch.Draw(minimapCloseTexture, new Vector2(250, 0), new Microsoft.Xna.Framework.Rectangle(0, 0, 25, 25), Color.White);

            }

            Game1.statics.menuItem.DrawMenu(graphics.Viewport.Width - 200, 0, "Menu (F10)", () => { Game1.gmenu.MenuPressed();}, spbatch, false, 50, 15);

            spbatch.End();

        }




    }
}
