﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.UnitsRelated
{
    [Serializable]
    public class Demolisher : Unit
    {
        public bool demolish;

        public Demolisher (int hp, int mana, float speed,
         int damage,
         bool flying,
         bool sailing,
         int goldCost,
         int woodCost,
         int oilCost,
         int range,
         int sight,
         Vector2 position,
        int buildtime,
            UnitType type,
        int armor, int owner)
            : base(hp, mana, speed,
                damage,
                flying,
                sailing,
                goldCost,
                woodCost,
                oilCost,
                range,
                sight,
                position,
                buildtime,
                type,
                armor, owner)
        {
            demolish = false;
        }

        public void DemolishRequest()
        {
              if (owner == Game1.currentPLayer) // network
              {
                  DwarvesDemolish dd = new DwarvesDemolish();
                  dd.dwarfindex = index;
                  dd.dwarfposition = position;
                  Game1.map.players[Game1.currentPLayer].playerMoves.dwarvesDemolish.Add(dd);
              }
            demolish = true;
        }




        public void Demolish()
        {

            foreach (Unit u in Game1.map.units)
            {
                if ((Vector2.Distance(this.position * 32, u.position * 32 + (u.spriteSize / 2)) <= 3.5 * 32) && u.disabled == 0)
                    u.hp -= 250; // KABOOM
            }
            foreach (Resource r in Game1.map.resources)
            {
                if (r != null)
                {
                    if (Vector2.Distance(this.position, r.Position) <= 3)
                        if (r.type == ResourceType.Wood)
                        {
                            r.amount = 0;

                        }

                }
            }


            for (int i = 0; i < Map.mapsize; i++)
                for (int j = 0; j < Map.mapsize; j++)
                    if (Game1.map.array[i, j] == MapElement.Rock && Vector2.Distance(this.position, new Vector2(i, j)) <= 3)
                    {
                        Game1.map.mapTextures[i, j].X = 14; // texture for cutted rocks
                        Game1.map.mapTextures[i, j].Y = 8;
                        Game1.map.array[i, j] = MapElement.Mud;
                        Game1.map.mapAreas.SetPosReachableGround(i, j);
                    }



            Animation.AddAnimation(AnimationType.Explosion, this.position * 32, true); // so we have a clean explosion animtion
            this.hp = -1;

        }

        public override void Update(Map map, GameTime gameTime)
        {
            base.Update(map, gameTime);

            if (demolish)
            {
                    Demolish();

            }

        }

        public override void DrawLeftPanelIcons(SpriteBatch spriteBatch)
        {
            Game1.leftPanel.DrawDemolishButton(Game1.map.players[this.owner].isOrc);

        }


    }
}
