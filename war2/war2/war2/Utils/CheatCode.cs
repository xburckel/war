﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace war2.Utils
{
    public class CheatCode
    {
        List<string> codes;

        public CheatCode()
        {
            codes = new List<string>();
            codes.Add("on screen");
            codes.Add("greedisgood");
            codes.Add("hatchet");
            codes.Add("gofast");
            codes.Add("dontkillme");
            codes.Add("debug");

        }

        public bool isACheatCode(string text)
        {
            return codes.Contains(text);
        }

        public void applyCheat(string text)
        {
            switch (text)
            {
                case "debug":
                    Game1.statics.debug = !Game1.statics.debug;
                    break;
                case "dontkillme":
                    Game1.statics.invulnerableCheatcodeOn = !Game1.statics.invulnerableCheatcodeOn;
                    break;
                case "on screen":
                    Game1.statics.fogOfWarEnabled = !Game1.statics.fogOfWarEnabled;
                    break;
                case "greedisgood":
                    foreach (Player p in Game1.map.players)
                    {
                        p.gold += 10000;
                        p.wood += 5000;
                        p.oil += 5000;
                    }
                    break;
                case "hatchet":
                    foreach (Resource r in Game1.map.resources)
                    {
                        if (r != null && r.type == ResourceType.Wood)
                            r.harvestTime = 2;
                    }
                    break;
                case "gofast":

                    Type type = typeof(UnitProperties); // MyClass is static class with static properties
                    foreach (var p in type.GetFields( System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public))
                    {
                        if (p.Name.EndsWith("_BUILD_TIME"))
                            p.SetValue(p, 1);
                    }
                    /*
                    UnitProperties.ARCHER_BUILD_TIME = 1;
                    UnitProperties.TOWNHALL_BUILD_TIME = 1;
                    UnitProperties.FARM_BUILD_TIME = 1;
                    UnitProperties.MILITARYBASE_BUILD_TIME = 1;
                    UnitProperties.LUMBERMILL_BUILD_TIME = 1;
                    UnitProperties.TOWER_BUILD_TIME = 1;
                    UnitProperties.FORGE_BUILD_TIME = 1;
                    UnitProperties.STABLE_BUILD_TIME = 1;
                    UnitProperties.NAVALPLATFORM_BUILD_TIME = 1;
                    UnitProperties.CHANTIERNAVAL_BUILD_TIME = 1;
                    UnitProperties.FOUNDRY_BUILD_TIME = 1;
                    UnitProperties.RAFINNERY_BUILD_TIME = 1;
                    UnitProperties.OILTANKER_BUILD_TIME = 1;
                    UnitProperties.FANTASSIN_BUILD_TIME = 1;
                    UnitProperties.BALLISTATOWER_BUILD_TIME = 1;
                    UnitProperties.CANNONTOWER_BUILD_TIME = 1;
                    UnitProperties.KNIGHT_BUILD_TIME = 1;
                    UnitProperties.PALADIN_BUILD_TIME = 1;
                    UnitProperties.PAYSAN_BUILD_TIME = 1;*/

                    break;
            }
        }



    }
}
