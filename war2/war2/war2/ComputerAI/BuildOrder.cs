﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.ComputerAI;
using war2.UnitsRelated;

namespace war2.AI
{
    // 
    /* 
     * Get 1 building, one harvester, one unit type per round
     */

    class BuildOrder
    {
        public List<Step> steps;
        private List<Unit> units;
        private List<Building> playerBuildings;
        private int usedFood;
        private int potentialFood;
        private int owner;

        bool HasTownHall = false;
        bool HasMilitaryBase = false;
        bool HasLumberMill = false;
        bool hasTierTwo = false;
        bool hasTierThree = false;
        bool hasForge = false;
        bool hasStables = false;
        bool hasMageTower = false;
        bool hasChurch = false;
        bool hasGryffonsLair = false;

        public BuildOrder(IA.AIType AItype, int owner)
        {


            this.owner = owner;
            steps = new List<Step>();
            units = new List<Unit>();
            playerBuildings = new List<Building>();
            steps.Add(new Step(UnitType.TownHall, 1, false, owner));
            steps.Add(new Step(UnitType.MilitaryBase, 1, false, owner));
            steps.Add(new Step(UnitType.LumberMill, 1, false, owner));
            steps.Add(new Step(UnitType.Forge, 1, false, owner));
            steps.Add(new Step(UnitType.StrongHold, 1, false, owner));

            steps.Add(new Step(UnitType.Stable, 1, false, owner));
            steps.Add(new Step(UnitType.MilitaryBase, 3, false, owner));
            steps.Add(new Step(UnitType.Fortress, 1, false, owner));
            steps.Add(new Step(UnitType.MilitaryBase, 4, false, owner));

            steps.Add(new Step(UnitType.Paysan, 12, false, owner));
            steps.Add(new Step(UnitType.Fantassin, 6, true, owner));

            steps.Add(new Step(UnitType.Archer, 3, false, owner));
            steps.Add(new Step(UnitType.Catapulte, 1, false, owner));

            steps.Add(new Step(UnitType.Paysan, 20, false, owner));
            steps.Add(new Step(UnitType.Chevalier, 12, false, owner));
            steps.Add(new Step(UnitType.Archer, 6, false, owner));
            steps.Add(new Step(UnitType.Catapulte, 3, false, owner));

            steps.Add(new Step(UnitType.Chevalier, 20, false, owner));
            steps.Add(new Step(UnitType.Catapulte, 5, false, owner));
            steps.Add(new Step(UnitType.Archer, 10, false, owner));


        }

        public void Update(List<Unit> playerUnits)
        {
            if (hasChurch) // useless for now but removes warning
                hasChurch = true;


            foreach (Step step in steps)
                step.Update(playerUnits);
            units = playerUnits;

            potentialFood = 0;
            usedFood = 0;

            HasTownHall = false;
            HasMilitaryBase = false;
            HasLumberMill = false;
            hasTierTwo = false;
            hasTierThree = false;
            hasForge = false;
            hasStables = false;
            hasMageTower = false;
            hasChurch = false;
            hasGryffonsLair = false;

            playerBuildings.Clear();
            foreach (Unit pu in playerUnits)
            {
                if (pu.isABuilding)
                    playerBuildings.Add((Building)pu);
                else
                    usedFood += 1;
                if (pu.type == UnitType.Farm)
                    potentialFood += 4;
                if (pu.type == UnitType.StrongHold || pu.type == UnitType.TownHall || pu.type == UnitType.Fortress)
                    potentialFood += 1;
               

            }

            foreach (Building b2 in playerBuildings)
            {
                if (b2.type == UnitType.TownHall || b2.type == UnitType.StrongHold || b2.type == UnitType.Fortress)
                    HasTownHall = true;

                if (b2.type == UnitType.StrongHold)
                    hasTierTwo = true;
                if (b2.type == UnitType.Fortress)
                    hasTierThree = true;

                if (b2.type == UnitType.MilitaryBase)
                {
                    HasMilitaryBase = true;
                }
                if (b2.type == UnitType.LumberMill)
                {
                    HasLumberMill = true;
                    if (b2.isBuilt() && b2.sa == null && Game1.map.players[owner].playerAmeliorations.getDamageAmelioration(UnitType.Archer) < UnitProperties.ARCHER_MAX_AMELIORATION_DAMAGE)
                        b2.Ameliorate(0);
                }
                if (b2.type == UnitType.Forge)
                {
                    hasForge = true;
                    if (b2.isBuilt() && b2.sa == null && Game1.map.players[owner].playerAmeliorations.getDamageAmelioration(UnitType.Fantassin) < UnitProperties.FANTASSIN_MAX_AMELIORATION_DAMAGE)
                        b2.Ameliorate(0);
                    if (b2.isBuilt() && b2.sa == null && Game1.map.players[owner].playerAmeliorations.getArmorAmelioration(UnitType.Fantassin) < UnitProperties.FANTASSIN_MAX_AMELIORATION_ARMOR)
                        b2.Ameliorate(1);
                }
                if (b2.type == UnitType.Church)
                {
                    if (b2.isBuilt() && b2.sa == null && !Game1.map.players[owner].playerAmeliorations.hasPaladin)
                        b2.Ameliorate(0);
                    hasChurch = true;
                }
                if (b2.type == UnitType.Stable)
                    hasStables = true;
                if (b2.type == UnitType.MageTower)
                    hasMageTower = true;
                if (b2.type == UnitType.GryffinTower)
                    hasGryffonsLair = true;
            }
        }

            public bool mustBuildHarvester()
            {
                foreach (Step step in steps)
                {
                    if (step.getNextInBuildOrder() == UnitType.Paysan)
                    {
                        return true;
                    }
                }
                    return false;
            }

            public UnitType getNextBuildingToBuild()
            {
                if (usedFood >= potentialFood && (HasTownHall || hasTierTwo || hasTierThree))
                    return UnitType.Farm;

                foreach (Step step in steps)
                {
                    int tmp = (int) step.getNextInBuildOrder();
                    if (tmp >=  (int) UnitType.Farm)
                    {
                        return (UnitType) tmp;
                    }
                }

                return UnitType.None;
            }

            public UnitType getNextUnitToBuild()
            {
                foreach (Step step in steps)
                {
                    int tmp = (int)step.getNextInBuildOrder();
                    if (tmp < (int)UnitType.Farm && (UnitType) tmp != UnitType.Paysan)
                    {
                        switch ((UnitType) tmp)
                        {
                            case UnitType.Fantassin:
                                if (HasMilitaryBase)
                                    return UnitType.Fantassin;
                                break;
                            case UnitType.Archer:
                                if (HasMilitaryBase && HasLumberMill)
                                    return UnitType.Archer;
                                break;
                            case UnitType.Catapulte:
                                if (HasMilitaryBase && hasForge)
                                    return UnitType.Catapulte;
                                break;
                            case UnitType.Chevalier:
                                if (HasMilitaryBase && hasStables)
                                    return UnitType.Chevalier;
                                break;
                            case UnitType.Griffon:
                                if (hasGryffonsLair)
                                    return UnitType.Griffon;
                                break;
                            case UnitType.Mage:
                                if (hasMageTower)
                                    return UnitType.Mage;
                                break;
                        }
                    }
                }

                return UnitType.None;
            }
            
        
    }
}
