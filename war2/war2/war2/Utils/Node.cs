﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.Utils
{
    public class Node
    {
        public int posX;
        public int posY;
        public List<Node> neighbours;
        public bool visited;

        public Node(int posx, int posy)
        {
            this.posX = posx;
            this.posY = posy;
            neighbours = new List<Node>();
        }

        public void addNeighbour(Node node)
        {
            neighbours.Add(node);
        }
  
    }
}
