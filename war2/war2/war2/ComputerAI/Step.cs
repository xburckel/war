﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.ComputerAI
{
    class Step
    {
        int owner;
        bool complete;
        UnitType unitType;
        int numberOfUnits;
        int numberDoneAlready;
        bool oneTimer;



        public Step(UnitType unitType, int numberOfUnits, bool oneTimer, int owner)
        {
            numberDoneAlready = 0;
            this.complete = false;
            this.unitType = unitType;
            this.numberOfUnits = numberOfUnits;
            this.oneTimer = oneTimer;
            this.owner = owner;
        }

        public bool Update(List<Unit> playerUnits)
        {
            numberDoneAlready = 0;
            foreach (Unit u in playerUnits)
            {
                if (u.type == unitType && u.owner == owner)
                    numberDoneAlready++;
            }

            if (numberDoneAlready >= numberOfUnits)
            {
                complete = true;
                return true;
            }
            else if (complete && oneTimer)
                return true;
            else
                complete = false;

            return false;
        }

        public UnitType getNextInBuildOrder()
        {

            if (!complete)
                return unitType;
            else
                return UnitType.None;
        }

    }
}
