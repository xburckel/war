﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.Menus
{
    public static class LeftPanelUtils
    {
        /**
         * Returns like this :
         * 7 8 9
         * 4 5 6
         * 1 2 3
         */
        public static int MouseOnLeftPanelIcon(MouseState aMouse)
        {
            if ((aMouse.X > 10 && aMouse.X < 56 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 90 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 135))
                return 4;
            else if (aMouse.X > 48
      && aMouse.X < 98 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 90 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 135)
                return 5;
            else if ((aMouse.X > 10
       && aMouse.X < 56 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 45 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 90))
                return 1;
            else if ((aMouse.X > 48
    && aMouse.X < 98 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 45 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 90))
                return 2;
            else if ((aMouse.X > 100
    && aMouse.X < 145 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 45 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 90))
                return 3;
            else if ((aMouse.X > 56
&& aMouse.X < 98 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 135 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 180))
                return 8;
            else if ((aMouse.X > 10
&& aMouse.X < 56 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 135 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 180))
                return 7;
            if ((aMouse.X > 98
&& aMouse.X < 190 && aMouse.Y < Game1.graphics.GraphicsDevice.Viewport.Height - 135 && aMouse.Y > Game1.graphics.GraphicsDevice.Viewport.Height - 180))
                return 9;
            

            return 0;
        }
        /**
         * Interger representing position from 1 to 9 to selected unit type
         * Used for building (e.g. from a harvester build panel)
         */
        public static UnitType ToBuilding(int position, bool advancedPanel = false, bool tankerPanel = false)
        {
            switch (position)
            {
                case (1):
                    if (advancedPanel && tankerPanel)
                        return UnitType.NavalPlatform;
                    else if (advancedPanel)
                        return UnitType.NavalChantier;
                    else
                        return UnitType.TownHall;
                case (2):
                    if (advancedPanel)
                        return UnitType.Foundry;
                    else
                        return UnitType.Farm;
                case (3):
                    if (advancedPanel)
                        return UnitType.Raffinery;
                    else
                        return UnitType.MilitaryBase;
                case (4):
                    if (advancedPanel)
                        return UnitType.Stable;
                    else
                        return UnitType.LumberMill;
                case (5):
                    if (advancedPanel)
                        return UnitType.GoblinWorkBench;
                    else
                        return UnitType.Forge;
                case (6):
                        return UnitType.None;
                case (7):
                    if (advancedPanel)
                        return UnitType.Church;
                    else
                        return UnitType.Tower;
                case (8):
                    if (advancedPanel)
                        return UnitType.MageTower;
                    break;
                case (9):
                    if (advancedPanel)
                        return UnitType.GryffinTower;
                    break;
            }
            return UnitType.None;
        }

        public static UnitType ToUnit(int position, Unit selectedBuilding)
        {
            return UnitType.None;
        }



    }
}
