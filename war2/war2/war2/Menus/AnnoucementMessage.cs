﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.Menus
{
    public class AnnoucementMessage : Message
    {
        public AnnoucementMessage(string text)
            : base(text)
        {
        }

        public override void Draw(SpriteBatch spbatch)
        {
            if (!Game1.isInMainMenu && !Game1.isInMenu)
            {
                if (duration > 0)
                    spbatch.DrawString(TopPanel.font, text, new Vector2(LeftPanel.LEFTPANEL_WIDTH + 100, Game1.graphics.PreferredBackBufferHeight - 50), Color.White);
            }
        }

        public override void Update(GameTime gametime)
        {
            if (duration > 0)
                duration -= gametime.ElapsedGameTime.Milliseconds;
        }


    }
}
