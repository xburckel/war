﻿using EpPathFinding;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using war2.UnitsRelated;

namespace war2.Utils
{
    class Utils
    {
        // return Int32.maxvalue if no path found
        public static float getTrueWalkingDistance(Vector2 position, Vector2 destination, List<Vector2> forcewalkable)
        {
            Unit tempunit = new Unit();
            List<GridPos> temp = tempunit.CalculatePath(position, destination, Game1.map, forcewalkable);
            float distance = 0;
            if (temp.Count > 0)
            {
                GridPos current = temp.First();
                foreach (GridPos p in temp)
                {
                    distance += Vector2.Distance(new Vector2(p.x, p.y), new Vector2(current.x, current.y));
                    current = p;
                }
                return distance;
            }
            else
                return Int32.MaxValue;
        }

        public static void dealSplashDamage(Vector2 position, int areaSize, int damage)
        {

            foreach (Unit u in Game1.map.units)
            {
                Rectangle rect = new Rectangle((int) (u.position.X * 32), (int) (u.position.Y * 32), (int) u.spriteSize.X, (int) u.spriteSize.Y);
                Rectangle rect2 = new Rectangle((int)(position.X * 32), (int)(position.Y * 32), areaSize, areaSize);

                if (rect.Intersects(rect2))
                    u.hp -= Math.Abs(damage - u.armor) * (Game1.statics.invulnerableCheatcodeOn ? 0 : 1);
            }
        }


        public static Texture2D CreateCircle(int radius)
        {
            Texture2D texture = new Texture2D(Game1.graphics.GraphicsDevice, radius, radius);
            Color[] colorData = new Color[radius * radius];

            float diam = radius / 2f;
            float diamsq = diam * diam;

            for (int x = 0; x < radius; x++)
            {
                for (int y = 0; y < radius; y++)
                {
                    int index = x * radius + y;
                    Vector2 pos = new Vector2(x - diam, y - diam);
                    if (pos.LengthSquared() <= diamsq)
                    {
                        colorData[index] = Color.White;
                    }
                    else
                    {
                        colorData[index] = Color.Transparent;
                    }
                }
            }

            texture.SetData(colorData);
            return texture;
        }
        public static Vector2 getTrueUnitCenter(Unit u)
        {
            return new Vector2(u.position.X * 32 + u.spriteSize.X / 2, u.position.Y * 32 + u.spriteSize.Y / 2);

        }

        public static Tuple<Orientation, bool> getOrientation(Vector2 positionOnMap, Vector2 targetPosition)
        {
            Orientation orientation = Orientation.Bottom;
            Vector2 direction = (targetPosition - positionOnMap);
            bool flip = false;

            if (Math.Abs(direction.X) < 0.4)
                direction.X = 0;
            if (Math.Abs(direction.Y) < 0.4)
                direction.Y = 0;

            if (direction.X < 0 && direction.Y < 0) // northwest
            {

                flip = true;
                orientation = Orientation.TopRight;

            }
            else if (direction.X > 0 && direction.Y > 0) // southeast
            {
                //rotation = MathHelper.PiOver2 + MathHelper.PiOver4;
                orientation = Orientation.BottomRight;
            }
            else if (direction.X > 0 && direction.Y < 0) // northeast
            {
                orientation = Orientation.TopRight;

            }
            else if (direction.X < 0 && direction.Y > 0) // southwest
            {
                flip = true;
                orientation = Orientation.BottomRight;
            }
            else if (direction.X < 0) // west
            {
                flip = true;
                orientation = Orientation.Right;
            }
            else if (direction.X > 0) // east
            {
                orientation = Orientation.Right;

            }
            else if (direction.Y > 0) // south
            {

                orientation = Orientation.Bottom;
            }
            else if (direction.Y < 0) // north
            {
                orientation = Orientation.Top;
            }
            return Tuple.Create(orientation, flip);
        }

        public static List<Vector2> getAllPositions(int sizex, int sizey, Vector2 startPos)
        {
             List<Vector2> tiles = new List<Vector2>();

             for (int i = 0; i < sizex; i++)
                 for (int j = 0; j < sizey; j++)
              {
                  if (Map.Inbounds((int) Math.Round(startPos.X + i), (int) Math.Round(startPos.Y + j)))
                      tiles.Add(new Vector2((int) Math.Round(startPos.X + i), (int) Math.Round(startPos.Y + j)));
              }
          return tiles;
        }

    }
}
