﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Timers;
using war2.UnitsRelated;

namespace war2
{
    public enum PacketTypes
    {
        LOGIN,
        STARTGAME,
        MOVE,
        GOOGTOGO,
        UPDATEUNITS,
        CONTINUE
    }



    public class Server // for an host
    {
        //static int variable; // timer

        //private int refreshtimer;
        //private int checkPacketsTimer;

        //public Dictionary<int, string> names;
        //static int counter = 0; // nb of packets received
        //public int sentcounter = 0; // nb of packets sent

        //static NetServer server;
        //// Configuration object
        //static NetPeerConfiguration Config;
        ////   IPEndPoint ipEndPoint;

        //public Dictionary<int, NetConnection> clientsnbs;


        //public Server()
        //{


        //    Game1.currentPLayer = 0;
        //    Game1.window.Title = "server";
        //    names = new Dictionary<int, string>();
        //    clientsnbs = new Dictionary<int, NetConnection>();
        //    names.Add(0, "Host");
        //    names.Add(1, "Client1");
        //    names.Add(2, "Client2");
        //    names.Add(3, "Client3");
        //    names.Add(4, "Client4");
        //    names.Add(5, "Client5");
        //    names.Add(6, "Client6");
        //    names.Add(7, "Client7");

        //    Game1.map.players = new List<Player>();

        //    Game1.map.players.Add(new Player(0, true, true, true));

        //    Config = new NetPeerConfiguration("game");

        //    // Set server port
        //    Config.Port = 14242;

        //    // Max client amount
        //    Config.MaximumConnections = 8;

        //    // Enable New messagetype. Explained later
        //    Config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);

        //    // Create new server based on the configs just defined
        //    server = new NetServer(Config);

        //    // Start it
        //    server.Start();

        //    // Eh..

        //    // Create list of "Characters" ( defined later in code ). This list holds the world state. Character positions
        //    //   List<Character> GameWorldState = new List<Character>();




        //    // Write to con..
        //    //   Console.WriteLine("Waiting for new connections and updateing world state to current ones");

        //}

        //public void Draw(SpriteBatch spbatch) // debug
        //{
        //    spbatch.Begin();
        //    spbatch.DrawString(Game1.statics.font, "packets received by server : " + server.Statistics.ReceivedPackets + "packets sent by server : " + server.Statistics.SentPackets, new Vector2(500, 500), Color.Green);
        //    spbatch.DrawString(Game1.statics.font, "messages sent by server : " + sentcounter + " messages received : " + Server.counter, new Vector2(500, 600), Color.Red);



        //    spbatch.End();
        //}

        //public void Disconnect()
        //{
        //    server.Shutdown("byebye");
        //}

        //public void StartGme()
        //{
        //    for (int i = Game1.map.players.Count; i < 8; i++) // fill non human Game1.map.players
        //    {
        //        Game1.map.players.Add(new Player(i, true));
        //    }
        //    Game1.map.Load("Maps.pud");

        //    SaveGameData save = new SaveGameData();

        //    save.resources = Game1.map.resources;
        //    save.units = Game1.map.units;


        //    save.players = Game1.map.players;



        //    // TODO
        //    foreach (Player p in Game1.map.players)
        //    {
        //        if (p.network)
        //        {
        //            if (p.number != 0) // not host
        //            {
        //                save.currentplayer = p.number;
        //                NetOutgoingMessage outmsg = server.CreateMessage();
        //                outmsg.Write((byte)PacketTypes.STARTGAME);
        //                byte[] data = Serialize(save);
        //                outmsg.Write(data.Length);

        //                outmsg.Write(data);
        //                server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.ReliableOrdered);
        //            }
        //        }
        //    }

        //    bool[] hasanswered = new bool[8];
        //    for (int i = 0; i < 8; i++)
        //    {
        //        hasanswered[i] = !Game1.map.players[i].network; // computers answer yesh by default
        //    }
        //    hasanswered[0] = true;// we are ready
        //    NetIncomingMessage inc = null;

        //    variable = 0;
        //    var timer = new Timer { Interval = 1000 }; // in milliseconds
        //    timer.Elapsed += (s, e) =>
        //    {
        //        if (++variable > 20)
        //            timer.Stop();
        //    };
        //    timer.Start();


        //    do
        //    {
        //        inc = server.ReadMessage();

        //        if (variable > 10)
        //            break;


        //        if (inc != null)
        //            foreach (Player p in Game1.map.players)
        //            {
        //                if (p.network && p.number != Game1.currentPLayer)
        //                {
        //                    if (inc != null)
        //                    {

        //                        if (clientsnbs[p.number].Equals(inc.SenderConnection))
        //                        {
        //                            if (inc.ReadByte() == (byte)PacketTypes.GOOGTOGO) // he loaded
        //                                hasanswered[p.number] = true;
        //                        }

        //                    }

        //                }
        //            }
        //    }
        //    while (!EveryoneReady(hasanswered));


        //    NetOutgoingMessage outmseg = server.CreateMessage();
        //    outmseg.Write((byte)PacketTypes.GOOGTOGO);
        //    server.SendMessage(outmseg, server.Connections, NetDeliveryMethod.ReliableOrdered, 0);




        //}




        //private bool EveryoneReady(bool[] hasanswered)
        //{
        //    for (int i = 0; i < 8; i++)
        //    {
        //        if (!hasanswered[i])
        //            return false;
        //    }

        //    return true;
        //}


        //public void Update(GameTime gameTime)
        //{
        //    NetIncomingMessage inc;
        //    // Object that can be used to store and read messages
        //    refreshtimer += gameTime.ElapsedGameTime.Milliseconds;
        //    checkPacketsTimer += gameTime.ElapsedGameTime.Milliseconds;

        //    // Check time
        //    DateTime time = DateTime.Now;

        //    // Create timespan of 30ms
        //    TimeSpan timetopass = new TimeSpan(0, 0, 0, 0, 30);

        //    /*
        //     *  TODO:
        //     *  receive moves
        //     * send moves
        //     * wait acknowlegde
        //     * send turn done
        //     */

        //    /*
        //    NetOutgoingMessage outmsg = server.CreateMessage();
        //    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //    outmsg.Write((byte)UpdateTypes.DwarvesDemolish);
        //    long tmp = 64;
        //    outmsg.Write(tmp);
        //    outmsg.Write((float) 1.5);
        //    outmsg.Write((float) 2.5);
        //    if (clientsnbs.Count > 0 && refreshtimer > 100)
        //        server.SendMessage(outmsg, clientsnbs[1], NetDeliveryMethod.ReliableOrdered);
        //    */

        //    while ((inc = server.ReadMessage()) != null)
        //    {
        //        ReceiveMoves(inc);
        //    }



        //       // refreshtimer = 0;
        //    if (refreshtimer > 50)
        //    {
        //        refreshtimer = 0;
        //        PlayerMoves mymoves = Game1.map.players[Game1.currentPLayer].playerMoves;

        //        mymoves.SendNetworkMessagesToHost(null, server);


        //        Game1.map.players[Game1.currentPLayer].playerMoves = new PlayerMoves(Game1.currentPLayer);

        //    }
            
        //}









        //private void ReceiveMoves(NetIncomingMessage inc)
        //{
        //    // Theres few different types of messages. To simplify this process, i left only 2 of em here
        //    switch (inc.MessageType)
        //    {
        //        // If incoming message is Request for connection approval
        //        // This is the very first packet/message that is sent from client
        //        // Here you can do new player initialisation stuff
        //        case NetIncomingMessageType.ConnectionApproval:

        //            // Read the first byte of the packet
        //            // ( Enums can be casted to bytes, so it be used to make bytes human readable )
        //            #region login
        //            if (inc.ReadByte() == (byte)PacketTypes.LOGIN)
        //            {
        //                // Approve clients connection ( Its sort of agreenment. "You can be my client and i will host you" )
        //                if (Game1.isInMainMenu)
        //                {
        //                    inc.SenderConnection.Approve();

        //                    // Init random
        //                    Random r = new Random();

        //                    // Add new character to the game.
        //                    // It adds new player to the list and stores name, ( that was sent from the client )
        //                    // Random x, y and stores client IP+Port
        //                    Game1.map.players.Add(new Player(Game1.map.players.Count, false, true, false));
        //                    clientsnbs.Add(Game1.map.players.Count - 1, inc.SenderConnection);


        //                    // first we write byte
        //                    // outmsg.Write((byte)PacketTypes.WORLDSTATE);

        //                    // then int
        //                    //  outmsg.Write(GameWorldState.Count);

        //                    // iterate trought every character ingame
        //                    //   foreach (Character ch in GameWorldState)
        //                    //    {
        //                    // This is handy method
        //                    // It writes all the properties of object to the packet
        //                    //       outmsg.WriteAllProperties(ch);
        //                    //    }

        //                    // Now, packet contains:
        //                    // Byte = packet type
        //                    // Int = how many Game1.map.players there is in game
        //                    // character object * how many Game1.map.players is in game

        //                    // Send message/packet to all connections, in reliably order, channel 0
        //                    // Reliably means, that each packet arrives in same order they were sent. Its slower than unreliable, but easyest to understand

        //                    // Debug
        //                    Console.WriteLine("Approved new connection and updated the world status");
        //                }
        //                else // refuse connections out of lobby
        //                {
        //                    inc.SenderConnection.Deny("not in lobby anymore");

        //                }
        //            }
        //            #endregion

        //            break;
        //        // Data type is all messages manually sent from client
        //        // ( Approval is automated process )
        //        case NetIncomingMessageType.Data:
        //            // Read first byte
        //            if (inc.LengthBytes >= 1)
        //            {
        //                int bytes = inc.ReadByte();
        //                if (bytes == (byte)PacketTypes.UPDATEUNITS)
        //                {
        //                    counter++;
        //                    // Check who sent the message
        //                    // This way we know, what character belongs to message sender
        //                    int i = 0;
        //                    for (i = 1; i < clientsnbs.Count; i++)
        //                    {
        //                        // If stored connection ( check approved message. We stored ip+port there, to character obj )
        //                        // Find the correct character
        //                        if (clientsnbs[i] != inc.SenderConnection)
        //                            continue;
        //                    }
        //                    UpdateTypes up = (UpdateTypes)inc.ReadByte();
        //                    switch (up)
        //                    {
        //                        case UpdateTypes.UnitPosUpdate:
        //                            long index = inc.ReadInt64();
        //                            Vector2 position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
        //                            Vector2 destination = new Vector2(inc.ReadFloat(), inc.ReadFloat());
        //                            Vector2 direction = new Vector2(inc.ReadFloat(), inc.ReadFloat());

        //                            int playernumber = 0;
        //                            foreach (Unit u in Game1.map.units)
        //                            {
        //                                if (u.index == index)
        //                                {
        //                                    u.destination = destination;
        //                                    u.position = position;
        //                                    u.direction = direction;
        //                                    playernumber = u.owner;
        //                                    break;
        //                                }
        //                            }
        //                            foreach (Player p in Game1.map.players)  //retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.UnitPosUpdate);
        //                                    outmsg.Write(index);
        //                                    outmsg.Write(position.X);
        //                                    outmsg.Write(position.Y);
        //                                    outmsg.Write(destination.X);
        //                                    outmsg.Write(destination.Y);
        //                                    outmsg.Write(direction.Y);
        //                                    outmsg.Write(direction.Y);

        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.UnreliableSequenced);
        //                                }
        //                            }
        //                            break;
        //                        case UpdateTypes.NewBuilding:
        //                            index = inc.ReadInt64();
        //                            position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
        //                            long builderIndex = inc.ReadInt64();
        //                            UnitType ut = (UnitType)inc.ReadInt32();
        //                            playernumber = 0;
        //                            foreach (Unit u in Game1.map.units)
        //                            {
        //                                if (u.index == builderIndex)
        //                                {
        //                                    playernumber = u.owner;
        //                                    Building buil = (Building)Game1.map.CreateUnit(ut, position, playernumber);
        //                                    buil.percentBuilt = 0;
        //                                    buil.builder = (Harvester)u;
        //                                    ((Harvester)u).constructingBuilding = buil;
        //                                    ((Harvester)u).position = buil.position;
        //                                    u.destination = buil.position;
        //                                    u.path.Clear();
        //                                    ((Harvester)u).buildingConstructPosition = buil.position;
        //                                    ((Harvester)u).BuildBuilding();
        //                                    Game1.map.newUnits.Last().index = index;

        //                                    break;
        //                                }
        //                            }
        //                            foreach (Player p in Game1.map.players)  //retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.NewBuilding);
        //                                    outmsg.Write(index);
        //                                    outmsg.Write(position.X);
        //                                    outmsg.Write(position.Y);
        //                                    outmsg.Write(builderIndex);
        //                                    outmsg.Write((int)ut);
        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.ReliableUnordered);
        //                                }
        //                            }
        //                            break;

        //                        case UpdateTypes.UnitAttack:
        //                            index = inc.ReadInt64();
        //                            position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
        //                            long targetIndex = inc.ReadInt64();
        //                            playernumber = 0;
        //                            foreach (Unit u in Game1.map.units)
        //                            {
        //                                if (u.index == index)
        //                                {
        //                                    u.position = position;
        //                                    playernumber = u.owner;
        //                                    foreach (Unit target in Game1.map.units)
        //                                    {
        //                                        if (target.index == targetIndex)
        //                                        {
        //                                            u.target = target;
        //                                            break;
        //                                        }
        //                                    }

        //                                    break;
        //                                }
        //                            }
        //                            foreach (Player p in Game1.map.players)  //retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.UnitAttack);
        //                                    outmsg.Write(index);
        //                                    outmsg.Write(position.X);
        //                                    outmsg.Write(position.Y);
        //                                    outmsg.Write(targetIndex);
        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.UnreliableSequenced);
        //                                }
        //                            }


        //                            break;
        //                        case UpdateTypes.DeadUnits:
        //                            playernumber = 0;
        //                            index = inc.ReadInt64();
        //                            foreach (Unit u in Game1.map.units)
        //                            {
        //                                if (u.index == index)
        //                                {
        //                                    playernumber = u.owner;
        //                                    u.hp = -1; // kill it
        //                                    break;
        //                                }
        //                            }

        //                            foreach (Player p in Game1.map.players)  //retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.DeadUnits);
        //                                    outmsg.Write(index);
        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.UnreliableSequenced);
        //                                }
        //                            }

        //                            break;

        //                        case UpdateTypes.UnitUpgrade:
        //                            index = inc.ReadInt64();
        //                            ut = (UnitType)inc.ReadByte();
        //                            playernumber = 0;
        //                            foreach (Unit u in Game1.map.units)
        //                            {
        //                                if (u.index == index)
        //                                {
        //                                    ((Building)u).upgradingto = ut;
        //                                    ((Building)u).Upgrade(ut);
        //                                    playernumber = u.owner;

        //                                    break;
        //                                }
        //                            }
        //                            foreach (Player p in Game1.map.players)  //retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.UnitUpgrade);
        //                                    outmsg.Write(index);
        //                                    outmsg.Write((byte)ut);

        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.UnreliableSequenced);
        //                                }
        //                            }
        //                            break;
        //                        case UpdateTypes.DwarvesDemolish:
        //                            index = inc.ReadInt64();
        //                            position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
        //                            playernumber = 0;
        //                            foreach (Unit u in Game1.map.units)
        //                            {
        //                                if (u.index == index)
        //                                {
        //                                    u.position = position;
        //                                    ((Demolisher)u).DemolishRequest();
        //                                    playernumber = u.owner;

        //                                    break;
        //                                }
        //                            }

        //                            foreach (Player p in Game1.map.players) // retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.DwarvesDemolish);
        //                                    outmsg.Write(index);
        //                                    outmsg.Write((byte)position.X);
        //                                    outmsg.Write((byte)position.Y);

        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.UnreliableSequenced);
        //                                }
        //                            }


        //                            break;
        //                        case UpdateTypes.TransportLoadRequest:
        //                            index = inc.ReadInt64();
        //                            long unitindex = inc.ReadInt64();
        //                            playernumber = 0;
        //                            position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
        //                            foreach (Unit u in Game1.map.units)
        //                            {
        //                                if (u.index == index)
        //                                {
        //                                    playernumber = u.owner;
        //                                    foreach (Unit u2 in Game1.map.units)
        //                                    {
        //                                        if (u2.index == unitindex)
        //                                        {
        //                                            u.position = position;
        //                                            u2.position = new Vector2(u.position.X, u.position.Y);
        //                                            ((Transport)u).carryRequest.Add(u2);
        //                                            break;

        //                                        }
        //                                    }

        //                                    break;
        //                                }
        //                            }

        //                            foreach (Player p in Game1.map.players)//  retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.TransportLoadRequest);
        //                                    outmsg.Write(index);
        //                                    outmsg.Write(unitindex);
        //                                    outmsg.Write(position.X);
        //                                    outmsg.Write(position.Y);
        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.ReliableUnordered);
        //                                }
        //                            }




        //                            break;

        //                        case UpdateTypes.TransportUnloadRequest:
        //                            index = inc.ReadInt64();
        //                            position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
        //                            playernumber = 0;
        //                            foreach (Unit u in Game1.map.units)
        //                            {
        //                                if (u.index == index)
        //                                {
        //                                    u.position = position;
        //                                    ((Transport)u).unloadRequest = true;

        //                                    break;
        //                                }
        //                            }

        //                            foreach (Player p in Game1.map.players) // retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.TransportUnloadRequest);
        //                                    outmsg.Write(index);
        //                                    outmsg.Write(position.X);
        //                                    outmsg.Write(position.Y);
        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.ReliableUnordered);
        //                                }
        //                            }


        //                            break;

        //                        case UpdateTypes.CancelBuilding:
        //                            index = inc.ReadInt64();
        //                            playernumber = 0;
        //                            foreach (Unit u in Game1.map.units)
        //                            {
        //                                if (u.index == index)
        //                                {

        //                                    ((Building)u).CancelBuild();
        //                                    playernumber = u.owner;
        //                                    break;
        //                                }
        //                            }

        //                            foreach (Player p in Game1.map.players) // retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.CancelBuilding);
        //                                    outmsg.Write(index);
        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.ReliableUnordered);
        //                                }
        //                            }
        //                            break;


        //                        case UpdateTypes.NewUnit:
        //                            index = inc.ReadInt64();
        //                            position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
        //                            ut = (UnitType)inc.ReadByte();
        //                            playernumber = inc.ReadInt32();


        //                            Game1.map.units.Add(Game1.map.CreateUnit(ut, position, playernumber));
        //                            Game1.map.units.Last().destination = position;
        //                            Game1.map.units.Last().index = index;


        //                            foreach (Player p in Game1.map.players) // retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg;

        //                                    outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.NewUnit);
        //                                    outmsg.Write(index);
        //                                    outmsg.Write(position.X);
        //                                    outmsg.Write(position.Y);
        //                                    outmsg.Write((byte)ut);
        //                                    outmsg.Write(playernumber);
        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.ReliableUnordered);
        //                                }
        //                            }
        //                            break;
        //                        case UpdateTypes.HarvesterEnteringBuilding:
        //                            index = inc.ReadInt64();
        //                            long buildindex = inc.ReadInt64();
        //                            playernumber = 0;
        //                            foreach (Unit u in Game1.map.units)
        //                            {
        //                                if (u.index == index)
        //                                {
        //                                    playernumber = u.owner;
        //                                    foreach (Unit u2 in Game1.map.units)
        //                                    {
        //                                        if (buildindex == u2.index)
        //                                        {
        //                                            ((Harvester)u).inbuilding = ((Building)u2);
        //                                            ((Harvester)u).timeharvesting = 2000;
        //                                            ((Harvester)u).carrying = ResourceType.None;
        //                                            break;
        //                                        }
        //                                    }

        //                                    break;
        //                                }
        //                            }


        //                            foreach (Player p in Game1.map.players)  //retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg;

        //                                    outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.HarvesterEnteringBuilding);
        //                                    outmsg.Write(index);
        //                                    outmsg.Write(buildindex);
        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.ReliableUnordered);
        //                                }
        //                            }
        //                            break;


        //                        case UpdateTypes.HarvesterHarvested:
        //                            index = inc.ReadInt64();
        //                            position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
        //                            playernumber = 0;
        //                            foreach (Unit u in Game1.map.units)
        //                            {
        //                                if (u.index == index)
        //                                {
        //                                    playernumber = u.owner;
        //                                    Resource r = Game1.map.resources[(int)position.X, (int)position.Y];
        //                                    if (r != null)
        //                                    {
        //                                        r.amount -= 100;
        //                                        ((Harvester)u).carrying = r.type;
        //                                    }
        //                                    break;
        //                                }
        //                            }
        //                            foreach (Player p in Game1.map.players) // retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg;

        //                                    outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.HarvesterHarvested);
        //                                    outmsg.Write(index);
        //                                    outmsg.Write(position.X);
        //                                    outmsg.Write(position.Y);
        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.ReliableUnordered);
        //                                }
        //                            }
        //                            break;
        //                        case UpdateTypes.Speull:
        //                            index = inc.ReadInt64();
        //                            Vector2 wizardposition = new Vector2(inc.ReadFloat(), inc.ReadFloat());
        //                            position = new Vector2(inc.ReadFloat(), inc.ReadFloat());
        //                            SpellType spt = (SpellType)inc.ReadByte();
        //                            playernumber = 0;
        //                            foreach (Unit u in Game1.map.units)
        //                            {
        //                                if (u.index == index)
        //                                {
        //                                    playernumber = u.owner;
        //                                    u.position = wizardposition;
        //                                    ((Wizard)u).selectedSpell = spt;
        //                                    ((Wizard)u).LaunchSpell((int)position.X, (int)position.Y);
        //                                    break;
        //                                }
        //                            }

        //                            foreach (Player p in Game1.map.players) // retransmit to other clients upon reception
        //                            {
        //                                if (p.number != playernumber && p.number != Game1.currentPLayer && p.network)
        //                                {
        //                                    NetOutgoingMessage outmsg;

        //                                    outmsg = server.CreateMessage();
        //                                    outmsg.Write((byte)PacketTypes.UPDATEUNITS);
        //                                    outmsg.Write((byte)UpdateTypes.Speull);
        //                                    outmsg.Write(index);
        //                                    outmsg.Write(wizardposition.X);
        //                                    outmsg.Write(wizardposition.Y);
        //                                    outmsg.Write(position.X);
        //                                    outmsg.Write(position.Y);
        //                                    outmsg.Write((byte) spt);
        //                                    server.SendMessage(outmsg, clientsnbs[p.number], NetDeliveryMethod.ReliableUnordered);
        //                                }
        //                            }


        //                            break;

        //                        /*
        //                         * Spells
        //                         * Harvesters harvesting
        //                         * 
        //                         */


        //                    }
        //                    try
        //                    {

        //                    }
        //                    catch (Exception e)
        //                    {
        //                        Exception v = e;
        //                    }

        //                    // Read next byte

        //                    // Handle movement. This byte should correspond to some direction
        //                    /*    if ((byte)MoveDirection.UP == b)
        //                            ch.Y--;
        //                        if ((byte)MoveDirection.DOWN == b)
        //                            ch.Y++;
        //                        if ((byte)MoveDirection.LEFT == b)
        //                            ch.X--;
        //                        if ((byte)MoveDirection.RIGHT == b)
        //                            ch.X++;
        //                        */
        //                    // Create new message
        //                    //       NetOutgoingMessage outmsg = server.CreateMessage();
        //                    /*
        //                    // Write byte, that is type of world state
        //                    outmsg.Write((byte)PacketTypes.WORLDSTATE);

        //                    // Write int, "how many Game1.map.players in game?"
        //                    outmsg.Write(GameWorldState.Count);

        //                    // Iterate throught all the Game1.map.players in game
        //                    foreach (Character ch2 in GameWorldState)
        //                    {
        //                        // Write all the properties of object to message
        //                        outmsg.WriteAllProperties(ch2);
        //                    }

        //                    // Message contains
        //                    // Byte = PacketType
        //                    // Int = Player count
        //                    // Character obj * Player count

        //                    // Send messsage to clients ( All connections, in reliable order, channel 0)
        //                    server.SendMessage(outmsg, server.Connections, NetDeliveryMethod.ReliableOrdered, 0);
        //                    break;
        //                * */
        //                }


        //            }
        //            break;

        //        case NetIncomingMessageType.StatusChanged:
        //            // In case status changed
        //            // It can be one of these
        //            // NetConnectionStatus.Connected;
        //            // NetConnectionStatus.Connecting;
        //            // NetConnectionStatus.Disconnected;
        //            // NetConnectionStatus.Disconnecting;
        //            // NetConnectionStatus.None;

        //            // NOTE: Disconnecting and Disconnected are not instant unless client is shutdown with disconnect()
        //            if (inc.SenderConnection.Status == NetConnectionStatus.Disconnected || inc.SenderConnection.Status == NetConnectionStatus.Disconnecting)
        //            {
        //                Game1.isInMainMenu = true;
        //            }

        //            break;
        //        default:
        //            // As i statet previously, theres few other kind of messages also, but i dont cover those in this example
        //            // Uncommenting next line, informs you, when ever some other kind of message is received
        //            //Console.WriteLine("Not Important Message");
        //            break;
        //    }
        //} // If New messages        }







        //public static byte[] Serialize(Object data)
        //{
        //    using (var memoryStream = new MemoryStream())
        //    {
        //        (new BinaryFormatter()).Serialize(memoryStream, data);
        //        return memoryStream.ToArray();
        //    }
        //}

        //public static Object Deserialize(byte[] data)
        //{
        //    using (var memoryStream = new MemoryStream(data))
        //        return ((new BinaryFormatter()).Deserialize(memoryStream));
        //}


    }
}
