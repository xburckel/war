﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2.UnitsRelated
{
    public enum SpellType { None, Blizzard, DeathAndDecay, FireExplosion, ArcaneBarrage, Resurrection }
       
    [Serializable]
    public class Spell
    {

        public int duration;
        public Vector2 position;
        Vector2 direction;
        public SpellType spt;
        public List<Animation> spellAnimations;
        public int Triggered; // TODO later
        float rotation = 0;
        public Spell(int duration, Vector2 Position, SpellType spt, Wizard w = null) // duration in milliseconds
        {
            if (w != null && spt == SpellType.ArcaneBarrage)
            {
                #region directions
                direction = new Vector2();
                direction.X = -(w.position.X - Position.X);
                direction.Y = -(w.position.Y - Position.Y);

                if (direction.X > 0 && direction.Y > 0) // northwest
                {
                    rotation = -MathHelper.PiOver4;
                }
                else if (direction.X < 0 && direction.Y < 0) // southeast
                {
                    rotation = MathHelper.PiOver2 + MathHelper.PiOver4;
                }
                else if (direction.X < 0 && direction.Y > 0) // northeast
                {
                    rotation = MathHelper.PiOver4;
                }
                else if (direction.X > 0 && direction.Y < 0) // southwest
                {
                    rotation = MathHelper.Pi + MathHelper.PiOver4;

                }
                else if (direction.X > 0) // west
                {
                    rotation = MathHelper.Pi + MathHelper.PiOver2;
                }
                else if (direction.X < 0) // east
                {
                    rotation = MathHelper.PiOver2;

                }
                else if (direction.Y < 0) // south
                {
                    rotation = MathHelper.Pi;
                }
                else if (direction.Y > 0) // north
                {
                    rotation = 0;
                }
                if (direction.X < 0)
                    direction.X = -1f;
                if (direction.X > 0)
                    direction.X = 1f;
                if (direction.Y < 0)
                    direction.Y = -1f;
                if (direction.Y > 0)
                    direction.Y = 1f;
                #endregion
            }


            Triggered = 0;
            this.duration = duration;
            this.position = new Vector2((float)Math.Round(Position.X) - 1, (float)Math.Round(Position.Y) - 1); // tying to center just a little little bit
            this.spt = spt;
            spellAnimations = new List<Animation>();
            switch (spt)
            {
                case SpellType.Blizzard:
                    Animation u = new Animation(AnimationType.blizzard, position);
                    Animation u2 = new Animation(AnimationType.blizzard, new Vector2((position.X + 0.4f) * 32, (position.Y + 0.4f) * 32));
                    Animation u3 = new Animation(AnimationType.blizzard, new Vector2((position.X + 0.8f) * 32, (position.Y + 0.8f) * 32));
                    spellAnimations.Add(u);
                    spellAnimations.Add(u2);
                    spellAnimations.Add(u3);
                    break;
                case SpellType.DeathAndDecay:
                    Animation u4 = new Animation(AnimationType.decay, position);
                    Animation u6 = new Animation(AnimationType.decay, new Vector2(position.X + 0.6f, position.Y + 0.6f) * 32);
                    spellAnimations.Add(u4);
                    spellAnimations.Add(u6);
                    break;
                case SpellType.FireExplosion:
                    Animation u5 = new Animation(AnimationType.SpellFire, position * 32);
                    spellAnimations.Add(u5);
                    break;
                case SpellType.ArcaneBarrage:
                    position.X += 3; // the sprite is like uber big
                    position.Y += 3;
                    Animation u7 = new Animation(AnimationType.ArcaneBarrage, position * 32);
                    spellAnimations.Add(u7);
                    break;
                case SpellType.Resurrection:
                    Cadavre toremove = null;
                    foreach (Cadavre c in Game1.map.cadavres)
                    {
                        if (c.deadUnit.owner == w.owner)
                        {
                            if (Vector2.Distance(position, new Vector2(c.deadUnit.position.X, c.deadUnit.position.Y)) <= 5)
                            {
                                toremove = c;
                                w.mana = 0;
                                Game1.map.units.Add(Game1.map.CreateUnit(c.deadUnit.type, new Vector2(c.deadUnit.position.X, c.deadUnit.position.Y), c.deadUnit.owner));
                                Game1.soundEngine.PlaySound(Game1.soundEngine.spellSounds[(int)SpellSounds.RESURRECTION], new Vector2(c.deadUnit.position.X, c.deadUnit.position.Y));
                                break;
                            }
                        }
                    }
                    if (toremove != null)
                        Game1.map.cadavres.Remove(toremove);
                    break;
            }

        }

        public void Update(GameTime gameTime)
        {
            if (spt == SpellType.ArcaneBarrage)
            {
                if (Map.Inbounds((int) position.X, (int) position.Y)) // while still on the map
                    duration = 10000;
                else
                    duration = 0;
            }



            if (duration > 0)
            {
                duration -= gameTime.ElapsedGameTime.Milliseconds;
            }
            foreach (Animation an in spellAnimations)
                an.Update(gameTime);
            int posx = (int)Math.Round(position.X);
            int posy = (int)Math.Round(position.Y);

            Triggered += gameTime.ElapsedGameTime.Milliseconds;
            if (spt == SpellType.ArcaneBarrage)
            {
                position.X += (float) gameTime.ElapsedGameTime.TotalSeconds * direction.X * 4;
                position.Y += (float) gameTime.ElapsedGameTime.TotalSeconds * direction.Y * 4;
                spellAnimations.Last().mapPos = new Vector2(position.X * 32, position.Y * 32);
            }
            if (Triggered > 400 && (spt == SpellType.Blizzard || spt == SpellType.DeathAndDecay)) // every 400 miliseconds
            {
                Triggered = 0;
            }
            else if (Triggered > 500 && (spt == SpellType.ArcaneBarrage)) // every 500 miliseconds
            {
                Triggered = 0;
            }
            else if (Triggered > 1000)
                Triggered = 0;
            if ((spt == SpellType.Blizzard || spt == SpellType.DeathAndDecay) && Triggered == 0)
            {

                foreach (Unit u in Game1.map.units)
                {
                    if ((int)Math.Round(u.position.X) == posx
                        && (int)Math.Round(u.position.Y) == posy)
                        u.hp -= 5; // bam
                }

            }
            if (spt == SpellType.FireExplosion && Triggered == 0)
            {
                foreach (Unit u in Game1.map.units)
                {
                    if (Vector2.Distance(u.position, position) <= 3)
                        u.hp -= 50; // bam
                }
            }
            if (spt == SpellType.ArcaneBarrage && Triggered == 0)
            {
                foreach (Unit u in Game1.map.units)
                {
                    if (Vector2.Distance(u.position, position) <= 3)
                        u.hp -= 10; // bam
                }
            }
        }

        public void Draw(SpriteBatch spbatch)
        {
            foreach (Animation an in spellAnimations)
                an.Draw(spbatch, rotation);
        }



    }
}
