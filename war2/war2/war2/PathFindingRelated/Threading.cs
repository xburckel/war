﻿using EpPathFinding;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace war2.PathFindingRelated
{
    class ThreadInfo
    {
        public Unit unit;
        public Vector2 destination;
        public Map map;
        public List<Vector2> tempPosWalkable; // list of position to force walkable (e.g. harvester building on water...)
        public List<Vector2> tempPosUnwalkable; // list of position to force unwalkable (e.g. positions we know cannot be reached)

        public ThreadInfo(Unit u, Vector2 destination, Map map = null, List<Vector2> tempPosWalkable = null, List<Vector2> tempPosUnwalkable = null)
        {
            this.unit = u;
            this.destination = destination;
            this.map = map;
            this.tempPosWalkable = tempPosWalkable;
            this.tempPosUnwalkable = tempPosUnwalkable;
            if (this.tempPosWalkable == null)
                this.tempPosWalkable = new List<Vector2>();
            if (this.map == null)
                this.map = Game1.map;
            if (this.tempPosUnwalkable == null)
                this.tempPosUnwalkable = new List<Vector2>();
        }

    }

    public class Threading 
    {
        List<Unit> onGoingRequests;
        Dictionary<Unit, List<GridPos>> finishedRequests;
        public delegate void PathDelegate(); // called when path has been computed

        public Threading()
        {
            onGoingRequests = new List<Unit>();
            finishedRequests = new Dictionary<Unit, List<GridPos>>();
        }

        public List<GridPos> pathFindingRequest(Unit u, Vector2 destination,Map map = null, List<Vector2> tempPostWalkable = null, List<Vector2> tempPosUnwalkable = null )
        {
            // first look from cache
            List<GridPos> cachepath = Game1.statics.pathFindingResults.GetPath(u.position, destination);
            // if there is a path in the cache, returns it
            if (cachepath != null)
                return cachepath;
            
            // else, new request
            // if finished looking, simply return the list of positions
            lock (onGoingRequests)
            {
                lock (finishedRequests)
                {
                    if (finishedRequests.ContainsKey(u))
                    {
                        List<GridPos> tmp = finishedRequests[u];
                        finishedRequests.Remove(u);
                        return tmp;
                    }
                    // If we are looking for path and not found yet, return empty list 
                    if (this.onGoingRequests.Contains(u))
                    {
                        return null;
                    }
                    // else initiate the lookup
                    onGoingRequests.Add(u);
                }
            }
            ThreadInfo ti = new ThreadInfo(u, destination, map, tempPostWalkable, tempPosUnwalkable);

            ThreadPool.QueueUserWorkItem(new WaitCallback(queuePathFinding), ti);
            return null;
            
        }

        private void queuePathFinding(object tinfo)
        {
            ThreadInfo threadInfo = tinfo as ThreadInfo;
            PathDelegate temp = new PathDelegate(new Action(() => addPathToList(threadInfo.unit, threadInfo.unit.CalculatePath(threadInfo.unit.position, threadInfo.destination, threadInfo.map, threadInfo.tempPosWalkable, threadInfo.tempPosUnwalkable), threadInfo.destination)));

            try
            {
                bool Completed = ExecuteWithTimeLimit(TimeSpan.FromMilliseconds(5000), () =>
                {
                    temp.Invoke();
                });
            }
            catch (IndexOutOfRangeException e)
            {
                Game1.statics.annouceMessage.UpdateMessage("Error index out of range" + e.Message.ToString());
                onGoingRequests.Clear();
                finishedRequests.Clear();
            }
        }

        private void addPathToList(Unit u, List<GridPos> l, Vector2 destination)
        {
            if (onGoingRequests.Contains(u)) // if the unit isn't dead in the meantime
            {
                lock (finishedRequests)
                {
                    finishedRequests.Add(u, new List<GridPos>(l));
                    Game1.statics.pathFindingResults.AddPath(u.position, destination, new List<GridPos>(l));
                    onGoingRequests.Remove(u);
                }
            }
        }

        public bool IsLookingForPath(Unit u)
        {
            return onGoingRequests.Contains(u);
        }

        public bool HasFoundPath(Unit u)
        {
            return finishedRequests.ContainsKey(u);
        }


        // after x time elapsed, kills the task if not finished
        public static bool ExecuteWithTimeLimit(TimeSpan timeSpan, Action codeBlock)
        {
            try
            {
                Task task = Task.Factory.StartNew(() => codeBlock());
                task.Wait(timeSpan);
                return task.IsCompleted;
            }
            catch (AggregateException ae)
            {
                throw ae.InnerExceptions[0];
            }
        }


        internal void cleanRequests(Unit u)
        {
            lock (finishedRequests)
            {
                finishedRequests.Remove(u);
            }
            lock (onGoingRequests)
            {
                onGoingRequests.Remove(u);
            }
        }
        /**
         * Updates the path of a unit if a pathfinding request is finished
         * */
        internal void CheckoutUnit(Unit unit)
        {
            /*if (this.finishedRequests.ContainsKey(unit) && this.finishedRequests[unit].Count > 0)
            {
                unit.path = this.finishedRequests[unit];
                this.onGoingRequests.Remove(unit);
            }*/
            if (unit.path.Count == 0 && Vector2.Distance(unit.position, unit.destination) > 0.5) // if unit is stuck, try to recompute paths
            {
                List<GridPos> tmp = this.pathFindingRequest(unit, unit.destination);
                if (tmp != null && tmp.Count > 0)
                    unit.path = tmp;
            }
        }

        /**
         * PathFindingRequests that returns new list gridpos in case of null (i.e. pathfinding ongoing)
         */
        internal List<GridPos> SafePathFindingRequest(Unit u, Vector2 destination, Map map = null, List<Vector2> tempPostWalkable = null, List<Vector2> tempPosUnwalkable = null)
        {
            List<GridPos> path = Game1.statics.threading.pathFindingRequest(u, destination, map, tempPostWalkable, tempPosUnwalkable);
            if (path == null)
                return new List<GridPos>();
            return path;
        }
    }
}
