﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace war2
{
    public class UnitTexture
    {
        War2Color color;
        bool isOrc;
        public Texture2D ArcherTexture;
        public Texture2D BallistaTexture;
        public Texture2D BattleshitTexture;
        public Texture2D DwarvesTexture;
        public Texture2D DestroyerTexture;
        public Texture2D FootmanTexture;
        public Texture2D FlyMachineTexture;
        public Texture2D SubmarineTexture;
        public Texture2D DragonTexture;
        public Texture2D SummerBuildingTexture;
        public Texture2D WinterBuildingTexture;
        public Texture2D OilTankersTexture;
        public Texture2D TransportTexture;
        public Texture2D KnightTexture;
        public Texture2D MageTexture;
        public Texture2D PeasantTexture;

        public UnitTexture(War2Color color, bool isOrc, ContentManager content)
        {
            this.color = color;
            this.isOrc = isOrc;

            if (color == War2Color.Blue && isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Grunt copie");
                ArcherTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Axe Thrower copie");
                BallistaTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Catapult copie");
                BattleshitTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Juggernaut copie");
                DwarvesTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Goblins copie");
                DestroyerTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Troll Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Goblin Zeppelin copie");
                SubmarineTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Giant Turtle copie");
                DragonTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Dragon copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Orc Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Orc Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Orc Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Orc Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Ogre copie");
                MageTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Death Knight copie");
                PeasantTexture = content.Load<Texture2D>("Units/OrcBlue/PC Computer - Warcraft II - Peon copie");
            }
            if (color == War2Color.Blue && !isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Footman");
                ArcherTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Archer");
                BallistaTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Ballista");
                BattleshitTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Battleship");
                DwarvesTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Dwarves");
                DestroyerTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Elven Destroyer");
                FlyMachineTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Gnomish Flying Machine");
                SubmarineTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Gnomish Submarine");
                DragonTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Gryphon Rider");
                SummerBuildingTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Human Buildings Summer");
                WinterBuildingTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Human Buildings Winter");
                OilTankersTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Human Oil Tanker");
                TransportTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Human Transport");
                KnightTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Knight");
                MageTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Mage");
                PeasantTexture = content.Load<Texture2D>("Units/HumanBlue/PC Computer - Warcraft II - Peasant");
            }
            if (color == War2Color.Red && isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Grunt");
                ArcherTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Axe Thrower");
                BallistaTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Catapult");
                BattleshitTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Juggernaut");
                DwarvesTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Goblins");
                DestroyerTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Troll Destroyer");
                FlyMachineTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Goblin Zeppelin");
                SubmarineTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Giant Turtle");
                DragonTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Dragon");
                SummerBuildingTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Orc Buildings Summer");
                WinterBuildingTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Orc Buildings Winter");
                OilTankersTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Orc Oil Tanker");
                TransportTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Orc Transport");
                KnightTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Ogre");
                MageTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Death Knight");
                PeasantTexture = content.Load<Texture2D>("Units/OrcRed/PC Computer - Warcraft II - Peon");
            }
            if (color == War2Color.Red && !isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Footman copie");
                ArcherTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Archer copie");
                BallistaTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Ballista copie");
                BattleshitTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Battleship copie");
                DwarvesTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Dwarves copie");
                DestroyerTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Elven Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Gnomish Flying Machine copie");
                SubmarineTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Gnomish Submarine copie");
                DragonTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Gryphon Rider copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Human Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Human Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Human Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Human Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Knight copie");
                MageTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Mage copie");
                PeasantTexture = content.Load<Texture2D>("Units/HumanRed/PC Computer - Warcraft II - Peasant copie");
            }
            if (color == War2Color.Black && isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Grunt copie");
                ArcherTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Axe Thrower copie");
                BallistaTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Catapult copie");
                BattleshitTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Juggernaut copie");
                DwarvesTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Goblins copie");
                DestroyerTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Troll Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Goblin Zeppelin copie");
                SubmarineTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Giant Turtle copie");
                DragonTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Dragon copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Orc Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Orc Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Orc Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Orc Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Ogre copie");
                MageTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Death Knight copie");
                PeasantTexture = content.Load<Texture2D>("Units/OrcBlack/PC Computer - Warcraft II - Peon copie");
            }
            if (color == War2Color.Black && !isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Footman copie");
                ArcherTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Archer copie");
                BallistaTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Ballista copie");
                BattleshitTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Battleship copie");
                DwarvesTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Dwarves copie");
                DestroyerTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Elven Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Gnomish Flying Machine copie");
                SubmarineTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Gnomish Submarine copie");
                DragonTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Gryphon Rider copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Human Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Human Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Human Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Human Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Knight copie");
                MageTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Mage copie");
                PeasantTexture = content.Load<Texture2D>("Units/HumanBlack/PC Computer - Warcraft II - Peasant copie");
            }
            if (color == War2Color.Yellow && isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Grunt copie");
                ArcherTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Axe Thrower copie");
                BallistaTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Catapult copie");
                BattleshitTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Juggernaut copie");
                DwarvesTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Goblins copie");
                DestroyerTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Troll Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Goblin Zeppelin copie");
                SubmarineTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Giant Turtle copie");
                DragonTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Dragon copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Orc Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Orc Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Orc Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Orc Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Ogre copie");
                MageTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Death Knight copie");
                PeasantTexture = content.Load<Texture2D>("Units/OrcYellow/PC Computer - Warcraft II - Peon copie");
            }
            if (color == War2Color.Yellow && !isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Footman copie");
                ArcherTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Archer copie");
                BallistaTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Ballista copie");
                BattleshitTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Battleship copie");
                DwarvesTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Dwarves copie");
                DestroyerTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Elven Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Gnomish Flying Machine copie");
                SubmarineTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Gnomish Submarine copie");
                DragonTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Gryphon Rider copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Human Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Human Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Human Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Human Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Knight copie");
                MageTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Mage copie");
                PeasantTexture = content.Load<Texture2D>("Units/HumanYellow/PC Computer - Warcraft II - Peasant copie");
            }
            if (color == War2Color.Orange && isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Grunt copie");
                ArcherTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Axe Thrower copie");
                BallistaTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Catapult copie");
                BattleshitTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Juggernaut copie");
                DwarvesTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Goblins copie");
                DestroyerTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Troll Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Goblin Zeppelin copie");
                SubmarineTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Giant Turtle copie");
                DragonTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Dragon copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Orc Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Orc Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Orc Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Orc Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Ogre copie");
                MageTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Death Knight copie");
                PeasantTexture = content.Load<Texture2D>("Units/OrcOrange/PC Computer - Warcraft II - Peon copie");
            }
            if (color == War2Color.Orange && !isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Footman copie");
                ArcherTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Archer copie");
                BallistaTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Ballista copie");
                BattleshitTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Battleship copie");
                DwarvesTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Dwarves copie");
                DestroyerTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Elven Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Gnomish Flying Machine copie");
                SubmarineTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Gnomish Submarine copie");
                DragonTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Gryphon Rider copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Human Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Human Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Human Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Human Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Knight copie");
                MageTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Mage copie");
                PeasantTexture = content.Load<Texture2D>("Units/HumanOrange/PC Computer - Warcraft II - Peasant copie");
            }
            if (color == War2Color.Violet && isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Grunt copie");
                ArcherTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Axe Thrower copie");
                BallistaTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Catapult copie");
                BattleshitTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Juggernaut copie");
                DwarvesTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Goblins copie");
                DestroyerTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Troll Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Goblin Zeppelin copie");
                SubmarineTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Giant Turtle copie");
                DragonTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Dragon copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Orc Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Orc Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Orc Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Orc Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Ogre copie");
                MageTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Death Knight copie");
                PeasantTexture = content.Load<Texture2D>("Units/OrcViolet/PC Computer - Warcraft II - Peon copie");
            }
            if (color == War2Color.Violet && !isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Footman copie");
                ArcherTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Archer copie");
                BallistaTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Ballista copie");
                BattleshitTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Battleship copie");
                DwarvesTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Dwarves copie");
                DestroyerTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Elven Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Gnomish Flying Machine copie");
                SubmarineTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Gnomish Submarine copie");
                DragonTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Gryphon Rider copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Human Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Human Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Human Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Human Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Knight copie");
                MageTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Mage copie");
                PeasantTexture = content.Load<Texture2D>("Units/HumanViolet/PC Computer - Warcraft II - Peasant copie");
            }
            if (color == War2Color.White && isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Grunt copie");
                ArcherTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Axe Thrower copie");
                BallistaTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Catapult copie");
                BattleshitTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Juggernaut copie");
                DwarvesTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Goblins copie");
                DestroyerTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Troll Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Goblin Zeppelin copie");
                SubmarineTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Giant Turtle copie");
                DragonTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Dragon copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Orc Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Orc Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Orc Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Orc Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Ogre copie");
                MageTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Death Knight copie");
                PeasantTexture = content.Load<Texture2D>("Units/OrcWhite/PC Computer - Warcraft II - Peon copie");
            }
            if (color == War2Color.White && !isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Footman copie");
                ArcherTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Archer copie");
                BallistaTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Ballista copie");
                BattleshitTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Battleship copie");
                DwarvesTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Dwarves copie");
                DestroyerTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Elven Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Gnomish Flying Machine copie");
                SubmarineTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Gnomish Submarine copie");
                DragonTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Gryphon Rider copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Human Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Human Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Human Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Human Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Knight copie");
                MageTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Mage copie");
                PeasantTexture = content.Load<Texture2D>("Units/HumanWhite/PC Computer - Warcraft II - Peasant copie");
            }

            if (color == War2Color.Green && isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Grunt copie");
                ArcherTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Axe Thrower copie");
                BallistaTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Catapult copie");
                BattleshitTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Juggernaut copie");
                DwarvesTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Goblins copie");
                DestroyerTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Troll Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Goblin Zeppelin copie");
                SubmarineTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Giant Turtle copie");
                DragonTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Dragon copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Orc Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Orc Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Orc Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Orc Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Ogre copie");
                MageTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Death Knight copie");
                PeasantTexture = content.Load<Texture2D>("Units/OrcGreen/PC Computer - Warcraft II - Peon copie");
            }
            if (color == War2Color.Green && !isOrc)
            {
                FootmanTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Footman copie");
                ArcherTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Archer copie");
                BallistaTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Ballista copie");
                BattleshitTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Battleship copie");
                DwarvesTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Dwarves copie");
                DestroyerTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Elven Destroyer copie");
                FlyMachineTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Gnomish Flying Machine copie");
                SubmarineTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Gnomish Submarine copie");
                DragonTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Gryphon Rider copie");
                SummerBuildingTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Human Buildings Summer copie");
                WinterBuildingTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Human Buildings Winter copie");
                OilTankersTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Human Oil Tanker copie");
                TransportTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Human Transport copie");
                KnightTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Knight copie");
                MageTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Mage copie");
                PeasantTexture = content.Load<Texture2D>("Units/HumanGreen/PC Computer - Warcraft II - Peasant copie");
            }
        }

    }
}
